# CK Editor Setup

## Download CkEditor

- Download from <https://ckeditor.com/cke4/builder>

  1. Standard
  2. Feature SelectioN:
  3. Add Additional Plugins:
  4. Paste As Plain Text
  5. Remove Plugins
  6. FileBrowser
  7. Blockquote
  8. Image
  9. Horizontal Rule
  10. Upload Image
  11. Styles Combo
  12. Tab Key
  13. Table\*
  14. OnChange
  15. Magic Line
  16. Moona-Lisa
  17. English + Optimized

- Extract downloaded zip and copy the `ckeditor` folder into `vendor`

## Update the `index.html`

```html
<!-- app/index.html -->
{{content-for "body"}}

<!-- No leading slash resolves CKEDITOR "no reference" issues -->
<script type="text/javascript">
  window.CKEDITOR_BASEPATH = "{{rootURL}}assets/ckeditor/"
</script>
<!-- add -->

<script src="{{rootURL}}assets/vendor.js"></script>
<script src="{{rootURL}}assets/front.js"></script>

{{content-for "body-footer"}}
```

## Edit the `tests/index.html` file:

```html
<!-- front/tests/index.html -->
<script type="text/javascript">
  window.CKEDITOR_BASEPATH = "{{rootURL}}assets/ckeditor/"
</script>
<!-- add -->

...

<script src="{{rootURL}}assets/front.js"></script>
<script src="{{rootURL}}assets/ckeditor/ckeditor.js"></script>
<!-- add -->
<script src="{{rootURL}}assets/tests.js"></script>
```

## Funnel ckeditor

- Update `front/ember-cli-build.js`

```javascript
// /ember-cli-build.js
const Funnel = require("broccoli-funnel")

let app = new EmberApp(defaults, {
  // Add options here
  fingerprint: {
    exclude: ["assets/ckeditor/"]
  }
})
app.import("vendor/ckeditor/ckeditor.js")
var ckeditorAssets = new Funnel("vendor/ckeditor", {
  srcDir: "/",
  destDir: "/assets/ckeditor"
})

return app.toTree([ckeditorAssets])
```
