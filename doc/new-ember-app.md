# Starting a new KB Software Ember App

## Dependencies

```
ember install @ember/render-modifiers
ember install ember-cli-postcss
ember install ember-concurrency
ember install ember-cli-mirage
npm i @fullhuman/postcss-purgecss -D
npm i broccoli-plugin -D
npm i ember-template-lint -D
npm i ember-template-lint-plugin-prettier -D
npm i postcss-import -D
npm i faker -D
npm i prettier -D
npm i tailwindcss -D
npm i tailwindcss-ember-power-select -D
```

Added to `package.json` as `npm run` commands in any Ember or other Node project

```
"scripts": {
  ...
  "prettier:hbs": "prettier  --write **/*.hbs --parser=glimmer",
  "prettier": "prettier --write \"**/*.{js,json,scss,md,html}\" \"!package.json\""
}
```

## OIDC Authentication

### Adapter

**`front/app/adapters/application.js`**

```
import KbDataAdapterMixin from "@kbsoftware/kb-base-ember-addons/adapters/kb-application"

export default class ProjectNameDataAdapter extends KbDataAdapterMixin {}
```

### Logging In

**`front/app/routes/login.js`**

```
import OidcUnauthenticationRouteMixin from "@kbsoftware/kb-base-ember-addons/mixins/oidc-unauthentication-route"
import Route from "@ember/routing/route"

export default class LoginRoute extends Route.extend(
  OidcUnauthenticationRouteMixin
) {}
```

**`front/app/templates/login.hbs`**

```
<KbWireframe::TopBar @title="Logging in..." />
<KbWireframe::Main::ModelLoading>
  Please wait. In a few seconds you will be redirected to Navigator's
  authentication page.
</KbWireframe::Main::ModelLoading>
{{outlet}}
```

### Holding point between Login Attempts/Failures

**`front/app/routes/authenticate.js`**

```
import Route from "@ember/routing/route"
import UnauthenticatedRouteMixin from "ember-simple-auth/mixins/unauthenticated-route-mixin"

export default class AuthenticateRoute extends Route.extend(
  UnauthenticatedRouteMixin
) {}
```

**`front/app/templates/authenticate.hbs`**

```
<KbWireframe::TopBar @topBarHeading="Authentication" />
<div class="w-full pt-6 p-1">
  <p>
    We were unable to authenticate you automatically. To retry: Click the link
    below and you will be redirected back to Navigator's standard
    authentication.
  </p>
</div>
<KbWireframe::main::Dashboard>
  <KbWireframe::main::Dashboard::Group>
    <KbWireframe::main::ToolBar::Tool
      @anchorText="Click here to login"
      @route="login"
      @icon="sign-in"
    />
  </KbWireframe::main::Dashboard::Group>
</KbWireframe::main::Dashboard>
{{outlet}}
```

## Current User

Determine the current user in the application.

**`front/app/routes/application.js`**

```
import ApplicationRouteMixin from "ember-simple-auth/mixins/application-route-mixin"
import Route from "@ember/routing/route"
import { inject as service } from '@ember/service';
import { task } from "ember-concurrency"

export default class ApplicationRoute extends Route.extend(
  ApplicationRouteMixin
) {
  beforeModel() {
    return this.loadCurrentContact.perform()
  }

  model() {
    return {
      routeModels: this.routeModelsTask.perform()
    }
  }

  @service session
  @service currentContact

  sessionAuthenticated() {
    super.sessionAuthenticated(...arguments)
    this.loadCurrentContact.perform()
  }

  sessionInvalidated() {
    this.transitionTo("login")
  }

  @(task(function*() {
    let models = new Object()
    if (this.currentContact.contact) {
      models["currentContact"] = yield this.currentContact.contact
    }
    return yield models
  }).restartable())
  routeModelsTask

  @(task(function*() {
    let currentContact = null
    try {
      let currentContact = yield this.currentContact.load()
    } catch (err) {
      yield this.session.invalidate()
    }
    if (!this.currentContact.contactId) {
      yield this.session.invalidate()
    }
  }).restartable())
  loadCurrentContact
}
```
