# Django Rest Requirements

## Urls.py

Use plural of modelname when registering the route. Use the single version as the `basename`.

```
from rest_framework import routers
router = routers.DefaultRouter()
router.register(
    r"<Modelname>s", <Appname><Modelname>ViewSet, basename="<Modelname>",
)
```

## Cors

In development, add `django-cors-headers` to `requirements/local.txt`

Add the following to your development settings file, e.g. `settings/dev_<User>.py`

```
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_WHITELIST = ("http://localhost:8000", "http://localhost:4200")

MIDDLEWARE += ("corsheaders.middleware.CorsMiddleware",)
INSTALLED_APPS += ("corsheaders",)
```

## OIDC Auth

Add `mozilla_django_oidc` to `requirements/base.txt`

## Settings

```
THIRD_PARTY_APPS = (
    ...,
    "mozilla_django_oidc",
    "rest_framework",
    "rest_framework.authtoken",
)
```

Environment settings in `settings/base.py`:

```
USE_OPENID_CONNECT = get_env_variable_bool("USE_OPENID_CONNECT")
AUTHENTICATION_BACKENDS = ("login.service.KBSoftwareOIDCAuthenticationBackend",)
LOGIN_REDIRECT_URL_FAILURE = reverse_lazy("login")
OIDC_CREATE_USER = False
OIDC_OP_AUTHORIZATION_ENDPOINT = get_env_variable(
    "OIDC_OP_AUTHORIZATION_ENDPOINT"
)
OIDC_OP_JWKS_ENDPOINT = get_env_variable("OIDC_OP_JWKS_ENDPOINT")
OIDC_OP_TOKEN_ENDPOINT = get_env_variable("OIDC_OP_TOKEN_ENDPOINT")
OIDC_OP_USER_ENDPOINT = "NOT_USED_BY_KB_LOGIN_SERVICE"
OIDC_RP_CLIENT_ID = get_env_variable("OIDC_RP_CLIENT_ID")
OIDC_RP_CLIENT_SECRET = get_env_variable("OIDC_RP_CLIENT_SECRET")
OIDC_RP_SIGN_ALGO = get_env_variable("OIDC_RP_SIGN_ALGO")
OIDC_USE_NONCE = get_env_variable_bool("OIDC_USE_NONCE")
```

Example `.private`

```
set -x OIDC_RP_CLIENT_ID "hashy12345-1234-5678-9a0b-a1b2c3d4e5"
set -x OIDC_RP_CLIENT_SECRET "QWERTYUIOP_ASDFGHJKL_ZXCVBNM"
set -x OIDC_OP_AUTHORIZATION_ENDPOINT "https://login.microsoftonline.com/hashy12345-1234-5678-9a0b-a1b2c3d4e5/oauth2/v2.0/authorize"
set -x OIDC_OP_JWKS_ENDPOINT "https://login.microsoftonline.com/common/discovery/v2.0/keys"
set -x OIDC_OP_TOKEN_ENDPOINT "https://login.microsoftonline.com/hashy12345-1234-5678-9a0b-a1b2c3d4e5/oauth2/v2.0/token"
set -x OIDC_RP_SIGN_ALGO "RS256"
set -x OIDC_USE_NONCE False
```

## Development

Settings for the email address you will use from your Microsoft Developer's Account Email **MSDAE**:

**`.private`**

```
set -x KB_TEST_EMAIL_FOR_OIDC "name.of.user@company.onmicrosoft.com"
set -x KB_TEST_EMAIL_USERNAME "admin"
```

**`settings/local.py`**

```
OPEN_ID_CONNECT_EMBER_REDIRECT_URI = "http://localhost:4200"
OPEN_ID_CONNECT_EMBER_REDIRECT_ROUTE = "login"
```

**`settings/dev_<Username>.py`**

```
KB_TEST_EMAIL_FOR_OIDC = get_env_variable("KB_TEST_EMAIL_FOR_OIDC")
KB_TEST_EMAIL_USERNAME = get_env_variable("KB_TEST_EMAIL_USERNAME")
```

Pick which user account you want to develop against and run the following command:

```
django-admin demo_data_login_oidc
```

To change from the default "KB_TEST" user pass the parameters for `username` and `MSDAE`

```
django-admin demo_data_login_oidc guest.user
```

Or even if you want to use a different Microsoft Email Address (which will need to be associated with the same Account as for the CLIENT_ID):

```
django-admin demo_data_login_oidc guest.user guest.user@company.onmicrosoft.com
```

## Urls

```
from login.api import ObtainAuthTokenOnCode

urlpatterns = [
  ...,
  url(
      regex=r"^back/token/$",
      view=ObtainAuthTokenOnCode.as_view(),
      name="api.token.auth",
  ),
  url(
      regex=r"^oidc/",
      view=include("mozilla_django_oidc.urls"),
    ),
]
```
