# Recipe of possible usage with examples from the Competencies app.

## How kb-base-ember-addons sit alongside the application's own components

It's been useful developing kb-base-ember-addons alongside an application which is going to use it. I thought you might be interested to see how I do use kb-base-ember-addons in the **navigator-training** application; as well as an explanation of how the app is set up.

This is `navigator-training/front/app/templates/skill/list.hbs`, which is a good example.

Remember: It serves the route `http://host/skills/list`

```html
<# navigator-training/front/app/templates/skill/list.hbs />

<Skill::ToolBar @icon="bullseye" @dataTestId="{{this.dataTestId}}" />
{{#if @model.routeModels.isRunning}}
<KbWireframe::Main::ModelLoading @dataTestId="{{this.dataTestId}}" />
{{else if @model.routeModels.value. skills}}
<Skill::List
  @model="{{@model.routeModels.value.skills}}"
  @linkRoute="skill.detail"
  @filterActive="{{true}}"
  @paginateBy="{{25}}"
  @dataTestId="{{this.dataTestId}}"
/>
{{else}}
<KbWireframe::Main::ModelNotFound
  @message="No skills found."
  @dataTestId="{{this.dataTestId}}"
/>
{{/if}} {{outlet}}
```

Notice that we are calling two application-level components alongside two kb-base-ember-addons.

By "application-level", I mean components in **navigator-training** which handle a particular type of view on the data. In the above example, given that this template is rendered on the route, it also calls the `navigator-training/front/app/component/skill/list` component. This is the pattern we're using on **navigator-training** and it just makes logical sense.

In **navigator-training** there is a "route" for every main object mentioned in the `models` folder. Many **navigator-training** folders obey a pattern of having an identical modelName/subFoldersFiles/subSubFile structure to handle any route for `host/<modelName>/<renderContext>`

On renderContexts - we mean routes with names like:

- `host/<modelName>/list` aka: list the hell out of the data it receives
- `host/<modelName>/edit` aka: directly edit 1 instance of the route's modelName
- `host/<modelName>/1/delete`: aka: just delete this instance and redirect to the list
- `host/<modelName>/1/skills/list` aka: a route which displays instance's list of skills.
- `host/<modelName>/1/skills/add` aka: form to link a skill to this instance.
- `host/<modelName>/1/skills/new` aka: create new skill and link to this instance.
- `host/<modelName>/1/skills/1/edit` aka: edit a skill related to this instance.
- `host/<modelName>/upload` aka: form to process upload csv files to a handler for the modelName.
- `host/<modelName>/1/upload` aka: form to process upload csv files to this instance.

And so on.

The "application-level" components handle the actual detail of delivering the functionality described in the route's final verb.

Let's take an example of the following route `http://host/contact/1/sales/summary`:

Then there should a component in the same place at `navigator-training/front/app/component/contact/sales/summary` will look like this:

```html
<# navigator-training/front/app/component/contact/sales/summary.hbs />
<div
  class="w-full probably border-perhaps tailwind class appropriate for an application-level component"
>
  <Contact::detail::Sales::Summary
    @model="{{@model.contact}}"
    @modelSales="{{@model.contactSalesFigures}}"
    @showWithKPIs="{{true}}"
    @dateStart="{{@dateRangeStart}}"
    @dateEnd="{{@dateRangeStart}}"
  />
  <Contact::detail::Sales::Barchart
    @model="{{@model.contact}}"
    @bestPerformer="{{@bestPerformer}}"
    @modelSales="{{@model.contactSalesFigures}}"
    @dateStart="{{@dateRangeStart}}"
    @dateEnd="{{@dateRangeStart}}"
  />
</div>
```

The above would be considered an "application-level" component.It takes all the main data from the route, and distributes

Where as this would be a useful-component level component, such as:

```html
<# navigator-training/front/app/component/contact/sales/sales-summary.hbs />
<table class="tailwind">
  {{#each this.organiseSalesIntoASummary as |summaryLine|}}
  <Contact::detail::Sales::Summary::TableRow
    @model="{{summaryLine}}"
    @showWithKPIs="{{true}}"
  />
  {{/each}}
</table>
```

## File Structure

In **navigator-training** there is an almost mirror image of file names in the `components`, `controllers`, `models`, `routes`, `templates`.

They will probably also render "modelName components"

```

<div class="tailwind class appropriate for an application-level">
  <Contact::detail::avatar @model={{@contactModel}}/>
</div>
```

## BatchForm Manager

Idea. A wrapper for forms which handles an array of records.

The manager only shows the form for an empty record - called the BatchForm

Above the BatchForm the BatchFormManager wraps an alert message (primarily) with a red background something like:

```
<--! components/kb-form-batch.hbs -->
<div class="w-full bg-{{this.recordsAffectedColor}}">
  Values on this form will be applied to {{this.recordsAffectedCount}}.
</div>

{{yield this.batch}}
```

Called by other components:

```
<KbFormBatch>
<KbForm
  @actionSubmitHook={{this.actionValidateThenSubmit}}
  @dataTestRole={{@dataTestRole}}
>
  <KbForm::Fieldset @legend={{@legend}}>
    <KbForm::String
      @key="name"
      @value={{@batch.com}}
      @label="Name"
      @required={{true}}
      @showValidations={{this.showValidations}}
      @validateHook={{this.actionValidate}}
      @dataTestRole={{@dataTestRole}}
    />

    @model=@batch
    @etc=@etc
    @onSubmit={{KbFormBatch.actionApplyBatchUpdate}}
  >
  {{#each this.recordsAffected as |data|}}
    <KbFormBatch::Detail @model={{data}} @batch=@batch>
  {{/each}}

</KbFormBatch>
```

Against each field the user can select "off". As they `off` fields like "name" the number of records which share the values of the first record, will increase... say for instance you turned `off` everything except `is_friend`, you'd expect the percent of records sharing will increase. The bg colour of the alert can drop back to yellow then green if the percent was high.

Against each field the user can select "lock". As they `lock` fields like "country" they know any changes to fields which are not `off` will only be applied to records with share the same value. `lock` could be described as `off+`. Since use of `off+` would further reduce the number of records affected by the BatchForm,
