"use strict"

var Funnel = require("broccoli-funnel")
const EmberAddon = require("ember-cli/lib/broccoli/ember-addon")

module.exports = function (defaults) {
  let app = new EmberAddon(defaults, {
    fingerprint: {
      exclude: ["assets/ckeditor/"]
    },
    autoImport: {
      webpack: {
        node: {
          fs: "empty"
        }
      }
    },
    postcssOptions: {
      compile: {
        enabled: false,
      },
    }
  })

  app.import("vendor/ckeditor/ckeditor.js")
  var ckeditorAssets = new Funnel("vendor/ckeditor", {
    srcDir: "/",
    destDir: "assets/ckeditor"
  })

  if (app.env === "test") {
    app.import("vendor/ember/ember-template-compiler.js", { type: "test" })
    // app.import(app.bowerDirectory + '/ember/ember-template-compiler.js', { type: 'test' });
  }

  return app.toTree([ckeditorAssets])
}
