"use strict"
const stringUtils = require("ember-cli-string-utils")

/**
 * @Example
 * ember g component-input-test-runner permission-settings
 *
 */
module.exports = {
  description: "Blueprint for a Component Integration Test Runner",

  /** locals
  The options passed to locals looks like this:
  {
    "entity": {
      "name": "foo",
      "options": {
        "isAdmin": "true"
      }
    },
    "dryRun": true,
    "type": "array"
  }*/
  locals(options) {
    let componentPath = options.entity.name
    let componentName = Array.from(options.entity.name.split("/")).pop()
    let componentClass = options.entity.name
      .split("/")
      .map(n => stringUtils.classify(n))
      .join("::")
    return {
      componentPath: componentPath,
      componentName: componentName,
      componentClass: componentClass
    }
  }
}
