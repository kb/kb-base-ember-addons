/** <%= componentClass %> Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"

const TESTKEY = "id"
const MOCK = {
  val1: "A",
  val2: "C",
  validate: "B",
  null: ""
}

const component = {
  componentPath: "<%= componentPath %>",
  componentName: "<%= componentName %>",
  componentClass: "<%= componentClass %>"
}

hasAttributes({
  ...component,
  scenario: "",
  hasAttributes: [
    {
      scenario: "attributes passed",
      givenAttrs: { title: "Click to <%= componentName %> me" },
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectAttributes: { title: "Click to <%= componentName %> me" }
    }
  ]
})

hasDataTestIds({
  ...component,
  scenario: "",
  hasDataTestIds: [
    {
      scenario: "<%= componentName %>",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-<%= componentName %>-id]"
    },
    {
      scenario: "",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-label-id]"
    },
    {
      scenario: "",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-error-id]"
    }
  ]
})

hasValues({
  ...component,
  scenario: "",
  hasValues: [
    {
      scenario: "starting <%= componentName %>",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.val1 },
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectValue: MOCK.val1
    },
    {
      scenario: "no starting <%= componentName %>",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.null },
      givenDataTestId: "[data-test-kb-form-input-id]",
      negateExpectation: true,
      expectValue: MOCK.val1
    }
  ]
})

isDescriptive({
  ...component,
  scenario: "",
  isDescriptive: [
    {
      scenario: "help text",
      givenAttrs: {
        "@key": TESTKEY,
        "@helpText": "Textually Helpful"
      },
      givenDataTestId: "",
      expectDescriptive: "Textually Helpful"
    },
    {
      scenario: "label: starting value",
      givenAttrs: { "@key": TESTKEY, "@label": "Labelled Descriptively" },
      givenDataTestId: "[data-test-kb-form-label-id]",
      expectDescriptive: "Labelled Descriptively"
    },
    {
      scenario: "showValidations required not valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectDescriptive: "This field is required"
    },
    {
      scenario: "showValidations required valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": MOCK.val1,
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectDescriptive: ""
    },
    {
      scenario: "showValidations not required not valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": false,
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      negateExpectation: true,
      expectDescriptive: "Required"
    }
  ]
})

isReactive({
  ...component,
  scenario: "change event handling",
  // NB: We're testing on behalf of funcHookName here; not changeHook itself.
  funcHookName: "changeHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.null },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [[TESTKEY, MOCK.val2]]
    },
    {
      scenario: "editing",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.val1 },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [[TESTKEY, MOCK.val2]]
    },
    {
      scenario: "erasing",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.val1 },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.null,
      expectParameters: [[TESTKEY, MOCK.null]]
    }
  ]
})

isReactive({
  ...component,
  scenario: "when not required",
  funcHookName: "validateHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@value": MOCK.null
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@value":MOCK.val1
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: {
        "@key": TESTKEY,
        "@value": MOCK.val1
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.null,
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "when required",
  funcHookName: "validateHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, false, "This field is required"],
        [TESTKEY, true, "Appears to be valid"]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": MOCK.val1
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, true, "Appears to be valid"],
        [TESTKEY, true, "Appears to be valid"]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": MOCK.val1
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.null,
      expectParameters: [
        [TESTKEY, true, "Appears to be valid"],
        [TESTKEY, false, "This field is required"]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "validation rules",
  funcHookName: "validateHook",
  isReactive: [
    {
      scenario: "min",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@min": MOCK.validate,
        "@value": MOCK.val1
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "renderOnly",
      expectParameters: [
        [TESTKEY, false, `${MOCK.validate} is the maximum allowed date`]
      ]
    },
    {
      scenario: "max",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@max": MOCK.validate,
        "@value": MOCK.val2
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "renderOnly",
      expectParameters: [
        [TESTKEY, false, `${MOCK.validate} is the maximum allowed date`]
      ]
    },
    {
      scenario: "invalid <%= componentName %>",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": "2013-13-31"
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "renderOnly",
      expectParameters: [[TESTKEY, false, "This date is not valid"]]
    },
    {
      scenario: "invalid format",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": "24-05-2010"
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "renderOnly",
      expectParameters: [[TESTKEY, false, "This date is not valid"]]
    }
  ]
})

willYield({
  ...component,
  scenario: "yields unconditionitionally",
})

willYield({
  ...component,
  scenario: "yields on condition",
  givenAttrs: {
    "@condition": true
  }
})

willYield({
  ...component,
  scenario: "no yield condition",
  negateExpectation: true
})
