import { click, currentURL, visit } from "@ember/test-helpers"
import { module, test } from "qunit"
import { selectChoose } from "ember-power-select/test-support"
import { setupApplicationTest } from "ember-qunit"
import { upload } from "ember-file-upload/test-support"
import registerPowerSelectHelpers from "ember-power-select/test-support/helpers"
import setupMirage from "ember-cli-mirage/test-support/setup-mirage"
import { setupRouteAcceptanceUtil } from "@kb/ember-addon/tests/suites/acceptance-util-setup-route"

registerPowerSelectHelpers()

const ROUTEMODEL = "data-test-kb"
const ROWACTION = "confirm-action-id"

export default function(testCase) {
  module(
    `Acceptance Test Suite | ${testCase.routeName} <%= dasherizedModuleName %>`,
    function(hooks) {
      setupApplicationTest(hooks)
      setupMirage(hooks)
      setupRouteAcceptanceUtil(hooks, "appadmin", testCase.mocks)

      test(`${testCase.moduleName} <%= dasherizedModuleName %>`, async function(assert) {
        let addRouteName = testCase.routeName
        let removeRouteName = addRouteName.substring(0, addRouteName.length - 4)
        let ADDBUTTON = `[${ROUTEMODEL}-${ROWACTION}='${addRouteName}']`
        let REMOVEBUTTON = `[${ROUTEMODEL}-${ROWACTION}='${removeRouteName}']`
        let LISTEDBY = `${ROUTEMODEL}-${testCase.modelName}-${testCase.listedBy}-id`

        assert.step("1: ASSERT: VISIT ROUTE")
        await visit(testCase.routeURL)
        assert.equal(currentURL(), testCase.routeURL)

        assert.verifySteps(["1: ASSERT: VISIT ROUTE"])
      })
    }
  )
}
