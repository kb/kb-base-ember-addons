/**
* @Example
* import <%= camelizedModuleName %> from "@kb/ember-addon/tests/suites/integration-component-<%= dasherizedModuleName %>"
*
<%= camelizedModuleName %>({
  ...component,
  <%= camelizedModuleName %>: [{
    scenario: "confirm onto step 2",
    givenAttrs: { "step": 1 },
    givenDataTestId: "[data-test-kb-form-input-id]",
    action: "fillIn",
    fillInValue: "Completed Step 1",
    expect<%= classifiedModuleName %>: "Continue to Step 2"
  }]
})
*/
import { click, fillIn, render } from "@ember/test-helpers"
import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { hbs } from "ember-cli-htmlbars"
import setupIntegrationComponentUtil from "@kb/ember-addon/tests/suites/integration-util-component"

export default function(testCase) {
  module(
    `Integration Test Suite | Component | ${testCase.componentPath} <%= dasherizedModuleName %>`,
    function(hooks) {
      setupRenderingTest(hooks)
      setupIntegrationComponentUtil(hooks, testCase)

      test(`${testCase.componentName} ${testCase.scenario}`, async function(assert) {
        if (testCase.<%= camelizedModuleName %>) {
          assert.step("1: TESTING <%= camelizedModuleName %> CASES")

          for (let caseScenario of testCase.<%= camelizedModuleName %>) {
            let SCENARIO = caseScenario.scenario
            let attrBlock = this.buildAttrBlock(caseScenario.givenAttrs)

            // funcHandler will test the result.
            let reactionCount = 0
            this.set("reactionHook", function(...hookParameters) {
              assert.ok(true, `${SCENARIO} IT REACTED`)
              assert.deepEqual(
                hookParameters,
                caseScenario.expectParameters[reactionCount],
                `${SCENARIO} PARAMETERS AS EXPECTED`
              )
              reactionCount += 1
            })

            const content = Ember.HTMLBars.compile(`
              <${testCase.componentClass}
                ${attrBlock}
                @${testCase.funcHookName}={{this.reactionHook}}
              />
            `)
            await render(content)

            if (caseScenario.action === "click") {
              await click(caseScenario.givenDataTestId)
            } else if (caseScenario.action === "fillIn") {
              await fillIn(caseScenario.givenDataTestId, caseScenario.fillInValue)
            } else if (caseScenario.action === "renderOnly") {
              // do nothing
            }

            if ( !caseScenario.negateExpectation ) {
              assert.dom(caseScenario.givenDataTestId)
                .<%= camelizedModuleName %>(
                  caseScenario.expect<%= classifiedModuleName %>
                )
            } else {
              assert.dom(caseScenario.givenDataTestId)
                .doesNot<%= classifiedModuleName %>(
                  caseScenario.expect<%= classifiedModuleName %>
                )
            }
          }
        }

        assert.verifySteps([
          "1: TESTING <%= camelizedModuleName %> CASES"
        ])
      })
    }
  )
}
