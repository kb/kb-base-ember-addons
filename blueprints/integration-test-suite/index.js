"use strict"
const stringUtils = require("ember-cli-string-utils")

/**
* @Example:

ember g integration-test-suite will-fetch-data

*/
module.exports = {
  description: "Blueprint for a Component Integration Test Suite",
  locals(options) {
    let componentPath = options.entity.name
    let componentName = Array.from(options.entity.name.split("/")).pop()
    let componentId = componentName
    let componentClass = options.entity.name
      .split("/")
      .map(n => stringUtils.classify(n))
      .join("::")
    return {
      componentPath: componentPath,
      componentName: componentName,
      componentId: componentId,
      componentClass: componentClass
    }
  }
}
