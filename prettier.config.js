// prettier.config.js or .prettierrc.js
module.exports = {
  trailingComma: "none",
  tabWidth: 2,
  semi: false,
  singleQuote: false,
  bracketSpacing: true,
  arrowParens: "avoid",
  printWidth: 80
  // parenthesisSpace: true
}
