ember-addon
***********

https://gitlab.com/kb/kb-base-ember-addons::

  # git clone to
  ~/dev/app/ember-addon/

14/08/2024
==========

Looks like I may have to release this again ref:

- Training evidence upload - check (or fix) empty evidence issue
  https://www.kbsoftware.co.uk/crm/ticket/7186/
- Downloads for external training requests are not working
  https://www.kbsoftware.co.uk/crm/ticket/7290/

The last time we released was January 2022!
We are using ``pnpm`` now, so it may take some effort!

Development
-----------

For development, I can build the project this way::

  cd ~/dev/app/ember-addon/
  rm -rf dist/ node_modules/ /tmp/embroider/; and rm -f pnpm-lock.yaml; and pnpm install;

.. tip:: Each time you update the code, stop the build in the main project,
         rebuild ``ember-addon``, then restart the build in the main project.

Update ``package.json`` in the root of your project e.g.
``~/dev/project/navigator-training/front/``

  "@kb/ember-addon": "file:../../../app/ember-addon",

And run it as follows::

  rm -rf dist/ node_modules/ /tmp/embroider/; and rm -f pnpm-lock.yaml; and pnpm install; and pnpm start

Issues
------

``ember-cli-build.js``

I had to add the following to get the code to compile::

  postcssOptions: {
    compile: {
      enabled: false,
    },
  }

``package.json``

``@babel/core`` missing + remove ``babel-eslint``
https://gitlab.com/kb/kb-base-ember-addons/-/commit/ff032b0e4390f8768be0ae4adc82ffe97f706b1a

Release
-------

Check ``CkEditor`` is still working
(ref the change to ``ember-cli-build.js`` above).

GitLab
======

npm package
-----------

Patch, install and publish:
https://www.kbsoftware.co.uk/docs/dev-gitlab.html#patch-install-and-publish

Repository
----------

::

  git@gitlab.com:kb/kb-base-ember-addons.git

.. note:: The original addons are on the ``master`` branch in the
          ``app/kb-base-ember-addons`` folder.

          This ``README.rst`` file is the first change on the ``tailwind-2``
          branch.  The code can be found in the ``app/ember-addon`` folder.

.. note:: The ``master`` branch is published to the ``npmjs`` package
          repository.

          The ``tailwind-2`` branch is published to GitLab (I hope)...
          https://gitlab.com/kb/kb-base-ember-addons/-/packages

Description
===========

Ember addons designed for KB Software Ember apps; ensuring a common look and
feel of the UI; as well as speeding up the work process.

KB Software's Ember apps are styled using https://tailwindcss.com/ and for
technical reasons, it is not possible to have Ember Addons provide the
requirement.

For that reason the dummy app is mostly unstyled and to see these addon's
demonstrated properly we suggest you clone and run our **Simple Ember** project:
https://gitlab.com/kb/simple-ember

This app also contains the intructions for including Tailwind CSS in your
project.

Compatibility

- Ember.js v3.14 or above
- Ember CLI v2.13 or above
- Node.js v10 or above

Development
===========

In the root directory of ``ember-addon``

Run and test as if a normal Ember App::

  ember serve

To test and use the AddOn from a KB project while working on both locally::

  cd ember-addon
  npm link

From root directory Ember App (usually ``project/name/front``)::

  cd project/name/front
  npm link @kb/ember-addon

Or you can also install the module from the local fileset::

  npm i ../ember-addon

Hiccups
-------

1. Often, after installing a new module in ``project/name``, you will find it
   necessary to link again.
2. If you are running 2 terminals (like Quake and Tilda) - run the links on the
   same bash.

Installation
============

::

  ember install @kb/ember-addon

Using the Add Ons

KB Software examples are also demonstrated by
https://gitlab.com/kb/simple-ember

``KbWireframe``

**KbWireframe** makes it easy to scaffold an Ember app with a KB Software nav
sidebar which collapses on a mobile phone.
Simply add the following to the ``application.hbs`` file::

  <KbWireframe>
    <KbWireframe::SideBar>
      <LinkTo @route="home" @class="bg-gray-800 text-white font-extrabold">
        HOME
      </LinkTo>
      <LinkTo @route="route1">
        <i class="fa fa-dashboard fa-fw"></i>
        Example Link 1
      </LinkTo>
    </KbWireframe::SideBar>
    <KbWireframe::Main @mainHeading="App Main Heading">
      {{outlet}}
    </KbWireframe::Main>
  </KbWireframe>

``KbForm``

**KbForm** helps build a KB Software styled form which has some basic validation
for different input types.

In the Javascript file of the template hosting the KbForm (which would be the
controller or component), add the following:

.. code-block:: javascript

  import Component from "@glimmer/component"
  import { action, set } from "@ember/object"
  import { inject as service } from '@ember/service';
  import { tracked } from "@glimmer/tracking"

  export default class PageWithKbForm extends Component {
    // And object to maintain the validation status of all the inputs.
    @tracked showValidations = false

    // And object to maintain the validation status of all the inputs.
    validationStatus = Object()

    // A simple count of which inputs are valid or not.
    get countOfValid() {
      return Object.values(this.validationStatus).filter(i => !i).length
    }

    get formIsValid() {
      return this.countOfValid === 0
    }

    @action
    actionOnValueChange(key, value) {
      set(this.args.model, key, value)
    }

    @action
    actionValidate(inputKey, validOrNotRequired) {
      // Each form input will set it's validation status when loaded or touched.
      this.validationStatus[inputKey] = validOrNotRequired
    }
  }

The addon provides a component with these methods already included which you can
extend.

.. code-block:: javascript

  import KbFormManager from "@kb/ember-addon/components/kb-form-manager"
  export default class ComponentWithKbForm extends KbFormComponent {}

A form is built by nesting the input components inside a form component.

.. code-block:: html

  <KbForm
    @buttonLabel="Submit Me"
    @buttonClass="kb-form-button"
    @actionSubmitHook="{{this.actionControllerSubmit}}"
  >
    <KbForm::Fieldset @legend="Please fill out required fields.">
      <KbForm::String
        @key="userName"
        @value="{{this.userName}}"
        @label="Name"
        @max="{{10}}"
        @min="{{3}}"
        @required="{{true}}"
        @showValidations="{{this.showValidations}}"
        @validateHook="{{this.actionValidate}}"
      ></KbForm::String>
      <KbForm::Boolean
        @key="terms"
        @value="{{this.termsAccepted}}"
        @label="Terms"
        @required="{{true}}"
        @showValidations="{{this.showValidations}}"
        @validateHook="{{this.actionValidate}}"
      ></KbForm::Boolean>
    </KbForm::Fieldset>
  </KbForm>

The following form inputs are available (more can be added as required):

- ``KbInputBoolean``
- ``KbInputDate``
- ``KbInputEmail``
- ``KbInputNumber``
- ``KbInputSelect``
- ``KbInputSelectMulti``
- ``KbInputString``
- ``KbInputTaggit``
- ``CkEditor``

The properties you can use are as follows:

- ``key``: *Recommended* as the `input` `id` and `label` `for` attribute.
- ``label``: *Recommended* for the input label.
- ``value``: *Required* value from the model or containing controller/component.
- ``min`` and ``max``: *Optional* date, numerical, or string length values
  depending on input type.
- ``required``: *Optional* indicating whether the input is a required field or
  not.
- ``required``: *Optional* indicating whether the input is a required field or
  not.
- ``actionOnValueChange``: *Optional* When the value is immutable.
- ``showValidations``: *Optional* to communicate from the host page when it is
  time to display validation messages (see above).
- ``validateHook``: *Optional* action (see above) which passes up to the host
  file validation state when the input changes or in the construction
  (data down/action up!).

.. note:: WIP: KbForm and its child inputs are not properly coupled.

``KbForm::Upload``

This control takes a property which expects an array of files.

.. code-block:: html

  <KbForm
    @legend="Please fill out required fields."
    @actionSubmitHook={{this.actionValidateThenSubmit}}
  >
    <KbForm::Upload
      @key="fileList"
      @value={{@model.fileList}}
      @fileType="image"
      @label="Upload Images"
      @zoneClass="box"
      @buttonClass="button"
      @required={{true}}
      @min={{1}}
      @max={{3}}
      @uploadUrl="http://127.0.0.1:5000/"
      @changeHook={{this.actionOnValueChange}}
      @showValidations={{this.showValidations}}
      @validateHook={{this.actionValidate}}
    />
  </KbForm>

``KbQuickPaginator``

**KbQuickPaginator** is an addon for creating a basic paginator for local data.
Ideal for small data sets. To use, simply wrap your existing list whether
bulleted or a table. The id is useful if you have more than one
`KbQuickPaginator` per page.

.. code-block:: html

  <KbQuickPaginator
    @paginateBy="{{10}}"
    @active="{{true}}"
    @items="{{this.model}}"
    as
    |listItems|
  >
    <ul>
      {{#each listItems as |item|}}
      <li>{{item}}</li>
      {{/each}}
    </ul>
  </KbQuickPaginator>

``KbGroupBy``

**KbGroupBy** is an addon for creating a grouping local data. Ideal for small
data sets. To use, simply wrap your existing list whether bulleted or a table.
It works in a similar way to Django's ``group_by`` template tag.

.. code-block:: html

  <KbGroupBy
    @groupBy="make"
    @active={{true}}
    @items={{this.model}} as |carMakeGroups|
  >
    {{#each carMakeGroups as |carMakeGroup|}}
      <h2>{{carMakeGroup.make}}</h2>
      <ul>
        {{#each carMakeGroup.items as |car|}}
          <li>{{car.model}}: {{car.type}} {{car.color}}</li>
        {{/each}}
      <ul>
    {{/each}}
  </KbGroupBy>

You can create multiple levels of grouping by nesting the component and using
``.items`` of the previous grouping object as the list for the next group.

.. code-block:: html

  <KbGroupBy
    @groupBy="make"
    @active={{true}}
    @items={{this.model}} as |carMakeGroups|
  >
    {{#each carMakeGroups as |carMakeGroup|}}
      <h3>{{carMakeGroup.make}}</h3>
      <KbGroupBy
        @groupBy="type"
        @items={{carMakeGroup.items}} as |carMakeTypes|
      >
        {{#each carMakeTypes as |carMakeType|}}
          <h4>{{carMakeType.type}}</h4>
          <ul>
            {{#each carMakeType.items as |car|}}
              <li>{{car.model}}: {{car.color}}</li>
            {{/each}}
          <ul>
        {{/each}}
      </KbGroupBy>
    {{/each}}
  </KbGroupBy>

``KbTaggitFilter``

**KbTaggitFilter** is an addon for filtering a list with a taggit field.

.. code-block:: html

  <KbTaggitFilter
    @tags={{this.departments}}
    @items={{this.model}}
    @tagsMatch="departmenttags"
    @anchorTextMatch="firstName"
    @small={{true}}
    @active={{true}} as |personList|
  >
    <table width="100%" class="table-fixed">
      <thead>
        <tr class="bg-gray-400">
          <th align="left">
            People
          </th>
          <th align="left">
            Departments
          </th>
        </tr>
      </thead>
      <tbody>
        {{#each personList as |person|}}
          <tr>
            <td align="left">
              {{person.firstName}}
            </td>
            <td align="left">
              {{person.departmenttags}}
            </td>
          </tr>
        {{/each}}
      </tbody>
    </table>
  </KbTaggitFilter>

``KbToolBar``

**KbToolBar** provides a style-ready container for a horizontal set of tabs and
BUTTONS.

.. code-block:: html

  <KbToolBar>
    <LinkTo @route="home">
      <i class="fa fa-reply"></i>
      Return
    </LinkTo>
    <LinkTo @route="settings">
      <i class="fa fa-cogs"></i>
      Settings
    </LinkTo>
  </KbToolBar>

``KbDashboard``

**KbDashboard** provides a style-ready container for groups of vertical BUTTONS.

.. code-block:: html

  <KbDashboard>
    <KbDashboard::Group
      @groupName="Group1"
    >
      <LinkTo @route="route1">
        <i class="fa fa-check-square fa-fw"></i>
        Link1
      </LinkTo>
      <LinkTo @route="route2">
        Link2
      </LinkTo>
    </KbDashboard::Group>
    <KbDashboard::Group
      @groupName="Group2"
    >
      <LinkTo @route="route3">
        Link3
      </LinkTo>
      <LinkTo @route="route4">
        Link4
      </LinkTo>
    </KbDashboard::Group>
  </KbDashboard>

``kb-model-route``

Provides 3 utility ``ember-concurrency`` based tasks for:

.. code-block:: javascript

  let model = this.getModel.perform(modelName, modelId)
  let model = this.createModel.perform(modelName, defaults, relations)
  let models = this.searchModel.perform(modelName, paramsTheApiExpects)

``kb-wireframe``

::

  kb-wireframe/main/model-not-found

long message::

  <KbWireframe::Main::ModelNotFound>
    No apps have been assigned to your profile. Please speak to a member
    of the technical department or your manager if you think this is an
    error.
  </KbWireframe::Main::ModelNotFound>

short message::

  <KbWireframe::Main::ModelNotFound @message="Couldn't find that.">

Usage

.. code-block:: javascript

  import KbModelRoute from '@kb/ember-addon/routes/kb-model-route'
  export default class MyModelRoute extends KbModelRoute {}

``kb-model-controller``

Provides 5 utility ember-concurrency based tasks for:

.. code-block:: javascript

  let model = this.saveModel.perform(modelObj, successMessage, transitionOnSuccessRoute)
  this.deleteModel.perform(modelObj, transitionOnToSuccessRoute)
  this.addManyToMany.perform(addModelObj, toModelObj [, addModelName])
  this.addNewManyToMany.perform(newModelObj, toModelObj, transitionToRoute [, addModelName])
  this.removeManyToMany.perform(removeModelObj, fromModelObj)

Usage

.. code-block:: javascript

  // controllers/my-model.js
  import KbModelController from "@kb/ember-addon/controllers/kb-model-controller"
  import { action } from "@ember/object"
  export default class MyModelController extends KbModelController {
    @action
    actionInTemplateController(modelOfThing, transitionTo) {
      this.deleteModel.perform(modelOfThing, transitionTo)
    }
  }

.. code-block:: html

  <!-- templates/my-model.hbs -->
  {{#each @things as |thing|}}
    <li>
      {{thing.name}}
      <button {{on "click" (fn this.actionInTemplateController thing "thing.list")}}>
        Delete
      </button>
    </li>
  {{/each}}

Contributing

See ``CONTRIBUTING.md`` guide for details.

- ``npm run prettier``
- ``npm run prettier:hbs``

License

This project is licensed under the Apache License.
