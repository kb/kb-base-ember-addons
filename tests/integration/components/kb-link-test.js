import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Component | kb-link", function (hooks) {
  setupRenderingTest(hooks)

  test("it renders", async function (assert) {
    await render(hbs`<KbLink />`)

    assert.equal(this.element.textContent.trim(), "")

    await render(hbs`
      <KbLink>
        template block text
      </KbLink>
    `)

    assert.equal(this.element.textContent.trim(), "template block text")
  })
})
