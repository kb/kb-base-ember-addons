import EmberObject from "@ember/object"
import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render, click } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

const jobs = [
  {
    id: 1,
    slug: "baker",
    name: "Baker"
  },
  {
    id: 2,
    slug: "butcher",
    name: "Butcher"
  },
  {
    id: 3,
    slug: "candlestick",
    name: "Candlestick Maker"
  }
]

const tags = [1, 2, 3].map(x => `Tag${x}`)

module("Integration | Component | kb-form kb input types", function (hooks) {
  setupRenderingTest(hooks)

  test("example form KbInputBoolean populates", async function (assert) {
    assert.expect(1)

    let expect = true
    this.set("jobs", jobs)
    this.set("tags", tags)
    this.set("model", EmberObject.create({ isActive: expect }))
    this.set("submit", params => {
      assert.equal(params.isActive, expect)
    })

    await render(hbs`
      <KbForm @actionSubmitHook={{this.submit}} as |form|>
        <KbForm::Boolean
          @label="Active"
          @key="isActive"
          @value={{this.model.isActive}}
          @changeHook={{form.actionToUpdateModelByKey}}
        />
      </KbForm>
    `)

    await click("[data-test-kb-submit-button-id]")
  })

  test("example form KbInputDate populates", async function (assert) {
    assert.expect(1)

    let expect = "1974-07-17"
    this.set("jobs", jobs)
    this.set("tags", tags)
    this.set("model", EmberObject.create({ dob: expect }))
    this.set("submit", params => {
      assert.equal(params.dob, expect)
    })

    await render(hbs`
      <KbForm @actionSubmitHook={{this.submit}} as |form|>
        <KbForm::Date
          @label="DoB"
          @key="dob"
          @value={{this.model.dob}}
          @changeHook={{form.actionToUpdateModelByKey}}
        />
      </KbForm>
    `)

    await click("[data-test-kb-submit-button-id]")
  })

  test("example form KbInputEmail populates", async function (assert) {
    assert.expect(1)

    let expect = "info@test.com"
    this.set("jobs", jobs)
    this.set("tags", tags)
    this.set("model", EmberObject.create({ email: expect }))
    this.set("submit", params => {
      assert.equal(params.email, expect)
    })

    await render(hbs`
      <KbForm @actionSubmitHook={{this.submit}} as |form|>
        <KbForm::Email
          @label="Email"
          @key="email"
          @value={{this.model.email}}
          @changeHook={{form.actionToUpdateModelByKey}}
        />
      </KbForm>
    `)

    await click("[data-test-kb-submit-button-id]")
  })

  test("example form KbInputNumber populates", async function (assert) {
    assert.expect(1)

    let expect = 432
    this.set("jobs", jobs)
    this.set("tags", tags)
    this.set("model", EmberObject.create({ score: expect }))
    this.set("submit", params => {
      assert.equal(params.score, expect)
    })

    await render(hbs`
      <KbForm @actionSubmitHook={{this.submit}} as |form|>
        <KbForm::Number
          @label="Score"
          @key="score"
          @value={{this.model.score}}
          @required={{false}}
          @changeHook={{form.actionToUpdateModelByKey}}
        />
      </KbForm>
    `)

    await click("[data-test-kb-submit-button-id]")
  })

  test("example form KbInputSelectMulti populates", async function (assert) {
    assert.expect(1)

    let expect = [1]
    this.set("jobs", jobs)
    this.set("tags", tags)
    this.set("model", EmberObject.create({ jobs: expect }))
    this.set("submit", params => {
      assert.deepEqual(params.jobs, expect)
    })

    await render(hbs`
      <KbForm @actionSubmitHook={{this.submit}} as |form|>
        <KbForm::SelectMulti
          @label="Jobs"
          @placeholder="Select Jobs"
          @key="jobs"
          @value={{this.model.jobs}}
          @options={{this.jobs}}
          @valueIsPrimitive={{true}}
          @valueField="id"
          @searchField="name"
          @searchEnabled={{false}}
          @required={{false}}
          @changeHook={{form.actionToUpdateModelByKey}}
        />
      </KbForm>
    `)

    await click("[data-test-kb-submit-button-id]")
  })

  test("example form KbInputSelect populates", async function (assert) {
    assert.expect(1)

    let expect = 2
    this.set("jobs", jobs)
    this.set("tags", tags)
    this.set("model", EmberObject.create({ job: expect }))
    this.set("submit", params => {
      assert.equal(params.job, expect)
    })

    await render(hbs`
    <KbForm @actionSubmitHook={{this.submit}} as |form|>
      <KbForm::Select
        @label="Job"
        @placeholder="Select a Job"
        @key="job"
        @value={{this.model.job}}
        @options={{this.jobs}}
        @valueIsPrimitive={{true}}
        @valueField="id"
        @searchField="name"
        @searchEnabled={{true}}
        @required={{false}}
        @changeHook={{form.actionToUpdateModelByKey}}
      />
    </KbForm>
  `)

    await click("[data-test-kb-submit-button-id]")
  })

  test("example form KbInputString populates", async function (assert) {
    assert.expect(1)

    let expect = "userName1"
    this.set("jobs", jobs)
    this.set("tags", tags)
    this.set("model", EmberObject.create({ userName: expect }))
    this.set("submit", params => {
      assert.equal(params.userName, expect)
    })

    await render(hbs`
  <KbForm @actionSubmitHook={{this.submit}} as |form|>
    <KbForm::String
      @label="User Name"
      @key="userName"
      @value={{this.model.userName}}
      @required={{false}}
      @changeHook={{form.actionToUpdateModelByKey}}
    />
  </KbForm>
`)

    await click("[data-test-kb-submit-button-id]")
  })

  test("example form KbInputTaggit populates", async function (assert) {
    assert.expect(1)

    let expect = ["Tag1", "Tag2", "Tag3"]
    this.set("jobs", jobs)
    this.set("tags", tags)
    this.set("model", EmberObject.create({ taggits: expect }))
    this.set("submit", params => {
      assert.deepEqual(params.taggits, expect)
    })

    await render(hbs`
    <KbForm @actionSubmitHook={{this.submit}} as |form|>
      <KbForm::Taggit
        @label="Taggit"
        @key="taggits"
        @value={{this.model.taggits}}
        @tags={{this.tags}}
        @required={{false}}
        @min={{1}}
        @max={{1}}
        @changeHook={{form.actionToUpdateModelByKey}}
        @showValidations={{form.showValidations}}
        @validateHook={{form.actionValidate}}
      />
    </KbForm>
  `)

    await click("[data-test-kb-submit-button-id]")
  })

  test("example form KbInputUpload populates", async function (assert) {
    assert.expect(1)

    let expect = ["File1.jpg", "File2.jpg", "File3.jpg"]
    this.set("jobs", jobs)
    this.set("tags", tags)
    this.set("model", EmberObject.create({ files: expect }))
    this.set("submit", params => {
      assert.deepEqual(params.files, expect)
    })

    await render(hbs`
      <KbForm @actionSubmitHook={{this.submit}} as |form|>
        <KbForm::Upload
          @key="files"
          @value={{this.model.files}}
          @fileType="image"
          @label="Upload Images"
          @zoneClass="box"
          @buttonClass="button"
          @required={{false}}
          @disabled={{@disabled}}
          @uploadUrl="upload.to"
          @changeHook={{form.actionToUpdateModelByKey}}
        />
      </KbForm>
    `)

    await click("[data-test-kb-submit-button-id]")
  })
})
