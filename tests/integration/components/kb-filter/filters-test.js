import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Component | kb-filter/filters", function (hooks) {
  setupRenderingTest(hooks)

  test("renders", async function (assert) {
    await render(hbs`<KbFilter::Filters />`)

    assert.equal(this.element.textContent.trim(), "")

    await render(hbs`
      <KbFilter::Filters>
        template block text
      </KbFilter::Filters>
    `)

    assert.equal(this.element.textContent.trim(), "template block text")
  })
})
