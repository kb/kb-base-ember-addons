/** KbFilter::DateRange Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"
import doesFiltering from "@kb/ember-addon/tests/suites/integration-component-does-filtering"

const FILTERKEY = "filtee"
const LIST = [
  {
    id: "No1",
    filtee: 1
  },
  {
    id: "No2",
    filtee: 2
  },
  {
    id: "No3",
    filtee: 3
  },
  {
    id: "No4",
    filtee: 4
  },
  {
    id: "No5",
    filtee: 5
  },
  {
    id: "EMPTY",
    filtee: ""
  }
]

const component = {
  componentPath: "kb-filter/numerical-range",
  componentName: "numerical-range",
  componentClass: "KbFilter::NumericalRange"
}

doesFiltering({
  ...component,
  scenario: "filter start",
  funcHookName: "changeHook",
  funcAction: "filter",
  doesFiltering: [
    {
      scenario: "filter when MATCH",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filterLow": 1,
        "@filterHigh": 2
      },
      resultCount: 2,
      results: { 1: "No1", 2: "No2" }
    },
    {
      scenario: "filter when false",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filterLow": "",
        "@filterHigh": ""
      },
      resultCount: 6,
      results: {
        1: "No1",
        2: "No2",
        3: "No3",
        4: "No4",
        5: "No5",
        6: "EMPTY"
      }
    },
    {
      scenario: "exact filter when true",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filterLow": 1,
        "@filterHigh": 2,
        "@exact": true
      },
      resultCount: 2,
      results: { 1: "No1", 2: "No2" }
    },
    {
      scenario: "exact filter when false",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filterLow": "",
        "@filterHigh": "",
        "@exact": true
      },
      resultCount: 6,
      results: {
        1: "No1",
        2: "No2",
        3: "No3",
        4: "No4",
        5: "No5",
        6: "EMPTY"
      }
    }
  ]
})

doesFiltering({
  ...component,
  scenario: "filter change handling",
  funcHookName: "changeHook",
  funcAction: "filter",
  doesFiltering: [
    {
      scenario: "filter low",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filterLow": 1,
        "@filterHigh": 4
      },
      givenDataTestId: `[data-test-kb-form-input-id='${FILTERKEY}LowFilter']`,
      action: "fillIn",
      fillInValue: "3",
      resultCount: 2,
      results: {
        1: "No3",
        2: "No4"
      }
    },
    {
      scenario: "filter high",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filterLow": 2,
        "@filterHigh": 2
      },
      givenDataTestId: `[data-test-kb-form-input-id='${FILTERKEY}HighFilter']`,
      action: "fillIn",
      fillInValue: "4",
      resultCount: 3,
      results: {
        1: "No2",
        2: "No3",
        3: "No4"
      }
    }
  ]
})

willYield({
  ...component,
  scenario: "does not yield",
  negateExpectation: true
})
