/** KbFilter::Number Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"
import doesFiltering from "@kb/ember-addon/tests/suites/integration-component-does-filtering"

const FILTERKEY = "filtee"
const LIST = [
  {
    id: "456",
    filtee: 456
  },
  {
    id: "EMPTY",
    filtee: ""
  },
  {
    id: "456",
    filtee: 456
  }
]

const component = {
  componentPath: "kb-filter/number",
  componentName: "number",
  componentClass: "KbFilter::Number"
}

doesFiltering({
  ...component,
  scenario: "filter start",
  funcHookName: "changeHook",
  funcAction: "filter",
  doesFiltering: [
    {
      scenario: "filter when MATCH",
      givenAttrs: { "@key": FILTERKEY, "@model": LIST, "@filter": 456 },
      resultCount: 2,
      results: { 1: "456", 2: "456" }
    },
    {
      scenario: "filter when false",
      givenAttrs: { "@key": FILTERKEY, "@model": LIST, "@filter": 0 },
      resultCount: 3,
      results: { 1: "456", 2: "EMPTY", 3: "456" }
    },
    {
      scenario: "exact filter when true",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filter": 456,
        "@exact": true
      },
      resultCount: 2,
      results: { 1: "456", 2: "456" }
    },
    {
      scenario: "exact filter when false",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filter": 0,
        "@exact": true
      },
      resultCount: 1,
      results: { 1: "EMPTY" }
    }
  ]
})

doesFiltering({
  ...component,
  scenario: "filter change handling",
  funcHookName: "changeHook",
  funcAction: "filter",
  doesFiltering: [
    {
      scenario: "filter when true",
      givenAttrs: { "@key": FILTERKEY, "@model": LIST, "@filter": 0 },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: 456,
      resultCount: 2,
      results: { 1: "456", 2: "456" }
    },
    {
      scenario: "filter when false",
      givenAttrs: { "@key": FILTERKEY, "@model": LIST, "@filter": 456 },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: "",
      resultCount: 3,
      results: { 1: 456, 2: "EMPTY", 3: 456 }
    },
    {
      scenario: "exact filter when true",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filter": "",
        "@exact": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: "456",
      resultCount: 2,
      results: { 1: "456", 2: "456" }
    },
    {
      scenario: "exact filter when false",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filter": 456,
        "@exact": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: "",
      resultCount: 1,
      results: { 1: "EMPTY" }
    }
  ]
})

willYield({
  ...component,
  scenario: "does not yield",
  negateExpectation: true
})
