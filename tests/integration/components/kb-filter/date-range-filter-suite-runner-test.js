/** KbFilter::DateRange Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"
import doesFiltering from "@kb/ember-addon/tests/suites/integration-component-does-filtering"

const FILTERKEY = "filtee"
const LIST = [
  {
    id: "2011-01-01",
    filtee: "2011-01-01"
  },
  {
    id: "2012-02-02",
    filtee: "2012-02-02"
  },
  {
    id: "2013-03-03",
    filtee: "2013-03-03"
  },
  {
    id: "2014-04-04",
    filtee: "2014-04-04"
  },
  {
    id: "2015-05-05",
    filtee: "2015-05-05"
  },
  {
    id: "EMPTY",
    filtee: ""
  }
]

const component = {
  componentPath: "kb-filter/date-range",
  componentName: "date-range",
  componentClass: "KbFilter::DateRange"
}

doesFiltering({
  ...component,
  scenario: "filter start",
  funcHookName: "changeHook",
  funcAction: "filter",
  doesFiltering: [
    {
      scenario: "filter when MATCH",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filterLow": "2011-01-01",
        "@filterHigh": "2012-02-02"
      },
      resultCount: 2,
      results: { 1: "2011-01-01", 2: "2012-02-02" }
    },
    {
      scenario: "filter when false",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filterLow": "",
        "@filterHigh": ""
      },
      resultCount: 6,
      results: {
        1: "2011-01-01",
        2: "2012-02-02",
        3: "2013-03-03",
        4: "2014-04-04",
        5: "2015-05-05",
        6: "EMPTY"
      }
    },
    {
      scenario: "exact filter when true",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filterLow": "2011-01-01",
        "@filterHigh": "2012-02-02",
        "@exact": true
      },
      resultCount: 2,
      results: { 1: "2011-01-01", 2: "2012-02-02" }
    },
    {
      scenario: "exact filter when false",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filterLow": "",
        "@filterHigh": "",
        "@exact": true
      },
      resultCount: 6,
      results: {
        1: "2011-01-01",
        2: "2012-02-02",
        3: "2013-03-03",
        4: "2014-04-04",
        5: "2015-05-05",
        6: "EMPTY"
      }
    }
  ]
})

doesFiltering({
  ...component,
  scenario: "filter change handling",
  funcHookName: "changeHook",
  funcAction: "filter",
  doesFiltering: [
    {
      scenario: "filter low",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filterLow": "2012-02-02",
        "@filterHigh": "2014-04-04"
      },
      givenDataTestId: `[data-test-kb-form-input-id='${FILTERKEY}LowFilter']`,
      action: "fillIn",
      fillInValue: "2013-03-03",
      resultCount: 2,
      results: {
        1: "2013-03-03",
        2: "2014-04-04"
      }
    },
    {
      scenario: "filter high",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filterLow": "2012-02-02",
        "@filterHigh": "2012-02-02"
      },
      givenDataTestId: `[data-test-kb-form-input-id='${FILTERKEY}HighFilter']`,
      action: "fillIn",
      fillInValue: "2014-04-04",
      resultCount: 3,
      results: {
        1: "2012-02-02",
        2: "2013-03-03",
        3: "2014-04-04"
      }
    }
  ]
})

willYield({
  ...component,
  scenario: "does not yield",
  negateExpectation: true
})
