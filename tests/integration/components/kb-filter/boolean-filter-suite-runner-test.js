/** KbFilter::Boolean Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"
import doesFiltering from "@kb/ember-addon/tests/suites/integration-component-does-filtering"

const FILTERKEY = "filtee"
const LIST = [
  {
    id: "true 1",
    filtee: true
  },
  {
    id: "false 2",
    filtee: false
  },
  {
    id: "true 3",
    filtee: true
  }
]

const component = {
  componentPath: "kb-filter/boolean",
  componentName: "boolean",
  componentClass: "KbFilter::Boolean"
}

doesFiltering({
  ...component,
  scenario: "filter start",
  funcHookName: "changeHook",
  funcAction: "filter",
  doesFiltering: [
    {
      scenario: "filter when true",
      givenAttrs: { "@key": FILTERKEY, "@model": LIST, "@filter": true },
      resultCount: 2,
      results: { 1: "true 1", 2: "true 3" }
    },
    {
      scenario: "filter when false",
      givenAttrs: { "@key": FILTERKEY, "@model": LIST, "@filter": false },
      resultCount: 3,
      results: { 1: "true 1", 2: "false 2", 3: "true 3" }
    },
    {
      scenario: "exact filter when true",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filter": true,
        "@exact": true
      },
      resultCount: 2,
      results: { 1: "true 1", 2: "true 3" }
    },
    {
      scenario: "exact filter when false",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filter": false,
        "@exact": true
      },
      resultCount: 1,
      results: { 1: "false 2" }
    }
  ]
})

doesFiltering({
  ...component,
  scenario: "filter change handling",
  funcHookName: "changeHook",
  funcAction: "filter",
  doesFiltering: [
    {
      scenario: "filter when true",
      givenAttrs: { "@key": FILTERKEY, "@model": LIST, "@filter": true },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "click",
      resultCount: 3,
      results: { 1: "true 1", 2: "false 2", 3: "true 3" }
    },
    {
      scenario: "filter when false",
      givenAttrs: { "@key": FILTERKEY, "@model": LIST, "@filter": false },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "click",
      resultCount: 2,
      results: { 1: "true 1", 2: "true 3" }
    },
    {
      scenario: "exact filter when true",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filter": true,
        "@exact": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "click",
      resultCount: 1,
      results: { 1: "false 2" }
    },
    {
      scenario: "exact filter when false",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@filter": false,
        "@exact": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "click",
      resultCount: 2,
      results: { 1: "true 1", 2: "true 3" }
    }
  ]
})

willYield({
  ...component,
  scenario: "does not yield",
  negateExpectation: true
})
