/** KbForm::Select Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"

const TESTKEY = "id"
const MOCK = {
  opts: ["Apple", "Banana", "Carrot"],
  val1: "Apple",
  val2: "Carrot",
  validate: "Banana",
  null: null
}

const component = {
  componentPath: "kb-form/radio",
  componentName: "radio",
  componentClass: "KbForm::Radio"
}

hasDataTestIds({
  ...component,
  scenario: "",
  hasDataTestIds: [
    {
      scenario: "input",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@optionsArePrimitive": true,
        "@valueField": "id",
        "@searchField": "name"
      },
      givenDataTestId: "[data-test-kb-form-input-label-id='Apple']"
    },
    {
      scenario: "input",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@optionsArePrimitive": true,
        "@valueField": "id",
        "@searchField": "name"
      },
      givenDataTestId: "[data-test-kb-form-input-label-id='Banana']"
    },
    {
      scenario: "input",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@optionsArePrimitive": true,
        "@valueField": "id",
        "@searchField": "name"
      },
      givenDataTestId: "[data-test-kb-form-input-label-id='Carrot']"
    },
    {
      scenario: "label",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@optionsArePrimitive": true,
        "@valueField": "id",
        "@searchField": "name"
      },
      givenDataTestId: "[data-test-kb-form-label-label-id='Apple']"
    },
    {
      scenario: "error",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@optionsArePrimitive": true,
        "@valueField": "id",
        "@searchField": "name"
      },
      givenDataTestId: "[data-test-kb-form-error-id]"
    }
  ]
})

isChecked({
  ...component,
  isChecked: [
    {
      scenario: "starting radio checked",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": MOCK.val1,
        "@optionsArePrimitive": true,
        "@valueField": "id",
        "@searchField": "name"
      },
      givenDataTestId: "[data-test-kb-form-input-label-id='Apple']"
    },
    {
      scenario: "not starting checked",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": "",
        "@optionsArePrimitive": true,
        "@valueField": "id",
        "@searchField": "name"
      },
      givenDataTestId: "[data-test-kb-form-input-label-id='Apple']",
      negateExpectation: true
    }
  ]
})

hasAttributes({
  ...component,
  scenario: "title text",
  hasAttributes: [
    {
      scenario: "help text",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": MOCK.val1,
        "@optionsArePrimitive": true,
        "@valueField": "id",
        "@searchField": "name"
      },
      givenDataTestId: "[data-test-kb-form-label-label-id='Apple']",
      expectAttributes: { title: "Click to select Apple", for: "idApple" }
    },
    {
      scenario: "help text",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": MOCK.val1,
        "@optionsArePrimitive": true,
        "@valueField": "id",
        "@searchField": "name"
      },
      givenDataTestId: "[data-test-kb-form-label-label-id='Banana']",
      expectAttributes: { title: "Click to select Banana", for: "idBanana" }
    },
    {
      scenario: "help text",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": MOCK.val1,
        "@optionsArePrimitive": true,
        "@valueField": "id",
        "@searchField": "name"
      },
      givenDataTestId: "[data-test-kb-form-label-label-id='Carrot']",
      expectAttributes: { title: "Click to select Carrot", for: "idCarrot" }
    }
  ]
})

isReactive({
  ...component,
  scenario: "change event handling",
  funcHookName: "changeHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@optionsArePrimitive": true,
        "@valueField": "id",
        "@searchField": "name"
      },
      givenDataTestId: "[data-test-kb-form-input-label-id='Carrot']",
      action: "click",
      expectParameters: [
        [TESTKEY, null],
        [TESTKEY, MOCK.val2]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": MOCK.val1,
        "@optionsArePrimitive": true,
        "@valueField": "id",
        "@searchField": "name"
      },
      givenDataTestId: "[data-test-kb-form-input-label-id='Carrot']",
      action: "click",
      expectParameters: [
        [TESTKEY, MOCK.val1],
        [TESTKEY, MOCK.val2]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "when not required",
  funcHookName: "validateHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@optionsArePrimitive": true,
        "@valueField": "id",
        "@searchField": "name"
      },
      givenDataTestId: "[data-test-kb-form-input-label-id='Carrot']",
      action: "click",
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": MOCK.val1,
        "@optionsArePrimitive": true,
        "@valueField": "id",
        "@searchField": "name"
      },
      givenDataTestId: "[data-test-kb-form-input-label-id='Carrot']",
      action: "click",
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    }
  ]
})

willYield({
  ...component,
  scenario: "yields unconditionitionally"
})
