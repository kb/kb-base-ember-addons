/** KbForm::CkEditor Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"

const TESTKEY = "id"
const MOCK = {
  val1: "Apple",
  val2: "Cantaloupe",
  validate: "7",
  null: ""
}

const component = {
  componentPath: "kb-form/ck-editor",
  componentName: "ck-editor",
  componentClass: "KbForm::CkEditor"
}

hasAttributes({
  ...component,
  scenario: "",
  hasAttributes: [
    {
      scenario: "attributes passed",
      givenAttrs: { title: "Click to ck-editor me" },
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectAttributes: { title: "Click to ck-editor me" }
    }
  ]
})

hasDataTestIds({
  ...component,
  scenario: "",
  hasDataTestIds: [
    {
      scenario: "textarea",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-input-id]"
    },
    {
      scenario: "label",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-label-id]"
    },
    {
      scenario: "error",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-error-id]"
    }
  ]
})

hasValues({
  ...component,
  scenario: "",
  hasValues: [
    {
      scenario: "starting string",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.val1 },
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectValue: MOCK.val1
    },
    {
      scenario: "no starting string",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.null },
      givenDataTestId: "[data-test-kb-form-input-id]",
      negateExpectation: true,
      expectValue: MOCK.val1
    }
  ]
})

isDescriptive({
  ...component,
  scenario: "",
  isDescriptive: [
    {
      scenario: "help text",
      givenAttrs: {
        "@key": TESTKEY,
        "@helpText": "Textually Helpful"
      },
      givenDataTestId: "p",
      expectDescriptive: "Textually Helpful"
    },
    {
      scenario: "label: starting value",
      givenAttrs: { "@key": TESTKEY, "@label": "Labelled Descriptively" },
      givenDataTestId: "[data-test-kb-form-label-id]",
      expectDescriptive: "Labelled Descriptively"
    },
    {
      scenario: "showValidations required not valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectDescriptive: "This field is required"
    },
    {
      scenario: "showValidations required valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": MOCK.val1,
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectDescriptive: ""
    },
    {
      scenario: "showValidations not required not valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": false,
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      negateExpectation: true,
      expectDescriptive: "Required"
    }
  ]
})

isReactive({
  ...component,
  scenario: "change event handling",

  funcHookName: "changeHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.null },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, MOCK.null],
        [TESTKEY, MOCK.val2]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.val1 },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, MOCK.val1],
        [TESTKEY, MOCK.val2]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.val1 },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.null,
      expectParameters: [
        [TESTKEY, MOCK.val1],
        [TESTKEY, MOCK.null]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "when not required",
  funcHookName: "validateHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@value": MOCK.null
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, true, MOCK.null],
        [TESTKEY, true, MOCK.null]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@value": MOCK.val1
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, true, MOCK.null],
        [TESTKEY, true, MOCK.null]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: {
        "@key": TESTKEY,
        "@value": MOCK.val1
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.null,
      expectParameters: [
        [TESTKEY, true, MOCK.null],
        [TESTKEY, true, MOCK.null]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "when required",
  funcHookName: "validateHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": MOCK.null
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, false, "This field is required"],
        [TESTKEY, true, "Appears to be valid"]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": MOCK.val1
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, true, "Appears to be valid"],
        [TESTKEY, true, "Appears to be valid"]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": MOCK.val1
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.null,
      expectParameters: [
        [TESTKEY, true, "Appears to be valid"],
        [TESTKEY, false, "This field is required"]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "validation rules",
  funcHookName: "validateHook",
  isReactive: [
    {
      scenario: "min",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@min": MOCK.validate,
        "@value": MOCK.val1
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "renderOnly",
      expectParameters: [
        [TESTKEY, false, `${MOCK.validate} is the minimum number of characters`]
      ]
    },
    {
      scenario: "max",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@max": MOCK.validate,
        "@value": MOCK.val2
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "renderOnly",
      expectParameters: [
        [TESTKEY, false, `${MOCK.validate} is the maximum number of characters`]
      ]
    }
  ]
})

willYield({
  ...component,
  scenario: "yields unconditionitionally"
})
