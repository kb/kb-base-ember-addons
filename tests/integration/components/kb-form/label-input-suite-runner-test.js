/** KbForm::Label Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasClass from "@kb/ember-addon/tests/suites/integration-component-has-class"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"

const TESTKEY = "id"
const component = {
  componentPath: "kb-form/label",
  componentName: "label",
  componentClass: "KbForm::Label"
}

hasAttributes({
  ...component,
  scenario: "",
  hasAttributes: [
    {
      scenario: "attributes passed",
      givenAttrs: { title: "Click to label me" },
      givenDataTestId: "[data-test-kb-form-label-id]",
      expectAttributes: { title: "Click to label me" }
    }
  ]
})

hasClass({
  ...component,
  scenario: "",
  hasClasses: [
    {
      scenario: "show error color",
      givenAttrs: { "@showTick": true },
      givenDataTestId: "[data-test-kb-form-label-status-id]",
      expectClasses: ["text-green-500"]
    },
    {
      scenario: "no error, no color",
      givenDataTestId: "[data-test-kb-form-label-status-id]",
      negateExpectation: true,
      expectClasses: ["text-green-500"]
    }
  ]
})

hasDataTestIds({
  ...component,
  scenario: "",
  hasDataTestIds: [
    {
      scenario: "form-label",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-label-id]"
    }
  ]
})

isDescriptive({
  ...component,
  scenario: "",
  isDescriptive: [
    {
      scenario: "label",
      givenAttrs: { "@key": TESTKEY, "@label": "Wizard Name" },
      givenDataTestId: "[data-test-kb-form-label-id]",
      expectDescriptive: "Wizard Name"
    },
    {
      scenario: "no starting label",
      givenAttrs: {
        "@key": TESTKEY
      },
      givenDataTestId: "[data-test-kb-form-label-id]",
      expectDescriptive: ""
    }
  ]
})

willYield({
  ...component,
  scenario: "yields unconditionitionally"
})
