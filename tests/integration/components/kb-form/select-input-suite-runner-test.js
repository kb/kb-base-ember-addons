/** KbForm::Select Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"

const TESTKEY = "id"
const MOCK = {
  opts: ["Apple", "Banana", "Carrot"],
  val1: "Apple",
  val2: "Carrot",
  validate: "Banana",
  null: null
}

const component = {
  componentPath: "kb-form/select",
  componentName: "select",
  componentClass: "KbForm::Select"
}

hasAttributes({
  ...component,
  scenario: "",
  hasAttributes: [
    {
      scenario: "attributes passed",
      givenAttrs: { title: "Click to select me" },
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectAttributes: { title: "Click to select me" }
    }
  ]
})

hasDataTestIds({
  ...component,
  scenario: "",
  hasDataTestIds: [
    {
      scenario: "input",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-input-id]"
    },
    {
      scenario: "label",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-label-id]"
    },
    {
      scenario: "error",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-error-id]"
    }
  ]
})

isDescriptive({
  ...component,
  scenario: "starting values",
  isDescriptive: [
    {
      scenario: "starting select",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": MOCK.val1
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectDescriptive: MOCK.val1
    },
    {
      scenario: "no starting select",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": MOCK.null
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      negateExpectation: true,
      expectDescriptive: MOCK.val1
    }
  ]
})

isDescriptive({
  ...component,
  scenario: "general text",
  isDescriptive: [
    {
      scenario: "help text",
      givenAttrs: {
        "@key": TESTKEY,
        "@helpText": "Textually Helpful"
      },
      givenDataTestId: "p",
      expectDescriptive: "Textually Helpful"
    },
    {
      scenario: "label: starting value",
      givenAttrs: { "@key": TESTKEY, "@label": "Labelled Descriptively" },
      givenDataTestId: "[data-test-kb-form-label-id]",
      expectDescriptive: "Labelled Descriptively"
    },
    {
      scenario: "showValidations required not valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectDescriptive: "This field is required"
    },
    {
      scenario: "showValidations required valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": MOCK.val1,
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectDescriptive: ""
    },
    {
      scenario: "showValidations not required not valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": false,
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      negateExpectation: true,
      expectDescriptive: "Required"
    }
  ]
})

isReactive({
  ...component,
  scenario: "change event handling",
  funcHookName: "changeHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "selectChoose",
      selectValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, undefined],
        [TESTKEY, MOCK.val2],
        [TESTKEY, MOCK.val2]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": MOCK.val1,
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "selectChoose",
      selectValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, MOCK.val1],
        [TESTKEY, MOCK.val2],
        [TESTKEY, MOCK.val2]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": MOCK.val1,
        "@allowClear": true,
        "@optionsArePrimitive": true
      },
      givenDataTestId: ".ember-power-select-clear-btn",
      action: "click",
      expectParameters: [
        [TESTKEY, MOCK.val1],
        [TESTKEY, undefined]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "when not required",
  funcHookName: "validateHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "selectChoose",
      selectValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": MOCK.val1,
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "selectChoose",
      selectValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": MOCK.val1,
        "@allowClear": true,
        "@optionsArePrimitive": true
      },
      givenDataTestId: ".ember-power-select-clear-btn",
      action: "click",
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "when required",
  funcHookName: "validateHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@required": true,
        "@value": MOCK.null,
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "selectChoose",
      selectValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, false, "This field is required"],
        [TESTKEY, true, "This field is required"],
        [TESTKEY, true, "This field is required"]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@required": true,
        "@value": MOCK.val1,
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "selectChoose",
      selectValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, true, "This field is required"],
        [TESTKEY, true, "This field is required"],
        [TESTKEY, true, "This field is required"]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@required": true,
        "@value": MOCK.val1,
        "@allowClear": true,
        "@optionsArePrimitive": true
      },
      givenDataTestId: ".ember-power-select-clear-btn",
      action: "click",
      expectParameters: [
        [TESTKEY, true, "This field is required"],
        [TESTKEY, false, "This field is required"]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "validation rules",
  funcHookName: "validateHook",
  isReactive: [
    {
      scenario: "required",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@required": true,
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "renderOnly",
      expectParameters: [[TESTKEY, false, "This field is required"]]
    }
  ]
})

willYield({
  ...component,
  scenario: "yields unconditionitionally"
})
