/** KbForm::SelectMulti Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"

const TESTKEY = "id"
const MOCK = {
  opts: ["Apple", "Banana", "Carrot"],
  val1: "Apple",
  val2: "Carrot",
  validate: 2,
  null: ""
}

const component = {
  componentPath: "kb-form/select-multi",
  componentName: "select-multi",
  componentClass: "KbForm::SelectMulti"
}

hasAttributes({
  ...component,
  scenario: "",
  hasAttributes: [
    {
      scenario: "attributes passed",
      givenAttrs: { title: "Click to select-multi me" },
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectAttributes: { title: "Click to select-multi me" }
    }
  ]
})

hasDataTestIds({
  ...component,
  scenario: "",
  hasDataTestIds: [
    {
      scenario: "select-multi",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-input-id]"
    },
    {
      scenario: "",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-label-id]"
    },
    {
      scenario: "",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-error-id]"
    }
  ]
})

isDescriptive({
  ...component,
  scenario: "starting values",
  isDescriptive: [
    {
      scenario: "starting select-multi",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": [MOCK.val1]
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectDescriptive: MOCK.val1
    },
    {
      scenario: "no starting select-multi",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": []
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      negateExpectation: true,
      expectDescriptive: MOCK.val1
    }
  ]
})

isDescriptive({
  ...component,
  scenario: "",
  isDescriptive: [
    {
      scenario: "help text",
      givenAttrs: {
        "@key": TESTKEY,
        "@helpText": "Textually Helpful"
      },
      givenDataTestId: "p",
      expectDescriptive: "Textually Helpful"
    },
    {
      scenario: "label: starting value",
      givenAttrs: { "@key": TESTKEY, "@label": "Labelled Descriptively" },
      givenDataTestId: "[data-test-kb-form-label-id]",
      expectDescriptive: "Labelled Descriptively"
    },
    {
      scenario: "showValidations required not valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@showValidations": true,
        "@min": 1
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectDescriptive: "You need to select at least 1 items"
    },
    {
      scenario: "showValidations required valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": [MOCK.val1],
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectDescriptive: ""
    },
    {
      scenario: "showValidations not required not valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": false,
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      negateExpectation: true,
      expectDescriptive: "Required"
    }
  ]
})

isReactive({
  ...component,
  scenario: "change event handling",
  funcHookName: "changeHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": [],
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "selectChoose",
      selectValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, []],
        [TESTKEY, [MOCK.val2]],
        [TESTKEY, [MOCK.val2]]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": [MOCK.val1],
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "selectChoose",
      selectValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, [MOCK.val1]],
        [TESTKEY, [MOCK.val1, MOCK.val2]],
        [TESTKEY, [MOCK.val1, MOCK.val2]]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@options": MOCK.opts,
        "@value": [MOCK.val1],
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "selectChoose",
      selectValue: MOCK.val1,
      expectParameters: [
        [TESTKEY, [MOCK.val1]],
        [TESTKEY, []],
        [TESTKEY, []]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "when not required",
  funcHookName: "validateHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": [],
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "selectChoose",
      selectValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": [MOCK.val1],
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "selectChoose",
      selectValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@value": [MOCK.val1],
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "selectChoose",
      selectValue: MOCK.val1,
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "when required",
  funcHookName: "validateHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@required": true,
        "@min": 1,
        "@value": [],
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "selectChoose",
      selectValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, false, `You need to select at least 1 items`],
        [TESTKEY, true, "Appears to be valid"],
        [TESTKEY, true, "Appears to be valid"]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@required": true,
        "@min": MOCK.validate,
        "@value": [MOCK.val1],
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "selectChoose",
      selectValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, false, `You need to select at least ${MOCK.validate} items`],
        [TESTKEY, true, "Appears to be valid"],
        [TESTKEY, true, "Appears to be valid"]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@required": true,
        "@min": MOCK.validate,
        "@value": [MOCK.val1, MOCK.val2],
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "selectChoose",
      selectValue: MOCK.val1,
      expectParameters: [
        [TESTKEY, true, "Appears to be valid"],
        [TESTKEY, false, `You need to select at least ${MOCK.validate} items`],
        [TESTKEY, false, `You need to select at least ${MOCK.validate} items`]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "validation rules",
  funcHookName: "validateHook",
  isReactive: [
    {
      scenario: "min",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@required": true,
        "@min": MOCK.validate,
        "@value": [MOCK.val1],
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "renderOnly",
      expectParameters: [
        [TESTKEY, false, `You need to select at least ${MOCK.validate} items`]
      ]
    },
    {
      scenario: "max",
      givenAttrs: {
        "@key": TESTKEY,
        "@options": MOCK.opts,
        "@required": true,
        "@max": MOCK.validate,
        "@value": MOCK.opts,
        "@optionsArePrimitive": true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "renderOnly",
      expectParameters: [
        [TESTKEY, false, `You cannot select more than ${MOCK.validate} items`]
      ]
    }
  ]
})

willYield({
  ...component,
  scenario: "yields unconditionitionally"
})
