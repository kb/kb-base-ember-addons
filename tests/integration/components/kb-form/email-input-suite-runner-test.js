/** KbForm::Email Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"

const TESTKEY = "id"
const MOCK = {
  val1: "wizard@mail.com",
  val2: "apprentice@mail.com",
  validate: 6,
  null: ""
}
Object.freeze(MOCK)

const component = {
  componentPath: "kb-form/email",
  componentName: "email",
  componentClass: "KbForm::Email"
}

hasAttributes({
  ...component,
  hasAttributes: [
    {
      scenario: "",
      givenAttrs: { title: "Click to boolean me" },
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectAttributes: { title: "Click to boolean me" }
    }
  ]
})

hasDataTestIds({
  ...component,
  hasDataTestIds: [
    {
      scenario: "form-input",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-input-id]"
    },
    {
      scenario: "",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-label-id]"
    },
    {
      scenario: "",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-error-id]"
    }
  ]
})

hasValues({
  ...component,
  hasValues: [
    {
      scenario: "starting email",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.val1 },
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectValue: MOCK.val1
    },
    {
      scenario: "no starting email",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.null },
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectValue: MOCK.null
    }
  ]
})

isDescriptive({
  ...component,
  isDescriptive: [
    {
      scenario: "help test",
      givenAttrs: {
        "@key": TESTKEY,
        "@helpText": "Textually Helpful"
      },
      givenDataTestId: "p",
      expectDescriptive: "Textually Helpful"
    },
    {
      scenario: "label",
      givenAttrs: { "@key": TESTKEY, "@label": "Labelled Descriptively" },
      givenDataTestId: "[data-test-kb-form-label-id]",
      expectDescriptive: "Labelled Descriptively"
    },
    {
      scenario: "showValidations required not valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectDescriptive: "This email address is not valid"
    },
    {
      scenario: "showValidations required valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": MOCK.val1,
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectDescriptive: ""
    },
    {
      scenario: "showValidations not required not valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": false,
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      negateExpectation: true,
      expectDescriptive: "required"
    }
  ]
})

isMutating({
  ...component,

  funcHookName: "changeHook",
  isMutating: [
    {
      scenario: "adding",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.null },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, MOCK.null],
        [TESTKEY, MOCK.val2]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.val1 },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, MOCK.val1],
        [TESTKEY, MOCK.val2]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.val1 },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.null,
      expectParameters: [
        [TESTKEY, MOCK.val1],
        [TESTKEY, ""]
      ]
    }
  ]
})

isReactive({
  ...component,

  funcHookName: "changeHook",
  scenario: "change event handling",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.null },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, MOCK.null],
        [TESTKEY, MOCK.val2]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.val1 },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, MOCK.val1],
        [TESTKEY, MOCK.val2]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.val1 },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.null,
      expectParameters: [
        [TESTKEY, MOCK.val1],
        [TESTKEY, MOCK.null]
      ]
    }
  ]
})

isReactive({
  ...component,
  // NB: We're testing on behalf of funcHookName here; not validateHook itself.
  funcHookName: "validateHook",
  scenario: "when not required",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": false,
        "@value": MOCK.null
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": false,
        "@value": MOCK.val1
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": false,
        "@value": MOCK.val1
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.null,
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    }
  ]
})

isReactive({
  ...component,
  funcHookName: "validateHook",
  scenario: "when required",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": MOCK.null
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, false, "This field is required"],
        [TESTKEY, true, "Appears to be valid"]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": MOCK.val1
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, true, "Appears to be valid"],
        [TESTKEY, true, "Appears to be valid"]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": MOCK.val1
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "fillIn",
      fillInValue: MOCK.null,
      expectParameters: [
        [TESTKEY, true, "Appears to be valid"],
        [TESTKEY, false, "This field is required"]
      ]
    }
  ]
})

isReactive({
  ...component,
  funcHookName: "validateHook",
  scenario: "validation rules",
  isReactive: [
    {
      scenario: "min",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@min": 10,
        "@value": "it@is.us"
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "renderOnly",
      expectParameters: [
        [TESTKEY, false, "10 is the minimum number of characters"]
      ]
    },
    {
      scenario: "max",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@max": 5,
        "@value": "it@is.us"
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "renderOnly",
      expectParameters: [
        [TESTKEY, false, "5 is the maximum number of characters"]
      ]
    }
  ]
})

isReactive({
  ...component,
  funcHookName: "validateHook",
  scenario: "email regex rules",
  isReactive: [
    {
      pauseTest: false,
      scenario: "no @ sign",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": "apprentice-mail.com"
      },
      action: "renderOnly",
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectParameters: [[TESTKEY, false, "This email address is not valid"]]
    },
    {
      pauseTest: false,
      scenario: "@ domain has no dot",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": "apprentice@mail"
      },
      action: "renderOnly",
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectParameters: [[TESTKEY, false, "This email address is not valid"]]
    },
    {
      scenario: "has space",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": "wizard apprentice@mail.com"
      },
      action: "renderOnly",
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectParameters: [[TESTKEY, false, "This email address is not valid"]]
    },
    {
      scenario: "has ?",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": "apprentice?@mail.com"
      },
      action: "renderOnly",
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectParameters: [[TESTKEY, false, "This email address is not valid"]]
    },
    {
      scenario: "singleword",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": "apprenticemailcom"
      },
      action: "renderOnly",
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectParameters: [[TESTKEY, false, "This email address is not valid"]]
    }
  ]
})

willYield({
  ...component,
  scenario: "yields unconditionitionally"
})
