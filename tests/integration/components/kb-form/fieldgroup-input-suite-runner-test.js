/** KbForm::Fieldgroup Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"

const TESTKEY = "id"
const component = {
  componentPath: "kb-form/fieldgroup",
  componentName: "fieldgroup",
  componentClass: "KbForm::Fieldgroup"
}

hasAttributes({
  ...component,
  scenario: "",
  hasAttributes: [
    {
      scenario: "attributes passed",
      givenAttrs: { title: "Click to fieldgroup me" },
      givenDataTestId: "[data-test-kb-form-fieldgroup-id]",
      expectAttributes: { title: "Click to fieldgroup me" }
    }
  ]
})

hasDataTestIds({
  ...component,
  scenario: "",
  hasDataTestIds: [
    {
      scenario: "form-input",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-fieldgroup-id]"
    }
  ]
})

willYield({
  ...component,
  scenario: "yields unconditionitionally"
})
