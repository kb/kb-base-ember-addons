/** KbForm::Error Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"

const TESTKEY = "id"
const component = {
  componentPath: "kb-form/error",
  componentName: "error",
  componentClass: "KbForm::Error"
}

hasAttributes({
  ...component,
  scenario: "",
  hasAttributes: [
    {
      scenario: "attributes passed",
      givenAttrs: { title: "Click to error me" },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectAttributes: { title: "Click to error me" }
    }
  ]
})

hasDataTestIds({
  ...component,
  scenario: "",
  hasDataTestIds: [
    {
      scenario: "form-error",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-error-id]"
    }
  ]
})

isDescriptive({
  ...component,
  scenario: "",
  isDescriptive: [
    {
      scenario: "help text",
      givenAttrs: {
        "@key": TESTKEY,
        "@showErr": true,
        "@validateMessage": "I am in error"
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectDescriptive: "I am in error"
    }
  ]
})

willYield({
  ...component,
  scenario: "on condition",
  givenAttrs: {
    "@showErr": true
  }
})

willYield({
  ...component,
  scenario: "condition false",
  negateExpectation: true
})
