/** KbForm::Upload Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"

import newFileData from "@kb/ember-addon/tests/suites/resource-file-data"

const TESTKEY = "id"
const MOCK = {
  val1: new File([newFileData], "file1.jpg", {
    type: "image/jpg"
  }),
  val2: new File([newFileData], "file2.jpg", {
    type: "image/jpg"
  }),
  val3: new File([newFileData], "file3.jpg", {
    type: "image/jpg"
  }),
  validate: 2,
  null: []
}

const component = {
  componentPath: "kb-form/upload",
  componentName: "upload",
  componentClass: "KbForm::Upload"
}

hasAttributes({
  ...component,
  scenario: "",
  hasAttributes: [
    {
      scenario: "attributes passed",
      givenAttrs: { title: "Click to upload me" },
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectAttributes: { title: "Click to upload me" }
    }
  ]
})

hasDataTestIds({
  ...component,
  scenario: "",
  hasDataTestIds: [
    {
      scenario: "upload",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-input-id]"
    },
    {
      scenario: "",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-label-id]"
    },
    {
      scenario: "",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-error-id]"
    },
    {
      scenario: "with uploaded files",
      givenAttrs: { "@key": TESTKEY, "@value": [MOCK.val1] },
      givenDataTestId: "[data-test-kb-uploaded-files-id]"
    },
    {
      scenario: "no uploaded files",
      givenAttrs: { "@key": TESTKEY, "@value": [] },
      givenDataTestId: "[data-test-kb-uploaded-files-id]",
      negateExpectation: true
    },
    {
      scenario: "uploaded can be deleted",
      givenAttrs: { "@key": TESTKEY, "@value": [MOCK.val1] },
      givenDataTestId: "[data-test-kb-uploaded-files-id]"
    }
  ]
})

isDescriptive({
  ...component,
  scenario: "starting values",
  isDescriptive: [
    {
      scenario: "starting upload",
      givenAttrs: { "@key": TESTKEY, "@value": [MOCK.val1] },
      givenDataTestId: "[data-test-kb-uploaded-files-id]",
      expectDescriptive: MOCK.val1.name
    }
  ]
})

isDescriptive({
  ...component,
  scenario: "basic text",
  isDescriptive: [
    {
      scenario: "help text",
      pauseTest: false,
      givenAttrs: {
        "@key": TESTKEY,
        "@helpText": "Textually Helpful"
      },
      givenDataTestId: "p.text-gray-500",
      expectDescriptive: "Textually Helpful"
    },
    {
      scenario: "label: starting value",
      givenAttrs: { "@key": TESTKEY, "@label": "Labelled Descriptively" },
      givenDataTestId: "[data-test-kb-form-label-id]",
      expectDescriptive: "Labelled Descriptively"
    },
    {
      scenario: "showValidations required not valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@showValidations": true,
        "@min": MOCK.validate,
        "@fileType": "image"
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectDescriptive: `At least ${MOCK.validate} images are required`
    },
    {
      scenario: "showValidations required valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": [MOCK.val1],
        "@showValidations": true,
        "@min": MOCK.validate,
        "@fileType": "image"
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectDescriptive: ""
    },
    {
      scenario: "showValidations not required not valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": false,
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      negateExpectation: true,
      expectDescriptive: "Required"
    }
  ]
})

isReactive({
  ...component,
  scenario: "change event handling",
  funcHookName: "changeHook",
  todo: true,
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@fileType": "image",
        "@accept": "application/png",
        "@value": MOCK.null,
        "@allowBackgroundUpload": true
      },
      givenDataTestId: `#upload-${TESTKEY}`,
      action: "upload",
      uploadValue: [MOCK.val2],
      expectParameters: [
        [TESTKEY, MOCK.null],
        [TESTKEY, [MOCK.val2]]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@fileType": "image",
        "@accept": "application/png",
        "@value": [MOCK.val1],
        "@allowBackgroundUpload": true
      },
      givenDataTestId: `#upload-${TESTKEY}`,
      action: "upload",
      uploadValue: [MOCK.val2],
      expectParameters: [
        [TESTKEY, [MOCK.val1]],
        [TESTKEY, [MOCK.val1, MOCK.val2]]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: { "@key": TESTKEY, "@value": [MOCK.val1] },
      givenDataTestId: "[data-test-kb-button-remove-file-id]",
      action: "click",
      expectParameters: [
        [TESTKEY, [MOCK.val1]],
        [TESTKEY, []]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "when not required",
  funcHookName: "validateHook",
  todo: true,
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@fileType": "image",
        "@accept": "application/png",
        "@value": []
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "upload",
      uploadValue: [MOCK.val2],
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@fileType": "image",
        "@accept": "application/png",
        "@value": [MOCK.val1]
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "upload",
      uploadValue: [MOCK.val2],
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: {
        "@key": TESTKEY,
        "@fileType": "image",
        "@accept": "application/png",
        "@value": [MOCK.val1]
      },
      givenDataTestId: "[data-test-kb-button-remove-file-id]",
      action: "click",
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "when required",
  funcHookName: "validateHook",
  todo: true,
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@fileType": "image",
        "@accept": "application/png",
        "@required": true,
        "@value": []
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "upload",
      uploadValue: [MOCK.val2],
      expectParameters: [
        [TESTKEY, false, "At least 1 images are required"],
        [TESTKEY, true, "Appears to be valid"]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@fileType": "image",
        "@accept": "application/png",
        "@required": true,
        "@value": MOCK.val1
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "upload",
      uploadValue: MOCK.val2,
      expectParameters: [
        [TESTKEY, true, "Appears to be valid"],
        [TESTKEY, true, "Appears to be valid"]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: {
        "@key": TESTKEY,
        "@fileType": "image",
        "@accept": "application/png",
        "@required": true,
        "@value": MOCK.val1
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "upload",
      uploadValue: MOCK.null,
      expectParameters: [
        [TESTKEY, true, "Appears to be valid"],
        [TESTKEY, false, "This field is required"]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "validation rules",
  funcHookName: "validateHook",
  todo: true,
  isReactive: [
    {
      scenario: "min",
      givenAttrs: {
        "@key": TESTKEY,
        "@fileType": "image",
        "@accept": "application/png",
        "@required": true,
        "@min": MOCK.validate,
        "@value": [MOCK.val1]
      },
      action: "renderOnly",
      expectParameters: [
        [TESTKEY, false, `At least ${MOCK.validate} images are required`]
      ]
    },
    {
      scenario: "max",
      givenAttrs: {
        "@key": TESTKEY,
        "@fileType": "image",
        "@accept": "application/png",
        "@required": true,
        "@max": MOCK.validate,
        "@value": [MOCK.val1, MOCK.val2, MOCK.val3]
      },
      action: "renderOnly",
      expectParameters: [
        [TESTKEY, false, `No more than ${MOCK.validate} images are allowed`]
      ]
    }
  ]
})

willYield({
  ...component,
  scenario: "yields unconditionitionally"
})
