/** KbForm::Boolean Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"

const TESTKEY = "id"
const MOCK = {
  true: true,
  false: false
}
Object.freeze(MOCK)

const component = {
  componentPath: "kb-form/boolean",
  componentName: "boolean",
  componentClass: "KbForm::Boolean"
}

hasAttributes({
  ...component,
  hasAttributes: [
    {
      scenario: "",
      givenAttrs: { title: "Click to boolean me" },
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectAttributes: { title: "Click to boolean me" }
    }
  ]
})

hasDataTestIds({
  ...component,
  hasDataTestIds: [
    {
      scenario: "form-input",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-input-id]"
    },
    {
      scenario: "form-label",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-label-id]"
    },
    {
      scenario: "form-error",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-error-id]"
    }
  ]
})

isChecked({
  ...component,
  isChecked: [
    {
      scenario: "starting checked",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.true },
      givenDataTestId: "[data-test-kb-form-input-id]"
    },
    {
      scenario: "not starting checked",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.false },
      givenDataTestId: "[data-test-kb-form-input-id]",
      negateExpectation: true
    }
  ]
})

isDescriptive({
  ...component,
  isDescriptive: [
    {
      scenario: "",
      givenAttrs: {
        "@key": TESTKEY,
        "@helpText": "Textually Helpful"
      },
      givenDataTestId: "p",
      expectDescriptive: "Textually Helpful"
    },
    {
      scenario: "input label",
      givenAttrs: { "@key": TESTKEY, "@label": "Labelled Descriptively" },
      givenDataTestId: "[data-test-kb-form-label-id]",
      expectDescriptive: "Labelled Descriptively"
    },
    {
      scenario: "showValidations required not valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectDescriptive: "This field is required"
    },
    {
      scenario: "showValidations required valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": MOCK.true,
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectDescriptive: ""
    },
    {
      scenario: "showValidations not required not valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": false,
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      negateExpectation: true,
      expectDescriptive: "required"
    }
  ]
})

isMutating({
  ...component,

  funcHookName: "changeHook",
  scenario: "change event handling",
  isMutating: [
    {
      scenario: "toggle true",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.false },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "click",
      expectParameters: [[TESTKEY, MOCK.true]]
    },
    {
      scenario: "toggle false",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.true },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "click",
      expectParameters: [[TESTKEY, MOCK.false]]
    }
  ]
})

isReactive({
  ...component,

  funcHookName: "changeHook",
  scenario: "when not required",
  isReactive: [
    {
      scenario: "toggled true",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.false },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "click",
      expectParameters: [
        [TESTKEY, MOCK.false],
        [TESTKEY, MOCK.true]
      ]
    },
    {
      scenario: "toggled false",
      givenAttrs: { "@key": TESTKEY, "@value": MOCK.true },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "click",
      expectParameters: [
        [TESTKEY, MOCK.true],
        [TESTKEY, MOCK.false]
      ]
    }
  ]
})

isReactive({
  ...component,
  // NB: We're testing on behalf of funcHookName here; not validateHook itself.
  funcHookName: "validateHook",
  scenario: "when required",
  isReactive: [
    {
      scenario: "toggled true",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": false,
        "@value": MOCK.false
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "click",
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    },
    {
      scenario: "toggled false",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": false,
        "@value": MOCK.true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "click",
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    }
  ]
})

isReactive({
  ...component,
  funcHookName: "validateHook",
  scenario: "validation rules",
  isReactive: [
    {
      scenario: "toggled true",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": MOCK.false
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "click",
      expectParameters: [
        [TESTKEY, false, "This field is required"],
        [TESTKEY, true, "Appears to be valid"]
      ]
    },
    {
      scenario: "toggled false",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@value": MOCK.true
      },
      givenDataTestId: "[data-test-kb-form-input-id]",
      action: "click",
      expectParameters: [
        [TESTKEY, true, "Appears to be valid"],
        [TESTKEY, false, "This field is required"]
      ]
    }
  ]
})

willYield({
  ...component,
  scenario: "yields unconditionitionally"
})
