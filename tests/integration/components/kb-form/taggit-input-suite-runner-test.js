/** KbForm::Taggit Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"

const TESTKEY = "id"
const MOCK = {
  tags: ["Apple", "Banana", "Carrot"],
  validate: 2
}

const component = {
  componentPath: "kb-form/taggit",
  componentName: "taggit",
  componentClass: "KbForm::Taggit"
}

hasAttributes({
  ...component,
  scenario: "",
  hasAttributes: [
    {
      scenario: "attributes passed",
      givenAttrs: { title: "Click to boolean me", "@tags": MOCK.tags },
      givenDataTestId: "[data-test-kb-form-input-id]",
      expectAttributes: { title: "Click to boolean me" }
    }
  ]
})

hasDataTestIds({
  ...component,
  scenario: "tags are selectable",
  hasDataTestIds: [
    {
      scenario: "no tags no inputs",
      givenAttrs: { "@key": TESTKEY },
      negateExpectation: true,
      givenDataTestId: "[data-test-kb-form-input-id]",
      negateExpectation: true
    },
    {
      scenario: "tags no inputs",
      givenAttrs: { "@key": TESTKEY, "@tags": MOCK.tags },
      givenDataTestId: "[data-test-kb-form-input-id]",
      count: 3
    },
    {
      scenario: "Tag Apple",
      givenAttrs: { "@key": TESTKEY, "@tags": MOCK.tags },
      givenDataTestId: `[data-test-kb-form-input-tag-id='Apple']`
    },
    {
      scenario: "Tag Banana",
      givenAttrs: { "@key": TESTKEY, "@tags": MOCK.tags },
      givenDataTestId: `[data-test-kb-form-input-tag-id='Banana']`
    },
    {
      scenario: "Tag Carrot",
      givenAttrs: { "@key": TESTKEY, "@tags": MOCK.tags },
      givenDataTestId: `[data-test-kb-form-input-tag-id='Carrot']`
    },
    {
      scenario: "not showNewTags: new Tags button",
      givenAttrs: { "@key": TESTKEY, "@showNewTags": false },
      givenDataTestId: "[data-test-kb-form-button-newtags-id]"
    },
    {
      scenario: "not showNewTags: new Tags input",
      givenAttrs: { "@key": TESTKEY, "@showNewTags": false },
      givenDataTestId: "[data-test-kb-form-input-newtags-id]",
      negateExpectation: true
    },
    {
      scenario: "showNewTags: new Tags button",
      givenAttrs: { "@key": TESTKEY, "@showNewTags": true },
      givenDataTestId: "[data-test-kb-form-button-newtags-id]",
      negateExpectation: true
    },
    {
      scenario: "showNewTags: new Tags input",
      givenAttrs: { "@key": TESTKEY, "@showNewTags": true },
      givenDataTestId: "[data-test-kb-form-input-newtags-id]"
    },
    {
      scenario: "not showNewTags: new Tags button",
      givenAttrs: {
        "@key": TESTKEY,
        "@showNewTags": false,
        "@noNewTags": true
      },
      givenDataTestId: "[data-test-kb-form-button-newtags-id]",
      negateExpectation: true
    },
    {
      scenario: "not showNewTags: new Tags input",
      givenAttrs: {
        "@key": TESTKEY,
        "@showNewTags": false,
        "@noNewTags": true
      },
      givenDataTestId: "[data-test-kb-form-input-newtags-id]",
      negateExpectation: true
    },
    {
      scenario: "showNewTags: new Tags button",
      givenAttrs: { "@key": TESTKEY, "@showNewTags": true, "@noNewTags": true },
      givenDataTestId: "[data-test-kb-form-button-newtags-id]",
      negateExpectation: true
    },
    {
      scenario: "showNewTags: new Tags input",
      givenAttrs: { "@key": TESTKEY, "@showNewTags": true, "@noNewTags": true },
      givenDataTestId: "[data-test-kb-form-input-newtags-id]",
      negateExpectation: true
    }
  ]
})

isChecked({
  ...component,
  scenario: "",
  isChecked: [
    {
      scenario: "Apple ticked",
      givenAttrs: {
        "@key": TESTKEY,
        "@tags": MOCK.tags,
        "@value": ["Apple", "Banana"]
      },
      givenDataTestId: `[data-test-kb-form-input-tag-id='Apple']`
    },
    {
      scenario: "Banana ticked",
      givenAttrs: {
        "@key": TESTKEY,
        "@tags": MOCK.tags,
        "@value": ["Apple", "Banana"]
      },
      givenDataTestId: `[data-test-kb-form-input-tag-id='Banana']`
    },
    {
      scenario: "Carrot not ticked",
      givenAttrs: {
        "@key": TESTKEY,
        "@tags": MOCK.tags,
        "@value": ["Apple", "Banana"]
      },
      givenDataTestId: `[data-test-kb-form-input-tag-id='Carrot']`,
      negateExpectation: true
    }
  ]
})

isDescriptive({
  ...component,
  scenario: "",
  isDescriptive: [
    {
      scenario: "help text",
      givenAttrs: {
        "@key": TESTKEY,
        "@helpText": "Textually Helpful",
        "@tags": MOCK.tags
      },
      givenDataTestId: "p",
      expectDescriptive: "Textually Helpful"
    },
    {
      scenario: "label",
      givenAttrs: {
        "@key": TESTKEY,
        "@label": "Labelled Descriptively",
        "@tags": MOCK.tags
      },
      givenDataTestId: "[data-test-kb-form-label-id]",
      expectDescriptive: "Labelled Descriptively"
    },
    {
      scenario: "showValidations required not valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": "true",
        "@showValidations": true,
        "@tags": MOCK.tags
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectDescriptive: "You need to tick at least 1 tags"
    },
    {
      scenario: "showValidations required valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": true,
        "@min": 1,
        "@value": ["Apple"],
        "@showValidations": true
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      expectDescriptive: ""
    },
    {
      scenario: "showValidations not required not valid",
      givenAttrs: {
        "@key": TESTKEY,
        "@required": false,
        "@showValidations": true,
        "@tags": MOCK.tags
      },
      givenDataTestId: "[data-test-kb-form-error-id]",
      negateExpectation: true,
      expectDescriptive: "required"
    }
  ]
})

isReactive({
  ...component,
  scenario: "change event handling",
  funcHookName: "changeHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: { "@key": TESTKEY, "@value": [], "@tags": MOCK.tags },
      givenDataTestId: `[data-test-kb-form-input-tag-id='Apple']`,
      action: "click",
      expectParameters: [
        [TESTKEY, []],
        [TESTKEY, ["Apple"]]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: { "@key": TESTKEY, "@value": ["Apple"], "@tags": MOCK.tags },
      givenDataTestId: `[data-test-kb-form-input-tag-id='Carrot']`,
      action: "click",
      expectParameters: [
        [TESTKEY, ["Apple"]],
        [TESTKEY, ["Apple", "Carrot"]]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: { "@key": TESTKEY, "@value": ["Apple"], "@tags": MOCK.tags },
      givenDataTestId: `[data-test-kb-form-input-tag-id='Apple']`,
      action: "click",
      expectParameters: [
        [TESTKEY, ["Apple"]],
        [TESTKEY, []]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "new tags",
  funcHookName: "changeHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@value": [],
        "@tags": MOCK.tags,
        "@showNewTags": true
      },
      givenDataTestId: "[data-test-kb-form-input-newtags-id]",
      action: "fillIn",
      fillInValue: "Date, Elderberry",
      expectParameters: [
        [TESTKEY, []],
        [TESTKEY, []],
        [TESTKEY, ["Date", "Elderberry"]]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@value": ["Apple"],
        "@tags": MOCK.tags,
        "@showNewTags": true
      },
      givenDataTestId: "[data-test-kb-form-input-newtags-id]",
      action: "fillIn",
      fillInValue: "Date, Elderberry",
      expectParameters: [
        [TESTKEY, ["Apple"]],
        [TESTKEY, ["Apple"]],
        [TESTKEY, ["Apple", "Date", "Elderberry"]]
      ]
    },
    {
      scenario: "duplicating",
      givenAttrs: {
        "@key": TESTKEY,
        "@value": ["Apple"],
        "@tags": MOCK.tags,
        "@showNewTags": true
      },
      givenDataTestId: "[data-test-kb-form-input-newtags-id]",
      action: "fillIn",
      fillInValue: "Apple, Elderberry",
      expectParameters: [
        [TESTKEY, ["Apple"]],
        [TESTKEY, ["Apple"]],
        [TESTKEY, ["Apple", "Elderberry"]]
      ]
    },
    {
      scenario: "empty",
      givenAttrs: {
        "@key": TESTKEY,
        "@value": ["Apple"],
        "@tags": MOCK.tags,
        "@showNewTags": true
      },
      givenDataTestId: "[data-test-kb-form-input-newtags-id]",
      action: "fillIn",
      fillInValue: ",Date,Elderberry,,Farkleberry,",
      expectParameters: [
        [TESTKEY, ["Apple"]],
        [TESTKEY, ["Apple"]],
        [TESTKEY, ["Apple", "Date", "Elderberry", "Farkleberry"]]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "not required: adding",
  funcHookName: "validateHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: { "@key": TESTKEY, "@value": [], "@tags": MOCK.tags },
      givenDataTestId: `[data-test-kb-form-input-tag-id='Apple']`,
      action: "click",
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    },
    {
      scenario: "not required: editing",
      givenAttrs: { "@key": TESTKEY, "@value": ["Apple"], "@tags": MOCK.tags },
      givenDataTestId: `[data-test-kb-form-input-tag-id='Carrot']`,
      action: "click",
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    },
    {
      scenario: "not required: erasing",
      givenAttrs: { "@key": TESTKEY, "@value": ["Apple"], "@tags": MOCK.tags },
      givenDataTestId: `[data-test-kb-form-input-tag-id='Apple']`,
      action: "click",
      expectParameters: [
        [TESTKEY, true, ""],
        [TESTKEY, true, ""]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "when required",
  funcHookName: "validateHook",
  isReactive: [
    {
      scenario: "adding",
      givenAttrs: {
        "@key": TESTKEY,
        "@value": [],
        "@tags": MOCK.tags,
        "@required": true,
        "@min": 1
      },
      givenDataTestId: `[data-test-kb-form-input-tag-id='Apple']`,
      action: "click",
      expectParameters: [
        [TESTKEY, false, `You need to tick at least 1 tags`],
        [TESTKEY, true, "Appears to be valid"]
      ]
    },
    {
      scenario: "editing",
      givenAttrs: {
        "@key": TESTKEY,
        "@value": ["Apple"],
        "@tags": MOCK.tags,
        "@required": true,
        "@min": 1
      },
      givenDataTestId: `[data-test-kb-form-input-tag-id='Banana']`,
      action: "click",
      expectParameters: [
        [TESTKEY, true, "Appears to be valid"],
        [TESTKEY, true, "Appears to be valid"]
      ]
    },
    {
      scenario: "erasing",
      givenAttrs: {
        "@key": TESTKEY,
        "@value": ["Apple"],
        "@tags": MOCK.tags,
        "@required": true,
        "@min": 1
      },
      givenDataTestId: `[data-test-kb-form-input-tag-id='Apple']`,
      action: "click",
      expectParameters: [
        [TESTKEY, true, "Appears to be valid"],
        [TESTKEY, false, `You need to tick at least 1 tags`]
      ]
    }
  ]
})

isReactive({
  ...component,
  scenario: "validation rules",
  funcHookName: "validateHook",
  isReactive: [
    {
      scenario: "max",
      pauseTest: false,
      givenAttrs: {
        "@key": TESTKEY,
        "@tags": MOCK.tags,
        "@value": ["Apple", "Banana", "Carrot"],
        "@required": true,
        "@max": 2
      },
      action: "renderOnly",
      givenDataTestId: `[data-test-kb-form-input-tag-id='Apple']`,
      expectParameters: [
        [TESTKEY, false, `You cannot tick more than ${MOCK.validate} tags`]
      ]
    },
    {
      scenario: "min",
      givenAttrs: {
        "@key": TESTKEY,
        "@tags": MOCK.tags,
        "@value": ["Apple"],
        "@required": true,
        "@min": 2
      },
      action: "renderOnly",
      givenDataTestId: `[data-test-kb-form-input-tag-id='Apple']`,
      expectParameters: [
        [TESTKEY, false, `You need to tick at least ${MOCK.validate} tags`]
      ]
    }
  ]
})

willYield({
  ...component,
  scenario: "yields unconditionitionally"
})
