/** KbForm::Fieldset Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"

const TESTKEY = "id"
const component = {
  componentPath: "kb-form/fieldset",
  componentName: "fieldset",
  componentClass: "KbForm::Fieldset"
}

hasAttributes({
  ...component,
  scenario: "",
  hasAttributes: [
    {
      scenario: "attributes passed",
      givenAttrs: { title: "Click to fieldset me" },
      givenDataTestId: "[data-test-kb-form-fieldset-id]",
      expectAttributes: { title: "Click to fieldset me" }
    }
  ]
})

hasDataTestIds({
  ...component,
  scenario: "data-test-kb",
  hasDataTestIds: [
    {
      scenario: "form-fieldset",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-fieldset-id]"
    }
  ]
})

isDescriptive({
  ...component,
  scenario: "legend",
  isDescriptive: [
    {
      scenario: "legend text",
      givenAttrs: {
        "@key": TESTKEY,
        "@legend": "A Total Legend"
      },
      givenDataTestId: "[data-test-kb-form-fieldset-id]",
      expectDescriptive: "A Total Legend"
    }
  ]
})

willYield({
  ...component,
  scenario: "yields unconditionitionally"
})
