/** KbForm::SubmitButtons Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"

const TESTKEY = "id"
const MOCK = {
  val1: "Chicken Tikka",
  val2: "Bangers & Mash",
  null: ""
}
Object.freeze(MOCK)

const component = {
  componentPath: "kb-form/submit-buttons",
  componentName: "submit-buttons",
  componentClass: "KbForm::SubmitButtons"
}

hasAttributes({
  ...component,
  scenario: "",
  hasAttributes: [
    {
      scenario: "attributes passed",
      givenAttrs: { title: "Click to submit-buttons me" },
      givenDataTestId: "[data-test-kb-submit-button-id]",
      expectAttributes: { title: "Click to submit-buttons me" }
    }
  ]
})

hasDataTestIds({
  ...component,
  scenario: "",
  hasDataTestIds: [
    {
      scenario: "form-input",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-submit-button-id]"
    },
    {
      scenario: "",
      givenAttrs: { "@key": TESTKEY, "@actionCancelHook": "true" },
      givenDataTestId: "[data-test-kb-cancel-button-id]"
    },
    {
      scenario: "",
      givenAttrs: {
        "@key": TESTKEY,
        "@noButtons": true,
        "@actionCancelHook": "true"
      },
      negateExpectation: true,
      givenDataTestId: "[data-test-kb-submit-button-id]"
    },
    {
      scenario: "",
      givenAttrs: {
        "@key": TESTKEY,
        "@noButtons": true,
        "@actionCancelHook": "true"
      },
      negateExpectation: true,
      givenDataTestId: "[data-test-kb-cancel-button-id]"
    }
  ]
})

isDescriptive({
  ...component,
  scenario: "",
  isDescriptive: [
    {
      scenario: "submit-button default label",
      givenAttrs: {
        "@key": TESTKEY
      },
      givenDataTestId: "[data-test-kb-submit-button-id]",
      expectDescriptive: "Submit"
    },
    {
      scenario: "submit-button labelling",
      givenAttrs: {
        "@key": TESTKEY,
        "@buttonText": "Click me, I'm a wizard"
      },
      givenDataTestId: "[data-test-kb-submit-button-id]",
      expectDescriptive: "Click me, I'm a wizard"
    },
    {
      scenario: "submit-button labelling",
      givenAttrs: {
        "@key": TESTKEY,
        "@actionCancelHook": true
      },
      givenDataTestId: "[data-test-kb-cancel-button-id]",
      expectDescriptive: "Cancel"
    }
  ]
})

isReactive({
  ...component,
  scenario: "cancel",
  funcHookName: "actionCancelHook",
  isReactive: [
    {
      scenario: "cancelling",
      givenAttrs: { "@key": TESTKEY },
      action: "click",
      fillInValue: MOCK.val2,
      givenDataTestId: "[data-test-kb-cancel-button-id]",
      expectParameters: [[false]]
    }
  ]
})

isReactive({
  ...component,
  scenario: "submit",
  funcHookName: "actionSubmitHook",
  isReactive: [
    {
      scenario: "submitting",
      givenAttrs: { "@key": TESTKEY },
      action: "click",
      fillInValue: MOCK.val2,
      givenDataTestId: "[data-test-kb-submit-button-id]",
      expectParameters: [[true]]
    }
  ]
})

willYield({
  ...component,
  scenario: "yields unconditionitionally"
})
