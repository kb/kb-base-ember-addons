/** KbForm Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"
import { hbs } from "ember-cli-htmlbars"

const TESTKEY = "id"
const MOCK = {
  val1: "A",
  val2: "C",
  validate: "B",
  null: ""
}

const component = {
  componentPath: "kb-form",
  componentName: "kb-form",
  componentClass: "KbForm"
}

hasAttributes({
  ...component,
  scenario: "",
  hasAttributes: [
    {
      scenario: "attributes passed",
      givenAttrs: { title: "Click to kb-form me" },
      givenDataTestId: "[data-test-kb-form-id]",
      expectAttributes: { title: "Click to kb-form me" }
    }
  ]
})

hasDataTestIds({
  ...component,
  scenario: "",
  hasDataTestIds: [
    {
      scenario: "kb-form",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-form-id]"
    },
    {
      scenario: "submit button",
      givenAttrs: {
        "@key": TESTKEY
      },
      givenDataTestId: "[data-test-kb-submit-button-id]"
    },
    {
      scenario: "cancel button",
      givenAttrs: {
        "@key": TESTKEY,
        "@actionCancelHook": "true"
      },
      givenDataTestId: "[data-test-kb-cancel-button-id]"
    },
    {
      scenario: "submit button",
      givenAttrs: {
        "@key": TESTKEY,
        "@noButtons": true
      },
      negateExpectation: true,
      givenDataTestId: "[data-test-kb-submit-button-id]"
    },
    {
      scenario: "cancel button",
      givenAttrs: {
        "@key": TESTKEY,
        "@noButtons": true,
        "@actionCancelHook": "true"
      },
      negateExpectation: true,
      givenDataTestId: "[data-test-kb-cancel-button-id]"
    }
  ]
})

isDescriptive({
  ...component,
  scenario: "",
  isDescriptive: [
    {
      scenario: "help text",
      givenAttrs: {
        "@key": TESTKEY,
        "@helpText": "Textually Helpful"
      },
      givenDataTestId: "div",
      expectDescriptive: "Textually Helpful"
    },
    {
      scenario: "help text needs help panel",
      givenAttrs: {
        "@key": TESTKEY,
        "@helpText": "Textually Helpful",
        "@noHelpPanel": true
      },
      givenDataTestId: "div",
      negateExpectation: true,
      expectDescriptive: "Textually Helpful"
    }
  ]
})

isReactive({
  ...component,
  scenario: "event handling",
  // NB: We're testing on behalf of funcHookName here; not changeHook itself.
  funcHookName: "actionSubmitHook",
  isReactive: [
    {
      scenario: "submits",
      givenAttrs: { "@key": TESTKEY },
      givenDataTestId: "[data-test-kb-submit-button-id]",
      action: "click",
      expectParameters: [[{}]]
    }
  ]
})

isReactive({
  ...component,
  scenario: "boolean handling",
  funcHookName: "actionSubmitHook",
  isReactive: [
    {
      scenario: "submits",
      givenAttrs: { "@key": TESTKEY, "@value": true },
      givenHBS: hbs`
        <KbForm @actionSubmitHook={{this.reactionHook}} as |form|>
          <KbForm::Boolean
            @key={{this.keyGiven}}
            @value={{this.valueGiven}}
            @changeHook={{form.actionToUpdateModelByKey}}
          />
        </KbForm>
      `,
      givenDataTestId: "[data-test-kb-submit-button-id]",
      action: "click",
      expectParameters: [[{ id: true }]]
    }
  ]
})

willYield({
  ...component,
  scenario: "yields unconditionitionally"
})
