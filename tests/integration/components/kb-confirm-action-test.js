import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { click, render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Component | kb-confirm-action", function (hooks) {
  setupRenderingTest(hooks)

  test("renders", async function (assert) {
    await render(hbs`<KbConfirmAction />`)
    assert.equal(this.element.textContent.trim(), "")
    assert.dom("[data-test-kb-confirm-action-id]").exists()
    assert.dom("[data-test-kb-confirm-action-id=true]").exists()
    await render(hbs`
      <KbConfirmAction
        @dataTestRole="delete"
      >
        template block text
      </KbConfirmAction>
    `)
    assert
      .dom("[data-test-kb-confirm-action-id='delete']")
      .includesText("template block text")
    assert.dom("[data-test-kb-confirm-id]").doesNotExist()
    assert.dom("[data-test-kb-cancel-id]").doesNotExist()
  })

  test("working", async function (assert) {
    await render(hbs`
      <KbConfirmAction
        @dataTestRole="deleteContact"
        @faIconOk="check-circle"
        @faIconCancel="times-circle"
        @confirmMessage="Are you sure?"
      >
        template block text
      </KbConfirmAction>
    `)
    assert.dom(this.element).doesNotIncludeText("Are you sure?")
    await click("[data-test-kb-confirm-action-id='deleteContact']")
    assert.dom(this.element).includesText("Are you sure?")
    assert.dom("[data-test-kb-confirm-id]").exists()
    assert.dom("[data-test-kb-cancel-id]").exists()
  })

  test("action handling", async function (assert) {
    assert.expect(3)
    let testHook = function () {
      assert.ok(true, "action confirmed")
    }
    this.set("testHook", testHook)
    await render(hbs`
      <KbConfirmAction
        @dataTestRole="deleteContact"
        @faIconOk="check-circle"
        @faIconCancel="times-circle"
        @confirmMessage="Are you sure?"
        @confirmHook={{this.testHook}}
      >
        template block text
      </KbConfirmAction>
    `)
    await click("[data-test-kb-confirm-action-id]")
    assert.dom(this.element).includesText("Are you sure?")
    await click("[data-test-kb-confirm-id]")
    assert.dom(this.element).doesNotIncludeText("Are you sure?")
  })

  test("action cancelling", async function (assert) {
    assert.expect(1)
    let testHook = function () {
      // This is not expected to fire.
      assert.ok(true, "action confirmed")
    }
    this.set("testHook", testHook)
    await render(hbs`
      <KbConfirmAction
        @dataTestRole="deleteContact"
        @faIconOk="check-circle"
        @faIconCancel="times-circle"
        @confirmMessage="Are you sure?"
        @confirmHook={{this.testHook}}
      >
        template block text
      </KbConfirmAction>
    `)
    await click("[data-test-kb-confirm-action-id]")
    await click("[data-test-kb-cancel-id]")
    assert.dom(this.element).doesNotIncludeText("Are you sure?")
  })
})
