import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Component | kb-wireframe/top-bar", function (hooks) {
  setupRenderingTest(hooks)

  test("renders", async function (assert) {
    await render(hbs`<KbWireframe::TopBar />`)

    assert.dom("[data-test-kb-wireframe-top-bar-id]").exists()
    assert.dom("[data-test-kb-wireframe-top-bar-id=true]").exists()
    assert.dom(this.element).hasNoText()
  })

  test("it renders block text", async function (assert) {
    await render(hbs`
      <KbWireframe::TopBar>
        template block text
      </KbWireframe::TopBar>
    `)

    assert.dom("[data-test-kb-wireframe-top-bar-id]").exists()
    assert.dom("[data-test-kb-wireframe-top-bar-id=true]").exists()
    assert.dom(this.element).containsText("template block text")
  })
})
