import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module(
  "Integration | Component | kb-wireframe/top-bar/user-info",
  function (hooks) {
    setupRenderingTest(hooks)

    test("renders", async function (assert) {
      await render(hbs`<KbWireframe::TopBar::UserInfo />`)

      assert.dom("[data-test-kb-wireframe-top-bar-user-info-id]").exists()
      assert.dom("[data-test-kb-wireframe-top-bar-user-info-id=true]").exists()
      assert.dom(this.element).hasNoText()
    })

    test("it does not render block text", async function (assert) {
      await render(hbs`
      <KbWireframe::TopBar::UserInfo>
        template block text
      </KbWireframe::TopBar::UserInfo>
    `)

      assert.dom("[data-test-kb-wireframe-top-bar-user-info-id]").exists()
      assert.dom("[data-test-kb-wireframe-top-bar-user-info-id=true]").exists()
      assert.dom(this.element).hasNoText()
    })
  }
)
