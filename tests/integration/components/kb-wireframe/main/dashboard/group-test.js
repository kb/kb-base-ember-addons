import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module(
  "Integration | Component | kb-wireframe/main/dashboard/group",
  function (hooks) {
    setupRenderingTest(hooks)

    test("renders", async function (assert) {
      await render(hbs`<KbWireframe::Main::Dashboard::Group />`)

      assert.dom("[data-test-kb-wireframe-main-dashboard-group-id]").exists()
      assert
        .dom("[data-test-kb-wireframe-main-dashboard-group-id=true]")
        .exists()
      assert.dom(this.element).hasNoText()
    })

    test("it renders block text", async function (assert) {
      await render(hbs`
      <KbWireframe::Main::Dashboard::Group>
        template block text
      </KbWireframe::Main::Dashboard::Group>
    `)

      assert.dom("[data-test-kb-wireframe-main-dashboard-group-id]").exists()
      assert
        .dom("[data-test-kb-wireframe-main-dashboard-group-id=true]")
        .exists()
      assert.dom(this.element).containsText("template block text")
    })
  }
)
