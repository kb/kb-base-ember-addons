import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module(
  "Integration | Component | kb-wireframe/main/dashboard",
  function (hooks) {
    setupRenderingTest(hooks)

    test("renders", async function (assert) {
      await render(hbs`<KbWireframe::Main::Dashboard />`)

      assert.dom("[data-test-kb-wireframe-main-dashboard-id]").exists()
      assert.dom("[data-test-kb-wireframe-main-dashboard-id=true]").exists()
      assert.dom(this.element).hasNoText()
    })

    test("it renders block text", async function (assert) {
      await render(hbs`
      <KbWireframe::Main::Dashboard>
        template block text
      </KbWireframe::Main::Dashboard>
    `)

      assert.dom("[data-test-kb-wireframe-main-dashboard-id]").exists()
      assert.dom("[data-test-kb-wireframe-main-dashboard-id=true]").exists()
      assert.dom(this.element).containsText("template block text")
    })
  }
)
