import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module(
  "Integration | Component | kb-wireframe/main/instruction",
  function (hooks) {
    setupRenderingTest(hooks)

    test("renders", async function (assert) {
      await render(hbs`<KbWireframe::Main::Instruction />`)

      assert.dom("[data-test-kb-wireframe-main-instruction-id]").exists()
      assert.dom("[data-test-kb-wireframe-main-instruction-id=true]").exists()
      assert.dom(this.element).hasNoText()
    })

    test("it renders block text", async function (assert) {
      await render(hbs`
      <KbWireframe::Main::Instruction>
        template block text
      </KbWireframe::Main::Instruction>
    `)

      assert.dom("[data-test-kb-wireframe-main-instruction-id]").exists()
      assert.dom("[data-test-kb-wireframe-main-instruction-id=true]").exists()
      assert.dom(this.element).containsText("template block text")
    })
  }
)
