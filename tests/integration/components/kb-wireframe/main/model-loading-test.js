import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module(
  "Integration | Component | kb-wireframe/main/model-loading",
  function (hooks) {
    setupRenderingTest(hooks)

    test("renders", async function (assert) {
      await render(hbs`<KbWireframe::Main::ModelLoading />`)

      assert.dom("[data-test-kb-wireframe-main-model-loading-id]").exists()
      assert.dom("[data-test-kb-wireframe-main-model-loading-id=true]").exists()
      assert.dom(this.element).containsText("Loading...")
    })

    test("it renders block text", async function (assert) {
      await render(hbs`
      <KbWireframe::Main::ModelLoading>
        template block text
      </KbWireframe::Main::ModelLoading>
    `)

      assert.dom("[data-test-kb-wireframe-main-model-loading-id]").exists()
      assert.dom("[data-test-kb-wireframe-main-model-loading-id=true]").exists()
      assert.dom(this.element).containsText("template block text")
      assert.dom(this.element).containsText("Loading...")
    })
  }
)
