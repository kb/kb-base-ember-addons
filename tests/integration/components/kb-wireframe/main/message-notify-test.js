import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module(
  "Integration | Component | kb-wireframe/main/message-notify",
  function (hooks) {
    setupRenderingTest(hooks)

    test("renders", async function (assert) {
      await render(hbs`<KbWireframe::Main::MessageNotify />`)

      assert.dom("[data-test-kb-wireframe-main-message-notify-id]").exists()
      assert
        .dom("[data-test-kb-wireframe-main-message-notify-id=true]")
        .exists()
      assert.dom(this.element).hasNoText()
    })

    test("it does not render block text", async function (assert) {
      await render(hbs`
      <KbWireframe::Main::MessageNotify>
        template block text
      </KbWireframe::Main::MessageNotify>
    `)

      assert.dom("[data-test-kb-wireframe-main-message-notify-id]").exists()
      assert
        .dom("[data-test-kb-wireframe-main-message-notify-id=true]")
        .exists()
      assert.dom(this.element).hasNoText()
    })
  }
)
