import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Component | kb-wireframe/side-bar/nav", function (hooks) {
  setupRenderingTest(hooks)

  test("renders", async function (assert) {
    await render(hbs`<KbWireframe::SideBar::Nav />`)

    assert.dom("[data-test-kb-wireframe-side-bar-nav-id]").exists()
    assert.dom("[data-test-kb-wireframe-side-bar-nav-id=true]").exists()
    assert.dom(this.element).hasNoText()
  })

  test("it renders block text", async function (assert) {
    await render(hbs`
      <KbWireframe::SideBar::Nav>
        template block text
      </KbWireframe::SideBar::Nav>
    `)

    assert.dom("[data-test-kb-wireframe-side-bar-nav-id]").exists()
    assert.dom("[data-test-kb-wireframe-side-bar-nav-id=true]").exists()
    assert.dom(this.element).containsText("template block text")
  })
})
