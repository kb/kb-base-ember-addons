import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { click, render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module(
  "Integration | Component | kb-wireframe/side-bar/nav-show",
  function (hooks) {
    setupRenderingTest(hooks)

    test("renders", async function (assert) {
      await render(hbs`<KbWireframe::SideBar::NavShow />`)

      assert.dom("[data-test-kb-wireframe-side-bar-nav-show-id]").exists()
      assert.dom("[data-test-kb-wireframe-side-bar-nav-show-id=true]").exists()
      assert.dom(this.element).hasNoText()
    })

    test("it renders block text", async function (assert) {
      await render(hbs`
      <KbWireframe::SideBar::NavShow>
        template block text
      </KbWireframe::SideBar::NavShow>
    `)

      assert.dom("[data-test-kb-wireframe-side-bar-nav-show-id]").exists()
      assert.dom("[data-test-kb-wireframe-side-bar-nav-show-id=true]").exists()
      assert.dom(this.element).containsText("template block text")
    })

    test("usage", async function (assert) {
      await render(hbs`
      <KbWireframe::SideBar::NavShow />
    `)
      assert.dom("button").doesNotHaveClass("hidden")
      assert.dom("button").hasClass("absolute")
      await render(hbs`
      <KbWireframe::SideBar::NavShow
        @isHidden={{true}}
      />
    `)
      assert.dom("button").hasClass("hidden")
      assert.dom("button").doesNotHaveClass("absolute")
    })

    test("action", async function (assert) {
      assert.expect(1)
      let testAction = function () {
        assert.ok(true, "Action fires")
      }
      this.set("testAction", testAction)
      await render(hbs`
      <KbWireframe::SideBar::NavShow
        @clickAction={{testAction}}
        @isHidden={{true}}
      />
    `)
      await click("button")
    })
  }
)
