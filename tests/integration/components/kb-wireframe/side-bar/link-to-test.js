import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module(
  "Integration | Component | kb-wireframe/side-bar/link-to",
  function (hooks) {
    setupRenderingTest(hooks)

    test("renders", async function (assert) {
      await render(hbs`<KbWireframe::SideBar::LinkTo @route='' />`)

      assert.dom("[data-test-kb-wireframe-side-bar-link-to-id]").exists()
      assert.dom("[data-test-kb-wireframe-side-bar-link-to-id=true]").exists()
      assert.dom(this.element).hasNoText()
    })

    test("it renders block text", async function (assert) {
      await render(hbs`
      <KbWireframe::SideBar::LinkTo @route=''>
        template block text
      </KbWireframe::SideBar::LinkTo>
    `)

      assert.dom("[data-test-kb-wireframe-side-bar-link-to-id]").exists()
      assert.dom("[data-test-kb-wireframe-side-bar-link-to-id=true]").exists()
      assert.dom(this.element).containsText("template block text")
    })
  }
)
