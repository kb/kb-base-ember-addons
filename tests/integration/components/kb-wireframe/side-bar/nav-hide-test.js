import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module(
  "Integration | Component | kb-wireframe/side-bar/nav-hide",
  function (hooks) {
    setupRenderingTest(hooks)

    test("renders", async function (assert) {
      await render(hbs`<KbWireframe::SideBar::NavHide />`)

      assert.dom("[data-test-kb-wireframe-side-bar-nav-hide-id]").exists()
      assert.dom("[data-test-kb-wireframe-side-bar-nav-hide-id=true]").exists()
      assert.dom(this.element).hasNoText()
    })

    test("it renders block text", async function (assert) {
      await render(hbs`
      <KbWireframe::SideBar::NavHide>
        template block text
      </KbWireframe::SideBar::NavHide>
    `)

      assert.dom("[data-test-kb-wireframe-side-bar-nav-hide-id]").exists()
      assert.dom("[data-test-kb-wireframe-side-bar-nav-hide-id=true]").exists()
      assert.dom(this.element).containsText("template block text")
    })
  }
)
