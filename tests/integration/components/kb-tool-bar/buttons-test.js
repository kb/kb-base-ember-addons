import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Component | kb-tool-bar/buttons", function (hooks) {
  setupRenderingTest(hooks)

  test("renders", async function (assert) {
    await render(hbs`<KbToolBar::Buttons />`)

    assert.dom("[data-test-kb-tool-bar-buttons-id]").exists()
    assert.dom("[data-test-kb-tool-bar-buttons-id=true]").exists()
    assert.dom(this.element).hasNoText()
  })

  test("it renders block text", async function (assert) {
    await render(hbs`
      <KbToolBar::Buttons>
        template block text
      </KbToolBar::Buttons>
    `)

    assert.dom("[data-test-kb-tool-bar-buttons-id]").exists()
    assert.dom("[data-test-kb-tool-bar-buttons-id=true]").exists()
    assert.dom(this.element).containsText("template block text")
  })
})
