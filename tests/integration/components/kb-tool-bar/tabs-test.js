import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Component | kb-tool-bar/tabs", function (hooks) {
  setupRenderingTest(hooks)

  test("renders", async function (assert) {
    await render(hbs`<KbToolBar::Tabs />`)

    assert.dom("[data-test-kb-tool-bar-tabs-id]").exists()
    assert.dom("[data-test-kb-tool-bar-tabs-id=true]").exists()
    assert.dom(this.element).hasNoText()
  })

  test("it renders block text", async function (assert) {
    await render(hbs`
      <KbToolBar::Tabs>
        template block text
      </KbToolBar::Tabs>
    `)

    assert.dom("[data-test-kb-tool-bar-tabs-id]").exists()
    assert.dom("[data-test-kb-tool-bar-tabs-id=true]").exists()
    assert.dom(this.element).containsText("template block text")
  })
})
