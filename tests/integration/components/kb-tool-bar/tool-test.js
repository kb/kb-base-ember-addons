import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Component | kb-tool-bar/tool", function (hooks) {
  setupRenderingTest(hooks)

  test("renders", async function (assert) {
    await render(hbs`<KbToolBar::Tool />`)

    assert.dom("[data-test-kb-tool-bar-tool-id]").exists()
    assert.dom("[data-test-kb-tool-bar-tool-id=true]").exists()
    assert.dom(this.element).hasNoText()
  })

  test("it does not render block text", async function (assert) {
    await render(hbs`
      <KbToolBar::Tool >
        template block text
      </KbToolBar::Tool>
    `)

    assert.dom("[data-test-kb-tool-bar-tool-id]").exists()
    assert.dom("[data-test-kb-tool-bar-tool-id=true]").exists()
    assert.dom(this.element).containsText("template block text")
  })

  test("it renders route name into the datatestId", async function (assert) {
    await render(hbs`
      <KbToolBar::Tool
        @dataTestRole="route.name"
        @route="route.name"
        @anchorText="Route Name"
        @icon="routeIcon"
      />
    `)
    assert.dom(".fa-routeIcon").exists()
    assert
      .dom("[data-test-kb-tool-bar-tool-id='route.name']")
      .containsText("Route Name")
    assert.dom("[data-test-kb-tool-bar-tool-id='route.name'] i").hasClass("fa")
    assert
      .dom("[data-test-kb-tool-bar-tool-id='route.name'] i")
      .hasClass("fa-routeIcon")
    assert
      .dom("[data-test-kb-tool-bar-tool-id='route.name'] i")
      .hasClass("fa-fw")
  })
})
