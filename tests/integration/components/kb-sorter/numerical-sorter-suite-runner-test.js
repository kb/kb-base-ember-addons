/** KbSorter::Boolean Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"
import doesFiltering from "@kb/ember-addon/tests/suites/integration-component-does-filtering"

const FILTERKEY = "filtee"
const LIST = [
  {
    id: "true 1",
    filtee: true
  },
  {
    id: "false 2",
    filtee: false
  },
  {
    id: "true 3",
    filtee: true
  }
]

const component = {
  componentPath: "kb-sorter/numerical",
  componentName: "numerical",
  componentClass: "KbSorter::Numerical"
}

hasAttributes({
  ...component,
  scenario: "",
  hasAttributes: [
    {
      scenario: "attributes passed",
      givenAttrs: { title: "Click to boolean me" },
      givenDataTestId: "[data-test-kb-sorter-id]",
      expectAttributes: { title: "Click to boolean me" }
    }
  ]
})

doesFiltering({
  ...component,
  scenario: "sort start",
  funcHookName: "clickHook",
  funcAction: "sort",
  doesFiltering: [
    {
      scenario: "doesn't sort by default",
      givenAttrs: { "@key": FILTERKEY, "@model": LIST },
      resultCount: 3,
      results: { 1: "true 1", 2: "false 2", 3: "true 3" }
    },
    {
      scenario: "start sorting asc",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@preset": "asc"
      },
      resultCount: 3,
      results: { 1: "false 2", 2: "true 1", 3: "true 3" }
    },
    {
      scenario: "start sorting dsc",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@preset": "dsc"
      },
      resultCount: 3,
      results: { 1: "true 1", 2: "true 3", 3: "false 2" }
    }
  ]
})

doesFiltering({
  ...component,
  scenario: "clicking sort",
  funcHookName: "clickHook",
  funcAction: "sort",
  doesFiltering: [
    {
      scenario: "sorts asc (from none) on first click",
      givenAttrs: { "@key": FILTERKEY, "@model": LIST },
      givenDataTestId: "[data-test-kb-sorter-id]",
      action: "click",
      resultCount: 3,
      results: { 1: "false 2", 2: "true 1", 3: "true 3" }
    },
    {
      scenario: "sorts dsc (from asc) on first click",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@preset": "asc"
      },
      givenDataTestId: "[data-test-kb-sorter-id]",
      action: "click",
      resultCount: 3,
      results: { 1: "true 1", 2: "true 3", 3: "false 2" }
    },
    {
      scenario: "sorts asc (from dsc) on first click",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@preset": "dsc"
      },
      givenDataTestId: "[data-test-kb-sorter-id]",
      action: "click",
      resultCount: 3,
      results: { 1: "false 2", 2: "true 1", 3: "true 3" }
    }
  ]
})

willYield({
  ...component,
  scenario: "does not yield",
  negateExpectation: true
})
