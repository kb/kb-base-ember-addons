/** KbSorter::Boolean Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"
import doesFiltering from "@kb/ember-addon/tests/suites/integration-component-does-filtering"

const FILTERKEY = "filtee"
const LIST = [
  {
    id: "Banana",
    filtee: "Banana"
  },
  {
    id: "Carrot",
    filtee: "Carrot"
  },
  {
    id: "Apple",
    filtee: "Apple"
  }
]

const component = {
  componentPath: "kb-sorter/string",
  componentName: "string",
  componentClass: "KbSorter::String"
}

hasAttributes({
  ...component,
  scenario: "",
  hasAttributes: [
    {
      scenario: "attributes passed",
      givenAttrs: { title: "Click to boolean me" },
      givenDataTestId: "[data-test-kb-sorter-id]",
      expectAttributes: { title: "Click to boolean me" }
    }
  ]
})

doesFiltering({
  ...component,
  scenario: "sort start",
  funcHookName: "clickHook",
  funcAction: "sort",
  doesFiltering: [
    {
      scenario: "doesn't sort by default",
      givenAttrs: { "@key": FILTERKEY, "@model": LIST },
      resultCount: 3,
      results: { 1: "Banana", 2: "Carrot", 3: "Apple" }
    },
    {
      scenario: "start sorting asc",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@preset": "asc"
      },
      resultCount: 3,
      results: { 1: "Apple", 2: "Banana", 3: "Carrot" }
    },
    {
      scenario: "start sorting dsc",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@preset": "dsc"
      },
      resultCount: 3,
      results: { 1: "Carrot", 2: "Banana", 3: "Apple" }
    }
  ]
})

doesFiltering({
  ...component,
  scenario: "clicking sort",
  funcHookName: "clickHook",
  funcAction: "sort",
  doesFiltering: [
    {
      scenario: "sorts asc (from none) on first click",
      givenAttrs: { "@key": FILTERKEY, "@model": LIST },
      givenDataTestId: "[data-test-kb-sorter-id]",
      action: "click",
      resultCount: 3,
      results: { 1: "Apple", 2: "Banana", 3: "Carrot" }
    },
    {
      scenario: "sorts dsc (from asc) on first click",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@preset": "asc"
      },
      givenDataTestId: "[data-test-kb-sorter-id]",
      action: "click",
      resultCount: 3,
      results: { 1: "Carrot", 2: "Banana", 3: "Apple" }
    },
    {
      scenario: "sorts asc (from dsc) on first click",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@preset": "dsc"
      },
      givenDataTestId: "[data-test-kb-sorter-id]",
      action: "click",
      resultCount: 3,
      results: { 1: "Apple", 2: "Banana", 3: "Carrot" }
    }
  ]
})

willYield({
  ...component,
  scenario: "does not yield",
  negateExpectation: true
})
