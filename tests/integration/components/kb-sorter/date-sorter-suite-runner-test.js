/** KbSorter::Boolean Test Suite Runner */
import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"
import doesFiltering from "@kb/ember-addon/tests/suites/integration-component-does-filtering"

const FILTERKEY = "filtee"
const LIST = [
  {
    id: "2011-01-01",
    filtee: new Date("2011-01-01")
  },
  {
    id: "2009-01-01",
    filtee: new Date("2009-01-01")
  },
  {
    id: "2010-01-01",
    filtee: new Date("2010-01-01")
  }
]

const component = {
  componentPath: "kb-sorter/date",
  componentName: "date",
  componentClass: "KbSorter::Date"
}

hasAttributes({
  ...component,
  scenario: "",
  hasAttributes: [
    {
      scenario: "attributes passed",
      givenAttrs: { title: "Click to boolean me" },
      givenDataTestId: "[data-test-kb-sorter-id]",
      expectAttributes: { title: "Click to boolean me" }
    }
  ]
})

doesFiltering({
  ...component,
  scenario: "sort start",
  funcHookName: "clickHook",
  funcAction: "sort",
  doesFiltering: [
    {
      scenario: "doesn't sort by default",
      givenAttrs: { "@key": FILTERKEY, "@model": LIST },
      resultCount: 3,
      results: { 1: "2011-01-01", 2: "2009-01-01", 3: "2010-01-01" }
    },
    {
      scenario: "start sorting asc",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@preset": "asc"
      },
      resultCount: 3,
      results: { 1: "2009-01-01", 2: "2010-01-01", 3: "2011-01-01" }
    },
    {
      scenario: "start sorting dsc",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@preset": "dsc"
      },
      resultCount: 3,
      results: { 1: "2011-01-01", 2: "2010-01-01", 3: "2009-01-01" }
    }
  ]
})

doesFiltering({
  ...component,
  scenario: "clicking sort",
  funcHookName: "clickHook",
  funcAction: "sort",
  doesFiltering: [
    {
      scenario: "sorts asc (from none) on first click",
      givenAttrs: { "@key": FILTERKEY, "@model": LIST },
      givenDataTestId: "[data-test-kb-sorter-id]",
      action: "click",
      resultCount: 3,
      results: { 1: "2009-01-01", 2: "2010-01-01", 3: "2011-01-01" }
    },
    {
      scenario: "sorts dsc (from asc) on first click",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@preset": "asc"
      },
      givenDataTestId: "[data-test-kb-sorter-id]",
      action: "click",
      resultCount: 3,
      results: { 1: "2011-01-01", 2: "2010-01-01", 3: "2009-01-01" }
    },
    {
      scenario: "sorts asc (from dsc) on first click",
      givenAttrs: {
        "@key": FILTERKEY,
        "@model": LIST,
        "@preset": "dsc"
      },
      givenDataTestId: "[data-test-kb-sorter-id]",
      action: "click",
      resultCount: 3,
      results: { 1: "2009-01-01", 2: "2010-01-01", 3: "2011-01-01" }
    }
  ]
})

willYield({
  ...component,
  scenario: "does not yield",
  negateExpectation: true
})
