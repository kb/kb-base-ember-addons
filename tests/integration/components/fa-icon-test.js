import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Component | fa-icon", function (hooks) {
  setupRenderingTest(hooks)

  test("renders", async function (assert) {
    await render(hbs`<FaIcon />`)

    assert.equal(this.element.textContent.trim(), "")

    await render(hbs`
      <FaIcon>
        template block text
      </FaIcon>
    `)

    assert.dom(this.element).doesNotContainText("template block text")
  })

  test("iconized", async function (assert) {
    await render(hbs`
      <FaIcon @icon="wizard" />
    `)

    assert.dom("i").hasClass("fa")
    assert.dom("i").hasClass("fa-wizard")
    assert.dom("i").hasClass("fa-fw")
    assert.dom("i").hasClass("align-middle")
  })

  test("classified", async function (assert) {
    await render(hbs`
    <FaIcon @icon="wizard" @class="wizard-class" />
  `)

    assert.dom("i").hasClass("wizard-class")
  })

  test("attributed", async function (assert) {
    await render(hbs`
    <FaIcon @icon="wizard" @class="wizard-class" class="more-class"/>
  `)

    assert.dom("i").hasClass("wizard-class")
    assert.dom("i").hasClass("more-class")
  })
})
