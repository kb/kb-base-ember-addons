import EmberObject from "@ember/object"
import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render, click } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

const jobs = [
  {
    id: 1,
    slug: "baker",
    name: "Baker"
  },
  {
    id: 2,
    slug: "butcher",
    name: "Butcher"
  },
  {
    id: 3,
    slug: "candlestick",
    name: "Candlestick Maker"
  }
]

const tags = [1, 2, 3].map(x => `Tag${x}`)

module(
  "Integration | Component | kb-form kb inputs validate",
  function (hooks) {
    setupRenderingTest(hooks)

    test("example form KbInputBoolean validates", async function (assert) {
      assert.expect(3)

      this.set("jobs", jobs)
      this.set("tags", tags)
      this.set("model", EmberObject.create({ isActive: false }))
      this.set("submit", params => {
        assert.notOk(
          "Test for Validation will have failed if this form submits"
        )
      })

      await render(hbs`
      <KbForm @actionSubmitHook={{this.submit}} as |form|>
        <KbForm::Boolean
          @label="Active"
          @key="isActive"
          @value={{this.model.isActive}}
          @required={{true}}
          @validateHook={{form.actionValidate}}
          @showValidations={{form.showValidations}}
          @changeHook={{form.actionToUpdateModelByKey}}
        />
      </KbForm>
    `)

      await click("[data-test-kb-submit-button-id]")

      assert.dom("form").containsText("Submit")
      assert.dom("form").containsText("Active")
      assert.dom("form").containsText("This field is required")
    })

    test("example form KbInputDate validates", async function (assert) {
      assert.expect(3)

      this.set("jobs", jobs)
      this.set("tags", tags)
      this.set("model", EmberObject.create({ dob: "2001-02-03" }))
      this.set("submit", params => {
        assert.notOk(
          "Test for Validation will have failed if this form submits"
        )
      })

      await render(hbs`
      <KbForm @actionSubmitHook={{this.submit}} as |form|>
        <KbForm::Date
          @label="DoB"
          @key="dob"
          @value={{this.model.dob}}
          @required={{true}}
          @min="2010-11-12"
          @validateHook={{form.actionValidate}}
          @showValidations={{form.showValidations}}
          @changeHook={{form.actionToUpdateModelByKey}}
        />
      </KbForm>
    `)

      await click("[data-test-kb-submit-button-id]")

      assert.dom("form").containsText("Submit")
      assert.dom("form").containsText("DoB Required")
      assert.dom("form").containsText("2010-11-12 is the minimum allowed date")
    })

    test("example form KbInputEmail validates", async function (assert) {
      assert.expect(3)

      this.set("jobs", jobs)
      this.set("tags", tags)
      this.set("model", EmberObject.create({ email: "not@valid" }))
      this.set("submit", params => {
        assert.notOk(
          "Test for Validation will have failed if this form submits"
        )
      })

      await render(hbs`
      <KbForm @actionSubmitHook={{this.submit}} as |form|>
        <KbForm::Email
          @label="Email"
          @key="email"
          @value={{this.model.email}}
          @required={{true}}
          @min={{3}}
          @validateHook={{form.actionValidate}}
          @showValidations={{form.showValidations}}
          @changeHook={{form.actionToUpdateModelByKey}}
        />
      </KbForm>
    `)

      await click("[data-test-kb-submit-button-id]")

      assert.dom("form").containsText("Submit")
      assert.dom("form").containsText("Email Required")
      assert.dom("form").containsText("This email address is not valid")
    })

    test("example form KbInputNumber validates", async function (assert) {
      assert.expect(3)

      this.set("jobs", jobs)
      this.set("tags", tags)
      this.set("model", EmberObject.create({ score: "1" }))
      this.set("submit", params => {
        assert.notOk(
          "Test for Validation will have failed if this form submits"
        )
      })

      await render(hbs`
      <KbForm @actionSubmitHook={{this.submit}} as |form|>
        <KbForm::Number
          @label="Score"
          @key="score"
          @value={{this.model.score}}
          @required={{true}}
          @min={{3}}
          @validateHook={{form.actionValidate}}
          @showValidations={{form.showValidations}}
          @changeHook={{form.actionToUpdateModelByKey}}
        />
      </KbForm>
    `)

      await click("[data-test-kb-submit-button-id]")

      assert.dom("form").containsText("Submit")
      assert.dom("form").containsText("Score Required")
      assert.dom("form").containsText("Too small")
    })

    test("example form KbInputSelectMulti validates", async function (assert) {
      assert.expect(3)

      this.set("jobs", jobs)
      this.set("tags", tags)
      this.set("model", EmberObject.create({ jobs: [] }))
      this.set("submit", params => {
        assert.notOk(
          "Test for Validation will have failed if this form submits"
        )
      })

      await render(hbs`
      <KbForm @actionSubmitHook={{this.submit}} as |form|>
        <KbForm::SelectMulti
          @label="Jobs"
          @placeholder="Select Jobs"
          @key="jobs"
          @value={{this.model.jobs}}
          @options={{this.jobs}}
          @valueField="id"
          @searchField="name"
          @searchEnabled={{false}}
          @required={{true}}
          @min={{3}}
          @validateHook={{form.actionValidate}}
          @showValidations={{form.showValidations}}
          @changeHook={{form.actionToUpdateModelByKey}}
        />
      </KbForm>
    `)

      await click("[data-test-kb-submit-button-id]")

      assert.dom("form").containsText("Submit")
      assert.dom("form").containsText("Jobs Required")
      assert.dom("form").containsText("You need to select at least 3 items")
    })

    test("example form KbInputSelect validates", async function (assert) {
      assert.expect(3)

      this.set("jobs", jobs)
      this.set("tags", tags)
      this.set("model", EmberObject.create({ job: "" }))
      this.set("submit", params => {
        assert.notOk(
          "Test for Validation will have failed if this form submits"
        )
      })

      await render(hbs`
    <KbForm @actionSubmitHook={{this.submit}} as |form|>
      <KbForm::Select
        @label="Job"
        @placeholder="Select a Job"
        @key="job"
        @value={{this.model.job}}
        @options={{this.jobs}}
        @valueField="id"
        @searchField="name"
        @searchEnabled={{true}}
        @required={{true}}
        @validateHook={{form.actionValidate}}
        @showValidations={{form.showValidations}}
        @changeHook={{form.actionToUpdateModelByKey}}
      />
    </KbForm>
  `)

      await click("[data-test-kb-submit-button-id]")

      assert.dom("form").containsText("Submit")
      assert.dom("form").containsText("Job Required")
      assert.dom("form").containsText("This field is required")
    })

    test("example form KbInputString validates", async function (assert) {
      assert.expect(3)

      this.set("jobs", jobs)
      this.set("tags", tags)
      this.set("model", EmberObject.create({ userName: "abc" }))
      this.set("submit", params => {
        assert.notOk(
          "Test for Validation will have failed if this form submits"
        )
      })

      await render(hbs`
  <KbForm @actionSubmitHook={{this.submit}} as |form|>
    <KbForm::String
      @label="User Name"
      @key="userName"
      @value={{this.model.userName}}
      @required={{true}}
      @min={{4}}
      @validateHook={{form.actionValidate}}
      @showValidations={{form.showValidations}}
      @changeHook={{form.actionToUpdateModelByKey}}
    />
  </KbForm>
`)

      await click("[data-test-kb-submit-button-id]")

      assert.dom("form").containsText("Submit")
      assert.dom("form").containsText("User Name Required")
      assert.dom("form").containsText("4 is the minimum number of characters")
    })

    test("example form KbInputTaggit validates", async function (assert) {
      assert.expect(3)

      this.set("jobs", jobs)
      this.set("tags", tags)
      this.set("model", EmberObject.create({ taggits: [] }))
      this.set("submit", params => {
        assert.notOk(
          "Test for Validation will have failed if this form submits"
        )
      })

      await render(hbs`
    <KbForm @actionSubmitHook={{this.submit}} as |form|>
      <KbForm::Taggit
        @label="Taggit"
        @key="taggits"
        @value={{this.model.taggits}}
        @tags={{this.tags}}
        @required={{true}}
        @min={{2}}
        @validateHook={{form.actionValidate}}
        @showValidations={{form.showValidations}}
        @changeHook={{form.actionToUpdateModelByKey}}
      />
    </KbForm>
  `)

      await click("[data-test-kb-submit-button-id]")

      assert.dom("form").containsText("Submit")
      assert.dom("form").containsText("Taggit Required")
      assert.dom("form").containsText("You need to tick at least 2 tags")
    })

    test("example form KbInputUpload validates", async function (assert) {
      assert.expect(4)

      this.set("jobs", jobs)
      this.set("tags", tags)
      this.set("model", EmberObject.create({ files: [] }))
      this.set("submit", params => {
        assert.notOk(
          "Test for Validation will have failed if this form submits"
        )
      })

      await render(hbs`
      <KbForm @actionSubmitHook={{this.submit}} as |form|>
        <KbForm::Upload
          @label="Images"
          @key="fileList"
          @value={{this.model.files}}
          @fileType="image"
          @accept="application/jpg"
          @uploadUrl="http://127.0.0.1:5000/"
          @allowBackgroundUpload={{true}}
          @required={{true}}
          @min={{2}}
          @validateHook={{form.actionValidate}}
          @showValidations={{form.showValidations}}
          @changeHook={{form.actionToUpdateModelByKey}}
          @dataTestRole={{@dataTestRole}}
        />
      </KbForm>
    `)

      await click("[data-test-kb-submit-button-id]")

      assert.dom("form").containsText("Submit")
      assert.dom("form").containsText("Images")
      assert.dom("form").containsText("Drag and drop onto this area to upload")
      assert.dom("form").containsText("At least 2 images are required")
    })
  }
)
