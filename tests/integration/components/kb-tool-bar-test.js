import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module(
  "Integration | Component | kb-wireframe/main/tool-bar",
  function (hooks) {
    setupRenderingTest(hooks)

    test("renders", async function (assert) {
      await render(hbs`<KbToolBar />`)

      assert.dom("[data-test-kb-tool-bar-id]").exists()
      assert.dom("[data-test-kb-tool-bar-id=true]").exists()
      assert.dom(this.element).hasNoText()
    })

    test("it renders block text", async function (assert) {
      await render(hbs`
      <KbToolBar>
        template block text
      </KbToolBar>
    `)

      assert.dom("[data-test-kb-tool-bar-id]").exists()
      assert.dom("[data-test-kb-tool-bar-id=true]").exists()
      assert.dom(this.element).containsText("template block text")
    })
  }
)
