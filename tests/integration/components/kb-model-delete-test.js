import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Component | kb-model-delete", function (hooks) {
  setupRenderingTest(hooks)

  test("renders", async function (assert) {
    await render(hbs`<KbModelDelete />`)

    assert.dom("[data-test-kb-model-delete-id]").exists()
    assert.dom("[data-test-kb-model-delete-id=true]").exists()
    assert.dom(this.element).hasNoText()
  })

  test("it renders block text", async function (assert) {
    await render(hbs`
      <KbModelDelete>
        template block text
      </KbModelDelete>
    `)

    assert.dom("[data-test-kb-model-delete-id]").exists()
    assert.dom("[data-test-kb-model-delete-id=true]").exists()
    assert.dom(this.element).containsText("template block text")
  })
})
