import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Component | kb-tab-bar/tabs", function (hooks) {
  setupRenderingTest(hooks)

  test("it renders", async function (assert) {
    await render(hbs`<KbTabBar::Tabs />`)

    assert.equal(this.element.textContent.trim(), "")

    await render(hbs`
      <KbTabBar::Tabs>
        template block text
      </KbTabBar::Tabs>
    `)

    assert.equal(this.element.textContent.trim(), "template block text")
  })
})
