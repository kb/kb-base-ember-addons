import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render, click } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Component | kb-tab-bar/tabs/tab", function (hooks) {
  setupRenderingTest(hooks)

  test("it renders", async function (assert) {
    await render(hbs`<KbTabBar::Tabs::Tab />`)
    assert.equal(this.element.textContent.trim(), "")

    await render(hbs`<KbTabBar::Tabs::Tab @label="label1" />`)
    assert.dom(this.element).includesText("label1")

    await render(hbs`
      <KbTabBar::Tabs::Tab>
        template block text
      </KbTabBar::Tabs::Tab>
    `)
    assert.dom(this.element).includesText("template block text")

    await render(hbs`
      <KbTabBar::Tabs::Tab @key="key1" @label="label1">
        template block text
      </KbTabBar::Tabs::Tab>
    `)
    assert.dom(this.element).includesText("template block text")
    assert.dom(this.element).includesText("label1")
  })

  test("it preselects", async function (assert) {
    await render(hbs`
      <KbTabBar @startTab="key2" @dataTestRole="tabBar" as |tabBar|>
        <KbTabBar::Tabs::Tab @tabBar={{tabBar}} @key="key1" />
        <KbTabBar::Tabs::Tab @tabBar={{tabBar}} @key="key2" />
        <KbTabBar::Tabs::Tab @tabBar={{tabBar}} @key="key3" />
      </KbTabBar>
    `)

    assert
      .dom("[data-test-kb-tab-bar-tabs-tab-id='tabBar-key1']")
      .doesNotHaveClass("active")
    assert
      .dom("[data-test-kb-tab-bar-tabs-tab-id='tabBar-key2']")
      .hasClass("active")
    assert
      .dom("[data-test-kb-tab-bar-tabs-tab-id='tabBar-key3']")
      .doesNotHaveClass("active")
  })

  test("it selects", async function (assert) {
    await render(hbs`
          <KbTabBar @startTab="key2" @dataTestRole="tabBar" as |tabBar|>
            <KbTabBar::Tabs::Tab @tabBar={{tabBar}} @key="key1" />
            <KbTabBar::Tabs::Tab @tabBar={{tabBar}} @key="key2" />
            <KbTabBar::Tabs::Tab @tabBar={{tabBar}} @key="key3" />
          </KbTabBar>
        `)

    await click("[data-test-kb-tab-bar-tabs-tab-id='tabBar-key1']")

    assert
      .dom("[data-test-kb-tab-bar-tabs-tab-id='tabBar-key1']")
      .hasClass("active")
    assert
      .dom("[data-test-kb-tab-bar-tabs-tab-id='tabBar-key2']")
      .doesNotHaveClass("active")
    assert
      .dom("[data-test-kb-tab-bar-tabs-tab-id='tabBar-key3']")
      .doesNotHaveClass("active")

    await click("[data-test-kb-tab-bar-tabs-tab-id='tabBar-key3']")

    assert
      .dom("[data-test-kb-tab-bar-tabs-tab-id='tabBar-key1']")
      .doesNotHaveClass("active")
    assert
      .dom("[data-test-kb-tab-bar-tabs-tab-id='tabBar-key2']")
      .doesNotHaveClass("active")
    assert
      .dom("[data-test-kb-tab-bar-tabs-tab-id='tabBar-key3']")
      .hasClass("active")
  })
})
