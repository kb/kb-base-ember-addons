import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Component | kb-tab-bar/panels/panel", function (hooks) {
  setupRenderingTest(hooks)

  test("it renders", async function (assert) {
    await render(hbs`<KbTabBar::Panels::Panel />`)

    assert.equal(this.element.textContent.trim(), "")

    await render(hbs`
      <KbTabBar::Panels::Panel>
        template block text
      </KbTabBar::Panels::Panel>
    `)

    assert.equal(this.element.textContent.trim(), "template block text")
  })
})
