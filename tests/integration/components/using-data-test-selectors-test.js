import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module(
  "Integration | Component | using-data-test-kb-selectors",
  function (hooks) {
    setupRenderingTest(hooks)

    test("selector would be present by default", async function (assert) {
      await render(hbs`
      <KbForm::Label />
    `)
      assert.dom("label").hasAttribute("data-test-kb-form-label-id")
      assert.dom("label").hasAttribute("data-test-kb-form-label-id", "true")
      assert.dom("[data-test-kb-form-label-id]").exists()
      assert.dom("[data-test-kb-form-label-id=true]").exists()
    })

    test("empty string, named selector defaults to true value", async function (assert) {
      await render(hbs`
      <KbForm::Label @dataTestRole="" @key="" />
    `)
      assert.dom("label").hasAttribute("data-test-kb-form-label-id")
      assert.dom("label").hasAttribute("data-test-kb-form-label-id", "true")
      assert.dom("[data-test-kb-form-label-id]").exists()
      assert.dom("[data-test-kb-form-label-id=true]").exists()
    })

    test("empty string, named selector adds extra selectors", async function (assert) {
      await render(hbs`
      <KbForm::Label @dataTestRole="idOf" @key="ofKey" />
    `)
      assert.dom("label").hasAttribute("data-test-kb-form-label-id")
      assert
        .dom("label")
        .hasAttribute("data-test-kb-form-label-id", "idOf-ofKey")
      assert.dom("[data-test-kb-form-label-id]").exists()
      assert.dom("[data-test-kb-form-label-id='idOf-ofKey']").exists()
    })

    test("takes a route selector", async function (assert) {
      await render(hbs`
      <KbForm::Label @dataTestRole="delectable" />
    `)
      assert.dom("label").hasAttribute("data-test-kb-form-label-id")
      assert
        .dom("label")
        .hasAttribute("data-test-kb-form-label-id", "delectable")
      assert.dom("[data-test-kb-form-label-id]").exists()
      assert.dom("[data-test-kb-form-label-id='delectable']").exists()
    })
  }
)
