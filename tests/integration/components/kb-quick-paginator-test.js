import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { click, render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Component | kb-quick-paginator", function (hooks) {
  setupRenderingTest(hooks)

  test("it renders paginated", async function (assert) {
    this.set("list", ["pap", "pep", "pip", "pop", "pup"])
    await render(hbs`
      <KbQuickPaginator
        @paginateBy={{2}}
        @dataTestRole="pappepperpippopperpuppy"
        @items={{this.list}} as |listItems|
      >
        {{#each listItems as |item|}}
          <p class="list-item">{{item}}</p>
        {{/each}}
      </KbQuickPaginator>
    `)
    assert.dom(".list-item").exists({ count: 2 })
    assert
      .dom(
        "[data-test-kb-quick-paginator-forward-id='pappepperpippopperpuppy']"
      )
      .exists()
    assert
      .dom(
        "[data-test-kb-quick-paginator-previous-id='pappepperpippopperpuppy']"
      )
      .doesNotExist()
    assert.dom(this.element).containsText("pap")
    assert.dom(this.element).containsText("pep")
    assert.dom(this.element).doesNotIncludeText("pip")
    assert.dom(this.element).doesNotIncludeText("pop")
    assert.dom(this.element).doesNotIncludeText("pup")
  })

  test("paginates on click", async function (assert) {
    this.set("list", ["pap", "pep", "pip", "pop", "pup"])
    await render(hbs`
      <KbQuickPaginator
        @dataTestRole="pappepperpippopperpuppy"
        @paginateBy={{2}}
        @items={{this.list}} as |listItems|
      >
        {{#each listItems as |item|}}
          <p class="list-item">{{item}}</p>
        {{/each}}
      </KbQuickPaginator>
    `)
    await click(
      "[data-test-kb-quick-paginator-forward-id='pappepperpippopperpuppy']"
    )
    assert.dom(".list-item").exists({ count: 2 })
    assert
      .dom(
        "[data-test-kb-quick-paginator-forward-id='pappepperpippopperpuppy']"
      )
      .exists()
    assert
      .dom(
        "[data-test-kb-quick-paginator-previous-id='pappepperpippopperpuppy']"
      )
      .exists()
    assert.dom(this.element).doesNotIncludeText("pap")
    assert.dom(this.element).doesNotIncludeText("pep")
    assert.dom(this.element).containsText("pip")
    assert.dom(this.element).containsText("pop")
    assert.dom(this.element).doesNotIncludeText("pup")
    await click(
      "[data-test-kb-quick-paginator-forward-id='pappepperpippopperpuppy']"
    )
    assert.dom(".list-item").exists({ count: 1 })
    assert
      .dom(
        "[data-test-kb-quick-paginator-forward-id='pappepperpippopperpuppy']"
      )
      .doesNotExist()
    assert
      .dom(
        "[data-test-kb-quick-paginator-previous-id='pappepperpippopperpuppy']"
      )
      .exists()
    assert.dom(this.element).doesNotIncludeText("pap")
    assert.dom(this.element).doesNotIncludeText("pep")
    assert.dom(this.element).doesNotIncludeText("pip")
    assert.dom(this.element).doesNotIncludeText("pop")
    assert.dom(this.element).containsText("pup")
    await click(
      "[data-test-kb-quick-paginator-previous-id='pappepperpippopperpuppy']"
    )
    assert.dom(".list-item").exists({ count: 2 })
    assert
      .dom(
        "[data-test-kb-quick-paginator-previous-id='pappepperpippopperpuppy']"
      )
      .exists()
    assert.dom(this.element).doesNotIncludeText("pap")
    assert.dom(this.element).doesNotIncludeText("pep")
    assert.dom(this.element).containsText("pip")
    assert.dom(this.element).containsText("pop")
    assert.dom(this.element).doesNotIncludeText("pup")
  })
})
