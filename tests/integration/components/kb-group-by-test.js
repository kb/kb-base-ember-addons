import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Component | kb-group-by", function (hooks) {
  setupRenderingTest(hooks)

  test("renders", async function (assert) {
    await render(hbs`<KbGroupBy />`)

    assert.dom(this.element).hasNoText()
  })

  test("it renders block text", async function (assert) {
    await render(hbs`
      <KbGroupBy>
        template block text
      </KbGroupBy>
    `)

    assert.dom(this.element).containsText("template block text")
  })

  test("it renders groups", async function (assert) {
    this.set("testModel", [
      { name: "ant", group: "a-group", isGroupParent: true },
      { name: "ape", group: "a-group", isGroupParent: false },
      { name: "auk", group: "a-group", isGroupParent: false },
      { name: "bat", group: "b-group", isGroupParent: true },
      { name: "bee", group: "b-group", isGroupParent: false },
      { name: "bug", group: "b-group", isGroupParent: false },
      { name: "cat", group: "c-group", isGroupParent: true },
      { name: "cod", group: "c-group", isGroupParent: false },
      { name: "cow", group: "c-group", isGroupParent: false }
    ])
    await render(hbs`
      <KbGroupBy
        @groupBy="group"
        @items={{this.testModel}} as |animals|
      >
        {{#each animals as |alphaGroup|}}
          <div class="animal-group-{{alphaGroup.value}}">
            <h3 class="animal-group">{{alphaGroup.value}}</h3>
            {{#each alphaGroup.items as |animal|}}
              <p class="animal-listed">{{animal.name}}</p>
            {{/each}}
          </div>
        {{/each}}
      </KbGroupBy>
    `)
    assert.dom(".animal-group").exists({ count: 3 })
    assert.dom(".animal-listed").exists({ count: 9 })
    assert.dom(".animal-group-a-group").exists({ count: 1 })
    assert.dom(".animal-group-a-group").containsText("ant")
    assert.dom(".animal-group-a-group").containsText("ape")
    assert.dom(".animal-group-a-group").containsText("auk")
    assert.dom(".animal-group-a-group").doesNotIncludeText("bat")
    assert.dom(".animal-group-a-group").doesNotIncludeText("cat")
  })
})
