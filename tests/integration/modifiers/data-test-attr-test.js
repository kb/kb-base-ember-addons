import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import hbs from "htmlbars-inline-precompile"

module("Integration | Modifier | data-test-attr", function (hooks) {
  setupRenderingTest(hooks)

  test("it renders", async function (assert) {
    await render(hbs`
      <div
        {{data-test-attr}}
      ></div>`)

    assert.ok(true)
  })

  test("it has the data-test selector", async function (assert) {
    await render(hbs`
      <div
        {{data-test-attr}}
      ></div>`)

    assert.dom("[data-test-kb-id]").exists()
    assert.dom("[data-test-kb-id=true]").exists()
  })

  test("handles positional argument", async function (assert) {
    await render(hbs`
      <div
        {{data-test-attr "happy-place" }}
      ></div>
    `)

    assert.dom("[data-test-kb-happy-place-id]").exists()
  })

  test("handles mulitple positional arguments", async function (assert) {
    await render(hbs`
      <div
        {{data-test-attr "happy-place" "my" "Very" "Tidy" "Place" }}
      ></div>
    `)

    assert.dom("[data-test-kb-happy-place-id]").exists()
    assert.dom("[data-test-kb-happy-place-id='my-Very-Tidy-Place']").exists()
  })

  test("handles missing mulitple positional arguments", async function (assert) {
    await render(hbs`
      <div
        {{data-test-attr "happy-place" "" "Tidy-Place" }}
      ></div>
    `)

    assert.dom("[data-test-kb-happy-place-id]").exists()
    assert.dom("[data-test-kb-happy-place-id='Tidy-Place']").exists()
  })

  test("handles missing positional attributes", async function (assert) {
    await render(hbs`
      <div
        {{data-test-attr "happy-place" @noSuchAttr "Tidy-Place" }}
      ></div>
    `)

    assert.dom("[data-test-kb-happy-place-id]").exists()
    assert.dom("[data-test-kb-happy-place-id='Tidy-Place']").exists()
  })

  test("handles named arguments", async function (assert) {
    await render(hbs`
      <div
        {{data-test-attr placeType="TidyPlace" placeOwner="Me"}}
      ></div>`)

    assert.dom("[data-test-kb-id]").exists()
    assert.dom("[data-test-kb-id=true]").exists()
    assert.dom("[data-test-kb-placeType-id='TidyPlace']").exists()
    assert.dom("[data-test-kb-placeOwner-id='Me']").exists()
  })

  test("handles arguments combo", async function (assert) {
    await render(hbs`
      <div
        {{data-test-attr "happy-place" placeType="TidyPlace" placeOwner="Me"}}
      ></div>`)

    assert.dom("[data-test-kb-happy-place-id]").exists()
    assert.dom("[data-test-kb-happy-place-id=true]").exists()
    assert.dom("[data-test-kb-happy-place-placeType-id='TidyPlace']").exists()
    assert.dom("[data-test-kb-happy-place-placeOwner-id='Me']").exists()
  })
})
