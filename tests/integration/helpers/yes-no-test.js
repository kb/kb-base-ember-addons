import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Helper | yes-no", function (hooks) {
  setupRenderingTest(hooks)

  test("it renders Yes", async function (assert) {
    this.set("inputValue", true)

    await render(hbs`{{yes-no inputValue}}`)

    assert.equal(this.element.textContent.trim(), "Yes")
  })

  test("it renders No", async function (assert) {
    this.set("inputValue", false)

    await render(hbs`{{yes-no inputValue}}`)

    assert.equal(this.element.textContent.trim(), "No")
  })
})
