import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Helper | eyes-only", function (hooks) {
  setupRenderingTest(hooks)

  test("eyes-only if no authentication", async function (assert) {
    this.set("authentication", false)
    this.set("session", {})

    await render(hbs`
      {{eyes-only authentication session}}
    `)

    assert.equal(this.element.textContent.trim(), "true")
  })

  test("eyes-only if no authentication", async function (assert) {
    this.set("authentication", false)
    this.set("session", {})

    await render(hbs`
      {{#if (eyes-only authentication session)}}eyes-only{{/if}}
    `)

    assert.equal(this.element.textContent.trim(), "eyes-only")
  })
})
