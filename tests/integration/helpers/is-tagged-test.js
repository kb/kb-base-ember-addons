import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import hbs from "htmlbars-inline-precompile"

module("Integration | Helper | is-tagged", function (hooks) {
  setupRenderingTest(hooks)

  test("it renders", async function (assert) {
    this.set("tags", ["tag1", "tag2", "tag3"])

    await render(hbs`{{is-tagged tags "tag1"}}`)

    assert.equal(this.element.textContent.trim(), "true")

    await render(hbs`{{is-tagged tags "tag4"}}`)

    assert.equal(this.element.textContent.trim(), "false")
  })

  test("is not tagged as hider with if", async function (assert) {
    this.set("tags", ["tag1", "tag2", "tag3"])

    await render(hbs`
      {{#if (is-tagged tags "tag1")}}Hello{{/if}}
    `)

    assert.dom(this.element).hasText("Hello")

    await render(hbs`
      {{#if (is-tagged tags "tag4")}}Hello{{/if}}
    `)

    assert.dom(this.element).hasText("")
  })
})
