import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import hbs from "htmlbars-inline-precompile"

module("Integration | Helper | is-equal", function (hooks) {
  setupRenderingTest(hooks)

  test("renders true when equal", async function (assert) {
    this.set("no1", "42")
    this.set("no2", "42")

    await render(hbs`{{is-equal no1 no2}}`)

    assert.dom(this.element).hasText("true")
  })

  test("renders false when not equal", async function (assert) {
    this.set("no1", "42")
    this.set("no2", "43")

    await render(hbs`{{is-equal no1 no2}}`)

    assert.dom(this.element).hasText("false")
  })

  test("equal with strings used in an attribute", async function (assert) {
    this.set("no1", "FR")
    this.set("no2", "FR")

    await render(hbs`
      <option value={{no1}} selected={{is-equal no1 no2}}>France</option>
    `)

    assert.ok(this.element.querySelector("option").selected)
  })

  test("not equal with strings used in an attribute", async function (assert) {
    this.set("no1", "FR")
    this.set("no2", "GB")

    await render(hbs`
      <option value={{no1}} selected={{is-equal no1 no2}}>France</option>
    `)

    assert.notOk(this.element.querySelector("option").selected)
  })

  test("is not equal as hider with if", async function (assert) {
    this.set("no1", "FR")
    this.set("no2", "GB")

    await render(hbs`
      {{#if (is-equal no1 no2)}}Hello{{/if}}
    `)

    assert.dom(this.element).hasText("")
  })

  test("is equal as visible with if", async function (assert) {
    this.set("no1", "FR")
    this.set("no2", "FR")

    await render(hbs`
      {{#if (is-equal no1 no2)}}Hello{{/if}}
    `)

    assert.dom(this.element).hasText("Hello")
  })

  test("works just dandy in a select dropdown", async function (assert) {
    var testChoices = [
      {
        val: "GB",
        txt: "United Kingdom"
      },
      {
        val: "FR",
        txt: "France"
      },
      {
        val: "US",
        txt: "United States of America"
      }
    ]
    this.set("model", testChoices[1])
    this.set("choices", testChoices)

    await render(hbs`
      <select id="txtSelector">
        <option value="" disabled="true" selected={{is-equal model.val ""}}>
          Select
        </option>
        {{#each choices as |c|}}
          <option value={{c.val}} selected={{is-equal model.val c.val}}>
            {{c.txt}}
          </option>
        {{/each}}
      </select>
    `)

    assert.equal(
      this.element.querySelector("#txtSelector").value,
      "FR",
      "model.txt is the selected option"
    )

    assert.equal(
      this.element.querySelector("#txtSelector").selectedIndex,
      2,
      "model.txt really is the selected option"
    )
  })

  test("has unselected dropdown for an empty model ", async function (assert) {
    var testModel = {
      val: "",
      txt: ""
    }

    var testChoices = [
      {
        val: "GB",
        txt: "United Kingdom"
      },
      {
        val: "FR",
        txt: "France"
      },
      {
        val: "US",
        txt: "United States of America"
      }
    ]
    this.set("model", testModel)
    this.set("choices", testChoices)

    await render(hbs`
      <select id="txtSelector">
        <option value="" disabled="true" selected={{is-equal model.val ""}}>
          Please Select
        </option>
        {{#each choices as |c|}}
          <option value={{c.val}} selected={{is-equal model.val c.val}}>
            {{c.txt}}
          </option>
        {{/each}}
      </select>
    `)

    assert.equal(
      this.element.querySelector("#txtSelector").value,
      "",
      '"Please Select" is the selected option'
    )

    assert.equal(
      this.element.querySelector("#txtSelector").selectedIndex,
      0,
      '"Please Select" really is the selected option'
    )
  })
})
