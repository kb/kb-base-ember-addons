import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Helper | get-select-option", function (hooks) {
  setupRenderingTest(hooks)

  test("it renders raw", async function (assert) {
    this.set("inputValue", "1234")

    await render(hbs`{{get-select-option inputValue}}`)

    assert.equal(this.element.textContent.trim(), "1234")

    await render(hbs`{{get-select-option inputValue "key"}}`)

    assert.equal(this.element.textContent.trim(), "1234")
  })

  test("it renders object by key", async function (assert) {
    this.set("inputValue", {
      id: 54,
      code: "code54",
      name: "name54"
    })

    await render(hbs`{{get-select-option inputValue "id"}}`)

    assert.equal(this.element.textContent.trim(), "54")

    await render(hbs`{{get-select-option inputValue "code"}}`)

    assert.equal(this.element.textContent.trim(), "code54")

    await render(hbs`{{get-select-option inputValue "name"}}`)

    assert.equal(this.element.textContent.trim(), "name54")
  })
})
