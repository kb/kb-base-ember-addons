import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Helper | kb-model-route-match", function (hooks) {
  setupRenderingTest(hooks)

  test("renders true when on model route", async function (assert) {
    await render(hbs`
      {{kb-model-route-match "wizard.spells" "wizard.amulets.add"}}
    `)

    assert.dom(this.element).hasText("true")
  })
})
