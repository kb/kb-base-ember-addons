import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { render } from "@ember/test-helpers"
import { hbs } from "ember-cli-htmlbars"

module("Integration | Helper | not", function (hooks) {
  setupRenderingTest(hooks)

  test("it's not true", async function (assert) {
    this.set("inputValue", true)
    await render(hbs`{{not inputValue}}`)
    assert.equal(this.element.textContent.trim(), "false")
  })

  test("it's not false", async function (assert) {
    this.set("inputValue", false)
    await render(hbs`{{not inputValue}}`)
    assert.equal(this.element.textContent.trim(), "true")
  })
})
