import { eyesOnly } from "dummy/helpers/eyes-only"
import { module, test } from "qunit"

module("Unit | Helper | eyes-only", function () {
  test("eyes-only no authentication", function (assert) {
    let result = eyesOnly([false, false, false])
    assert.ok(result)
  })
  test("eyes-only authentication when not authenticated", function (assert) {
    let result = eyesOnly([true, false, false])
    assert.notOk(result)
  })
  test("eyes-only authentication when authenticated", function (assert) {
    let result = eyesOnly([true, true, false])
    assert.notOk(result)
  })
  test("eyes-only appadmin when not authenticated", function (assert) {
    let result = eyesOnly(["appadmin", false, false])
    assert.notOk(result)
  })
  test("eyes-only appadmin when authenticated but not appadmin", function (assert) {
    let result = eyesOnly(["appadmin", true, false])
    assert.notOk(result)
  })
  test("eyes-only appadmin when appadmin but not authenticated", function (assert) {
    let result = eyesOnly(["appadmin", false, "appadmin"])
    assert.notOk(result)
  })
  test("eyes-only appadmin when appadmin and authenticated", function (assert) {
    let result = eyesOnly(["appadmin", true, "appadmin"])
    assert.ok(result)
  })
  test("eyes-only appadmin when manager but not authenticated", function (assert) {
    let result = eyesOnly(["appadmin", false, "manager"])
    assert.notOk(result)
  })
  test("eyes-only appadmin when manager and authenticated", function (assert) {
    let result = eyesOnly(["appadmin", true, "manager"])
    assert.notOk(result)
  })
  test("eyes-only appadmin when listed and authenticated", function (assert) {
    let result = eyesOnly([["appadmin", "manager"], true, "appadmin"])
    assert.ok(result)
  })
  test("eyes-only manager when listed and authenticated", function (assert) {
    let result = eyesOnly([["appadmin", "manager"], true, "manager"])
    assert.ok(result)
  })
  test("eyes-only manager when not listed and authenticated", function (assert) {
    let result = eyesOnly([["superuser"], true, "manager"])
    assert.notOk(result)
  })
  test("eyes-only a whole tonne of stuff", function (assert) {
    assert.notOk(eyesOnly(["manager", false, "manager"]))
    assert.notOk(eyesOnly([["manager"], false, "manager"]))
    assert.notOk(eyesOnly([["manager", "appadmin"], false, "manager"]))
    assert.notOk(eyesOnly([[false, true], false, "manager"]))
    assert.notOk(eyesOnly([[true, false], false, "manager"]))
    assert.notOk(eyesOnly([["authenticated", "appadmin"], false, "manager"]))
    assert.ok(eyesOnly(["manager", true, "manager"]), "manager==manager")
    assert.ok(eyesOnly([["manager"], true, "manager"]), "[manager]==manager")
    assert.ok(
      eyesOnly([["manager", "appadmin"], true, "manager"]),
      "[manager,appadmin]==manager"
    )
    assert.notOk(
      eyesOnly([[false, true], true, "manager"]),
      "[false,true]==manager"
    )
    assert.ok(
      eyesOnly([["authenticated"], true, "manager"]),
      "authenticated==manager"
    )
    assert.ok(eyesOnly(["authenticated", true, false]), "authenticated==false")
    assert.notOk(
      eyesOnly(["authenticated", false, false]),
      "authenticated==false"
    )
    assert.ok(
      eyesOnly(["authenticated", true, "manager"]),
      "authenticated==manager"
    )
  })
})
