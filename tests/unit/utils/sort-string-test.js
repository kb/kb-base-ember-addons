import sortString from "dummy/utils/sort-string"
import { module, test } from "qunit"

module("Unit | Utility | sort-string", function () {
  test("sortString", function (assert) {
    assert.deepEqual(["a", "c", "B"].sort(sortString), ["a", "B", "c"])
    assert.deepEqual(["B", "a"].sort(sortString), ["a", "B"])
    assert.deepEqual(["a"].sort(sortString), ["a"])
    assert.deepEqual([].sort(sortString), [])
  })
  test("sortString desc", function (assert) {
    assert.deepEqual(
      ["a", "c", "B"].sort((a, b) => sortString(a, b, { desc: true })),
      ["c", "B", "a"]
    )
    assert.deepEqual(
      ["B", "a"].sort((a, b) => sortString(a, b, { desc: true })),
      ["B", "a"]
    )
    assert.deepEqual(
      ["a"].sort((a, b) => sortString(a, b, { desc: true })),
      ["a"]
    )
    assert.deepEqual(
      [].sort((a, b) => sortString(a, b, { desc: true })),
      []
    )
  })
  test("sortString object values", function (assert) {
    assert.deepEqual(
      [{ x: "a" }, { x: "c" }, { x: "B" }].sort((a, b) =>
        sortString(a, b, { key: "x" })
      ),
      [{ x: "a" }, { x: "B" }, { x: "c" }]
    )
    assert.deepEqual(
      [{ x: "B" }, { x: "a" }].sort((a, b) => sortString(a, b, { key: "x" })),
      [{ x: "a" }, { x: "B" }]
    )
    assert.deepEqual(
      [{ x: "a" }].sort((a, b) => sortString(a, b, { key: "x" })),
      [{ x: "a" }]
    )
    assert.deepEqual(
      [].sort((a, b) => sortString(a, b, { key: "x" })),
      []
    )
  })
  test("sortString object values desc", function (assert) {
    assert.deepEqual(
      [{ x: "a" }, { x: "c" }, { x: "B" }].sort((a, b) =>
        sortString(a, b, { key: "x", desc: true })
      ),
      [{ x: "c" }, { x: "B" }, { x: "a" }]
    )
    assert.deepEqual(
      [{ x: "a" }, { x: "B" }].sort((a, b) =>
        sortString(a, b, { key: "x", desc: true })
      ),
      [{ x: "B" }, { x: "a" }]
    )
    assert.deepEqual(
      [{ x: "a" }].sort((a, b) => sortString(a, b, { key: "x", desc: true })),
      [{ x: "a" }]
    )
    assert.deepEqual(
      [].sort((a, b) => sortString(a, b, { key: "x", desc: true })),
      []
    )
  })
})
