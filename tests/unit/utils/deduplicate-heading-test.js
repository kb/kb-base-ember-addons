import deduplicateHeading from "dummy/utils/deduplicate-heading"
import { module, test } from "qunit"

module("Unit | Utility | deduplicate-heading", function () {
  test("it works", function (assert) {
    assert.equal("Workflow", deduplicateHeading("Workflows Workflow"))
    assert.equal("Task Complete", deduplicateHeading("Tasks Task Complete"))
  })
})
