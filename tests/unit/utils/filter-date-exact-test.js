import filterDateExact from "dummy/utils/filter-date-exact"
import { module, test } from "qunit"

module("Unit | Utility | filter-date-exact", function () {
  test("filterDateExact", function (assert) {
    assert.ok(filterDateExact("2001-01-01", "2001-01-01"))
    assert.ok(filterDateExact("2000-01-01", "2000-01-01"))
    assert.notOk(filterDateExact("2001-01-01", "2000-01-01"))
    assert.notOk(filterDateExact("2000-01-01", "2001-01-01"))
  })
  test("filterDateExact null values", function (assert) {
    assert.ok(filterDateExact("", ""))
    assert.notOk(filterDateExact("", "2000-01-01"))
    assert.notOk(filterDateExact("2000-01-01", ""))
  })
})
