import kbDateSorter from "dummy/utils/kb-date-sorter"
import { module, test } from "qunit"

module("Unit | Utility | kb-date-sorter", function () {
  test("it works", function (assert) {
    let dates = ["2010-01-01", "2030-01-01", "2020-01-01"]
    assert.ok(dates.sort(kbDateSorter))
  })

  test("it sorts", function (assert) {
    let dates = ["2010-01-01", "2030-01-01", "2020-01-01"]
    assert.deepEqual(dates.sort(kbDateSorter), [
      "2010-01-01",
      "2020-01-01",
      "2030-01-01"
    ])
  })

  test("it ignores non dates", function (assert) {
    let dates = ["2010-01-01", "2030-01-01", "2020-01-01", null, "dummy"]
    assert.deepEqual(dates.sort(kbDateSorter), [
      "2010-01-01",
      "2020-01-01",
      "2030-01-01",
      null,
      "dummy"
    ])
  })

  test("it sorts out null values", function (assert) {
    let dates = [null, "2020-01-01", null, "2030-01-01", null, "2010-01-01"]
    assert.deepEqual(dates.sort(kbDateSorter), [
      "2010-01-01",
      "2020-01-01",
      "2030-01-01",
      null,
      null,
      null
    ])
  })

  test("it sorts out non dates", function (assert) {
    let dates = [
      "dummy1",
      "2020-01-01",
      null,
      "2030-01-01",
      "dummy2",
      "2010-01-01"
    ]
    assert.deepEqual(dates.sort(kbDateSorter), [
      "2010-01-01",
      "2020-01-01",
      "2030-01-01",
      "dummy1",
      null,
      "dummy2"
    ])
  })

  test("it handles mix of real and string dates", function (assert) {
    let dates = [
      new Date(2010, 1, 1),
      "dummy",
      new Date(2030, 1, 1),
      "2020-01-01",
      null
    ]
    assert.deepEqual(dates.sort(kbDateSorter), [
      new Date(2010, 1, 1),
      "2020-01-01",
      new Date(2030, 1, 1),
      "dummy",
      null
    ])
  })
})
