import filterNumberIgnoredwhenzero from "dummy/utils/filter-number-ignoredwhenzero"
import { module, test } from "qunit"

module("Unit | Utility | filter-number-ignoredwhenzero", function () {
  test("filterNumberIgnoredwhenzero", function (assert) {
    assert.ok(filterNumberIgnoredwhenzero(1, 1))
    assert.ok(filterNumberIgnoredwhenzero(0, 0))
    assert.ok(filterNumberIgnoredwhenzero(1, 0))
    assert.notOk(filterNumberIgnoredwhenzero(0, 1))
  })
  test("filterNumberIgnoredwhenzero val or filter is string", function (assert) {
    assert.ok(filterNumberIgnoredwhenzero("1", 1))
    assert.ok(filterNumberIgnoredwhenzero(1, "1"))
    assert.ok(filterNumberIgnoredwhenzero("0", 0))
    assert.ok(filterNumberIgnoredwhenzero(0, "0"))
    assert.ok(filterNumberIgnoredwhenzero("1", "1"))
    assert.ok(filterNumberIgnoredwhenzero("0", "0"))
    assert.ok(filterNumberIgnoredwhenzero("1", "0"))
    assert.notOk(filterNumberIgnoredwhenzero("0", "1"))
  })

  test("filterNumberIgnoredwhenzero dates", function (assert) {
    assert.ok(
      filterNumberIgnoredwhenzero(
        new Date("2009-08-07").valueOf(),
        new Date("2009-08-07").valueOf()
      )
    )
    assert.ok(
      filterNumberIgnoredwhenzero(
        new Date("").valueOf(),
        new Date("").valueOf()
      )
    )
    assert.ok(
      filterNumberIgnoredwhenzero(
        new Date("2009-08-07").valueOf(),
        new Date("").valueOf()
      )
    )
    assert.notOk(
      filterNumberIgnoredwhenzero(
        new Date(""),
        new Date("2009-08-07").valueOf()
      )
    )
  })
})
