import objValue from "dummy/utils/obj-value"
import { module, test } from "qunit"

module("Unit | Utility | obj-value", function () {
  test("it works", function (assert) {
    let result = objValue()
    assert.deepEqual(result, undefined)
  })
  test("handles non objects", function (assert) {
    assert.deepEqual(objValue("a"), "a")
    assert.deepEqual(objValue(1), 1)
    assert.deepEqual(objValue([1, 2, 3]), [1, 2, 3])
  })
  test("handles objects", function (assert) {
    assert.deepEqual(objValue({ a: 1 }), { a: 1 })
    assert.deepEqual(objValue({ a: 1 }, "a"), 1)
    assert.deepEqual(objValue({ a: 1 }, "b"), { a: 1 })
    assert.deepEqual(objValue({ a: 1, b: 2 }, "b"), 2)
  })
})
