import filterDateGte from "dummy/utils/filter-date-gte"
import { module, test } from "qunit"

module("Unit | Utility | filter-date-gte", function () {
  test("filterDateGte", function (assert) {
    assert.ok(filterDateGte("2001-01-01", "2001-01-01"))
    assert.ok(filterDateGte("2000-01-01", "2000-01-01"))
    assert.ok(filterDateGte("2001-01-01", "2000-01-01"))
    assert.notOk(filterDateGte("2000-01-01", "2001-01-01"))
  })
})
