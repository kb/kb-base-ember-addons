import filterStringExactsensitive from "dummy/utils/filter-string-exactsensitive"
import { module, test } from "qunit"

module("Unit | Utility | filter-string-exactsensitive", function () {
  test("filterStringExactsensitive", function (assert) {
    assert.notOk(filterStringExactsensitive("fruit", "it"))
    assert.notOk(filterStringExactsensitive("FRUIT", "IT"))
    assert.ok(filterStringExactsensitive("fruit", "fruit"))
    assert.notOk(filterStringExactsensitive("fruit", "FRUIT"))
    assert.notOk(filterStringExactsensitive("fruit", ""))
    assert.ok(filterStringExactsensitive("", ""))
    assert.notOk(filterStringExactsensitive("fruit", "fruit it"))
    assert.notOk(filterStringExactsensitive("fruit", "veg"))
    assert.notOk(filterStringExactsensitive(null, "veg"))
    assert.notOk(filterStringExactsensitive("veg", null))
    assert.ok(filterStringExactsensitive(null, null))
  })
})
