import kbChildrenTheModelDoesntHave from "dummy/utils/kb-children-the-model-doesnt-have"
import { module, test } from "qunit"

module("Unit | Utility | kb-children-the-model-doesnt-have", function () {
  test("it works", function (assert) {
    let result = kbChildrenTheModelDoesntHave()
    assert.ok(result)
  })
})
