import safeSelect from "dummy/utils/safe-select"
import { module, test } from "qunit"

module("Unit | Utility | safe-select", function () {
  test("safeSelect", function (assert) {
    assert.equal(safeSelect("a"), "a")
    assert.equal(safeSelect("a", "nonkey"), "a")
    assert.equal(safeSelect({ a: "A", b: "B" }, "a"), "A")
    assert.equal(safeSelect({ a: "A", b: "B" }, "b"), "B")
  })
  test("with primitive value, safeSelect displayKey from options", function (assert) {
    assert.equal(
      safeSelect(2, "name", "id", [
        { id: 1, name: "A" },
        { id: 2, name: "B" }
      ]),
      "B"
    )
    assert.equal(
      safeSelect(2, "id", "id", [
        { id: 1, name: "A" },
        { id: 2, name: "B" }
      ]),
      "2"
    )
    assert.equal(
      safeSelect("B", "id", "name", [
        { id: 1, name: "A" },
        { id: 2, name: "B" }
      ]),
      "2"
    )
  })
})
