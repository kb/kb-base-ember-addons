import sortDate from "dummy/utils/sort-date"
import { module, test } from "qunit"

module("Unit | Utility | sort-date", function () {
  test("sortDate", function (assert) {
    assert.deepEqual(
      ["2010-01-10", "2030-03-30", "2020-02-20"].sort(sortDate),
      ["2010-01-10", "2020-02-20", "2030-03-30"]
    )
    assert.deepEqual(["2020-02-20", "2010-01-10"].sort(sortDate), [
      "2010-01-10",
      "2020-02-20"
    ])
    assert.deepEqual(["2010-01-10"].sort(sortDate), ["2010-01-10"])
    assert.deepEqual([].sort(sortDate), [])
  })
  test("sortDate desc", function (assert) {
    assert.deepEqual(
      ["2010-01-10", "2030-03-30", "2020-02-20"].sort((a, b) =>
        sortDate(a, b, { desc: true })
      ),
      ["2030-03-30", "2020-02-20", "2010-01-10"]
    )
    assert.deepEqual(
      ["2020-02-20", "2010-01-10"].sort((a, b) =>
        sortDate(a, b, { desc: true })
      ),
      ["2020-02-20", "2010-01-10"]
    )
    assert.deepEqual(
      ["2010-01-10"].sort((a, b) => sortDate(a, b, { desc: true })),
      ["2010-01-10"]
    )
    assert.deepEqual(
      [].sort((a, b) => sortDate(a, b, { desc: true })),
      []
    )
  })
  test("sortDate object values", function (assert) {
    assert.deepEqual(
      [{ x: "2010-01-10" }, { x: "2030-03-30" }, { x: "2020-02-20" }].sort(
        (a, b) => sortDate(a, b, { key: "x" })
      ),
      [{ x: "2010-01-10" }, { x: "2020-02-20" }, { x: "2030-03-30" }]
    )
    assert.deepEqual(
      [{ x: "2020-02-20" }, { x: "2010-01-10" }].sort((a, b) =>
        sortDate(a, b, { key: "x" })
      ),
      [{ x: "2010-01-10" }, { x: "2020-02-20" }]
    )
    assert.deepEqual(
      [{ x: "2010-01-10" }].sort((a, b) => sortDate(a, b, { key: "x" })),
      [{ x: "2010-01-10" }]
    )
    assert.deepEqual(
      [].sort((a, b) => sortDate(a, b, { key: "x" })),
      []
    )
  })
  test("sortDate object values desc", function (assert) {
    assert.deepEqual(
      [{ x: "2010-01-10" }, { x: "2030-03-30" }, { x: "2020-02-20" }].sort(
        (a, b) => sortDate(a, b, { key: "x", desc: true })
      ),
      [{ x: "2030-03-30" }, { x: "2020-02-20" }, { x: "2010-01-10" }]
    )
    assert.deepEqual(
      [{ x: "2010-01-10" }, { x: "2020-02-20" }].sort((a, b) =>
        sortDate(a, b, { key: "x", desc: true })
      ),
      [{ x: "2020-02-20" }, { x: "2010-01-10" }]
    )
    assert.deepEqual(
      [{ x: "2010-01-10" }].sort((a, b) =>
        sortDate(a, b, { key: "x", desc: true })
      ),
      [{ x: "2010-01-10" }]
    )
    assert.deepEqual(
      [].sort((a, b) => sortDate(a, b, { key: "x", desc: true })),
      []
    )
  })
})
