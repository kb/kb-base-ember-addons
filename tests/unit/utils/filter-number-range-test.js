import filterNumberRange from "dummy/utils/filter-number-range"
import { module, test } from "qunit"

module("Unit | Utility | filter-number-range", function () {
  test("filterNumberRange", function (assert) {
    assert.ok(filterNumberRange(0, 0, 2))
    assert.ok(filterNumberRange(1, 0, 2))
    assert.ok(filterNumberRange(2, 0, 2))
    assert.notOk(filterNumberRange(3, 0, 1))
    assert.notOk(filterNumberRange(0, 1, 2))
  })
  test("filterNumberRange low only", function (assert) {
    assert.ok(filterNumberRange(1, 1, 0))
    assert.ok(filterNumberRange(2, 1, 0))
    assert.notOk(filterNumberRange(0, 1, 0))
  })
  test("filterDateRange high only", function (assert) {
    assert.ok(filterNumberRange(1, 0, 1))
    assert.ok(filterNumberRange(0, 0, 1))
    assert.notOk(filterNumberRange(2, 0, 1))
  })
})
