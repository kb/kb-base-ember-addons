import filterCombiner from "dummy/utils/filter-combiner"
import filterBooleanExact from "dummy/utils/filter-boolean-exact"
import filterBooleanIgnoredwhenfalse from "dummy/utils/filter-boolean-ignoredwhenfalse"
import filterTaggitExact from "dummy/utils/filter-taggit-exact"
import filterTaggitOverlap from "dummy/utils/filter-taggit-overlap"
import filterNumberExact from "dummy/utils/filter-number-exact"
import filterNumberGte from "dummy/utils/filter-number-gte"
import filterNumberIgnoredwhenzero from "dummy/utils/filter-number-ignoredwhenzero"
import filterNumberLte from "dummy/utils/filter-number-lte"
import filterStringIncludesinsensitive from "dummy/utils/filter-string-includesinsensitive"
import filterStringIncludessensitive from "dummy/utils/filter-string-includessensitive"
import { module, test } from "qunit"

const data = [
  {
    name: "Apple",
    active: true,
    size: 10,
    tags: ["a"]
  },
  {
    name: "Boysenberry",
    active: false,
    size: 10,
    tags: ["b"]
  },
  {
    name: "Crab apple",
    active: false,
    size: 20,
    tags: ["c", "a"]
  },
  {
    name: "Bing Cherry",
    active: true,
    size: 20,
    tags: ["b", "c"]
  },
  {
    name: "Elder Berry",
    active: false,
    size: 20,
    tags: ["e", "b"]
  },
  {
    name: "Honey Crisp Apples",
    active: false,
    size: 30,
    tags: ["h", "c", "a"]
  },
  {
    name: "",
    active: false,
    size: 0,
    tags: []
  }
]

module("Unit | Utility | filter-combiner", function () {
  test("it works", function (assert) {
    const defaultFilter = {
      name: row => filterStringIncludesinsensitive(row.name, ""),
      active: row => filterBooleanIgnoredwhenfalse(row.active, false),
      size: row => row.size === 0,
      tags: row => filterTaggitOverlap(row.tags, [])
    }
    const result = data.filter(row => filterCombiner(row, defaultFilter))
    assert.equal(result.length, 1)
  })
  test("empty string filter", function (assert) {
    const input = ""
    const emptyFilter = {
      name: row => filterStringIncludesinsensitive(row.name, input)
    }
    const result = data.filter(row => filterCombiner(row, emptyFilter))
    assert.equal(result.length, 7)
  })
  test("lengthy string filter", function (assert) {
    const input = "apple"
    const lengthyFilter = {
      name: row => filterStringIncludesinsensitive(row.name, input)
    }
    const result = data.filter(row => filterCombiner(row, lengthyFilter))
    assert.equal(result.length, 3)
  })
  test("empty tags filter", function (assert) {
    const input = []
    const emptyFilter = {
      tags: row => filterTaggitOverlap(row.tags, input)
    }
    const result = data.filter(row => filterCombiner(row, emptyFilter))
    assert.equal(result.length, 7)
  })
  test("active tags filter", function (assert) {
    const input = ["h"]
    const activeFilter = {
      tags: row => filterTaggitOverlap(row.tags, input)
    }
    const result = data.filter(row => filterCombiner(row, activeFilter))
    assert.equal(result.length, 1)
  })
  test("active tags filter || (aka Any) match default", function (assert) {
    const input = ["a", "c"]
    const activeFilter = {
      tags: row => filterTaggitOverlap(row.tags, input)
    }
    const result = data.filter(row => filterCombiner(row, activeFilter))
    assert.equal(result.length, 4)
    assert.deepEqual(
      result.map(r => r.name),
      ["Apple", "Crab apple", "Bing Cherry", "Honey Crisp Apples"]
    )
  })
  test("active tags filter || (aka Any) match", function (assert) {
    const input = ["a", "c"]
    const exact = false
    const activeFilter = {
      tags: row => filterTaggitOverlap(row.tags, input)
    }
    const result = data.filter(row => filterCombiner(row, activeFilter, exact))
    assert.equal(result.length, 4)
    assert.deepEqual(
      result.map(r => r.name),
      ["Apple", "Crab apple", "Bing Cherry", "Honey Crisp Apples"]
    )
  })
  test("active tags filter && (aka Exact) no match 1", function (assert) {
    const input = ["c"]
    const exact = true
    const activeFilter = {
      tags: row => filterTaggitExact(row.tags, input)
    }
    const result = data.filter(row => filterCombiner(row, activeFilter, exact))
    assert.equal(result.length, 0, "no rows have ['c'] exactly")
  })
  test("active tags filter && (aka Exact) match 1", function (assert) {
    const input = ["a"]
    const exact = true
    const activeFilter = {
      tags: row => filterTaggitExact(row.tags, input)
    }
    const result = data.filter(row => filterCombiner(row, activeFilter, exact))
    assert.equal(result.length, 1, "1 row has ['a'] exactly")
    assert.deepEqual(
      result.map(r => r.name),
      ["Apple"]
    )
  })
  test("active tags filter && (aka Exact) match 2", function (assert) {
    const input = ["a", "c"]
    const exact = true
    const activeFilter = {
      tags: row => filterTaggitExact(row.tags, input)
    }
    const result = data.filter(row => filterCombiner(row, activeFilter, exact))
    assert.equal(result.length, 1, "1 row has ['c', 'a'] exactly")
    assert.deepEqual(
      result.map(r => r.name),
      ["Crab apple"]
    )
  })
  test("active tags filter && (aka Exact) match 3", function (assert) {
    const input = ["a", "c", "h"]
    const exact = true
    const activeFilter = {
      tags: row => filterTaggitExact(row.tags, input)
    }
    const result = data.filter(row => filterCombiner(row, activeFilter, exact))
    assert.equal(result.length, 1)
    assert.deepEqual(
      result.map(r => r.name),
      ["Honey Crisp Apples"]
    )
  })
  test("zero filter", function (assert) {
    const input = 0
    const zeroFilter = {
      size: row => filterNumberIgnoredwhenzero(row.size, input)
    }
    const result = data.filter(row => filterCombiner(row, zeroFilter))
    assert.equal(result.length, 7)
  })
  test("positive number filter lte", function (assert) {
    const input = 15
    const positiveFilter = {
      size: row => filterNumberLte(row.size, input)
    }
    const result = data.filter(row => filterCombiner(row, positiveFilter))
    assert.equal(result.length, 3)
    assert.deepEqual(
      result.map(r => r.name),
      ["Apple", "Boysenberry", ""]
    )
  })
  test("positive number filter gte", function (assert) {
    const input = 25
    const positiveFilter = {
      size: row => filterNumberGte(row.size, input)
    }
    const result = data.filter(row => filterCombiner(row, positiveFilter))
    assert.equal(result.length, 1)
  })
  test("zero exact", function (assert) {
    const input = 0
    const positiveFilter = {
      size: row => filterNumberExact(row.size, input)
    }
    const result = data.filter(row => filterCombiner(row, positiveFilter))
    assert.equal(result.length, 1)
    assert.deepEqual(
      result.map(r => r.name),
      [""]
    )
  })
  test("positive number exact", function (assert) {
    const input = 30
    const positiveFilter = {
      size: row => filterNumberExact(row.size, input)
    }
    const result = data.filter(row => filterCombiner(row, positiveFilter))
    assert.equal(result.length, 1)
    assert.deepEqual(
      result.map(r => r.name),
      ["Honey Crisp Apples"]
    )
  })
  test("false boolean filter", function (assert) {
    const input = false
    const falseFilter = {
      active: row => filterBooleanIgnoredwhenfalse(row.active, input)
    }
    const result = data.filter(row => filterCombiner(row, falseFilter))
    assert.equal(result.length, 7)
  })
  test("true boolean filter", function (assert) {
    const input = true
    const trueFilter = {
      active: row => filterBooleanIgnoredwhenfalse(row.active, input)
    }
    const result = data.filter(row => filterCombiner(row, trueFilter))
    assert.equal(result.length, 2)
  })
  test("true boolean && (Exact)", function (assert) {
    const input = true
    const trueFilter = {
      active: row => filterBooleanExact(row.active, input)
    }
    const result = data.filter(row => filterCombiner(row, trueFilter))
    assert.equal(result.length, 2)
  })
  test("default filter is || (Any fields should match)", function (assert) {
    const input1 = "APPLE"
    const input2 = 15
    const trueFilter = {
      name: row => filterStringIncludesinsensitive(row.name, input1),
      size: row => filterNumberGte(row.size, input2)
    }
    const result = data.filter(row => filterCombiner(row, trueFilter))
    assert.equal(result.length, 2)
    assert.deepEqual(
      result.map(r => r.name),
      ["Crab apple", "Honey Crisp Apples"]
    )
  })
  test("default filter is && (Exact where all fields must match)", function (assert) {
    const input1 = "Apple"
    const input2 = 5
    const exact = true
    const trueFilter = {
      name: row => filterStringIncludessensitive(row.name, input1),
      size: row => filterNumberGte(row.size, input2)
    }
    const result = data.filter(row => filterCombiner(row, trueFilter, exact))
    assert.equal(result.length, 2)
    assert.deepEqual(
      result.map(r => r.name),
      ["Apple", "Honey Crisp Apples"]
    )
  })
})
