import filterNumberGte from "dummy/utils/filter-number-gte"
import { module, test } from "qunit"

module("Unit | Utility | filter-number-gte", function () {
  test("filterNumberGte", function (assert) {
    assert.ok(filterNumberGte(1, 1))
    assert.ok(filterNumberGte(0, 0))
    assert.ok(filterNumberGte(1, 0))
    assert.notOk(filterNumberGte(0, 1))
  })
})
