import filterStringIncludesinsensitive from "dummy/utils/filter-string-includesinsensitive"
import { module, test } from "qunit"

module("Unit | Utility | filter-string-includesinsensitive", function () {
  test("filterStringIncludesinsensitive", function (assert) {
    assert.ok(filterStringIncludesinsensitive("fruit", "it"))
    assert.ok(filterStringIncludesinsensitive("FRUIT", "it"))
    assert.ok(filterStringIncludesinsensitive("fruit", "fruit"))
    assert.ok(filterStringIncludesinsensitive("fruit", "FRUIT"))
    assert.ok(filterStringIncludesinsensitive("fruit", ""))
    assert.notOk(filterStringIncludesinsensitive("fruit", "fruit it"))
    assert.notOk(filterStringIncludesinsensitive("fruit", "veg"))
    assert.notOk(filterStringIncludesinsensitive(null, "veg"))
    assert.ok(filterStringIncludesinsensitive("veg", null))
    assert.ok(filterStringIncludesinsensitive(null, null))
  })
})
