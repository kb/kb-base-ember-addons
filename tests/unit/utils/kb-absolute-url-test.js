import { setupTest } from "ember-qunit"
import getAbsoluteUrl from "dummy/utils/kb-absolute-url"
import { module, test } from "qunit"

module("Unit | Utility | absolute-url", function () {
  test("it transforms a relative url to an absolute one", function (assert) {
    const url = "/login"
    assert.equal(
      getAbsoluteUrl(url),
      `${location.protocol}//${location.host}/login`
    )
  })

  test("it does not transform an absolute url", function (assert) {
    const url = "http://myTestHost/login"
    assert.equal(getAbsoluteUrl(url), url)
  })
})
