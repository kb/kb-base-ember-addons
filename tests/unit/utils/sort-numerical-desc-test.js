import sortNumericalDesc from "dummy/utils/sort-numerical-desc"
import { module, test } from "qunit"

module("Unit | Utility | sort-numerical-desc", function () {
  test("sortNumericalDesc", function (assert) {
    assert.deepEqual([1, 3, 2].sort(sortNumericalDesc), [3, 2, 1])
    assert.deepEqual([2, 1].sort(sortNumericalDesc), [2, 1])
    assert.deepEqual([1].sort(sortNumericalDesc), [1])
    assert.deepEqual([].sort(sortNumericalDesc), [])
  })
  test("sortNumericalDesc object values", function (assert) {
    assert.deepEqual(
      [{ x: 1 }, { x: 3 }, { x: 2 }].sort((a, b) =>
        sortNumericalDesc(a, b, { key: "x" })
      ),
      [{ x: 3 }, { x: 2 }, { x: 1 }]
    )
    assert.deepEqual(
      [{ x: 1 }, { x: 2 }].sort((a, b) =>
        sortNumericalDesc(a, b, { key: "x" })
      ),
      [{ x: 2 }, { x: 1 }]
    )
    assert.deepEqual(
      [{ x: 1 }].sort((a, b) => sortNumericalDesc(a, b, { key: "x" })),
      [{ x: 1 }]
    )
    assert.deepEqual(
      [].sort((a, b) => sortNumericalDesc(a, b, { key: "x" })),
      []
    )
  })
})
