import filterDateIgnoredwhendateless from "dummy/utils/filter-date-ignoredwhendateless"
import { module, test } from "qunit"

module("Unit | Utility | filter-date-ignoredwhendateless", function () {
  test("filterNumberIgnoredwhenzero", function (assert) {
    assert.ok(filterDateIgnoredwhendateless("2001-01-01", "2001-01-01"))
    assert.ok(filterDateIgnoredwhendateless("", ""))
    assert.ok(filterDateIgnoredwhendateless("2001-01-01", ""))
    assert.notOk(filterDateIgnoredwhendateless("", "2001-01-01"))
  })
  test("filterNumberIgnoredwhenzero dates", function (assert) {
    assert.ok(
      filterDateIgnoredwhendateless(
        new Date(2012, 11, 10),
        new Date(2012, 11, 10)
      )
    )
    assert.ok(filterDateIgnoredwhendateless(new Date(""), new Date("")))
    assert.ok(
      filterDateIgnoredwhendateless(new Date(2012, 11, 10), new Date(""))
    )
    assert.notOk(
      filterDateIgnoredwhendateless(new Date(""), new Date(2012, 11, 10))
    )
  })
})
