import filterNumberLte from "dummy/utils/filter-number-lte"
import { module, test } from "qunit"

module("Unit | Utility | filter-number-lte", function () {
  test("filterNumberLte", function (assert) {
    assert.ok(filterNumberLte(1, 1))
    assert.ok(filterNumberLte(0, 0))
    assert.notOk(filterNumberLte(1, 0))
    assert.ok(filterNumberLte(0, 1))
  })
})
