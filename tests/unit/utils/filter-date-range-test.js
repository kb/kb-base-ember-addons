import filterDateRange from "dummy/utils/filter-date-range"
import { module, test } from "qunit"

module("Unit | Utility | filter-date-range", function () {
  test("filterDateRange", function (assert) {
    assert.ok(filterDateRange("2000-01-01", "2000-01-01", "2000-02-03"))
    assert.ok(filterDateRange("2000-02-02", "2000-01-01", "2000-03-03"))
    assert.ok(filterDateRange("2000-03-03", "2000-01-01", "2000-03-03"))
    assert.notOk(filterDateRange("1999-01-01", "2000-01-01", "2001-01-01"))
    assert.notOk(filterDateRange("2999-01-01", "2000-01-01", "2001-01-01"))
  })
  test("filterDateRange low only", function (assert) {
    assert.ok(filterDateRange("2000-01-01", "2000-01-01", ""))
    assert.ok(filterDateRange("2000-02-02", "2000-01-01", ""))
    assert.notOk(filterDateRange("1999-01-01", "2000-01-01", ""))
  })
  test("filterDateRange high only", function (assert) {
    assert.ok(filterDateRange("2000-01-01", "", "2000-01-01"))
    assert.ok(filterDateRange("1999-12-12", "", "2000-01-01"))
    assert.notOk(filterDateRange("2000-02-02", "", "2000-01-01"))
  })
})
