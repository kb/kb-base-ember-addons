import filterTaggitExact from "dummy/utils/filter-taggit-exact"
import { module, test } from "qunit"

module("Unit | Utility | filter-taggit-exact", function () {
  test("filterTaggitExact", function (assert) {
    assert.ok(filterTaggitExact(["c", "b", "a"], ["a", "c", "b"]))
    assert.notOk(filterTaggitExact(["c", "a"], ["a", "c", "b"]))
    assert.ok(filterTaggitExact(["c", "a"], ["a", "c"]))
    assert.notOk(filterTaggitExact(["c", "a"], ["c", "b"]))
    assert.ok(filterTaggitExact(["a"], ["a"]))
    assert.notOk(filterTaggitExact(["a"], []))
    assert.notOk(filterTaggitExact([], ["a"]))
    assert.ok(filterTaggitExact([], []))
  })
})
