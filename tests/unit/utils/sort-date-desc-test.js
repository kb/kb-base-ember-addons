import sortDateDesc from "dummy/utils/sort-date-desc"
import { module, test } from "qunit"

module("Unit | Utility | sort-date-desc", function () {
  test("sortDateDesc", function (assert) {
    assert.deepEqual(
      ["2010-01-10", "2030-03-30", "2020-02-20"].sort(sortDateDesc),
      ["2030-03-30", "2020-02-20", "2010-01-10"]
    )
    assert.deepEqual(["2020-02-20", "2010-01-10"].sort(sortDateDesc), [
      "2020-02-20",
      "2010-01-10"
    ])
    assert.deepEqual(["2010-01-10"].sort(sortDateDesc), ["2010-01-10"])
    assert.deepEqual([].sort(sortDateDesc), [])
  })
  test("sortDateDesc object values", function (assert) {
    assert.deepEqual(
      [{ x: "2010-01-10" }, { x: "2030-03-30" }, { x: "2020-02-20" }].sort(
        (a, b) => sortDateDesc(a, b, { key: "x" })
      ),
      [{ x: "2030-03-30" }, { x: "2020-02-20" }, { x: "2010-01-10" }]
    )
    assert.deepEqual(
      [{ x: "2010-01-10" }, { x: "2020-02-20" }].sort((a, b) =>
        sortDateDesc(a, b, { key: "x" })
      ),
      [{ x: "2020-02-20" }, { x: "2010-01-10" }]
    )
    assert.deepEqual(
      [{ x: "2010-01-10" }].sort((a, b) => sortDateDesc(a, b, { key: "x" })),
      [{ x: "2010-01-10" }]
    )
    assert.deepEqual(
      [].sort((a, b) => sortDateDesc(a, b, { key: "x" })),
      []
    )
  })
})
