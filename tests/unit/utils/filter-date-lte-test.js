import filterDateLte from "dummy/utils/filter-date-lte"
import { module, test } from "qunit"

module("Unit | Utility | filter-date-lte", function () {
  test("filterDateLte", function (assert) {
    assert.ok(filterDateLte("2001-01-01", "2001-01-01"))
    assert.ok(filterDateLte("2000-01-01", "2000-01-01"))
    assert.notOk(filterDateLte("2001-01-01", "2000-01-01"))
    assert.ok(filterDateLte("2000-01-01", "2001-01-01"))
  })
})
