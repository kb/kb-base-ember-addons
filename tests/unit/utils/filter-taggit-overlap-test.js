import filterTaggitOverlap from "dummy/utils/filter-taggit-overlap"
import { module, test } from "qunit"

module("Unit | Utility | filter-taggit-overlap", function () {
  test("filterTaggitOverlap", function (assert) {
    assert.ok(filterTaggitOverlap(["c", "b", "a"], ["a", "c", "b"]))
    assert.ok(filterTaggitOverlap(["c", "a"], ["a", "c", "b"]))
    assert.ok(filterTaggitOverlap(["c", "a"], ["a", "c"]))
    assert.ok(filterTaggitOverlap(["c", "a"], ["c", "b"]))
    assert.ok(filterTaggitOverlap(["a"], ["a"]))
    assert.ok(filterTaggitOverlap(["a"], []))
    assert.notOk(filterTaggitOverlap([], ["a"]))
    assert.ok(filterTaggitOverlap([], []))
  })
})
