import filterStringIncludessensitive from "dummy/utils/filter-string-includessensitive"
import { module, test } from "qunit"

module("Unit | Utility | filter-string-includessensitive", function () {
  test("filterStringIncludessensitive", function (assert) {
    assert.ok(filterStringIncludessensitive("fruit", "it"))
    assert.notOk(filterStringIncludessensitive("FRUIT", "it"))
    assert.ok(filterStringIncludessensitive("fruit", "fruit"))
    assert.notOk(filterStringIncludessensitive("fruit", "FRUIT"))
    assert.ok(filterStringIncludessensitive("fruit", ""))
    assert.notOk(filterStringIncludessensitive("fruit", "fruit it"))
    assert.notOk(filterStringIncludessensitive("fruit", "veg"))
    assert.notOk(filterStringIncludessensitive(null, "veg"))
    assert.ok(filterStringIncludessensitive("veg", null))
    assert.ok(filterStringIncludessensitive(null, null))
  })
})
