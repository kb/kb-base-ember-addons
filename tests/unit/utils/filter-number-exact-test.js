import filterNumberExact from "dummy/utils/filter-number-exact"
import { module, test } from "qunit"

module("Unit | Utility | filter-number-exact", function () {
  test("filterNumberExact", function (assert) {
    assert.ok(filterNumberExact(1, 1))
    assert.ok(filterNumberExact(0, 0))
    assert.notOk(filterNumberExact(1, 0))
    assert.notOk(filterNumberExact(0, 1))
  })
})
