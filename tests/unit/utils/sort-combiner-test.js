import sortCombiner from "dummy/utils/sort-combiner"
import sortDate from "dummy/utils/sort-date"
import sortDateDesc from "dummy/utils/sort-date-desc"
import sortString from "dummy/utils/sort-string"
import sortStringDesc from "dummy/utils/sort-string-desc"
import sortNumerical from "dummy/utils/sort-numerical"
import sortNumericalDesc from "dummy/utils/sort-numerical-desc"
import { module, test } from "qunit"

const data = [
  {
    name: "Apple",
    active: true,
    size: 1,
    dated: "2004-04-01"
  },
  {
    name: "Boysenberry",
    active: false,
    size: 2,
    dated: "2001-02-02"
  },
  {
    name: "Crab apple",
    active: false,
    size: 4,
    dated: "2004-05-07"
  },
  {
    name: "Bing Cherry",
    active: true,
    size: 5,
    dated: "2012-01-01"
  },
  {
    name: "Elder Berry",
    active: true,
    size: 7,
    dated: "2001-01-01"
  },
  {
    name: "Honey Crisp Apples",
    active: false,
    size: 12,
    dated: "2004-05-01"
  },
  {
    name: "",
    active: false,
    size: 0,
    dated: "2000-01-01"
  }
]

module("Unit | Utility | sort-combiner", function () {
  test("it works with boolean", function (assert) {
    const defaultSorter = {
      active: sortNumerical
    }
    const result = sortCombiner(data, defaultSorter)
    assert.deepEqual(
      result.map(row => [row.name, row.active]),
      [
        ["Boysenberry", false],
        ["Crab apple", false],
        ["Honey Crisp Apples", false],
        ["", false],
        ["Apple", true],
        ["Bing Cherry", true],
        ["Elder Berry", true]
      ]
    )
  })

  test("it works with boolean desc", function (assert) {
    const defaultSorter = {
      active: sortNumericalDesc
    }
    const result = sortCombiner(data, defaultSorter)
    assert.deepEqual(
      result.map(row => [row.name, row.active]),
      [
        ["Apple", true],
        ["Bing Cherry", true],
        ["Elder Berry", true],
        ["Boysenberry", false],
        ["Crab apple", false],
        ["Honey Crisp Apples", false],
        ["", false]
      ]
    )
  })

  test("it works with dates", function (assert) {
    const defaultSorter = {
      dated: sortDate
    }
    const result = sortCombiner(data, defaultSorter)
    assert.deepEqual(
      result.map(row => row.dated),
      [
        "2000-01-01",
        "2001-01-01",
        "2001-02-02",
        "2004-04-01",
        "2004-05-01",
        "2004-05-07",
        "2012-01-01"
      ]
    )
  })

  test("it works with dates desc", function (assert) {
    const defaultSorter = {
      dated: sortDateDesc
    }
    const result = sortCombiner(data, defaultSorter)
    assert.deepEqual(
      result.map(row => row.dated),
      [
        "2000-01-01",
        "2001-01-01",
        "2001-02-02",
        "2004-04-01",
        "2004-05-01",
        "2004-05-07",
        "2012-01-01"
      ].reverse()
    )
  })

  test("it works with numbers", function (assert) {
    const defaultSorter = {
      size: sortNumerical
    }
    const result = sortCombiner(data, defaultSorter)
    assert.deepEqual(
      result.map(row => row.size),
      [0, 1, 2, 4, 5, 7, 12]
    )
  })

  test("it works with numbers desc", function (assert) {
    const defaultSorter = {
      size: sortNumericalDesc
    }
    const result = sortCombiner(data, defaultSorter)
    assert.deepEqual(
      result.map(row => row.size),
      [0, 1, 2, 4, 5, 7, 12].reverse()
    )
  })

  test("it works with strings", function (assert) {
    const defaultSorter = {
      name: sortString
    }
    const result = sortCombiner(data, defaultSorter)
    assert.deepEqual(
      result.map(row => row.name),
      [
        "",
        "Apple",
        "Bing Cherry",
        "Boysenberry",
        "Crab apple",
        "Elder Berry",
        "Honey Crisp Apples"
      ]
    )
  })

  test("it works with strings desc", function (assert) {
    const defaultSorter = {
      name: sortStringDesc
    }
    const result = sortCombiner(data, defaultSorter)
    assert.deepEqual(
      result.map(row => row.name),
      [
        "",
        "Apple",
        "Bing Cherry",
        "Boysenberry",
        "Crab apple",
        "Elder Berry",
        "Honey Crisp Apples"
      ].reverse()
    )
  })

  test("the order of combination matters: i.e. boolean first", function (assert) {
    const result = sortCombiner(data, {
      active: sortNumerical,
      dated: sortDate
    })
    assert.deepEqual(
      result.map(row => [row.active, row.dated]),
      [
        [false, "2000-01-01"],
        [true, "2001-01-01"],
        [false, "2001-02-02"],
        [true, "2004-04-01"],
        [false, "2004-05-01"],
        [false, "2004-05-07"],
        [true, "2012-01-01"]
      ]
    )
  })

  test("the order of combination matters: i.e. boolean first desc", function (assert) {
    const result = sortCombiner(data, {
      active: sortNumerical,
      dated: sortDate
    })
    assert.deepEqual(
      result.map(row => [row.active, row.dated]),
      [
        [false, "2000-01-01"],
        [true, "2001-01-01"],
        [false, "2001-02-02"],
        [true, "2004-04-01"],
        [false, "2004-05-01"],
        [false, "2004-05-07"],
        [true, "2012-01-01"]
      ]
    )
  })

  test("the order of combination matters: i.e. boolean last", function (assert) {
    const result = sortCombiner(data, {
      dated: sortDate,
      active: sortNumerical
    })
    assert.deepEqual(
      result.map(row => [row.active, row.dated]),
      [
        [false, "2000-01-01"],
        [false, "2001-02-02"],
        [false, "2004-05-01"],
        [false, "2004-05-07"],
        [true, "2001-01-01"],
        [true, "2004-04-01"],
        [true, "2012-01-01"]
      ]
    )
  })

  test("the order of combination matters: i.e. boolean last desc", function (assert) {
    const result = sortCombiner(data, {
      dated: sortString,
      active: sortNumericalDesc
    })
    assert.deepEqual(
      result.map(row => [row.active, row.dated]),
      [
        [true, "2001-01-01"],
        [true, "2004-04-01"],
        [true, "2012-01-01"],
        [false, "2000-01-01"],
        [false, "2001-02-02"],
        [false, "2004-05-01"],
        [false, "2004-05-07"]
      ]
    )
  })
})
