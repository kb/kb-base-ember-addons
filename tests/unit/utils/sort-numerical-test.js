import sortNumerical from "dummy/utils/sort-numerical"
import { module, test } from "qunit"

module("Unit | Utility | sort-numerical", function () {
  test("sortNumerical", function (assert) {
    assert.deepEqual([1, 3, 2].sort(sortNumerical), [1, 2, 3])
    assert.deepEqual([2, 1].sort(sortNumerical), [1, 2])
    assert.deepEqual([1].sort(sortNumerical), [1])
    assert.deepEqual([].sort(sortNumerical), [])
  })
  test("sortNumerical desc", function (assert) {
    assert.deepEqual(
      [1, 3, 2].sort((a, b) => sortNumerical(a, b, { desc: true })),
      [3, 2, 1]
    )
    assert.deepEqual(
      [2, 1].sort((a, b) => sortNumerical(a, b, { desc: true })),
      [2, 1]
    )
    assert.deepEqual(
      [1].sort((a, b) => sortNumerical(a, b, { desc: true })),
      [1]
    )
    assert.deepEqual(
      [].sort((a, b) => sortNumerical(a, b, { desc: true })),
      []
    )
  })
  test("sortNumerical object values", function (assert) {
    assert.deepEqual(
      [{ x: 1 }, { x: 3 }, { x: 2 }].sort((a, b) =>
        sortNumerical(a, b, { key: "x" })
      ),
      [{ x: 1 }, { x: 2 }, { x: 3 }]
    )
    assert.deepEqual(
      [{ x: 2 }, { x: 1 }].sort((a, b) => sortNumerical(a, b, { key: "x" })),
      [{ x: 1 }, { x: 2 }]
    )
    assert.deepEqual(
      [{ x: 1 }].sort((a, b) => sortNumerical(a, b, { key: "x" })),
      [{ x: 1 }]
    )
    assert.deepEqual(
      [].sort((a, b) => sortNumerical(a, b, { key: "x" })),
      []
    )
  })
  test("sortNumerical object values desc", function (assert) {
    assert.deepEqual(
      [{ x: 1 }, { x: 3 }, { x: 2 }].sort((a, b) =>
        sortNumerical(a, b, { key: "x", desc: true })
      ),
      [{ x: 3 }, { x: 2 }, { x: 1 }]
    )
    assert.deepEqual(
      [{ x: 1 }, { x: 2 }].sort((a, b) =>
        sortNumerical(a, b, { key: "x", desc: true })
      ),
      [{ x: 2 }, { x: 1 }]
    )
    assert.deepEqual(
      [{ x: 1 }].sort((a, b) => sortNumerical(a, b, { key: "x", desc: true })),
      [{ x: 1 }]
    )
    assert.deepEqual(
      [].sort((a, b) => sortNumerical(a, b, { key: "x", desc: true })),
      []
    )
  })
})
