import filterBooleanIgnoredwhenfalse from "dummy/utils/filter-boolean-ignoredwhenfalse"
import { module, test } from "qunit"

module("Unit | Utility | filter-boolean-ignoredwhenfalse", function () {
  test("filterBooleanIgnoredwhenfalse", function (assert) {
    assert.ok(filterBooleanIgnoredwhenfalse(true, true), "true true")
    assert.ok(filterBooleanIgnoredwhenfalse(false, false), "false false")
    assert.ok(filterBooleanIgnoredwhenfalse(true, false), "true false")
    assert.notOk(filterBooleanIgnoredwhenfalse(false, true), "false true")
  })
  test("filterBooleanIgnoredwhenfalse as string", function (assert) {
    assert.ok(
      filterBooleanIgnoredwhenfalse("true", "true"),
      "true true as string"
    )
    assert.ok(
      filterBooleanIgnoredwhenfalse("false", "false", "false false as string")
    )
    assert.ok(
      filterBooleanIgnoredwhenfalse("true", "false", "true false as string")
    )
    assert.notOk(
      filterBooleanIgnoredwhenfalse("false", "true"),
      "false true as string"
    )
  })
  test("filterBooleanIgnoredwhenfalse as null", function (assert) {
    assert.ok(filterBooleanIgnoredwhenfalse(true, true), "null true")
    assert.ok(filterBooleanIgnoredwhenfalse(null, null), "null null")
    assert.ok(filterBooleanIgnoredwhenfalse(true, null), "null true")
    assert.notOk(filterBooleanIgnoredwhenfalse(null, true), "null true")
  })
})
