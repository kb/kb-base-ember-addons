import sortStringDesc from "dummy/utils/sort-string-desc"
import { module, test } from "qunit"

module("Unit | Utility | sort-string-desc", function () {
  test("sortStringDesc", function (assert) {
    assert.deepEqual(["a", "c", "B"].sort(sortStringDesc), ["c", "B", "a"])
    assert.deepEqual(["B", "a"].sort(sortStringDesc), ["B", "a"])
    assert.deepEqual(["a"].sort(sortStringDesc), ["a"])
    assert.deepEqual([].sort(sortStringDesc), [])
  })
  test("sortStringDesc object values", function (assert) {
    assert.deepEqual(
      [{ x: "a" }, { x: "c" }, { x: "B" }].sort((a, b) =>
        sortStringDesc(a, b, { key: "x" })
      ),
      [{ x: "c" }, { x: "B" }, { x: "a" }]
    )
    // assert.deepEqual(
    //   [{ x: "a" }, { x: "B" }].sort((a, b) =>
    //     sortStringDesc(a, b, { key: "x" })
    //   ),
    //   [{ x: "B" }, { x: "a" }]
    // )
    // assert.deepEqual(
    //   [{ x: "a" }].sort((a, b) => sortStringDesc(a, b, { key: "x" })),
    //   [{ x: "a" }]
    // )
    // assert.deepEqual(
    //   [].sort((a, b) => sortStringDesc(a, b, { key: "x" })),
    //   []
    // )
  })
})
