import filterBooleanExact from "dummy/utils/filter-boolean-exact"
import { module, test } from "qunit"

module("Unit | Utility | filter-boolean-exact", function () {
  test("filterBooleanExact", function (assert) {
    assert.ok(filterBooleanExact(true, true), "true true")
    assert.ok(filterBooleanExact(false, false), "false false")
    assert.notOk(filterBooleanExact(true, false), "true false")
    assert.notOk(filterBooleanExact(false, true), "false true")
  })
  test("filterBooleanExact as string", function (assert) {
    assert.ok(filterBooleanExact("true", "true"), "true true as string")
    assert.ok(filterBooleanExact("false", "false", "false false as string"))
    assert.notOk(filterBooleanExact("true", "false", "true true as string"))
    assert.notOk(filterBooleanExact("false", "true"), "false true as string")
  })
  test("filterBooleanExact as null", function (assert) {
    assert.ok(filterBooleanExact(true, true), "null true")
    assert.ok(filterBooleanExact(null, null), "null null")
    assert.notOk(filterBooleanExact(true, null), "null true")
    assert.notOk(filterBooleanExact(null, true), "null true")
  })
})
