import { module, test } from "qunit"
import { setupTest } from "ember-qunit"

module("Unit | Service | side-bar-visibility", function (hooks) {
  setupTest(hooks)

  test("it exists", function (assert) {
    let service = this.owner.lookup("service:side-bar-visibility")
    assert.ok(service)
  })
})
