import { upload } from "ember-file-upload/mirage"

export default function () {
  this.post(
    `/upload/`,
    upload(function (db, request) {
      let fileName = request.requestBody.file.name
      return { url: "#dud", name: fileName }
    })
  )
}
