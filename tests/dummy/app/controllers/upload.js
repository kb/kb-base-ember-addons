import Controller from "@ember/controller"
import { action } from "@ember/object"
import { inject as service } from "@ember/service"

export default class UploadController extends Controller {
  @service kbMessages

  @action
  controllerSubmitHook(params) {
    this.kbMessages.addError("We will now process your files.")
    this.model.document.fileList.forEach(file => {
      this.kbMessages.addError(`${file} processed!`)
    })
  }
}
