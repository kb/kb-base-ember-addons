import Controller from "@ember/controller"
import { action } from "@ember/object"
import { inject as service } from "@ember/service"

export default class FormController extends Controller {
  @service kbMessages

  @action
  controllerSubmitHook(params) {
    this.kbMessages.addError("The form has been submitted!")
    for (const property in params) {
      console.log(`${property}: ${params[property]}`)
    }
  }
}
