import Controller from "@ember/controller"
import { action } from "@ember/object"
import { inject as service } from "@ember/service"

export default class ConfirmerController extends Controller {
  @service kbMessages

  @action
  buttonConfirm() {
    this.kbMessages.addError("The action has been confirmed!")
  }
}
