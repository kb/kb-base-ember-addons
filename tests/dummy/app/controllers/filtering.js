import Controller from "@ember/controller"

export default class FilteringController extends Controller {
  queryParams = ["byMake", "byModel", "byShape", "byMot"]
  tags = ["red", "green", "blue"]
  shapes = ["hatchback", "saloon", "sportscoupe"]
}
