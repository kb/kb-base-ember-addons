import Controller from "@ember/controller"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"
import { htmlSafe } from "@ember/template"

export default class ConfirmerController extends Controller {
  @tracked htmlValue = ""

  @action
  controllerSubmitHook(params) {
    this.htmlValue = params.description
  }

  get htmlValueSafe() {
    return htmlSafe(this.htmlValue)
  }
}
