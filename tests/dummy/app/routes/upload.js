import Route from "@ember/routing/route"
import RSVP from "rsvp"

export default class UploadRoute extends Route {
  model() {
    return RSVP.hash({
      document: { fileList: [] }
    })
  }
}
