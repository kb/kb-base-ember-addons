import Route from "@ember/routing/route"
import RSVP from "rsvp"

export default class RadioRoute extends Route {
  model() {
    return {
      singlePrimitiveIdValue: {
        job: 2
      },
      singlePrimitiveValue: {
        job: "gatherer"
      },
      jobs: [
        {
          id: 1,
          slug: "baker",
          name: "Baker"
        },
        {
          id: 2,
          slug: "butcher",
          name: "Butcher"
        },
        {
          id: 3,
          slug: "candlestick",
          name: "Candlestick Maker"
        }
      ],
      primitiveJobs: ["hunter", "gatherer", "manager"]
    }
  }
}
