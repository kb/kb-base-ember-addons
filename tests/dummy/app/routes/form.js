import Route from "@ember/routing/route"
import RSVP from "rsvp"

export default class FormRoute extends Route {
  model() {
    return RSVP.hash({
      person: {
        userName: "fred",
        description: "Hmmmm",
        job: {},
        jobs: [],
        taggits: ["TAG1", "TAG3"],
        termsAccepted: true,
        archived: false,
        dob: "1930-10-20",
        score: 42,
        radio: 2
      },
      jobs: [
        {
          id: 1,
          slug: "baker",
          name: "Baker"
        },
        {
          id: 2,
          slug: "butcher",
          name: "Butcher"
        },
        {
          id: 3,
          slug: "candlestick",
          name: "Candlestick Maker"
        }
      ],
      tags: [1, 2, 3].map(x => `TAG${x}`)
    })
  }
}
