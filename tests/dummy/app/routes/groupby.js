import Route from "@ember/routing/route"

export default class PaginatorRoute extends Route {
  model() {
    return [
      { make: "citroen", model: "xantia sx 1905cc", shape: "hatchback" },
      { make: "ford", model: "focus ghia 2.0 4dr", shape: "saloon" },
      { make: "peugeot", model: "106 xn 1124cc", shape: "hatchback" },
      { make: "ford", model: "mondeo gh.x 2.0td", shape: "saloon" },
      { make: "vauxhall", model: "tigra 1.6 coupe", shape: "sportscoupe" },
      { make: "volkswagen", model: "golf gl 1781cc", shape: "hatchback" },
      { make: "peugeot", model: "406 lx 1905cc", shape: "saloon" },
      { make: "vauxhall", model: "omega gls 1998cc", shape: "saloon" },
      { make: "saab", model: "9000 cse ecopower 2290cc", shape: "hatchback" },
      { make: "ford", model: "fiesta zetec ghia 1388cc", shape: "hatchback" },
      { make: "ford", model: "cougar 16v 2.0 3dr", shape: "sportscoupe" },
      { make: "bmw", model: "3 series 328i 2793cc", shape: "sportscoupe" },
      { make: "renault", model: "clio rn 1.2 3dr", shape: "hatchback" },
      { make: "ford", model: "fiesta zet 1.4 3dr", shape: "hatchback" },
      { make: "vauxhall", model: "astra ls", shape: "hatchback" },
      { make: "rover", model: "25 il 1.6 5dr", shape: "hatchback" },
      { make: "vauxhall", model: "corsa gls 1.2 5dr", shape: "hatchback" },
      { make: "ford", model: "puma rac 1.7 3dr", shape: "sportscoupe" },
      { make: "nissan", model: "primera si 1597", shape: "hatchback" },
      { make: "vauxhall", model: "vectra envoy 1598", shape: "hatchback" },
      { make: "saab", model: "900 turbo 8v 1985cc", shape: "saloon" },
      { make: "rover", model: "75 connoisseur se 2497cc", shape: "saloon" },
      { make: "ford", model: "fiesta lx 1.25", shape: "hatchback" },
      { make: "peugeot", model: "406 lx 1998cc", shape: "saloon" },
      { make: "rover", model: "200 216 si 1589", shape: "hatchback" }
    ]
      .sort((itemA, itemB) => {
        return itemA.shape < itemB.shape ? -1 : 1
      })
      .sort((itemA, itemB) => {
        return itemA.make < itemB.make ? -1 : 1
      })
  }
}
