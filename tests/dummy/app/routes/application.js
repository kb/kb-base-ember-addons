import Route from "@ember/routing/route"
import { action } from "@ember/object"

export default class ApplicationRoute extends Route {
  routeAfterAuthentication = "protected"
  routeIfAlreadyAuthenticated = "protected"
  @action
  actionRefreshApplicationRoute() {
    this.refresh()
  }
}
