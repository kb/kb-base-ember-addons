import Route from "@ember/routing/route"
import RSVP from "rsvp"

export default class SelectRoute extends Route {
  model() {
    return {
      singleObjectValue: {
        job: {
          id: 1,
          slug: "baker",
          name: "Baker"
        }
      },
      singlePrimitiveValue: {
        job: 1
      },
      multiObjectValue: {
        job: [
          {
            id: 1,
            slug: "baker",
            name: "Baker"
          },
          {
            id: 3,
            slug: "candlestick",
            name: "Candlestick Maker"
          }
        ]
      },
      multiPrimitiveValue: {
        job: [1, 3]
      },
      jobs: [
        {
          id: 1,
          slug: "baker",
          name: "Baker"
        },
        {
          id: 2,
          slug: "butcher",
          name: "Butcher"
        },
        {
          id: 3,
          slug: "candlestick",
          name: "Candlestick Maker"
        }
      ],
      singlePrimitiveListValue: {
        job: ["manager"]
      },
      multiPrimitiveListValue: {
        job: ["hunter", "manager"]
      },
      primitiveJobs: ["hunter", "gatherer", "manager"],
      singlePrimitiveNoValue: {
        job: undefined
      }
    }
  }
}
