import Route from "@ember/routing/route"
import RSVP from "rsvp"

export default class CkeditorRoute extends Route {
  model() {
    return RSVP.hash({
      html: { description: "<ul><li>Hello</li><li>World</li></ul>" }
    })
  }
}
