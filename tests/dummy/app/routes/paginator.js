import Route from "@ember/routing/route"

export default class PaginatorRoute extends Route {
  model() {
    return ["pap", "pep", "pip", "pop", "pup"]
  }
}
