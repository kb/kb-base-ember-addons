import Route from "@ember/routing/route"

export default class TaggitFilteringRoute extends Route {
  model() {
    return [
      {
        make: "citroen",
        model: "xantia sx 1905cc",
        shape: "hatchback",
        colors: ["red"],
        mot: true
      },
      {
        make: "ford",
        model: "focus ghia 2.0 4dr",
        shape: "saloon",
        colors: ["blue", "green"],
        mot: false
      },
      {
        make: "peugeot",
        model: "106 xn 1124cc",
        shape: "hatchback",
        colors: ["red", "green"],
        mot: true
      },
      {
        make: "ford",
        model: "mondeo gh.x 2.0td",
        shape: "saloon",
        colors: ["blue"],
        mot: true
      },
      {
        make: "vauxhall",
        model: "tigra 1.6 coupe",
        shape: "sportscoupe",
        colors: ["blue", "red"],
        mot: false
      },
      {
        make: "volkswagen",
        model: "golf gl 1781cc",
        shape: "hatchback",
        colors: ["red", "green"],
        mot: false
      },
      {
        make: "peugeot",
        model: "406 lx 1905cc",
        shape: "saloon",
        colors: ["red", "blue"],
        mot: true
      },
      {
        make: "vauxhall",
        model: "omega gls 1998cc",
        shape: "saloon",
        colors: ["green"],
        mot: false
      },
      {
        make: "saab",
        model: "9000 cse ecopower 2290cc",
        shape: "hatchback",
        colors: ["red", "blue"],
        mot: true
      },
      {
        make: "ford",
        model: "fiesta zetec ghia 1388cc",
        shape: "hatchback",
        colors: ["red", "green"],
        mot: true
      },
      {
        make: "ford",
        model: "cougar 16v 2.0 3dr",
        shape: "sportscoupe",
        colors: ["green"],
        mot: false
      },
      {
        make: "bmw",
        model: "3 series 328i 2793cc",
        shape: "sportscoupe",
        colors: ["blue", "green"],
        mot: false
      },
      {
        make: "renault",
        model: "clio rn 1.2 3dr",
        shape: "hatchback",
        colors: ["red", "green", "blue"],
        mot: true
      },
      {
        make: "ford",
        model: "fiesta zet 1.4 3dr",
        shape: "hatchback",
        colors: ["blue", "green"],
        mot: false
      },
      {
        make: "vauxhall",
        model: "astra ls",
        shape: "hatchback",
        colors: ["red", "green"],
        mot: true
      },
      {
        make: "rover",
        model: "25 il 1.6 5dr",
        shape: "hatchback",
        colors: ["red"],
        mot: true
      },
      {
        make: "vauxhall",
        model: "corsa gls 1.2 5dr",
        shape: "hatchback",
        colors: ["red", "green", "blue"],
        mot: false
      },
      {
        make: "ford",
        model: "puma rac 1.7 3dr",
        shape: "sportscoupe",
        colors: ["blue"],
        mot: false
      },
      {
        make: "nissan",
        model: "primera si 1597",
        shape: "hatchback",
        colors: ["red", "green"],
        mot: true
      },
      {
        make: "vauxhall",
        model: "vectra envoy 1598",
        shape: "hatchback",
        colors: ["blue", "green"],
        mot: false
      },
      {
        make: "saab",
        model: "900 turbo 8v 1985cc",
        shape: "saloon",
        colors: ["red", "green"],
        mot: true
      },
      {
        make: "rover",
        model: "75 connoisseur se 2497cc",
        shape: "saloon",
        colors: ["red", "green"],
        mot: true
      },
      {
        make: "ford",
        model: "fiesta lx 1.25",
        shape: "hatchback",
        colors: ["red", "blue"],
        mot: false
      },
      {
        make: "peugeot",
        model: "406 lx 1998cc",
        shape: "saloon",
        colors: ["blue"],
        mot: false
      },
      {
        make: "rover",
        model: "200 216 si 1589",
        shape: "hatchback",
        colors: ["blue", "green"],
        mot: true
      }
    ]
  }
}
