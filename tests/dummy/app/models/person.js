import Model, { attr } from "@ember-data/model"

export default class PersonModel extends Model {
  @attr userName
  @attr job
  @attr taggits
  @attr termsAccepted
}
