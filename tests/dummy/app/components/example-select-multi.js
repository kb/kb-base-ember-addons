import Component from "@glimmer/component"
import { computed, action } from "@ember/object"
import { tracked } from "@glimmer/tracking"

export default class SelectMultiComponent extends Component {
  @tracked val = this.args.options.filter(o => o.id != 2)
}
