import EmberRouter from "@ember/routing/router"
import config from "dummy/config/environment"

export default class Router extends EmberRouter {
  location = config.locationType
  rootURL = config.rootURL
}

Router.map(function () {
  this.route("home", { path: "" })
  this.route("confirmer")
  this.route("dashboard")
  this.route("ckeditor")
  this.route("groupby")
  this.route("filtering")
  this.route("form")
  this.route("select")
  this.route("radio")
  this.route("paginator")
  this.route("upload")
})
