import { module, test } from "qunit"
import { visit, fillIn, typeIn, currentURL } from "@ember/test-helpers"
import { setupApplicationTest } from "ember-qunit"

module("Acceptance | fill in example form", function (hooks) {
  setupApplicationTest(hooks)

  test("visiting /form", async function (assert) {
    await visit("/form")

    assert.equal(currentURL(), "/form")

    let DATATESTID = "[data-test-kb-form-input-id='userName']"
    await fillIn(DATATESTID, "joe")
    assert.dom(DATATESTID).hasValue("joe")
    await typeIn(DATATESTID, "Hello World")
    assert.dom(DATATESTID).hasValue("joeHello World")
  })
})
