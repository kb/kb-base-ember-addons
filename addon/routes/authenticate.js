import Route from "@ember/routing/route"
import { inject as service } from "@ember/service"

export default class AuthenticateRoute extends Route {
  /**
   * PJK 27/08/2021, I think this route is used for a failed login.
   * A failed login cannot redirect to 'login' because we would end up with an
   * endless loop of logins (assuming they fail every time).
   */

  @service session

  beforeModel(transition) {
    /**
     * If the user is already logged in, then redirect to the 'help' route.
     */
    this.session.prohibitAuthentication("help")
  }
}
