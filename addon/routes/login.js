import config from "@kb/ember-addon/config"
import ENV from "front/config/environment"
import getAbsoluteUrl from "@kb/ember-addon/utils/kb-absolute-url"
import Route from "@ember/routing/route"
import { inject as service } from "@ember/service"
import { uuid } from "ember-cli-uuid"

const { host, clientId, authEndPoint, loginHintName } = config

export default class LoginRoute extends Route {
  @service kbMessages
  @service router
  @service session

  queryParams = {
    code: { refreshModel: false },
    state: { refreshModel: false }
  }

  beforeModel(transition) {
    /**
     * If the user is already logged in, then redirect to the 'help' route.
     */
    let simpleAuth = ENV["ember-simple-auth"]
    if (simpleAuth.routeAfterAuthentication) {
      this.session.prohibitAuthentication(simpleAuth.routeAfterAuthentication)
    } else {
      console.error(
        "'login' route needs 'ENV['ember-simple-auth'].routeAfterAuthentication'"
      )
    }
  }

  /**
   * Handle unauthenticated requests
   *
   * This handles two cases:
   *
   * 1. The URL contains an authentication code and a state. In this case the
   *    client will trade the code for a standard Django Rest token.
   *
   * 2. The URL does not contain an authentication. In this case the client
   *    will be redirected to the configured identity provider login mask, which will
   *    then redirect to this route after a successful login.
   *
   * @param {*} model The model of the route
   * @param {Ember.Transition} transition The current transition
   * @param {Object} transition.to The destination of the transition
   * @param {Object} transition.to.queryParams The query params of the transition
   * @param {String} transition.to.queryParams.code The authentication code given by the identity provider
   * @param {String} transition.to.queryParams.state The state given by the identity provider
   */
  async afterModel(_, transition) {
    if (!authEndPoint) {
      throw new Error("Please define the authEndPoint!")
    }

    if (ENV.environment === "development" || ENV.environment === "test") {
      let mirage = ENV["ember-cli-mirage"]
      if (mirage && mirage.enabled) {
        /**
         * PJK 24/11/2021, If Mirage is enabled, then just return.
         * Why would we want to call the Django back end?
         * throw new Error("Why is Mirage enabled!")
         */
        console.log("mirage.enabled", mirage.enabled)
        return
        /**
         * return await this._handleCallbackRequest(
         *   transition.to.queryParams["code"],
         *   this.get("session.data.state")
         * )
         */
      } else if (ENV.APP.KbTestingWithCypress) {
        return await this._handleCallbackRequest(
          "kb-testing-with-cypress",
          this.get("session.data.state")
        )
      }
    }
    const queryParams = transition.to
      ? transition.to.queryParams
      : transition.queryParams
    if (queryParams.code) {
      return await this._handleCallbackRequest(
        queryParams.code,
        queryParams.state
      )
    } else {
      return this._handleRedirectRequest(queryParams)
    }
  }

  get redirectUri() {
    const { protocol, host } = location
    const path = this.router.urlFor(config.redirectEndPoint)
    return `${protocol}//${host}${path}`
  }

  /**
   * Authenticate with the authentication code given by the identity provider in the redirect.
   *
   * This will check if the passed state equals the state in the application to
   * prevent from CSRF attacks.
   *
   * If the authentication fails, it will redirect to this route again but
   * remove application state and query params. This is very unlikely to happen.
   *
   * If the authentication succeeds the default behaviour of ember-simple-auth
   * will apply and redirect to the entry point of the authenticated part of
   * the application.
   *
   * @param {String} code The authentication code passed by the identity provider
   * @param {String} state The state (uuid4) passed by the identity provider
   */
  async _handleCallbackRequest(code, state) {
    let self = this
    if (state !== this.get("session.data.state")) {
      /**
       * WIP - Mirage testing
       * https://www.kbsoftware.co.uk/crm/ticket/1634/
       */
      throw new Error("State did not match")
    }
    await this.session
      .authenticate("authenticator:oidc", {
        code,
        state
      })
      .catch(ex => {
        console.error(ex)
        this.kbMessages.addError("Cannot authenticate", ex)
        self.transitionTo("authenticate")
      })
  }

  /**
   * Redirect the client to the configured identity provider login.
   *
   * This will also generate a uuid4 state which the application stores to the
   * local storage. When authenticating, the state passed by the identity provider needs to
   * match this state, otherwise the authentication will fail to prevent from
   * CSRF attacks.
   */
  _handleRedirectRequest(queryParams) {
    const state = uuid()
    this.session.set("data.state", state)
    /**
     * Store the `nextURL` in the localstorage so when the user returns after
     * the login he can be sent to the initial destination.
     */
    if (!this.session.get("data.nextURL")) {
      this.session.set(
        "data.nextURL",
        this.session.get("attemptedTransition.intent.url")
      )
    }

    // forward `login_hint` query param if present
    const key = loginHintName || "login_hint"

    /**
     * WIP - Mirage testing
     * https://www.kbsoftware.co.uk/crm/ticket/1634/
     *
     * throw new Error("Why are we here? (at 'redirectQueryParams')")
     */
    const redirectQueryParams = [
      `client_id=${clientId}`,
      `redirect_uri=${this.redirectUri}`,
      `response_type=code`,
      `state=${state}`,
      `scope=openid email`,
      queryParams[key] ? `${key}=${queryParams[key]}` : null
    ]
      .filter(Boolean)
      .join("&")
    let redirectUrl = `${getAbsoluteUrl(
      host
    )}${authEndPoint}?${redirectQueryParams}`
    this._redirectToUrl(redirectUrl)
  }

  _redirectToUrl(url) {
    location.replace(url)
  }
}
