import deduplicateHeading from "@kb/ember-addon/utils/deduplicate-heading"
import Route from "@ember/routing/route"
import { action } from "@ember/object"
import { capitalize } from "@ember/string"
import { inject as service } from "@ember/service"
import { pluralize } from "ember-inflector"
import { task, timeout } from "ember-concurrency"

const DEBOUNCE_MS = 500

/** kb-model-route

Provides 4 utility ember-concurrency based tasks for:
  - let model = this.getModel.perform(modelName, modelId)
  - let model = this.createModel.perform(modelName, defaults, relations)
  - let models = this.searchModel.perform(modelName, paramsTheApiExpects)
  - let currentLoggedInContact = this.fetchCurrentContact.perform()

@Example: routes/my-model.js

import KbModelRoute from '@kb/ember-addon/routes/kb-model-route'
export default class MyModelRoute extends KbModelRoute {}
*/

export default class KbModelRoute extends Route {
  @service kbMessages
  @service kbPage
  @service store
  // Override this to have KbModelRoute check for "hasDirtyAttributes"
  checkDirty = []

  @action
  willTransition(transition) {
    if (this.controller.model.routeModels) {
      let models = this.controller.model.routeModels.value
      if (models && this.checkDirty && this.checkDirty.length > 0) {
        for (let modelName of this.checkDirty) {
          if (models[modelName].hasDirtyAttributes) {
            models[modelName].rollbackAttributes()
            let actionName = models[modelName].get("id")
              ? "Editing"
              : "Creating"
            this.kbMessages.addMessage(`${actionName} ${modelName} abandoned`)
            // this.checkDirty.splice(this.checkDirty.indexOf(modelName), 1)
          }
        }
      }
    }
  }

  setupController(controller, model) {
    super.setupController(controller, model)
    let routeId = model.routeId ? String(model.routeId) : null
    let rootRoute = this.routeName.split(".")[0]
    let routeHeading = this.routeName
      .split(".")
      .slice(0, 4)
      .map(n => capitalize(n))
      .join(" ")
    let routeModel = capitalize(rootRoute)
    routeHeading = deduplicateHeading(routeHeading)
    this.controller.set("routeIcon", this.routeIcon)
    this.controller.set("routeHeading", routeHeading)
    this.controller.set("routeModel", routeModel)
    this.controller.set("routeName", this.routeName)
    this.controllerFor(rootRoute).set("routeIcon", this.routeIcon)
    this.controllerFor(rootRoute).set("routeHeading", routeHeading)
    this.controllerFor(rootRoute).set("routeModel", routeModel)
    this.controllerFor(rootRoute).set("routeName", this.routeName)
    this.controllerFor("application").set("routeIcon", this.routeIcon)
    this.controllerFor("application").set("routeHeading", routeHeading)
    this.controllerFor("application").set("routeModel", routeModel)
    this.controllerFor("application").set("routeName", this.routeName)
    this.controllerFor("application").set("routeId", routeId)
    if (this.pageTitle === undefined) {
      this.kbPage.setTitle(routeHeading)
    } else {
      this.kbPage.setTitle(this.pageTitle)
    }
  }

  @action
  refreshModelRoute() {
    this.refresh()
  }

  @task *getApp(slug) {
    let searchApp = yield this.store.query("app", { slug: slug })
    let model = null
    if (searchApp.length) {
      model = yield searchApp.objectAt(0)
    }
    return model
  }

  /** Get a single Model */
  @task *getModel(modelName, modelId) {
    let model = yield this.store.findRecord(modelName, modelId)
    return model
  }

  /** Get a single Model */
  @task *queryModel(modelName, params) {
    let model = yield this.store.queryRecord(modelName, params)
    return model
  }

  /** Create a single Model */
  @task *createModel(modelName, defaults, relations) {
    if (!defaults) defaults = {}
    if (relations) {
      for (let [key, value] of Object.entries(relations)) {
        defaults[key] = yield this.store.findRecord(key, value)
      }
    }
    let model = yield this.store.createRecord(modelName, defaults)
    return model
  }

  /** Search for all models */
  @task *findAll(modelName) {
    yield timeout(DEBOUNCE_MS)
    let models = yield this.store.findAll(modelName)
    return models
  }

  /** Search for all loaded models */
  @task *peekAll(modelName) {
    yield timeout(DEBOUNCE_MS)
    let models = yield this.store.peekAll(modelName)
    // There may be none of these, but do more than peek to find out.
    if (!models.length) {
      models = yield this.findAll.perform(modelName)
    }
    return models
  }

  /** Search for a list of Models */
  @task *searchModel(modelName, params) {
    yield timeout(DEBOUNCE_MS)
    let model = yield this.store.query(modelName, params)
    return model
  }

  /** Access the currentContact service */
  @task *fetchCurrentContact() {
    let currentContact = yield this.currentContact
    let model = yield currentContact
      .loadCurrentContact()
      .then(contact => {
        // Return user or null
        return contact
      })
      .catch(() => {
        this.session.invalidate()
        return null
      })
    return model
  }
}
