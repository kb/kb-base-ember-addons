import Modifier from "ember-modifier"
import { isBlank } from "@ember/utils"

export default class DataTestAttrModifier extends Modifier {
  get firstPositional() {
    if (!isBlank(this.args.positional)) {
      return `kb-${this.args.positional[0]}`
    }
    return "kb"
  }

  get dataTestAttributeName() {
    return `data-test-${this.firstPositional}-id`
  }

  get dataTestAttributeValue() {
    if (this.args.positional.length < 2) {
      return true
    }
    let val = this.args.positional.slice(1).filter(Boolean).join("-")
    return val ? val : true
  }

  didReceiveArguments() {
    let self = this
    this.element.setAttribute(
      this.dataTestAttributeName,
      this.dataTestAttributeValue
    )
    Object.entries(this.args.named).forEach(([name, val]) => {
      self.element.setAttribute(
        `data-test-${this.firstPositional}-${name}-id`,
        val
      )
    })
  }
}
