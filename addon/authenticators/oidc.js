import Base from "ember-simple-auth/authenticators/base"
import ENV from "ember-get-config"
import { Promise } from "rsvp"
import { inject } from "@ember/service"
import { isEmpty } from "@ember/utils"
import { run } from "@ember/runloop"
import composeFetchCall from "@kb/ember-addon/utils/kb-fetch-call"
import config from "@kb/ember-addon/config"
import fetch from "fetch"

const { tokenEndPoint } = config
const { environment } = ENV

export default Base.extend({
  cookies: inject(),

  authenticate({ code }) {
    /**
     * TODO Should we convert this to an 'async' method?
     * For an example, see https://youtu.be/bSWN4_EbTPI?t=394
     */
    return new Promise((resolve, reject) => {
      let self = this
      let csrfToken = this.cookies.read("csrftoken")
      // console.log("ENVIRONMENT", environment)
      // console.log("ENVIRONMENT", environment === "production")
      fetch(
        `${tokenEndPoint}`,
        composeFetchCall(
          "post",
          { code: code },
          csrfToken,
          "",
          environment === "production"
        )
      )
        .then(response => response.json())
        .then(json => {
          run(() => {
            if (json.detail) {
              throw json.detail
            }
            if (!json.contact_id) {
              throw "User not found"
            }
            if (!json.token) {
              throw "Token not provided"
            }
            resolve({ token: json.token, contactId: json.contact_id })
          })
        })
        .catch(ex => {
          reject(ex)
        })
    })
  },

  /*
   * PJK 07/12/2021, I think this method should return a promise?
   *
   * If I uncomment this method, ``ember-simple-auth`` raises an exception
   * referring to ``then`` not being implemented.
   *
   * We do nothing here, so commment it out for now!
   *
   * invalidate(data) {
   *   console.log("oidc::authenticator::invalidate")
   * },
   */

  restore(data) {
    return new Promise((resolve, reject) => {
      if (!isEmpty(data.token)) {
        resolve(data)
      } else {
        reject()
      }
    })
  }
})
