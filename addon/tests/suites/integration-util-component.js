const ROUTEMODEL = "data-test-kb"

function freezeDateAt() {
  return Date.parse("2020-06-01")
}
let originalDateNow = Date.now

export default (hooks = self, testCase) => {
  hooks.beforeEach(function () {
    let self = this
    Date.now = freezeDateAt
    this.buildAttrBlock = givenAttrs => {
      let attrBlock = new String()
      for (let attrName in givenAttrs) {
        let attrPropertyName = attrName.replace(/[^a-zA-Z ]/g, "")
        self.set(`${attrPropertyName}Given`, givenAttrs[attrName])
        attrBlock += `${attrName}={{this.${attrPropertyName}Given}} `
      }
      return attrBlock
    }
  })

  hooks.afterEach(function () {
    Date.now = originalDateNow
  })
}
