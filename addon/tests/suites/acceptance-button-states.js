import setupMirage from "ember-cli-mirage/test-support/setup-mirage"
import { authenticateSession } from "ember-simple-auth/test-support"
import { currentURL, pauseTest, visit } from "@ember/test-helpers"
import { module, test, todo } from "qunit"
import { setupApplicationTest } from "ember-qunit"
import {
  setupRouteAcceptanceUtil,
  PERMISSION_LEVELS
} from "@kb/ember-addon/tests/suites/acceptance-util-setup-route"

export default function (testCase) {
  module(
    `buttonsRouteAcceptanceTest | ${testCase.routeName} | ${testCase.scenario}`,
    function (hooks) {
      setupApplicationTest(hooks)
      setupMirage(hooks)
      setupRouteAcceptanceUtil(hooks, "appadmin", testCase.mocks)

      if (testCase.todo) {
        todo(`todo`, async function (assert) {})
      } else {
        test(`testing that buttons are correctly shown or hidden`, async function (assert) {
          await authenticateSession({
            contactId: PERMISSION_LEVELS["appadmin"],
            token: "faketoken"
          })

          assert.step("1: ASSERT: VISIT DETAIL")
          await visit(testCase.routeURL)
          assert.equal(currentURL(), testCase.routeURL)

          if (testCase.pauseTest) {
            await pauseTest()
          }

          assert.step("2: ASSERT: BUTTON PRESENCE")
          for (let button of testCase.buttonsPresent) {
            assert.dom(button).exists({ count: 1 })
          }

          assert.step("3: ASSERT: BUTTON ABSENCE")
          for (let button of testCase.buttonsAbsent) {
            assert.dom(button).doesNotExist()
          }

          assert.verifySteps([
            "1: ASSERT: VISIT DETAIL",
            "2: ASSERT: BUTTON PRESENCE",
            "3: ASSERT: BUTTON ABSENCE"
          ])
        })
      }
    }
  )
}
