/**
 * @Example
 * import isReactive from "@kb/ember-addon/tests/suites/integration-component-is-reactive"
 *
isReactive({
  ...component,
  // NB: We're testing on behalf of funcHookName here; not changeHook itself.
  funcHookName: "changeHook",
  isReactive: [{
    scenario: "click toggles value",
    givenAttrs: { "@key": TESTKEY, "@value": false },
    action: "click",

    expectParameters: [[TESTKEY, true]]
  }]
})
*/
import {
  click,
  fillIn,
  render,
  pauseTest,
  settled,
  typeIn
} from "@ember/test-helpers"
import { module, test, todo } from "qunit"
import { selectChoose } from "ember-power-select/test-support"
import { selectFiles } from "ember-file-upload/test-support"
import { upload } from "ember-file-upload/mirage"
import { setupRenderingTest } from "ember-qunit"
import { hbs } from "ember-cli-htmlbars"
import registerPowerSelectHelpers from "ember-power-select/test-support/helpers"
import setupIntegrationComponentUtil from "@kb/ember-addon/tests/suites/integration-util-component"

export default function (testCase) {
  module(
    `Integration Test | Component | ${testCase.componentPath} is-reactive`,
    function (hooks) {
      if (testCase.todo) {
        todo(`todo`, async function (assert) {})
      } else {
        setupRenderingTest(hooks)
        setupIntegrationComponentUtil(hooks, testCase)

        test(`${testCase.componentName}.${testCase.funcHookName}(${testCase.scenario})`, async function (assert) {
          if (testCase.isReactive) {
            let expectReactionsCount = testCase.isReactive
              .map(opts =>
                opts.action !== "renderOnly" ? opts.expectParameters.length : 1
              )
              .reduce((count, callsExpected) => count + callsExpected)
            expectReactionsCount *= 2 // IT REACTED, PARAMETERS AS EXPECTED
            expectReactionsCount += testCase.isReactive.length // 1 REACTABLE for each action
            expectReactionsCount += 2 // 1 step + verifySteps
            assert.expect(expectReactionsCount)

            assert.step("1: TESTING isReactive CASES")

            for (let caseScenario of testCase.isReactive) {
              let SCENARIO = caseScenario.scenario
              let attrBlock = this.buildAttrBlock(caseScenario.givenAttrs)

              // funcHandler will test the result.
              let reactionCount = 0
              this.set("reactionHook", function (...hookParameters) {
                assert.ok(true, `${SCENARIO} IT REACTED`)
                assert.deepEqual(
                  hookParameters,
                  caseScenario.expectParameters[reactionCount],
                  `${SCENARIO} PARAMETERS AS EXPECTED`
                )
                reactionCount += 1
              })

              if (caseScenario.givenHBS) {
                await render(caseScenario.givenHBS)
              } else {
                const content = Ember.HTMLBars.compile(`
                <${testCase.componentClass}
                  ${attrBlock}
                  @${testCase.funcHookName}={{this.reactionHook}}
                />
              `)
                await render(content)
              }

              if (caseScenario.pauseTest) {
                await pauseTest()
              }

              assert
                .dom(caseScenario.givenDataTestId)
                .exists({ count: 1 }, `${SCENARIO} REACTABLE`)

              if (caseScenario.action === "click") {
                await click(caseScenario.givenDataTestId)
              } else if (caseScenario.action === "fillIn") {
                await fillIn(
                  caseScenario.givenDataTestId,
                  caseScenario.fillInValue
                )
              } else if (caseScenario.action === "selectChoose") {
                await selectChoose(
                  caseScenario.givenDataTestId,
                  caseScenario.selectValue
                )
              } else if (caseScenario.action === "upload") {
                await upload(
                  caseScenario.givenDataTestId,
                  ...caseScenario.uploadValue
                )
              } else if (caseScenario.action === "renderOnly") {
                // do nothing
              }
              await settled()
            }
          }

          assert.verifySteps(["1: TESTING isReactive CASES"])
        })
      }
    }
  )
}
