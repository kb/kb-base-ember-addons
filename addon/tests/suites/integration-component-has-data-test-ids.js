/**
 * @Example
 * import hasDataTestIds from "@kb/ember-addon/tests/suites/integration-component-has-data-test-ids"
 *
 hasDataTestIds({
   ...component,
   hasDataTestIds: [
     {
       givenAttrs: { "@key": TESTKEY },

       givenDataTestId: "[data-test-kb-form-label-id]"
     },
     {
       givenAttrs: { "@key": TESTKEY },

       givenDataTestId: "[data-test-kb-form-error-id]"
     }
   ]
 })
 */
import { click, pauseTest, render } from "@ember/test-helpers"
import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { hbs } from "ember-cli-htmlbars"
import setupIntegrationComponentUtil from "@kb/ember-addon/tests/suites/integration-util-component"

export default function (testCase) {
  module(
    `Integration Test | Component | ${testCase.componentPath} has-data-test-id`,
    function (hooks) {
      setupRenderingTest(hooks)
      setupIntegrationComponentUtil(hooks, testCase)

      test(`${testCase.componentName} ${testCase.scenario}`, async function (assert) {
        if (testCase.hasDataTestIds) {
          assert.step("1: ASSERT: COMPONENT DATA_TEST_IDS")

          for (let caseScenario of testCase.hasDataTestIds) {
            let SCENARIO = caseScenario.scenario
            let attrBlock = this.buildAttrBlock(caseScenario.givenAttrs)

            const content = Ember.HTMLBars.compile(`
              <${testCase.componentClass} ${attrBlock}/>
            `)
            await render(content)

            if (testCase.pauseTest) {
              await pauseTest()
            }

            if (!caseScenario.negateExpectation) {
              assert
                .dom(caseScenario.givenDataTestId)
                .exists(
                  { count: caseScenario.count || 1 },
                  `${SCENARIO} COMPONENT data-test-attr FOUND`
                )
            } else {
              assert
                .dom(caseScenario.givenDataTestId)
                .doesNotExist(`${SCENARIO} COMPONENT NOT FOUND`)
            }
          }
        }

        assert.verifySteps(["1: ASSERT: COMPONENT DATA_TEST_IDS"])
      })
    }
  )
}
