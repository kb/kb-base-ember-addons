/**
 * @Example
 * import isChecked from "@kb/ember-addon/tests/suites/integration-component-is-checked"
 *
 isChecked({
   ...component,
   isChecked: [
     { givenAttrs: { "@key": TESTKEY, "@value": true }, negateExpectation: FALSE },
     { givenAttrs: { "@key": TESTKEY, "@value": false }, negateExpectation: true }
   ]
 })
 */
import { click, pauseTest, render } from "@ember/test-helpers"
import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { hbs } from "ember-cli-htmlbars"
import setupIntegrationComponentUtil from "@kb/ember-addon/tests/suites/integration-util-component"

export default function (testCase) {
  module(
    `Integration Test | Component | ${testCase.componentPath} is-checked`,
    function (hooks) {
      setupRenderingTest(hooks)
      setupIntegrationComponentUtil(hooks, testCase)

      test(`${testCase.componentName} ${testCase.scenario}`, async function (assert) {
        if (testCase.isChecked) {
          assert.step("1: TESTING isChecked CASES")

          for (let caseScenario of testCase.isChecked) {
            let SCENARIO = caseScenario.scenario
            let attrBlock = this.buildAttrBlock(caseScenario.givenAttrs)

            const content = Ember.HTMLBars.compile(`
              <${testCase.componentClass} ${attrBlock}/>
            `)
            await render(content)

            if (testCase.pauseTest) {
              await pauseTest()
            }

            if (!caseScenario.negateExpectation) {
              assert
                .dom(caseScenario.givenDataTestId)
                .isChecked(`${SCENARIO} CHECKED`)
            } else {
              assert
                .dom(caseScenario.givenDataTestId)
                .isNotChecked(`${SCENARIO} NOT CHECKED`)
            }
          }
        }

        assert.verifySteps(["1: TESTING isChecked CASES"])
      })
    }
  )
}
