import { click, currentURL, pauseTest, visit } from "@ember/test-helpers"
import { module, test, todo } from "qunit"
import { setupApplicationTest } from "ember-qunit"
import setupMirage from "ember-cli-mirage/test-support/setup-mirage"
import { setupRouteAcceptanceUtil } from "@kb/ember-addon/tests/suites/acceptance-util-setup-route"

const ROUTEMODEL = "data-test-kb"
const ROWACTION = "confirm-action-id"

export default function (testCase) {
  module(
    `removeRouteAcceptanceTest | ${testCase.routeName} |  ${testCase.scenario}`,
    function (hooks) {
      setupApplicationTest(hooks)
      setupMirage(hooks)
      setupRouteAcceptanceUtil(hooks, "appadmin", testCase.mocks)

      if (testCase.todo) {
        todo(`todo`, async function (assert) {})
      } else {
        test(`testing that that clicking removes this relation from ${ROUTEMODEL}`, async function (assert) {
          let REMOVEBUTTON = `[${ROUTEMODEL}-${ROWACTION}='${testCase.routeName}']`

          assert.step("1: ASSERT: VISIT MANY LIST")
          await visit(testCase.routeURL)
          assert.equal(currentURL(), testCase.routeURL)

          if (testCase.pauseTest) {
            await pauseTest()
          }

          assert.step("2: ASSERT: LIST INTEGRITY")
          assert.dom(REMOVEBUTTON).exists({ count: 3 })

          assert.step("3: ASSERT: CAN REMOVE")
          await click(REMOVEBUTTON)
          assert.dom(REMOVEBUTTON).exists({ count: 2 })

          assert.verifySteps([
            "1: ASSERT: VISIT MANY LIST",
            "2: ASSERT: LIST INTEGRITY",
            "3: ASSERT: CAN REMOVE"
          ])
        })
      }
    }
  )
}
