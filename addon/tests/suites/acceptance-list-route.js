import setupMirage from "ember-cli-mirage/test-support/setup-mirage"
import { authenticateSession } from "ember-simple-auth/test-support"
import { click, currentURL, pauseTest, visit } from "@ember/test-helpers"
import { module, test, todo } from "qunit"
import { setupApplicationTest } from "ember-qunit"
import {
  setupRouteAcceptanceUtil,
  PERMISSION_LEVELS
} from "@kb/ember-addon/tests/suites/acceptance-util-setup-route"

const ROUTEMODEL = "data-test-kb"

export default function (testCase) {
  module(
    `listRouteAcceptanceTest | ${testCase.routeName} |  ${testCase.scenario}`,
    function (hooks) {
      setupApplicationTest(hooks)
      setupMirage(hooks)
      setupRouteAcceptanceUtil(hooks, "appadmin", testCase.mocks)

      if (testCase.todo) {
        todo(`todo`, async function (assert) {})
      } else {
        test(`testing that renders the list correctly`, async function (assert) {
          let CONTAINEDBY = `${ROUTEMODEL}-list-table-id`
          let LISTEDBY = `${ROUTEMODEL}-list-table-row-id`

          await authenticateSession({
            contactId: PERMISSION_LEVELS["appadmin"],
            token: "faketoken"
          })

          assert.step("1: ASSERT: VISIT LIST")
          await visit(testCase.routeURL)
          assert.equal(currentURL(), testCase.routeURL)

          if (testCase.pauseTest) {
            await pauseTest()
          }

          assert.step("2: ASSERT: LIST INTEGRITY")
          assert.dom(`[${LISTEDBY}]`).exists({ count: testCase.listCount })

          assert.dom(`[${LISTEDBY}-id='1']`).exists({ count: 1 })
          assert.dom(`[${LISTEDBY}-id='2']`).exists({ count: 1 })
          assert.dom(`[${LISTEDBY}-id='3']`).exists({ count: 1 })
          if (testCase.listItemURL) {
            assert.dom(`[${LISTEDBY}-id='1'] a`).exists({ count: 1 })
          }

          assert.step("3: ASSERT: DISPLAY INTEGRITY")
          if (testCase.containsText) {
            for (let lookFor of testCase.containsText) {
              assert.dom(`[${CONTAINEDBY}]`).containsText(lookFor)
            }
          }

          assert.step("4: ASSERT: LIST LINKS TO DETAIL")
          if (testCase.listItemURL) {
            assert
              .dom(`[${LISTEDBY}-id='1'] a`)
              .hasAttribute("href", testCase.listItemURL)
            await click(`[${LISTEDBY}-id='1'] a`)
            assert.equal(currentURL(), testCase.listItemURL)
          }

          assert.verifySteps([
            "1: ASSERT: VISIT LIST",
            "2: ASSERT: LIST INTEGRITY",
            "3: ASSERT: DISPLAY INTEGRITY",
            "4: ASSERT: LIST LINKS TO DETAIL"
          ])
        })
      }
    }
  )
}
