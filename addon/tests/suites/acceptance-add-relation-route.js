import setupMirage from "ember-cli-mirage/test-support/setup-mirage"
import { authenticateSession } from "ember-simple-auth/test-support"
import { click, currentURL, pauseTest, visit } from "@ember/test-helpers"
import { module, test, todo } from "qunit"
import { setupApplicationTest } from "ember-qunit"
import {
  setupRouteAcceptanceUtil,
  PERMISSION_LEVELS
} from "@kb/ember-addon/tests/suites/acceptance-util-setup-route"

const ROUTEMODEL = "data-test-kb"
const ROWACTION = "confirm-action-id"
const TOOLBUTTON = "tool-bar-tool-id"

export default function (testCase) {
  module(
    `addRelationRouteAcceptanceTest | ${testCase.routeName} | ${testCase.scenario}`,
    function (hooks) {
      setupApplicationTest(hooks)
      setupMirage(hooks)
      setupRouteAcceptanceUtil(hooks, "appadmin", testCase.mocks)

      if (testCase.todo) {
        todo(`todo`, async function (assert) {})
      } else {
        test(`testing that adds a new relation from the list and removes one`, async function (assert) {
          await authenticateSession({
            contactId: PERMISSION_LEVELS["appadmin"],
            token: "faketoken"
          })

          let addRouteName = testCase.routeName
          let ADDBUTTON = `[${ROUTEMODEL}-${ROWACTION}='${addRouteName}']`
          let removeRouteName = addRouteName.substring(
            0,
            addRouteName.length - 4
          )
          let REMOVEBUTTON = `[${ROUTEMODEL}-${ROWACTION}='${removeRouteName}']`
          let LISTEDBY = `${ROUTEMODEL}-list-table-row-id`

          assert.step("1: ASSERT: VISIT MANY LIST")
          await visit(testCase.routeURL)
          assert.equal(currentURL(), testCase.routeURL)

          if (testCase.pauseTest) {
            await pauseTest()
          }

          assert.step("2: ASSERT: NAVIGATE TO ADD2MANY LIST")
          await click(`[${ROUTEMODEL}-${TOOLBUTTON}='${testCase.routeName}']`)
          assert.equal(currentURL(), testCase.route2Add)

          if (testCase.pauseTest) {
            await pauseTest()
          }

          console.log(REMOVEBUTTON)

          assert.step("3: ASSERT: LIST INTEGRITY")
          assert.dom(REMOVEBUTTON).exists({ count: testCase.initialHaveList })
          assert.dom(ADDBUTTON).exists({ count: testCase.initialAddList })

          assert.step("4: ASSERT: LIST LINKS TO OPPOSITE RELATION")

          if (testCase.listItemURL) {
            assert
              .dom(`[${LISTEDBY}-id='1'] a`)
              .hasAttribute("href", testCase.listItemURL)
          }

          assert.step("5: ASSERT: CAN REMOVE")
          await click(REMOVEBUTTON)

          if (testCase.pauseTest) {
            await pauseTest()
          }

          assert
            .dom(REMOVEBUTTON)
            .exists({ count: testCase.initialHaveList - 1 })
          assert.dom(ADDBUTTON).exists({ count: testCase.initialAddList + 1 })

          assert.step("6: ASSERT: CAN ADD")
          await click(ADDBUTTON)

          if (testCase.pauseTest) {
            await pauseTest()
          }

          assert.dom(REMOVEBUTTON).exists({ count: testCase.finalHaveList })
          assert.dom(ADDBUTTON).exists({ count: testCase.finalAddList })

          assert.step("7: ASSERT: NAVIGATE WHEN DONE")

          await click(
            `[${ROUTEMODEL}-${TOOLBUTTON}='${testCase.routeName}.done']`
          )

          assert.equal(currentURL(), testCase.routeURL)

          assert.verifySteps([
            "1: ASSERT: VISIT MANY LIST",
            "2: ASSERT: NAVIGATE TO ADD2MANY LIST",
            "3: ASSERT: LIST INTEGRITY",
            "4: ASSERT: LIST LINKS TO OPPOSITE RELATION",
            "5: ASSERT: CAN REMOVE",
            "6: ASSERT: CAN ADD",
            "7: ASSERT: NAVIGATE WHEN DONE"
          ])
        })
      }
    }
  )
}
