import { authenticateSession } from "ember-simple-auth/test-support"
import sharedScenario from "front/mirage/scenarios/shared"

/**
 * PJK 24/11/2021, Put the ``loggedout`` state first because once we login
 * I don't seem to be able to logout!
 */
let PERMISSION_LEVELS = {
  loggedout: 0,
  authenticated: 101,
  manager: 102,
  appadmin: 103
}

let setupRouteAcceptanceUtil = function (
  hooks = self,
  targetPermissionLevel,
  mocks
) {
  hooks.beforeEach(function () {
    if (!this.owner) {
      throw new Error(
        "You must call setupMirage() before calling setupRouteAcceptanceUtil()."
      )
    }
    sharedScenario(this.server)
    let CURRENT_CONTACT = {
      last_name: "Smith",
      isAppAdministrator: false,
      isManager: false
    }
    this.server.create(
      "contact",
      Object.assign(CURRENT_CONTACT, {
        id: 101,
        first_name: "AuthenticatedJoe",
        isAppAdministrator: false,
        isManager: false
      })
    )
    this.server.create(
      "contact",
      Object.assign(CURRENT_CONTACT, {
        id: 102,
        first_name: "ManagerJoe",
        isAppAdministrator: false,
        isManager: true
      })
    )
    this.server.create(
      "contact",
      Object.assign(CURRENT_CONTACT, {
        id: 103,
        first_name: "AppAdminJoe",
        isAppAdministrator: true,
        isManager: false
      })
    )
    /**
     * PJK 24/11/2021, Is this duplicate code?
     * See ``addon/tests/suites/acceptance-perm-check-route.js``
     *
     * if (PERMISSION_LEVELS[targetPermissionLevel]) {
     *   authenticateSession({
     *     contactId: PERMISSION_LEVELS[targetPermissionLevel],
     *     token: "faketoken"
     *   })
     * }
     */
    for (let model in mocks) {
      for (let givenAttrs of mocks[model]) {
        let it = this.server.create(model, givenAttrs)
      }
    }
  })
}

export { setupRouteAcceptanceUtil, PERMISSION_LEVELS }
