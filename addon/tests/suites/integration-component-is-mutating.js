/**
* @Example
* import isMutating from "@kb/ember-addon/tests/suites/integration-component-is-mutating"
*
isMutating({
  ...component,
  isMutating: [{
    scenario: "confirm on step 1",
    givenAttrs: { "step": 1 },

    expectIsMutating: "You are on Step 1"
  }],
})
*/
import { click, fillIn, pauseTest, render } from "@ember/test-helpers"
import { module, test, todo } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { hbs } from "ember-cli-htmlbars"
import setupIntegrationComponentUtil from "@kb/ember-addon/tests/suites/integration-util-component"

export default function (testCase) {
  module(
    `Integration Test | Component | ${testCase.componentPath} is-mutating`,
    function (hooks) {
      setupRenderingTest(hooks)
      setupIntegrationComponentUtil(hooks, testCase)

      todo(
        `${testCase.componentName} ${testCase.scenario}`,
        async function (assert) {
          if (testCase.isMutating) {
            assert.step("1: TESTING isMutating CASES")

            for (let caseScenario of testCase.isMutating) {
              let SCENARIO = caseScenario.scenario

              let inputValue = caseScenario.givenAttrs["@value"]
              // delete caseScenario.givenAttrs["@value"]
              let attrBlock = this.buildAttrBlock(caseScenario.givenAttrs)

              // funcHandler will test the result.
              let reactionCount = 0
              this.set("mutatableValue", inputValue)
              this.set("reactionHook", function (...hookParameters) {
                assert.ok(true, `${SCENARIO} IT REACTED`)
                assert.deepEqual(
                  hookParameters,
                  caseScenario.expectParameters[reactionCount],
                  `${SCENARIO} PARAMETERS AS EXPECTED`
                )
                reactionCount += 1
              })

              const content = Ember.HTMLBars.compile(`
              <${testCase.componentClass}
                ${attrBlock}
                @value={{this.mutatableValue}}
                @${testCase.funcHookName}={{this.reactionHook}}
              />
            `)
              await render(content)

              if (testCase.pauseTest) {
                await pauseTest()
              }

              if (caseScenario.action === "click") {
                await click(caseScenario.givenDataTestId)
              } else if (caseScenario.action === "fillIn") {
                await fillIn(
                  caseScenario.givenDataTestId,
                  caseScenario.fillInValue
                )
              } else if (caseScenario.action === "renderOnly") {
                // do nothing
              }

              if (testCase.pauseTest) {
                await pauseTest()
              }

              // DDAU?
              if (!caseScenario.negateExpectation) {
                assert.equal(
                  this.get("mutatableValue"),
                  caseScenario.fillInValue,
                  `${SCENARIO} PASSED VALUE WAS MUTATED`
                )
              } else {
                assert.notEqual(
                  this.get("mutatableValue"),
                  caseScenario.fillInValue,
                  `${SCENARIO} PASSED VALUE NOT MUTABLE`
                )
              }
            }
          }

          assert.verifySteps(["1: TESTING isMutating CASES"])
        }
      )
    }
  )
}
