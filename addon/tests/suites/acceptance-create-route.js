import {
  click,
  currentURL,
  fillIn,
  pauseTest,
  visit
} from "@ember/test-helpers"
import registerPowerSelectHelpers from "ember-power-select/test-support/helpers"
import setupMirage from "ember-cli-mirage/test-support/setup-mirage"
import { authenticateSession } from "ember-simple-auth/test-support"
import { module, test, todo } from "qunit"
import { selectChoose } from "ember-power-select/test-support"
import { setupApplicationTest } from "ember-qunit"
import { upload } from "ember-file-upload/mirage"
import {
  setupRouteAcceptanceUtil,
  PERMISSION_LEVELS
} from "@kb/ember-addon/tests/suites/acceptance-util-setup-route"

registerPowerSelectHelpers()

const ROUTEMODEL = "data-test-kb"
const FORMSUBMITBUTTON = "submit-button-id"

export default function (testCase) {
  module(
    `createRouteAcceptanceTest | ${testCase.routeName} | ${testCase.scenario}`,
    function (hooks) {
      setupApplicationTest(hooks)
      setupMirage(hooks)
      setupRouteAcceptanceUtil(hooks, "appadmin", testCase.mocks)

      if (testCase.todo) {
        todo(`todo`, async function (assert) {})
      } else {
        test(`testing that submits form with new record then in list`, async function (assert) {
          let CONTAINEDBY = `[${ROUTEMODEL}-dl-id-id='1']`

          await authenticateSession({
            contactId: PERMISSION_LEVELS["appadmin"],
            token: "faketoken"
          })

          assert.step("1: ASSERT: VISIT FORM")

          await visit(testCase.routeURL)

          assert.equal(currentURL(), testCase.routeURL)

          assert.step("2: FORM FILL IN")
          for (let fill of testCase.fillIn) {
            await fillIn(fill.sel, fill.val)
          }
          for (let sel of testCase.click) {
            await click(sel)
          }
          for (let fill of testCase.select) {
            await selectChoose(fill.sel, fill.val)
          }
          for (let file of testCase.selectFiles) {
            await upload(file.sel, file.val)
          }

          if (testCase.pauseTest) {
            await pauseTest()
          }

          assert.step("3: SUBMIT FORM")
          await click(`[${ROUTEMODEL}-${FORMSUBMITBUTTON}]`)

          assert.step("4: ASSERT: REDIRECTED")
          assert.equal(currentURL(), testCase.redirectURL)

          if (testCase.pauseTest) {
            await pauseTest()
          }

          assert.step("5: ASSERT: EDIT INTEGRITY")
          for (let lookFor of testCase.containsText) {
            assert.dom(CONTAINEDBY).containsText(lookFor)
          }

          if (testCase.pauseTest) {
            await pauseTest()
          }

          assert.verifySteps([
            "1: ASSERT: VISIT FORM",
            "2: FORM FILL IN",
            "3: SUBMIT FORM",
            "4: ASSERT: REDIRECTED",
            "5: ASSERT: EDIT INTEGRITY"
          ])
        })
      }
    }
  )
}
