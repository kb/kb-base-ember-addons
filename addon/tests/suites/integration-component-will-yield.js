/**
 * @Example
 * import willYield from "@kb/ember-addon/tests/suites/integration-component-will-yield"
 *
 * willYield({
 *   ...component,
 *   dothNot: true
 * })
 */
import { click, pauseTest, render } from "@ember/test-helpers"
import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { hbs } from "ember-cli-htmlbars"
import setupIntegrationComponentUtil from "@kb/ember-addon/tests/suites/integration-util-component"

export default function (testCase) {
  module(
    `Integration Test | Component | ${testCase.componentPath} will-yield`,
    function (hooks) {
      setupRenderingTest(hooks)
      setupIntegrationComponentUtil(hooks, testCase)

      test(`${testCase.componentName} ${testCase.scenario}`, async function (assert) {
        let SCENARIO = testCase.scenario
        let attrBlock = this.buildAttrBlock(testCase.givenAttrs)

        assert.step("1: RENDER FOR WillYield TEST")

        const content = Ember.HTMLBars.compile(`
          <${testCase.componentClass} ${attrBlock}>
              template block text
          </${testCase.componentClass}>
        `)
        await render(content)

        if (testCase.pauseTest) {
          await pauseTest()
        }

        assert.step("2: ASSERT: TO YIELD OR NOT TO YIELD")

        if (testCase.negateExpectation) {
          assert
            .dom(this.element)
            .doesNotContainText("template block text", "NOT YIELDING CONTENT")
        } else {
          assert
            .dom(this.element)
            .containsText("template block text", "YIELDED")
        }

        assert.verifySteps([
          "1: RENDER FOR WillYield TEST",
          "2: ASSERT: TO YIELD OR NOT TO YIELD"
        ])
      })
    }
  )
}
