import setupMirage from "ember-cli-mirage/test-support/setup-mirage"
import { authenticateSession } from "ember-simple-auth/test-support"
import { click, currentURL, pauseTest, visit } from "@ember/test-helpers"
import { module, test, todo } from "qunit"
import { setupApplicationTest } from "ember-qunit"
import {
  setupRouteAcceptanceUtil,
  PERMISSION_LEVELS
} from "@kb/ember-addon/tests/suites/acceptance-util-setup-route"

const ROUTEMODEL = "data-test-kb"
const DELETEBUTTON = "delete-button"

export default function (testCase) {
  module(
    `deleteRouteAcceptanceTest | ${testCase.routeName} | ${testCase.scenario}`,
    function (hooks) {
      setupApplicationTest(hooks)
      setupMirage(hooks)
      setupRouteAcceptanceUtil(hooks, "appadmin", testCase.mocks)

      if (testCase.todo) {
        todo(`todo`, async function (assert) {})
      } else {
        test(`testing that clicks confirm button then gone from list`, async function (assert) {
          let CONTAINEDBY = `[${ROUTEMODEL}-dl-id-id='1']`

          await authenticateSession({
            contactId: PERMISSION_LEVELS["appadmin"],
            token: "faketoken"
          })

          assert.step("1: ASSERT: VISIT DELETE")
          await visit(testCase.routeURL)
          assert.equal(currentURL(), testCase.routeURL)

          if (testCase.pauseTest) {
            await pauseTest()
          }

          assert.step("2: CLICK CONFIRM DELETE")
          await click(
            `[${ROUTEMODEL}-${DELETEBUTTON}-id='${testCase.routeName}']`
          )

          if (testCase.pauseTest) {
            await pauseTest()
          }

          assert.step("3: ASSERT: REDIRECTED")
          assert.equal(currentURL(), testCase.redirectURL)

          if (testCase.pauseTest) {
            await pauseTest()
          }

          assert.step("4: ASSERT: EDIT INTEGRITY")
          assert.dom(CONTAINEDBY).doesNotExist()

          assert.verifySteps([
            "1: ASSERT: VISIT DELETE",
            "2: CLICK CONFIRM DELETE",
            "3: ASSERT: REDIRECTED",
            "4: ASSERT: EDIT INTEGRITY"
          ])
        })
      }
    }
  )
}
