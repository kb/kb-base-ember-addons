/**
 * @Example
 * import isAccessible from "@kb/ember-addon/tests/suites/integration-component-is-accessible"
 *
 * isAccessible({
 *   ...component,
 *   isAccessibles: [
 *     // [ caseScenario.givenAttrs, caseScenario.negateExpectation, dataTestIdToExpectOrNot ],
 *   ],
 * })
 */
import { click, pauseTest, render } from "@ember/test-helpers"
import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { hbs } from "ember-cli-htmlbars"
import setupIntegrationComponentUtil from "@kb/ember-addon/tests/suites/integration-util-component"

export default function (testCase) {
  module(
    `Integration Test | Component | ${testCase.componentPath} is-accessible`,
    function (hooks) {
      setupRenderingTest(hooks)
      setupIntegrationComponentUtil(hooks, testCase)

      test(`${testCase.componentName} ${testCase.scenario}`, async function (assert) {
        assert.step("1: RENDER FOR IsAccessible TEST")

        const content = Ember.HTMLBars.compile(`
          <${testCase.classified}
            @dataTestRole="delectable"
          />
        `)
        await render(content)

        if (testCase.pauseTest) {
          await pauseTest()
        }

        assert.dom(caseScenario.givenDataTestId).exists({ count: 1 })
        assert
          .dom(caseScenario.givenDataTestId)
          .hasAttribute("dataTestRole", "delectable")
        assert.dom(caseScenario.givenDataTestId).hasNoText()

        assert.step("2: TESTING isAccessible CASES")

        if (testCase.isAccessibles) {
          for (let caseScenario of testCase.isAccessibles) {
            let SCENARIO = caseScenario.scenario
            assert
              .dom(dom)
              .isAccessible(
                caseScenario.expectAccessible,
                `${SCENARIO} IS ACCESSIBLE`
              )
          }
        }

        assert.verifySteps([
          "1: RENDER FOR IsAccessible TEST",
          "2: TESTING isAccessible CASES"
        ])
      })
    }
  )
}
