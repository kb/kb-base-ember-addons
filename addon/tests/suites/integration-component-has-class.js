/**
* @Example
* import hasClass from "@kb/ember-addon/tests/suites/integration-component-has-class"
*
hasClass({
  ...component,
  hasClasses: [{
    scenario: "confirm onto step 2",
    givenAttrs: { "step": 1 },
    action: "fillIn",
    fillInValue: "Completed Step 1"

    expectHasClass: "Continue to Step 2"
  }]
})
*/
import { click, fillIn, pauseTest, render } from "@ember/test-helpers"
import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { hbs } from "ember-cli-htmlbars"
import setupIntegrationComponentUtil from "@kb/ember-addon/tests/suites/integration-util-component"

export default function (testCase) {
  module(
    `Integration Test | Component | ${testCase.componentPath} has-class`,
    function (hooks) {
      setupRenderingTest(hooks)
      setupIntegrationComponentUtil(hooks, testCase)

      test(`${testCase.componentName} ${testCase.scenario}`, async function (assert) {
        if (testCase.hasClasses) {
          assert.step("1: TESTING hasClasses CASES")

          for (let caseScenario of testCase.hasClasses) {
            let SCENARIO = caseScenario.scenario
            let attrBlock = this.buildAttrBlock(caseScenario.givenAttrs)

            const content = Ember.HTMLBars.compile(`
              <${testCase.componentClass}
                ${attrBlock}
                @${testCase.funcHookName}={{this.reactionHook}}
              />
            `)
            await render(content)

            if (testCase.pauseTest) {
              await pauseTest()
            }

            for (let expectClass of caseScenario.expectClasses) {
              if (!caseScenario.negateExpectation) {
                assert.dom(caseScenario.givenDataTestId).hasClass(expectClass)
              } else {
                assert
                  .dom(caseScenario.givenDataTestId)
                  .doesNotHaveClass(expectClass)
              }
            }
          }
        }

        assert.verifySteps(["1: TESTING hasClasses CASES"])
      })
    }
  )
}
