import {
  click,
  currentURL,
  fillIn,
  pauseTest,
  visit
} from "@ember/test-helpers"
import registerPowerSelectHelpers from "ember-power-select/test-support/helpers"
import setupMirage from "ember-cli-mirage/test-support/setup-mirage"
import { authenticateSession } from "ember-simple-auth/test-support"
import { module, test, todo } from "qunit"
import { selectChoose } from "ember-power-select/test-support"
import { setupApplicationTest } from "ember-qunit"
import { upload } from "ember-file-upload/mirage"
import {
  setupRouteAcceptanceUtil,
  PERMISSION_LEVELS
} from "@kb/ember-addon/tests/suites/acceptance-util-setup-route"

registerPowerSelectHelpers()

const ROUTEMODEL = "data-test-kb"
const FORMSUBMITBUTTON = "submit-button-id"
const TOOLBUTTON = "tool-bar-tool-id"

export default function (testCase) {
  module(
    `newRelationRouteAcceptance | ${testCase.routeName} |  ${testCase.scenario}`,
    function (hooks) {
      setupApplicationTest(hooks)
      setupMirage(hooks)
      setupRouteAcceptanceUtil(hooks, "appadmin", testCase.mocks)

      if (testCase.todo) {
        todo(`todo`, async function (assert) {})
      } else {
        test(`testing that creates new record and then belongsTo ${ROUTEMODEL}`, async function (assert) {
          await authenticateSession({
            contactId: PERMISSION_LEVELS["appadmin"],
            token: "faketoken"
          })

          let CONTAINEDBY = `[${ROUTEMODEL}-dl-id]`
          let LISTEDBY = `[${ROUTEMODEL}-list-table-row-id-id='1']`

          assert.step("1: ASSERT: VISIT MANY LIST")
          await visit(testCase.routeURL)
          assert.equal(currentURL(), testCase.routeURL)

          assert.step("2: ASSERT: NAVIGATE TO NEW FORM")
          await click(`[${ROUTEMODEL}-${TOOLBUTTON}='${testCase.routeName}']`)
          assert.equal(currentURL(), testCase.route2New)

          if (testCase.pauseTest) {
            await pauseTest()
          }

          assert.step("3: FORM FILL IN")
          for (let fill of testCase.fillIn) {
            await fillIn(fill.sel, fill.val)
          }
          for (let sel of testCase.click) {
            await click(sel)
          }
          for (let fill of testCase.select) {
            await selectChoose(fill.sel, fill.val)
          }
          for (let file of testCase.selectFiles) {
            await upload(file.sel, file.val)
          }

          assert.step("4: SUBMIT FORM")
          await click(`[${ROUTEMODEL}-${FORMSUBMITBUTTON}]`)

          assert.step("5: ASSERT: REDIRECTED")
          assert.equal(currentURL(), testCase.routeURL)

          assert.step("6: ASSERT: NEW MANY MODEL INTEGRITY")
          // This test fails the addition test because belongsTo relationships are
          // not included in the requestBody payload: e.g. contact.additional and
          // the relationship fails.
          if (testCase.containsText) {
            for (let lookFor of testCase.containsText) {
              assert.dom(LISTEDBY).containsText(lookFor)
            }
          }
          if (testCase.doesNotContainText) {
            for (let lookFor of testCase.doesNotContainText) {
              assert.dom(CONTAINEDBY).doesNotContainText(lookFor)
            }
          }

          assert.verifySteps([
            "1: ASSERT: VISIT MANY LIST",
            "2: ASSERT: NAVIGATE TO NEW FORM",
            "3: FORM FILL IN",
            "4: SUBMIT FORM",
            "5: ASSERT: REDIRECTED",
            "6: ASSERT: NEW MANY MODEL INTEGRITY"
          ])
        })
      }
    }
  )
}
