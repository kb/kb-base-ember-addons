import setupMirage from "ember-cli-mirage/test-support/setup-mirage"
import { authenticateSession } from "ember-simple-auth/test-support"
import { currentURL, pauseTest, visit } from "@ember/test-helpers"
import { module, test, todo } from "qunit"
import { setupApplicationTest } from "ember-qunit"
import {
  setupRouteAcceptanceUtil,
  PERMISSION_LEVELS
} from "@kb/ember-addon/tests/suites/acceptance-util-setup-route"

const ROUTEMODEL = "data-test-kb"
const TOOLBUTTON = "tool-bar-tool-id"

export default function (testCase) {
  module(
    `noNewRelationRouteAcceptance | ${testCase.routeName} |  ${testCase.scenario}`,
    function (hooks) {
      setupApplicationTest(hooks)
      setupMirage(hooks)
      setupRouteAcceptanceUtil(hooks, "appadmin", testCase.mocks)

      if (testCase.todo) {
        todo(`todo`, async function (assert) {})
      } else {
        test(`testing that it is not possible to add relations to ${ROUTEMODEL}`, async function (assert) {
          await authenticateSession({
            contactId: PERMISSION_LEVELS["appadmin"],
            token: "faketoken"
          })

          assert.step("1: ASSERT: VISIT MANY LIST")
          await visit(testCase.routeURL)
          assert.equal(currentURL(), testCase.routeURL)

          if (testCase.pauseTest) {
            await pauseTest()
          }

          assert.step("2: ASSERT: MISSING BUTTON TO NEW FORM")
          assert
            .dom(
              `[${ROUTEMODEL}-${TOOLBUTTON}='${testCase.relationModelName}']`
            )
            .doesNotExist()

          assert.verifySteps([
            "1: ASSERT: VISIT MANY LIST",
            "2: ASSERT: MISSING BUTTON TO NEW FORM"
          ])
        })
      }
    }
  )
}
