/**
 * @Example
 * import hasValues from "@kb/ember-addon/tests/suites/integration-component-has-values"
 *
 * hasValues({
 *   ...component,
 *   hasValues: [
 *      // [ caseScenario.givenAttrs, caseScenario.negateExpectation, textToExpectOrNot ],
 *      [ { "step": 1 }, true, "You are on Step 1" ],
 *      [ { "step": 2 }, false, "You are on Step 1" ],
 *   ],
 * })
 */
import { click, pauseTest, render } from "@ember/test-helpers"
import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { hbs } from "ember-cli-htmlbars"
import setupIntegrationComponentUtil from "@kb/ember-addon/tests/suites/integration-util-component"

export default function (testCase) {
  module(
    `Integration Test | Component | ${testCase.componentPath} has-values`,
    function (hooks) {
      setupRenderingTest(hooks)
      setupIntegrationComponentUtil(hooks, testCase)

      test(`${testCase.componentName} ${testCase.scenario}`, async function (assert) {
        if (testCase.hasValues) {
          assert.step("1: TESTING hasValues CASES")

          for (let caseScenario of testCase.hasValues) {
            let SCENARIO = caseScenario.scenario
            let attrBlock = this.buildAttrBlock(caseScenario.givenAttrs)

            const content = Ember.HTMLBars.compile(`
              <${testCase.componentClass} ${attrBlock}/>
            `)
            await render(content)

            if (testCase.pauseTest) {
              await pauseTest()
            }

            if (!caseScenario.negateExpectation) {
              assert
                .dom(caseScenario.givenDataTestId)
                .hasValue(caseScenario.expectValue, `${SCENARIO} HAS VALUE`)
            } else {
              assert
                .dom(caseScenario.givenDataTestId)
                .hasNoValue(`${SCENARIO} HAS NO VALUE`)
            }
          }
        }

        assert.verifySteps(["1: TESTING hasValues CASES"])
      })
    }
  )
}
