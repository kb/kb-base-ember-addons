import {
  authenticateSession,
  invalidateSession
} from "ember-simple-auth/test-support"
import setupMirage from "ember-cli-mirage/test-support/setup-mirage"
import { currentURL, pauseTest, settled, visit } from "@ember/test-helpers"
import { module, test, todo } from "qunit"
import { setupApplicationTest } from "ember-qunit"
import {
  setupRouteAcceptanceUtil,
  PERMISSION_LEVELS
} from "@kb/ember-addon/tests/suites/acceptance-util-setup-route"

export default function (testCase) {
  module(
    `permCheckRouteAcceptance | ${testCase.modelName} |  ${testCase.scenario}`,
    function (hooks) {
      setupApplicationTest(hooks)
      setupMirage(hooks)
      setupRouteAcceptanceUtil(
        hooks,
        testCase.targetPermissionLevel,
        testCase.mocks
      )

      if (testCase.todo) {
        todo(`todo`, async function (assert) {})
      } else {
        // Test all the given routes
        for (let routeUrl of testCase.routeUrls) {
          test(`testing permission accessing ${testCase.targetPermissionLevel} secured route ${routeUrl} using all PERMISSION_LEVELS`, async function (assert) {
            let targetPermissionLevelVal =
              PERMISSION_LEVELS[testCase.targetPermissionLevel]

            // Test the routeUrl against every permission level available.
            for (let testLevel in PERMISSION_LEVELS) {
              // Start by invalidating the session.
              await invalidateSession()
              let currentContact = this.owner.lookup("service:currentContact")
              currentContact.invalidate()
              // Get the numerical level of the nest PERMISSION LEVEL
              let testLevelVal = PERMISSION_LEVELS[testLevel]
              // Authenticate the session if not "loggedout"
              if (testLevelVal) {
                await authenticateSession({
                  contactId: testLevelVal,
                  token: "faketoken"
                })
                await currentContact.load()
              }
              // Attempt to visit the given URL
              await visit(routeUrl)

              await settled()

              if (testCase.pauseTest) {
                await pauseTest()
              }

              // Test that the route redirects if the permission level is not met
              assert.step(`${testLevel}: ASSERT: TESTED AUTHENTICATION`)
              if (testLevelVal >= targetPermissionLevelVal) {
                // Expect current URL to match the request if permission level is met
                assert.equal(currentURL(), routeUrl)
              } else {
                // Expect redirect if permission level is not met
                if (testLevel === "loggedout") {
                  assert.equal(currentURL(), "/authenticate")
                } else {
                  assert.equal(currentURL(), testCase.routeAfterAuthentication)
                }
              }
            }
            assert.verifySteps([
              "loggedout: ASSERT: TESTED AUTHENTICATION",
              "authenticated: ASSERT: TESTED AUTHENTICATION",
              "manager: ASSERT: TESTED AUTHENTICATION",
              "appadmin: ASSERT: TESTED AUTHENTICATION"
            ])
          })
        }
      }
    }
  )
}
