/**
 * @Example
 * import isDescriptive from "@kb/ember-addon/tests/suites/integration-component-is-descriptive"
 *
 isDescriptive({
   ...component,
   isDescriptive: [
     {
       givenAttrs: { "@key": TESTKEY, "@label": "Hello World" },

       expectDescriptive: "Hello World"
     },
     {
       givenAttrs: {
         "@key": TESTKEY,
         "@required": false,
         "@showValidations": true
       },
       negateExpectation: true,
       expectDescriptive: "This field is required"
     }
   ]
 })
 */
import { click, pauseTest, render } from "@ember/test-helpers"
import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { hbs } from "ember-cli-htmlbars"
import setupIntegrationComponentUtil from "@kb/ember-addon/tests/suites/integration-util-component"

export default function (testCase) {
  module(
    `Integration Test | Component | ${testCase.componentPath} is-descriptive`,
    function (hooks) {
      setupRenderingTest(hooks)
      setupIntegrationComponentUtil(hooks, testCase)

      test(`${testCase.componentName} ${testCase.scenario}`, async function (assert) {
        if (testCase.isDescriptive) {
          assert.step("1: TESTING isDescriptive CASES")

          for (let caseScenario of testCase.isDescriptive) {
            let SCENARIO = caseScenario.scenario
            let attrBlock = this.buildAttrBlock(caseScenario.givenAttrs)

            const content = Ember.HTMLBars.compile(`
              <${testCase.componentClass} ${attrBlock}/>
            `)
            await render(content)

            if (caseScenario.pauseTest) {
              await pauseTest()
            }

            if (!caseScenario.negateExpectation) {
              assert
                .dom(caseScenario.givenDataTestId)
                .containsText(
                  caseScenario.expectDescriptive,
                  `${SCENARIO} DESCRIPTIVE`
                )
            } else {
              assert
                .dom(caseScenario.givenDataTestId)
                .doesNotContainText(
                  caseScenario.expectDescriptive,
                  `${SCENARIO} NON DESCRIPTIVE`
                )
            }
          }
        }

        assert.verifySteps(["1: TESTING isDescriptive CASES"])
      })
    }
  )
}
