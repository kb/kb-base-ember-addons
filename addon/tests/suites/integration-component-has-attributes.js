/**
 * @Example
 * import hasAttributes from "@kb/ember-addon/tests/suites/integration-component-has-attributes"
 *
 hasAttributes({
   ...component,
   hasAttributes: [
     {
       givenAttrs: { title: "Click to boolean me" },

       expectAttributes: { title: "Click to boolean me" }
     }
   ]
 })
 */
import { click, pauseTest, render } from "@ember/test-helpers"
import { module, test } from "qunit"
import { setupRenderingTest } from "ember-qunit"
import { hbs } from "ember-cli-htmlbars"
import setupIntegrationComponentUtil from "@kb/ember-addon/tests/suites/integration-util-component"

export default function (testCase) {
  module(
    `Integration Test | Component | ${testCase.componentPath} has-attributes`,
    function (hooks) {
      setupRenderingTest(hooks)
      setupIntegrationComponentUtil(hooks, testCase)

      test(`${testCase.componentName} ${testCase.scenario}`, async function (assert) {
        if (testCase.hasAttributes) {
          assert.step("1: TESTING hasAttributes CASES")

          for (let caseScenario of testCase.hasAttributes) {
            let SCENARIO = caseScenario.scenario
            let attrBlock = this.buildAttrBlock(caseScenario.givenAttrs)

            const content = Ember.HTMLBars.compile(`
              <${testCase.componentClass} ${attrBlock}/>
            `)
            await render(content)

            if (testCase.pauseTest) {
              await pauseTest()
            }

            for (let attrName in caseScenario.expectAttributes) {
              if (!caseScenario.negateExpectation) {
                assert
                  .dom(caseScenario.givenDataTestId)
                  .hasAttribute(
                    attrName,
                    caseScenario.expectAttributes[attrName],
                    `${SCENARIO} ATTRIBUTE FOUND WITH EXPECTED VALUE`
                  )
              } else {
                assert
                  .dom(caseScenario.givenDataTestId)
                  .doesNotHaveAttribute(
                    attrName,
                    caseScenario.expectAttributes[attrName],
                    `${SCENARIO} NO ATTRIBUTE FOUND WITH EXPECTED VALUE`
                  )
              }
            }
          }
        }

        assert.verifySteps(["1: TESTING hasAttributes CASES"])
      })
    }
  )
}
