/**
* @Example
* import doesFiltering from "@kb/ember-addon/tests/suites/integration-component-does-filtering"
*
doesFiltering({
  ...component,
  doesFiltering: [{
    scenario: "confirm onto step 2",
    givenAttrs: { "step": 1 },
    givenDataTestId: "[data-test-kb-form-input-id]",
    action: "fillIn",
    fillInValue: "Completed Step 1",
    expectDoesFiltering: "Continue to Step 2"
  }]
})
*/
import { click, fillIn, pauseTest, render } from "@ember/test-helpers"
import { module, test } from "qunit"
import { selectChoose } from "ember-power-select/test-support"
import { setupRenderingTest } from "ember-qunit"
import { hbs } from "ember-cli-htmlbars"
import setupIntegrationComponentUtil from "@kb/ember-addon/tests/suites/integration-util-component"

export default function (testCase) {
  module(
    `Integration Test | Component | ${testCase.componentPath} does-filtering`,
    function (hooks) {
      setupRenderingTest(hooks)
      setupIntegrationComponentUtil(hooks, testCase)

      test(`${testCase.componentName} ${testCase.scenario}`, async function (assert) {
        if (testCase.doesFiltering) {
          assert.step("1: TESTING doesFiltering CASES")

          for (let caseScenario of testCase.doesFiltering) {
            let SCENARIO = caseScenario.scenario
            let attrBlock = this.buildAttrBlock(caseScenario.givenAttrs)

            const content = Ember.HTMLBars.compile(`
              <KbFilter @model={{this.modelGiven}} as |filterManager|>
                <${testCase.componentClass}
                  @key={{this.keyGiven}}
                  @value={{this.filterGiven}}
                  @valueLow={{this.filterLowGiven}}
                  @valueHigh={{this.filterHighGiven}}
                  @exact={{this.exactGiven}}
                  @options={{this.optionsGiven}}
                  @tags={{this.tagsGiven}}
                  @preset={{this.presetGiven}}
                  @allowClear={{true}}
                  @valueIsPrimitive={{this.valueIsPrimitiveGiven}}
                  @optionsArePrimitive={{this.optionsArePrimitiveGiven}}
                  @${testCase.funcHookName}={{filterManager.${testCase.funcAction}}}
                />
                <div data-test-filter-results>
                  {{#each filterManager.filteredItemsSorted as |listItem index|}}
                    <li>{{listItem.id}}</li>
                  {{/each}}
                </div>
              </KbFilter>
            `)
            await render(content)

            if (testCase.pauseTest) {
              await pauseTest()
            }

            if (caseScenario.givenDataTestId) {
              if (caseScenario.action === "click") {
                await click(caseScenario.givenDataTestId)
              } else if (caseScenario.action === "fillIn") {
                await fillIn(
                  caseScenario.givenDataTestId,
                  caseScenario.fillInValue
                )
              } else if (caseScenario.action === "selectChoose") {
                await selectChoose(
                  caseScenario.givenDataTestId,
                  caseScenario.selectValue
                )
              } else if (caseScenario.action === "renderOnly") {
                // do nothing
              }
            }

            if (testCase.pauseTest) {
              await pauseTest()
            }

            assert
              .dom("[data-test-filter-results] li")
              .exists({ count: caseScenario.resultCount })

            for (let [index, filtee] of Object.entries(caseScenario.results)) {
              assert
                .dom(`[data-test-filter-results] li:nth-child(${index})`)
                .containsText(filtee)
            }
          }
        }

        assert.verifySteps(["1: TESTING doesFiltering CASES"])
      })
    }
  )
}
