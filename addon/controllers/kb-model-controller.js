import Controller from "@ember/controller"
import { action } from "@ember/object"
import { inject as service } from "@ember/service"
import { run } from "@ember/runloop"
import { task } from "ember-concurrency"

/** kb-model-controller

Provides 5 utility ember-concurrency based tasks for:
    - let model = this.saveModel.perform(modelObj, successMessage, transitionTo)
    - this.deleteModel.perform(modelObj, transitionOnToSuccessRoute)
    - this.addManyRelationToRouteModel.perform(relationModelObj, routeModelObj [, relationModelName])
    - this.addNewManyRelationToRouteModel.perform(relationModelObj, routeModelObj, transitionToRoute [, relationModelName])
    - this.removeManyRelationFromRouteModel.perform(relationModelObj, routeModelObj)

@Example: controllers/my-model.js

import KbModelController from '@kb/ember-addon/controllers/kb-model-controller'
import { action } from "@ember/object"
export default class MyModelController extends KbModelController {
  @action
  actionForTemplate(modelOfThing, transitionTo) {
    this.deleteModel.perform(modelOfThing, transitionTo)
  }
}

@Example: templates/my-model.hbs

{{#each @things as |thing|}}
  <li>
    {{thing.name}}
    <button {{on "click" (fn this.actionForTemplate thing "thing.list")}}>
      Delete
    </button>
  </li>
{{/each}}
*/

export default class KbModelController extends Controller {
  @service currentContact
  @service kbMessages
  @service session

  get rootRoute() {
    return this.routeName.split(".")[0]
  }

  @action
  refreshRoute() {
    this.send("refreshModelRoute")
  }

  @action
  actionSaveModel(params) {
    this.saveModel.perform(
      params.modelObj,
      params.successMessage,
      params.transitionTo
    )
  }

  @action
  actionToggleModelField(modelObj, toggleField) {
    this.toggleModelField.perform(modelObj, toggleField)
  }

  @action
  actionDeleteModel(modelObj, transitionTo) {
    this.deleteModel.perform(modelObj, transitionTo)
  }

  //** Save a single Model */
  @task(function* (modelObj, successMessage, transitionTo) {
    let self = this
    return yield modelObj
      .save()
      .then(model => {
        run(() => {
          if (successMessage) {
            this.kbMessages.addMessage(successMessage)
          }
          if (transitionTo) {
            let modelId = model.get("id")
            if (!transitionTo.hasOwnProperty("params")) {
              transitionTo.params = [modelId]
            } else {
              if (!transitionTo.params.find(p => Boolean(p))) {
                transitionTo.params = [modelId]
              }
            }
            this.transitionToRoute(transitionTo.route, ...transitionTo.params)
          }
          return model
        })
      })
      .catch(reason => {
        this.kbMessages.addError("Cannot save model", reason)
      })
  })
  saveModel

  //** Delete a single Model */
  @task(function* (modelObj, transitionTo) {
    yield modelObj.reload()
    yield modelObj.destroyRecord().catch(reason => {
      this.kbMessages.addError("Cannot delete model", reason)
    })
    if (transitionTo) {
      this.transitionToRoute(transitionTo)
    }
  })
  deleteModel

  @task(function* (relationModelObj, routeModelObj, relationModelName) {
    if (!relationModelName) {
      relationModelName = yield relationModelObj.constructor.modelName
    }

    // The Route side model has already been loaded "Fat"... Only the objects on
    // the Many side are "Skinny". We need to reload this to get the full model
    // with existing relationships so they are maintained.
    yield relationModelObj.reload()
    yield routeModelObj[relationModelName].removeObject(relationModelObj)

    // DRF doesn't need to save both sides of the Many2Many... only save the Route side
    // yield this.saveModel.perform(relationModelObj)
    yield this.saveModel.perform(routeModelObj)
  })
  removeManyRelationFromRouteModel

  @task(function* (relationModelObj, routeModelObj, relationModelName) {
    if (!relationModelName) {
      relationModelName = yield relationModelObj.constructor.modelName
    }

    yield relationModelObj.reload()
    yield routeModelObj[relationModelName].pushObject(relationModelObj)

    // DRF doesn't need to save both sides of the Many2Many... only save the Route side
    // yield this.saveModel.perform(relationModelObj)
    yield this.saveModel.perform(routeModelObj)
  })
  addManyRelationToRouteModel

  @task(function* (
    relationModelObj,
    routeModelObj,
    transitionTo,
    relationModelName
  ) {
    if (!relationModelName) {
      relationModelName = yield relationModelObj.constructor.modelName
    }

    yield relationModelObj.save().catch(reason => {
      this.kbMessages.addError("Cannot add relationship to model", reason)
    })

    yield this.addManyRelationToRouteModel.perform(
      relationModelObj,
      routeModelObj,
      relationModelName
    )
    if (transitionTo) {
      this.transitionToRoute(transitionTo.route, ...transitionTo.params)
    }
  })
  addNewManyRelationToRouteModel

  @task(function* (
    routeModelObj,
    routeModelName,
    relationModelObj,
    relationModelName,
    transitionTo
  ) {
    if (!routeModelName) {
      routeModelName = yield routeModelObj.constructor.modelName
    }
    if (!relationModelName) {
      relationModelName = yield relationModelObj.constructor.modelName
    }
    relationModelObj[routeModelName] = null
    yield this.saveModel.perform(relationModelObj)
    yield this.saveModel.perform(routeModelObj)
    if (transitionTo) {
      this.transitionToRoute(transitionTo.route, ...transitionTo.params)
    }
  })
  removeOneRelationFromRouteModel

  @task(function* (
    routeModelObj,
    routeModelName,
    relationModelObj,
    relationModelName,
    transitionTo
  ) {
    if (!routeModelName) {
      routeModelName = yield routeModelObj.constructor.modelName
    }
    if (!relationModelName) {
      relationModelName = yield relationModelObj.constructor.modelName
    }

    yield (relationModelObj[routeModelName] = routeModelObj)
    yield this.saveModel.perform(relationModelObj)

    yield routeModelObj[relationModelName].pushObject(relationModelObj)
    // yield this.saveModel.perform(routeModelObj)
    if (transitionTo) {
      this.transitionToRoute(transitionTo.route, ...transitionTo.params)
    }
  })
  addOneRelationToRouteModel

  @task(function* (modelObj, toggleField) {
    modelObj[toggleField] = !modelObj[toggleField]
    yield modelObj.save().catch(reason => {
      this.kbMessages.addError("Cannot toggle value", reason)
    })
  })
  toggleModelBooleanField
}
