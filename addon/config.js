import ENV from "ember-get-config"

export default Object.assign(
  {
    afterLogoutUri: null,
    amountOfRetries: 3,
    authEndPoint: null,
    authHeaderName: "Authorization",
    authPrefix: "Bearer",
    clientId: "client",
    endSessionEndpoint: null,
    expiresIn: 3600 * 1000,
    host: "http://localhost:4200",
    redirectEndPoint: null,
    refreshLeeway: 1000 * 30,
    retryTimeout: 3000,
    tokenEndPoint: null,
    tokenPropertyName: "access_token"
  },
  ENV.oidcAuth || {}
)
