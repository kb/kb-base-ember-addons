import Component from "@glimmer/component"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"

export default class ToggleComponent extends Component {
  get caption() {
    if (this.args.value) {
      return this.args.captionOn
    } else {
      return this.args.captionOff
    }
  }

  get helpText() {
    if (this.args.value) {
      return this.args.helpTextOn
    } else {
      return this.args.helpTextOff
    }
  }

  get toggleColour() {
    if (this.args.value) {
      return "bg-indigo-600"
    } else {
      return "bg-gray-200"
    }
  }

  get togglePosition() {
    if (this.args.value) {
      return "translate-x-0"
    } else {
      return "translate-x-5"
    }
  }

  @action
  toggleValue() {
    if (this.args.toggleValue) {
      this.args.toggleValue(this.args.value)
    }
  }
}
