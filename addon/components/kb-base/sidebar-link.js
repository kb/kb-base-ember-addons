import Component from "@glimmer/component"
import { inject as service } from "@ember/service"

export default class KbBaseSidebarLinkComponent extends Component {
  @service router

  get linkClass() {
    let result = ""
    let currentRoute = this.router.currentRouteName
    if (currentRoute === this.args.route) {
      result = "bg-gray-900 text-white"
    } else {
      result = "text-gray-300 hover:bg-gray-700 hover:text-white"
    }
    if (this.args.mobile) {
      result = result + " text-base"
    } else {
      result = result + " text-sm"
    }
    return result
  }

  get svgClass() {
    let result = ""
    let currentRoute = this.router.currentRouteName
    if (currentRoute === this.args.route) {
      result = "text-gray-300"
    } else {
      result = "text-gray-400 group-hover:text-gray-300"
    }
    if (this.args.mobile) {
      result = result + " mr-4"
    } else {
      result = result + " mr-3"
    }
    return result
  }

  get svgName() {
    if (this.args.svgName) {
      return this.args.svgName
    } else {
      return "kb-svg/sparkles"
    }
  }
}
