import Component from "@glimmer/component"
import ENV from "front/config/environment"
import { action } from "@ember/object"
import { inject as service } from "@ember/service"
import { tracked } from "@glimmer/tracking"

export default class KbBaseComponent extends Component {
  @service kbPage
  @service router

  @tracked offCanvasMenuState = false
  @tracked profileMenuState = false
  @tracked siteLogoUrl

  constructor() {
    super(...arguments)
    this.siteLogoUrl = ENV.APP.siteLogoUrl
  }

  /**
   * PJK, 23/11/2021, Remove *Dashboard* option from profile menu for now...
   * https://chat.kbsoftware.co.uk/kb/pl/4x7wwumqk3fydrx5wr36n5uemr
   *
   * Click on user profile (top-right), then dashboard.
   *
   * @action
   * contactDashboard() {
   *   this.router.transitionTo(this.kbPage.contactRoute, this.kbPage.contactId)
   *   this.profileMenuState = false
   * }
   *
   */

  @action
  signOut() {
    /** Click on user profile (top-right), then sign-out. */
    this.kbPage.signOutAction()
    this.profileMenuState = false
  }

  @action
  toggleOffCanvasMenuState() {
    this.offCanvasMenuState = !this.offCanvasMenuState
  }

  @action
  toggleProfileMenuState() {
    this.profileMenuState = !this.profileMenuState
  }
}
