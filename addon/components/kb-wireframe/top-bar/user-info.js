import Component from "@glimmer/component"
import { inject } from "@ember/service"

export default class KbWireframeTopBarUserInfoComponent extends Component {
  @inject session
  @inject currentContact

  get contactName() {
    if (this.currentContact.contact) {
      return `${this.currentContact.contact.get(
        "firstName"
      )} ${this.currentContact.contact.get("lastName")}`
    }
    if (this.args.userName) return this.args.userName
  }
}
