import KbComponentBase from "../kb-component-base"
import { inject as service } from "@ember/service"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"

export default class SideBarComponent extends KbComponentBase {
  @service sideBarVisibility
  @tracked mobileVisible = false

  constructor(args, isDestroying, isDestroyed) {
    super(args, isDestroying, isDestroyed)
    this.sideBarVisibility.setNotifyFunction(this.notify)
  }

  @action
  toggleSideBar() {
    this.sideBarVisibility.toggle()
  }

  @action
  closeSideBar() {
    this.sideBarVisibility.close()
  }

  @action
  notify(state) {
    this.mobileVisible = state
  }

  get sideBarClass() {
    let mClasses = ""

    if (this.mobileVisible) {
      mClasses = "absolute z-10 flex"
    } else {
      mClasses = "hidden"
    }

    return `${mClasses} h-full md:flex md:flex-shrink-0`
  }
}
