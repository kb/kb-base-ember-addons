import KbComponentBase from "../../kb-component-base"
import { action } from "@ember/object"

export default class NavHideComponent extends KbComponentBase {
  @action
  click() {
    this.args.clickAction()
  }
}
