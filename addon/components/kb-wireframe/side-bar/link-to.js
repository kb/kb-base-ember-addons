import Component from "@glimmer/component"
import { inject as service } from "@ember/service"
import { action } from "@ember/object"

export default class KbWireframeSideBarMenuItemComponent extends Component {
  @service sideBarVisibility

  @action
  closeSideBar() {
    this.sideBarVisibility.close()
  }
}
