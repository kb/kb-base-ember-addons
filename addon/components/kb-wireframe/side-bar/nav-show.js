import KbComponentBase from "../../kb-component-base"
import { action } from "@ember/object"

export default class NavShowComponent extends KbComponentBase {
  @action
  click() {
    this.args.clickAction()
  }
  get classNames() {
    return `${this.visibility} border-none cursor-pointer m-1 p-1 object-right-top bg-transparent text-gray-900 block sm:hidden`
  }
  get visibility() {
    return this.args.isHidden ? "hidden" : "absolute"
  }
}
