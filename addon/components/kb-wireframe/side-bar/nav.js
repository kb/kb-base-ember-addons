import KbComponentBase from "../../kb-component-base"

export default class NavComponent extends KbComponentBase {
  get visibility() {
    return this.args.isShown ? "when-shown" : "when-hidden"
  }
}
