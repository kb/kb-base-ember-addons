import KbComponentBase from "../kb-component-base"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"

export default class MainComponent extends KbComponentBase {
  @tracked navVisible = false
  @action
  toggleNav() {
    this.navVisible = !this.navVisible
  }
}
