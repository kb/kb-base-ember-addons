import Component from "@glimmer/component"

export default class KbTableHeadCellComponent extends Component {
  get alignClass() {
    if (this.args.align === "right") {
      return "text-right"
    } else {
      return "text-left"
    }
  }
}
