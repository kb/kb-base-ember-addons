import Component from "@glimmer/component"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"

export default class SortButtonComponent extends Component {
  get sortIcon() {
    if (this.args.sortOrder === "asc") {
      return "M19 9l-7 7-7-7"
    } else if (this.args.sortOrder === "desc") {
      return "M5 15l7-7 7 7"
    } else {
      return "M8 9l4-4 4 4m0 6l-4 4-4-4"
    }
  }

  @action
  toggleSort() {
    if (this.args.sortBy && this.args.toggleSort) {
      this.args.toggleSort(this.args.sortBy)
    }
  }
}
