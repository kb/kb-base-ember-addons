import Component from "@glimmer/component"

export default class KbTableBodyCellComponent extends Component {
  get alignClass() {
    if (this.args.align === "right") {
      return "text-right"
    }
  }
}
