import Component from "@glimmer/component"

export default class KbBadgeComponent extends Component {
  get badgeClass() {
    let result = ""
    if (this.args.badgeType === "encouragement") {
      result = "bg-green-100 text-green-800"
    } else if (this.args.badgeType === "information") {
      result = "bg-blue-100 text-blue-800"
    } else {
      result = "bg-red-100 text-red-800"
    }
    return result
  }
}
