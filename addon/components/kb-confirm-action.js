import KbComponentBase from "./kb-component-base"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"

export default class KbConfirmActionComponent extends KbComponentBase {
  @tracked confirmShown = false

  get buttonClass() {
    let result = ""
    if (this.args.buttonSize === "small") {
      result = "text-xs"
    } else {
      result = "text-sm"
    }
    if (this.args.buttonType === "warning") {
      result =
        result + " text-red-700 bg-red-100 hover:bg-red-200 focus:ring-red-500"
    } else {
      result =
        result +
        " text-blue-700 bg-blue-100 hover:bg-blue-200 focus:ring-blue-500"
    }
    return result
  }

  @action
  toggleConfirm() {
    if (this.args.deactivate) {
      this.args.confirmHook()
    } else {
      this.confirmShown = !this.confirmShown
    }
  }

  @action
  confirmed() {
    this.confirmShown = false
    this.args.confirmHook()
  }
}
