import KbComponentBase from "./kb-component-base"
import groupBy from "../utils/kb-group-by"

export default class KbGroupWithParentComponent extends KbComponentBase {
  get groupedWithParentItems() {
    if (!this.args.items) {
      return []
    }
    // Find all the available parents, i.e. records others want to group to.
    let parentsPresent = new Set(
      this.args.items
        .filter(s => s[this.args.groupTo])
        .map(s => String(s[this.args.groupTo]))
    )
    let prepped = this.args.items.map(s => {
      // Which parent should group this record?
      let parentWanted = String(s[this.args.groupBy])
      // Either this is the parent, else: If not available, we will treat this
      /// as its own parent
      let groupByThisIfParentOrIfParentMissing = String(s[this.args.groupTo])
      // Evaluate how this record should be grouped.
      // i.e. to self or to-normal-parent-if-avail.
      let groupBy = parentsPresent.has(parentWanted)
        ? parentWanted
        : groupByThisIfParentOrIfParentMissing
      // s[this.args.groupBy] = groupBy
      s.internalGroupBy = groupBy
      // Flag as parent if this record is really a parent.
      s.isGroupParent = groupByThisIfParentOrIfParentMissing === groupBy
      return s
    })
    // .sort((a, b) => a.internalGroupBy - b.internalGroupBy)
    return groupBy(prepped, "internalGroupBy")
  }
}
