import KbComponentBase from "./kb-component-base"
import { action, set } from "@ember/object"
import { inject as service } from "@ember/service"
import { tracked } from "@glimmer/tracking"

export default class KbFormComponent extends KbComponentBase {
  @service kbMessages

  // And object to maintain the validation status of all the inputs.
  @tracked showValidations = false

  // And object to maintain the validation status of all the inputs.
  @tracked validationStatus = Object()

  // And object to maintain the validation status of all the inputs.
  @tracked valuesParameters = Object()

  // And object to maintain connection to child inputs and their properties.
  @tracked childInputs = Object()

  // A simple count of which inputs are valid or not.
  get countOfValid() {
    return Object.values(this.validationStatus).filter(i => !i.valid).length
  }

  get formIsValid() {
    return this.countOfValid === 0
  }

  get buttonLabel() {
    return this.args.buttonLabel ? this.args.buttonLabel : "Submit"
  }

  get buttonClass() {
    return this.args.buttonClass ? `${this.args.buttonClass}` : "kb-form-button"
  }

  // Override to format your return parameters.
  get saveHookParameters() {
    return this.valuesParameters
  }

  @action
  setStartingModel() {
    if (this.args.model) {
      this.valuesParameters = this.args.model
    }
  }

  @action
  actionToRegisterChildInput(key, childInput) {
    set(this.childInputs, key, childInput)
  }

  @action
  actionToUpdateModelById(key, value) {
    let valueId = value.get("id")
    set(this.valuesParameters, key, valueId)
  }

  @action
  actionToUpdateModelByKey(key, value) {
    set(this.valuesParameters, key, value)
  }

  @action
  actionValidate(key, validOrNotRequired, errorMessage) {
    // Each form input will set it's validation status when loaded or touched.
    this.validationStatus[key] = {
      valid: validOrNotRequired,
      errorMessage: errorMessage
    }
  }

  // Override this if you must.
  @action
  actionValidateThenSubmit(event) {
    if (event) {
      if (event.preventDefault) event.preventDefault()
    }
    this.showValidations = true
    if (this.formIsValid) {
      this.args.actionSubmitHook(this.saveHookParameters)
    } else {
      this.kbMessages.addError(`${this.countOfValid} field(s) not validated.`)
      for (let field of Object.keys(this.validationStatus)) {
        if (!this.validationStatus[field].valid) {
          this.kbMessages.addError(
            `${field}: ${this.validationStatus[field].errorMessage}`
          )
        }
      }
    }
  }
}
