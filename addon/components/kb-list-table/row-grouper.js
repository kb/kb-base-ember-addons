import Component from "@glimmer/component"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"

export default class RowGrouperComponent extends Component {
  @tracked isOpen = false

  @action
  actionToggle() {
    this.isOpen = !this.isOpen
  }

  get buttonIcon() {
    return this.isOpen ? "up" : "down"
  }

  get cellClass() {
    if (this.hasHiderableContent) {
      return "font-bold"
    } else if (this.args.parentGrouper) {
      return "pl-4"
    }
  }

  /**
  Important: This property is accessed by parent and child rows alike. In many
  cases, the parent row-component recursively calls the same row-component as
  its children. This is why we distinguish between rows that have the
  "hiderableContent" or not.
  */
  get rowClass() {
    // If parent with hasHiderableContent
    if (this.hasHiderableContent) {
      if (this.isOpen) {
        return "border-black border-l-4 border-r-4"
      }
    } else {
      if (this.args.parentGrouper && !this.args.parentGrouper.isOpen) {
        return "hidden"
      }
    }
  }

  /**
  When a parent row-component calls a different component for its
  "hiderableContent", this property can be passed to the children so it knows
  whether it is visible or not.
  */
  get childRowClass() {
    // Hide if has parent with hiderableContent which is not open.
    if (!this.isOpen) {
      return "hidden"
    }
  }

  /**
  What counts as having "hiderableContent" will depend on the parent. Pass this
  into the parentGrouper. Remember the grouper of the parent row can
  recursively call the child.
  */
  get hasHiderableContent() {
    return this.args.hasHiderableContent
  }
}
