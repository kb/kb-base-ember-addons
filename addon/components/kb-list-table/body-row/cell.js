import Component from "@glimmer/component"
import { action } from "@ember/object"

export default class CellComponent extends Component {
  @action
  actionClick() {
    if (this.args.clickHook) {
      this.args.clickHook()
    }
  }
}
