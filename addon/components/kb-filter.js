import KbComponentBase from "./kb-component-base"
import filterCombiner from "@kb/ember-addon/utils/filter-combiner"
import sortCombiner from "@kb/ember-addon/utils/sort-combiner"
import { action, set } from "@ember/object"
import { tracked } from "@glimmer/tracking"

export default class KbFilterComponent extends KbComponentBase {
  @tracked filterBys = new Object()
  @tracked sortBys = new Map()

  @action
  filter(key, func) {
    set(this.filterBys, key, func)
    // Needed to trigger properties which track this.
    this.filterBys = this.filterBys
  }

  @action
  sort(key, func) {
    if (this.sortBys.has(key)) {
      this.sortBys.delete(key)
    }
    this.sortBys.set(key, func)
    this.sortBys = this.sortBys
  }

  get filteredItems() {
    if (this.args.model && this.args.model.length) {
      return this.args.model.filter(row => filterCombiner(row, this.filterBys))
    } else {
      return []
    }
  }

  get filteredItemsSorted() {
    let sortBys = Object.fromEntries(this.sortBys)
    return sortCombiner(this.filteredItems, sortBys)
  }
}
