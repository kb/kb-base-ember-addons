import Component from "@glimmer/component"
import { action } from "@ember/object"

export default class KbButtonComponent extends Component {
  /**
   * A button (not the submit button on a form)...
   *
   * .. tip:: For the submit button on a form, use ``KbForm::Form::Button``
   */

  @action
  onClick() {
    if (this.args.onClick) {
      this.args.onClick()
    } else {
      console.error("'KbButtonComponent' needs 'this.args.onClick'")
    }
  }

  get buttonClass() {
    if (this.args.buttonType === "submit") {
      return "text-white bg-blue-600 hover:bg-blue-700 focus:ring-blue-500"
    } else if (this.args.buttonType === "warning") {
      return "text-red-700 bg-red-100 hover:bg-red-200 focus:ring-red-500"
    } else {
      return "text-blue-700 bg-blue-100 hover:bg-blue-200 focus:ring-blue-500"
    }
  }

  get horizontalPadding() {
    if (this.args.paddingRight === false) {
      return "pl-3"
    } else {
      return "px-3"
    }
  }

  get verticalPadding() {
    if (this.args.verticalPadding === false) {
      return ""
    } else {
      return "py-2 "
    }
  }
}
