import Component from "@glimmer/component"

export default class KbSvgClearComponent extends Component {
  get svgClass() {
    if (this.args.class) {
      // if used in the sidebar menu
      return "h-6 w-6 " + this.args.class
    } else {
      return "h-5 w-5 text-gray-400"
    }
  }
}
