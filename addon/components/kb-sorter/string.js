import KbSorterBaseComponent from "./base"
import sortString from "@kb/ember-addon/utils/sort-string"
import sortStringDesc from "@kb/ember-addon/utils/sort-string-desc"

export default class KbSorterStringComponent extends KbSorterBaseComponent {
  sortAscFunction = sortString
  sortDescFunction = sortStringDesc
}
