import Component from "@glimmer/component"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"
import sortNumerical from "@kb/ember-addon/utils/sort-numerical"
import sortNumericalDesc from "@kb/ember-addon/utils/sort-numerical-desc"

export default class KBSorterBaseComponent extends Component {
  @tracked dir = ""
  sortAscFunction = sortNumerical
  sortDescFunction = sortNumericalDesc

  @action
  actionSortPreset() {
    if (this.args.clickHook) {
      this.dir = this.args.preset
      if (this.dir) {
        this._sort(this.dir)
      }
    }
  }

  @action
  actionSort() {
    if (this.args.clickHook) {
      this.dir = this.dir === "asc" ? "dsc" : "asc"
      this._sort(this.dir)
    }
  }

  @action
  _sort(dir) {
    let sortFunc = dir === "dsc" ? this.sortDescFunction : this.sortAscFunction
    this.args.clickHook(this.args.key, sortFunc)
  }
}
