import KbSorterBaseComponent from "./base"
import sortDate from "@kb/ember-addon/utils/sort-date"
import sortDateDesc from "@kb/ember-addon/utils/sort-date-desc"

export default class KbSorterDateComponent extends KbSorterBaseComponent {
  sortAscFunction = sortDate
  sortDescFunction = sortDateDesc
}
