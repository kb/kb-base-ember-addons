import KbComponentBase from "./kb-component-base"
import { action } from "@ember/object"
import { inject } from "@ember/service"
import { tracked } from "@glimmer/tracking"

export default class KbQuickPaginatorComponent extends KbComponentBase {
  @inject scroller
  @tracked page = 1
  isHorizontal = false
  nextIcon = "down"
  prevIcon = "up"
  offset = -50
  startLastPage = false
  @action
  nextClicked() {
    if (this.page + 1 <= this.numberOfPages) {
      this.page++
    }
    this.scrollToTop()
  }
  // Show the previous page of items
  @action
  previousClicked() {
    if (this.page > 0) {
      this.page--
    }
  }
  // Go to the clicked page of items
  @action
  pageClicked(pageNumber) {
    this.page = pageNumber
  }

  @action
  scrollToTop() {
    // let scrollToId = this.topId
    // TODO: Deprecated scroller module. Try another way!
    // this.scroller.scrollVertical(scrollToId, {
    //   duration: 1000,
    //   offset: this.offset
    // })
  }
  get bottomId() {
    return `#paginator-bottom-${this.args.id}`
  }
  // The total number of pages that our items span
  get numberOfItems() {
    if (this.args.items) {
      return this.args.items.length
    } else {
      return 0
    }
  }
  // The total number of pages that our items span
  get numberOfPages() {
    let numPages = 0
    if (this.args.paginateBy) {
      if (this.args.items) {
        var n = this.args.items.length
        var c = parseInt(this.args.paginateBy)
        numPages = Math.floor(n / c)
        if (n % c > 0) {
          numPages += 1
        }
      }
    }
    return numPages
  }
  get paginatedItems() {
    if (this.args.paginateBy) {
      if (this.args.items) {
        var i = (parseInt(this.page) - 1) * parseInt(this.args.paginateBy)
        var j = i + parseInt(this.args.paginateBy)
        var items = this.args.items
        let pagitems = items.slice(i, j)
        return pagitems
      } else {
        return null
      }
    } else {
      return this.args.items
    }
  }
  // An array containing the number of each page: [1, 2, 3, 4, 5, ...]
  get pageNumbers() {
    var n = Array(this.numberOfPages)
    for (var i = 0; i < n.length; i++) {
      n[i] = i + 1
    }
    return n
  }
  // Whether or not to show the "next" button
  get showNext() {
    return this.page < this.numberOfPages && this.numberOfPages
  }
  // Whether or not to show the "previous" button
  get showPrevious() {
    return this.page > 1
  }
  get topId() {
    return `#paginator-top-${this.args.id}`
  }
  constructor() {
    super(...arguments)
    if (this.args.startLastPage) {
      let lastPage = this.numberOfPages
      if (lastPage) {
        this.set("page", lastPage)
        this.scrollToTop()
      }
    }
    this.startLastPage = false
  }
}
