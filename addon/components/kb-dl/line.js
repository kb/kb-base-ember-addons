import Component from "@glimmer/component"

export default class KbDlLineComponent extends Component {
  get lineClass() {
    if (this.args.firstLine)
      return "sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5 font-bold"
    else
      return "mt-8 sm:mt-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:border-t sm:border-gray-200 sm:px-6 sm:py-5"
  }
}
