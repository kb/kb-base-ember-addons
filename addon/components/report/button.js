import Component from "@glimmer/component"
import ENV from "front/config/environment"
import FileSaver from "file-saver"
import { action } from "@ember/object"
import { inject as service } from "@ember/service"
import { task, timeout } from "ember-concurrency"
import { tracked } from "@glimmer/tracking"

export default class ReportButtonComponent extends Component {
  @service cookies
  @service kbMessages
  @service session
  @service store
  @tracked downloadFileName
  @tracked downloadUrl
  @tracked isDownload = false
  @tracked reportScheduleId = undefined

  downloadReport() {
    let self = this
    let csrfToken = this.cookies.read("csrftoken")
    let authToken = this.session.data.authenticated.token
    let downloadUrl = `${ENV.APP.API_URL}${this.downloadUrl}`
    fetch(downloadUrl, {
      responseType: "blob",
      headers: {
        "X-CSRFToken": csrfToken,
        credentials: "include",
        Authorization: "Token " + authToken
      }
    })
      .then(response => {
        if (response.ok) {
          return response.blob()
        }
        console.error(response)
        this.kbMessages.addError("Cannot download file", response)
      })
      .then(myBlob => {
        FileSaver.saveAs(myBlob, this.downloadFileName)
        this.isDownload = false
      })
      .catch(error => {
        console.error(error)
        this.kbMessages.addError("Cannot download file", error)
      })
  }

  @action
  createReport() {
    /**
     * Report parameters (``this.args.reportParameters``) are for the
     * Django 'report' app.
     *
     * .. note:: Name the parameters in python format
     *           i.e. ``from_date`` *not* ``fromDate``.
     *
     * .. note:: The ``parameters`` are separated in the object so we can add
     *           in the report title in future.
     */
    if (this.isDownload) {
      this.downloadReport()
    } else {
      if (this.args.reportParameters && this.args.reportSlug) {
        let parameters = this.args.reportParameters.parameters
        this.reportScheduleTask.perform(this.args.reportSlug, parameters)
      } else {
        console.error(
          "'ReportButtonComponent' needs 'this.args.reportParameters' " +
            "and 'this.args.reportSlug'"
        )
      }
    }
  }

  @task *reportScheduleTask(slug, params) {
    /**
     * 1. Create the report schedule.
     * 2. Monitor the report progress.
     */
    this.reportScheduleId = undefined
    let reportSchedule = undefined
    try {
      reportSchedule = this.store.createRecord("reportSchedule", {})
      reportSchedule.parameters = params
      reportSchedule.slug = slug
      yield reportSchedule.save()
    } catch (e) {
      this.kbMessages.addError("Cannot create the report schedule", e)
    }
    if (reportSchedule) {
      this.reportScheduleId = reportSchedule.id
    }
    if (this.reportScheduleId) {
      try {
        // check the report progress 10 times
        for (let count = 0; count < 10; count++) {
          yield timeout(900)
          reportSchedule = yield this.store.findRecord(
            "reportSchedule",
            this.reportScheduleId,
            {
              reload: true
            }
          )
          if (reportSchedule.isComplete) {
            this.downloadFileName = reportSchedule.downloadFileName
            this.downloadUrl = reportSchedule.downloadUrl
            this.isDownload = true
            break
          }
        }
      } catch (e) {
        this.kbMessages.addError("Cannot get the status of the report", e)
      }
    }
  }
}
