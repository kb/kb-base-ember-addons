import KbFilterBaseComponent from "./base"
import { action } from "@ember/object"
import filterTaggitExact from "@kb/ember-addon/utils/filter-taggit-exact"
import filterTaggitOverlap from "@kb/ember-addon/utils/filter-taggit-overlap"

export default class KbFilterTaggitComponent extends KbFilterBaseComponent {
  filterExactFunction = filterTaggitExact
  filterLooselyFunction = filterTaggitOverlap
}
