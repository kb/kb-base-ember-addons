import Component from "@glimmer/component"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"

export default class KbFilterBaseComponent extends Component {
  @tracked sortDesc = false
  filterExactFunction = Boolean
  filterLooselyFunction = Boolean
  filterListIncludesFunction = Boolean

  get fieldgroupPadding() {
    return this.args.padFieldGroupTop ? "pl-2 pt-8" : "pl-2"
  }

  @action
  actionOnFilterChange(key, filterValue) {
    // Immediately override the key we sent to the kb-form-input.
    key = this.args.key
    if (this.args.changeHook) {
      if (this.args.exact) {
        this.args.changeHook(key, row =>
          this.filterExactFunction(row[key], filterValue)
        )
      } else if (this.args.listIncludes) {
        this.args.changeHook(key, row =>
          this.filterListIncludesFunction(row[key], filterValue)
        )
      } else {
        this.args.changeHook(key, row =>
          this.filterLooselyFunction(row[key], filterValue)
        )
      }
    }
  }
}
