import KbFilterBaseComponent from "./base"
import filterDateExact from "@kb/ember-addon/utils/filter-date-exact"
import filterDateGte from "@kb/ember-addon/utils/filter-date-gte"
import filterDateIgnoredwhendateless from "@kb/ember-addon/utils/filter-date-ignoredwhendateless"
import filterDateLte from "@kb/ember-addon/utils/filter-date-lte"

export default class KbFilterDateComponent extends KbFilterBaseComponent {
  filterExactFunction = filterDateExact
  filterLooselyFunction = filterDateIgnoredwhendateless
}
