import KbFilterBaseComponent from "./base"
import filterNumberExact from "@kb/ember-addon/utils/filter-number-exact"
import filterNumberGte from "@kb/ember-addon/utils/filter-number-gte"
import filterNumberIgnoredwhenzero from "@kb/ember-addon/utils/filter-number-ignoredwhenzero"
import filterNumberLte from "@kb/ember-addon/utils/filter-number-lte"

export default class KbFilterNumberComponent extends KbFilterBaseComponent {
  filterExactFunction = filterNumberExact
  filterLooselyFunction = filterNumberIgnoredwhenzero
}
