import Component from "@glimmer/component"

export default class KbFilterFiltersComponent extends Component {
  get justifyClass() {
    /**
     * For diff, see:
     * https://drive.google.com/file/d/1TmVDuauMC2YwBLbj4uJFnDU2rGhBCUpr/view?usp=sharing
     *
     * <div class="bg-white shadow">
     * -  <div class="px-4 sm:px-6 lg:max-w-full lg:mx-auto lg:px-8">
     * +  <div class="px-4 sm:px-6 lg:max-w-full lg:mx-auto lg:px-0">
     *      <div
     * -      class="py-6 md:flex md:items-center md:justify-between lg:border-t lg:border-gray-200"
     * +      class="py-6 lg:flex lg:items-center lg:justify-start lg:border-t lg:border-gray-200"
     *
     */
    let hasProfile = this.args.hasProfile ?? false
    if (hasProfile) {
      return "md:justify-between"
    } else {
      return "md:justify-start"
    }
  }
}
