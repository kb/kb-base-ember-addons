import KbFilterBaseComponent from "./base"
import filterStringExactsensitive from "@kb/ember-addon/utils/filter-string-exactsensitive"
import filterStringIncludesinsensitive from "@kb/ember-addon/utils/filter-string-includesinsensitive"

export default class KbFilterStringComponent extends KbFilterBaseComponent {
  filterExactFunction = filterStringExactsensitive
  filterLooselyFunction = filterStringIncludesinsensitive
}
