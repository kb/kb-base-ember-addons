import KbFilterBaseComponent from "./base"
import filterStringExactsensitive from "@kb/ember-addon/utils/filter-string-exactsensitive"
import filterStringIncludesinsensitive from "@kb/ember-addon/utils/filter-string-includesinsensitive"
import filterListIncludes from "@kb/ember-addon/utils/filter-list-includes"

export default class KbFilterSelectComponent extends KbFilterBaseComponent {
  filterExactFunction = filterStringExactsensitive
  filterLooselyFunction = filterStringIncludesinsensitive
  filterListIncludesFunction = filterListIncludes
}
