import KbFilterBaseComponent from "./base"
import filterBooleanExact from "@kb/ember-addon/utils/filter-boolean-exact"
import filterBooleanIgnoredwhenfalse from "@kb/ember-addon/utils/filter-boolean-ignoredwhenfalse"

export default class KbFilterBooleanComponent extends KbFilterBaseComponent {
  filterExactFunction = filterBooleanExact
  filterLooselyFunction = filterBooleanIgnoredwhenfalse
}
