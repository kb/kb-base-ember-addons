import Component from "@glimmer/component"
import filterNumberRange from "@kb/ember-addon/utils/filter-number-range"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"

export default class KbFilterKbFilterNumericalRangeComponent extends Component {
  filterExactFunction = filterNumberRange

  @tracked range = new Object()

  @action
  actionOnFilterChange(key, value) {
    let keyLow = `${this.args.key}LowFilter`
    let keyHigh = `${this.args.key}HighFilter`
    this.range[key] = value
    if (this.args.changeHook) {
      this.args.changeHook(key, row =>
        this.filterExactFunction(
          row[this.args.key],
          this.range[keyLow],
          this.range[keyHigh]
        )
      )
    }
  }
}
