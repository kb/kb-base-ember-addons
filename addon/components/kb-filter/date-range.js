import KbFilterRangeComponent from "./numerical-range"
import filterDateRange from "@kb/ember-addon/utils/filter-date-range"

export default class KbFilterKbFilterDateRangeComponent extends KbFilterRangeComponent {
  filterExactFunction = filterDateRange
}
