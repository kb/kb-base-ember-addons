import KbComponentBase from "./kb-component-base"
import groupBy from "../utils/kb-group-by"

export default class KbGroupByComponent extends KbComponentBase {
  get groupedItems() {
    return groupBy(this.args.items, this.args.groupBy)
  }
}
