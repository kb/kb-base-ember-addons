import KbFormBaseComponent from "./base"
import { action } from "@ember/object"
import { isBlank } from "@ember/utils"
import { tracked } from "@glimmer/tracking"

export default class KbFormTaggitComponent extends KbFormBaseComponent {
  tooMaxMessage = `You cannot tick more than ${this.maxParam} tags`
  tooMinMessage = `You need to tick at least ${this.minParam} tags`
  emptyMessage = `You need to tick at least ${this.minParam} tags`
  @tracked addingNewTags = false
  @tracked tagList = []
  @tracked newTags = ""

  get showNewTags() {
    return this.args.showNewTags || this.addingNewTags
  }

  get value() {
    let newTags = []
    if (this.newTags) {
      newTags = this.newTags
        .split(",")
        .map(t => t.trim())
        .filter(Boolean)
    }
    let tags = [...new Set(this.tagList.concat(newTags))]
    return tags
  }

  @action
  setNewTags(key, newTags) {
    this.newTags = newTags
    this.touched()
  }

  @action
  setTag(value) {
    if (this.tagList.includes(value)) {
      this.tagList.splice(this.tagList.indexOf(value), 1)
    } else {
      this.tagList.push(value)
    }
    this.touched()
  }

  @action
  setStartingValue(val) {
    if (!isBlank(this.args.value)) {
      if (this.args.value.includes(",")) {
        this.tagList = this.args.value.split(",")
      } else {
        this.tagList = this.args.value
      }
    }
    this.actionValidate()
    this.actionChange(true)
    if (this.args.form) {
      this.args.form.actionToRegisterChildInput(this.args.key, this)
    }
  }

  get minMaxValue() {
    return this.value.length
  }
}
