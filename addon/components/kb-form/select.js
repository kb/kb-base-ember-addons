import KbFormBaseComponent from "./base"
import objValue from "@kb/ember-addon/utils/obj-value"
import { action } from "@ember/object"
import { isBlank } from "@ember/utils"
import { tracked } from "@glimmer/tracking"

export default class KbFormSelectComponent extends KbFormBaseComponent {
  @tracked selectedItem = null

  get value() {
    if (this.selectedItem) {
      if (this.args.valueIsPrimitive && !this.args.optionsArePrimitive) {
        let valInnit = objValue(this.selectedItem, this.args.valueField)
        return valInnit
      } else {
        return this.selectedItem
      }
    }
  }

  get selectedValue() {
    return this.selectedItem
  }

  get fullyValid() {
    return this.selectedItem
  }

  @action
  verifyPresence(select) {
    if (this.value) {
      this.touched()
    } else {
      return this.validOrNotRequired
    }
  }

  @action
  changeValue(select) {
    this.selectedItem = select
    this.touched()
  }

  @action
  setStartingValue() {
    if (!isBlank(this.args.value)) {
      if (this.args.optionsArePrimitive || !this.args.options) {
        this.selectedItem = this.args.value
      } else {
        // Here we "pick" the objects from the `@options` so that a true reference
        // is created, and the item shows grey in the dropdown.
        let selectValue = this.args.value
        if (!this.args.valueIsPrimitive) {
          selectValue = objValue(this.args.value, this.args.valueField)
        }
        this.selectedItem = this.args.options.find(
          o => String(selectValue) === String(objValue(o, this.args.valueField))
        )
      }
    }
    this.actionValidate()
    this.actionChange(true)
    if (this.args.form) {
      this.args.form.actionToRegisterChildInput(this.args.key, this)
    }
  }
}
