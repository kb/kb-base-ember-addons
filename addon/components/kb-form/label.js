import KbComponentBase from "../kb-component-base"

export default class KbFormLabelComponent extends KbComponentBase {
  get labelClass() {
    return `${this.args.class} block text-sm font-medium text-gray-700`
  }
}
