import KbFormBaseComponent from "./base"
import { action } from "@ember/object"
import { inject as service } from "@ember/service"
import { isEmpty } from "@ember/utils"
import { task } from "ember-concurrency"
import { tracked } from "@glimmer/tracking"

export default class KbFormUploadComponent extends KbFormBaseComponent {
  /**
   * Documentation
   * https://www.kbsoftware.co.uk/docs/dev-ember-addons-kb.html#upload-file
   *
   * Uses ``ember-file-upload``:
   *
   * - https://adopted-ember-addons.github.io/ember-file-upload/
   * - https://adopted-ember-addons.github.io/ember-file-upload/docs
   * - https://github.com/adopted-ember-addons/ember-file-upload
   */

  tooMaxMessage = `No more than ${this.maxParam} ${this.args.fileType}s are allowed`
  tooMinMessage = `At least ${this.minParam} ${this.args.fileType}s are required`
  emptyMessage = `At least ${this.minParam} ${this.args.fileType}s are required`

  @service fileQueue
  @service kbMessages
  @service session
  @tracked attemptedFiles = []

  constructor() {
    super(...arguments)
    if (this.args.key) {
      let fileQueue = this.fileQueue.find(this.args.key)
      if (fileQueue) {
        let files = []
        // make sure the queue is empty when we start
        for (const file of fileQueue.files) {
          files.push(file)
        }
        for (const file of files) {
          fileQueue.files.removeObject(file)
        }
      }
    } else {
      console.error("'KbFormUploadComponent' needs 'this.args.key'")
    }
  }

  get minMaxValue() {
    return this.value ? this.value.length : 0
  }

  get allowBackgroundUpload() {
    return this.args.allowBackgroundUpload
  }

  get maxReached() {
    if (this.args.max) {
      if (this.minMaxValue >= this.args.max) {
        return true
      }
    }
    return false
  }

  get disabled() {
    return Boolean(this.maxReached && this.isRequired)
  }

  get customTestFailed() {
    if (!this.value.length) {
      return this.emptyMessage
    }
    return false
  }

  @action
  uploadIf(file) {
    /**
     * Called by ``FileUpload``, ``@onfileadd``.
     *
     * File will be automatically uploaded if ``allowBackgroundUpload``.
     */
    let q = this.fileQueue.find(this.args.key)
    if (this.args.maxFileSize && file.size > this.args.maxFileSize) {
      // Check maximum size
      q.files.removeObject(file)
      this.kbMessages.addError(
        `'${file.name}' will not be uploaded (maximum file size is ${this.args.maxFileSize} bytes)`
      )
    } else if (this.attemptedFiles.findIndex(f => f === file.name) > -1) {
      // Prevent duplicates
      q.files.removeObject(file)
    } else {
      this.attemptedFiles.push(file.name)
      // Prevent user from queuing up too many files.
      let fileSetIsValid = q.files.length <= this.args.max || !this.isRequired
      if (fileSetIsValid && this.allowBackgroundUpload) {
        this.upload(file)
      } else {
        this.emptyMessage = `${this.tooMaxMessage}. Choose which to upload`
        this.touched()
      }
    }
  }

  @action
  upload(file) {
    if (this.args.uploadUrl === undefined) {
      console.error("'KbFormUploadComponent' needs 'this.args.uploadUrl'")
    } else {
      this.uploadFile.perform(file, this.args.uploadUrl)
    }
  }

  @(task(function* (file, uploadUrl) {
    const headers = {}
    let { notUsed, token } = yield this.session.get("data.authenticated")
    if (token && !isEmpty(token)) {
      headers["Authorization"] = "Token " + token
    }
    try {
      let response = yield file.upload(uploadUrl, { headers: headers })
    } catch (reason) {
      // The file will be left in the queue.
      // An *Upload* button will give the user the option to retry.
      let message = reason
      if ('body' in reason) {
        let body = reason["body"]
        if ('errors' in body) {
          message = body["errors"]
        }
      }
      this.kbMessages.addError("Cannot upload file", message)
    }
    this.touched()
  })
    .maxConcurrency(3)
    .enqueue())
  uploadFile
}
