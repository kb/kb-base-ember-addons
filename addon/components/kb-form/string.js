import KbFormBaseComponent from "./base"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"

export default class KbFormStringComponent extends KbFormBaseComponent {
  tooMaxMessage = `${this.args.max} is the maximum number of characters`
  tooMinMessage = `${this.args.min} is the minimum number of characters`

  @tracked value = ""

  @action
  setStartingValue(val) {
    this.value = this.args.value ? this.args.value : ""
    this.actionValidate()
    this.actionChange(true)
    if (this.args.form) {
      this.args.form.actionToRegisterChildInput(this.args.key, this)
    }
  }

  get minMaxValue() {
    return this.value.length
  }
}
