import KbFormBaseComponent from "./base"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"

export default class KbFormBooleanComponent extends KbFormBaseComponent {
  @tracked value = false

  get customTestFailed() {
    if (
      !this.value ||
      this.value === "false" ||
      this.value === "0" ||
      this.value === "undefined"
    ) {
      return this.emptyMessage
    } else {
      return false
    }
  }

  @action
  setStartingValue(val) {
    let safeBoolean = this.args.value === "false" ? false : this.args.value
    this.value = Boolean(safeBoolean)
    this.actionValidate()
    this.actionChange(true)
    if (this.args.form) {
      this.args.form.actionToRegisterChildInput(this.args.key, this)
    }
  }

  @action
  toggleValue() {
    this.value = !this.value
    this.touched()
  }
}
