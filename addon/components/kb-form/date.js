import KbFormBaseComponent from "./base"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"
// BETTER? import moment from 'moment'

function formatDate(date) {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear()

  if (month.length < 2) month = "0" + month
  if (day.length < 2) day = "0" + day

  return [year, month, day].join("-")
}

export default class KbFormDateComponent extends KbFormBaseComponent {
  tooMaxMessage = `${this.args.max} is the maximum allowed date`
  tooMinMessage = `${this.args.min} is the minimum allowed date`

  @tracked value = false

  @action
  setStartingValue(val) {
    this.value = this.args.value ? formatDate(this.args.value) : ""
    this.actionValidate()
    this.actionChange(true)
    if (this.args.form) {
      this.args.form.actionToRegisterChildInput(this.args.key, this)
    }
  }

  get customTestFailed() {
    if (Date.parse(this.value)) {
      return false
    }
    return "This date is not valid"
  }

  get minMaxValue() {
    let valDate = this.value
    return valDate
  }
  get maxParam() {
    return this.args.max ? formatDate(this.args.max) : ""
  }
  get minParam() {
    return this.args.min ? formatDate(this.args.min) : ""
  }
}
