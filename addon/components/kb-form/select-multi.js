import KbFormBaseComponent from "./base"
import objValue from "@kb/ember-addon/utils/obj-value"
import { action } from "@ember/object"
import { isBlank } from "@ember/utils"
import { tracked } from "@glimmer/tracking"

export default class KbFormSelectComponent extends KbFormBaseComponent {
  tooMaxMessage = `You cannot select more than ${this.maxParam} items`
  tooMinMessage = `You need to select at least ${this.minParam} items`
  emptyMessage = `You need to select at least ${this.minParam} items`

  @tracked selectedItems = []

  // This is what goes back to the form. If you are expecting primitive values
  // instead of an object (i.e. a model instance from options which are model
  // instance) then you set the `@valueIsPrimitive` property.
  get value() {
    if (this.args.valueIsPrimitive && !this.args.optionsArePrimitive) {
      return this.selectedItems.map(selectedItem =>
        objValue(selectedItem, this.args.valueField)
      )
    } else {
      return this.selectedItems
    }
  }

  // This is for the PowerSelect component. Simply keep the selectedItems as
  // objects or primitives.
  get selectedValue() {
    return this.selectedItems
  }

  get minMaxValue() {
    return this.value.length
  }

  @action
  changeValue(selecteds) {
    this.selectedItems = selecteds
    this.touched()
  }

  @action
  setStartingValue() {
    // It is important to build the initially selectedItems (i.e. `@value`) by
    // *picking* them from the `@options` list itself. This is because you want
    // those items to show as selected in the dropdown (i.e. selecting them will
    // remove from list instead of just adding them twice if you don't handle
    // this properly). If object based, the selected objects will not "===" the
    // options objects even if they are identical (i.e. they'll be no reference
    // between the selected items and the options). Doing it this way creates a
    // reference between the selected items and the options.
    // ...
    // ... but we don't have to do any of that if the `@value` has no initial
    // value.
    if (!isBlank(this.args.value)) {
      // ... and we don't have to do any of that is the `@options` are not
      // objects.
      if (this.args.optionsArePrimitive || !this.args.options) {
        this.selectedItems = this.args.value
      } else {
        let selectList = []
        // Here we handle an unlikely issue that the initial `@value` is a csv
        // string.
        if (this.args.value.includes(",")) {
          selectList = this.args.value.split(",")
        } else {
          selectList = this.args.value
        }
        // Here we handle the initial `@value` whether it is a list of objects
        // OR a list of primitive values representing the id field
        // ("valueField") of the options list.
        let selectValueList
        if (!this.args.valueIsPrimitive) {
          selectValueList = selectList.map(listItem =>
            String(objValue(listItem, this.args.valueField))
          )
        } else {
          selectValueList = selectList.map(listItem => String(listItem))
        }
        // Here we "pick" the objects from the `@options`.
        if (this.args.options) {
        } else {
        }
        this.selectedItems = this.args.options.filter(o =>
          selectValueList.includes(String(objValue(o, this.args.valueField)))
        )
      }
    }
    // These calls, as with all kb-form controls, "actions up" to the form.
    this.actionValidate()
    this.actionChange(true)
    if (this.args.form) {
      this.args.form.actionToRegisterChildInput(this.args.key, this)
    }
  }
}
