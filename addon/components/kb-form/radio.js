import KbFormBaseComponent from "./base"
import objValue from "@kb/ember-addon/utils/obj-value"
import { action } from "@ember/object"
import { isBlank } from "@ember/utils"
import { tracked } from "@glimmer/tracking"

export default class KbFormRadioComponent extends KbFormBaseComponent {
  @tracked selectedItem = null

  get radioOptions() {
    // We will deal with options as objects... even if primitive...
    let self = this
    if (this.args.optionsArePrimitive) {
      //... by converting the list to an object list.
      return this.args.options.map(opt => {
        let obj = new Object()
        obj[self.args.valueField] = opt
        obj[self.args.searchField] = opt
        return obj
      })
    } else {
      //... already an object list
      return this.args.options
    }
  }

  get value() {
    return this.selectedItem
  }

  get fullyValid() {
    return this.selectedItem
  }

  @action
  verifyPresence(select) {
    if (this.value) {
      this.touched()
    } else {
      return this.validOrNotRequired
    }
  }

  @action
  changeValue(radioValue) {
    // Deal with selectedItem as a primitive...
    this.selectedItem = objValue(radioValue, this.args.valueField)
    this.touched()
  }

  @action
  setStartingValue() {
    if (!isBlank(this.args.value)) {
      // Deal with selectedItem as a primitive...
      this.selectedItem = this.args.value
    }
    this.actionValidate()
    this.actionChange(true)
    if (this.args.form) {
      this.args.form.actionToRegisterChildInput(this.args.key, this)
    }
  }
}
