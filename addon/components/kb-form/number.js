import KbFormBaseComponent from "./base"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"

export default class KbFormNumberComponent extends KbFormBaseComponent {
  @tracked value = 0

  @action
  setStartingValue(val) {
    this.value = Number(this.args.value)
    this.actionValidate()
    this.actionChange(true)
    if (this.args.form) {
      this.args.form.actionToRegisterChildInput(this.args.key, this)
    }
  }

  get customTestFailed() {
    if (this.value === 0 || this.value === "0" || this.value === "undefined") {
      return "Should not be zero"
    } else {
      return false
    }
  }
}
