import KbComponentBase from "../kb-component-base"
import { action } from "@ember/object"
import { isBlank } from "@ember/utils"
import { tracked } from "@glimmer/tracking"

export default class KbFormBaseComponent extends KbComponentBase {
  // True once the user has interacted with this component even once.
  @tracked wasTouched = false
  // The message displayed by the component when validOrNotRequired. This may
  // get updated to a different message by the `fullyValid` property.
  @tracked validateMessage = "This field is required"
  /**
   * @file
   * Handle any pertinent actions, onclick, onchange, onblur, etc.
   *
   * @fires {actionValidate(key, value)}
   * So that the form-manager can keep track of all its fields' validation.
   *
   * @fires {actionChange(key, value)}
   * So that the form-manager can change its own internal values(key, value).
   *
   * @event
   * Don't override this. Call it from a input specific action:
   *
   * @Example
   * customHandler() {
   *   this.touched()
   * }
   */
  @action
  touched() {
    // Flag this "touched" input-component `this.wasTouched` which gives the
    // component permission to display any error messages, etc,  even before the
    // form is submitted.
    this.wasTouched = true
    this.actionValidate()
    this.actionChange(this.wasTouched)
  }

  /**
   * @file
   * By default we expect the Input component (or addon) to mut the value.
   *
   * @fires {actionValidate(key, value)}
   * So that the form-manager can keep track of all its fields' validation.
   *
   * @event
   * If the value is not mutable inside the component, you will need to change
   * this to preset the component's "@tracked" property:
   *
   * @Example
   * setStartingValue(val) {
   *   this.value = this.args.value === "false" ? false : this.args.value
   *   this.actionValidate()
   * }
   */
  @action
  setStartingValue(val) {
    this.actionValidate()
    this.actionChange(true)
    if (this.args.form) {
      this.args.form.actionToRegisterChildInput(this.args.key, this)
    }
  }

  /**
   * @file
   * Calls a passed function (we call "validateHook") reporting on the internal
   * validation status of the component.
   *
   * @fires {validateHook(fieldKey, whetherValid, errorMessageIfRequired)}
   * So that the form-manager can keep track of all its fields' validation.
   *
   * @Example
   * <KbForm as |form|>
   *   <KbForm:Date @validateHook={{form.actionToValidateAllMyInputs}} />
   *   <KbForm:Time @validateHook={{form.actionToValidateAllMyInputs}} />
   * </KbForm>
   */
  @action
  actionValidate() {
    if (this.args.validateHook) {
      this.args.validateHook(
        this.args.key,
        this.validOrNotRequired,
        !this.isRequired ? "" : this.validateMessage
      )
    }
  }

  /**
   * @file
   * On change, each input attached to a changeHook will action up its key and
   * value so that the parentComponent/controller can update the data.
   *
   * @fires {changeHook(fieldKey, value)}
   * So that the form-manager can keep track of all its fields' values.
   *
   * @Example
   * <KbForm as |form|>
   *   <KbForm:Date @changeHook={{this.controllerActionToSubmitFormToAPI}} />
   *   <KbForm:Time @changeHook={{form.controllerActionToSubmitFormToAPI}} />
   * </KbForm>
   */
  @action
  actionChange(wasTouched) {
    if (this.args.changeHook && wasTouched) {
      this.args.changeHook(this.args.key, this.changeHookValue)
    }
  }

  /** `validOrNotRequired` is the field's verification state.
   * @Recommend: Do not override. */
  get validOrNotRequired() {
    let showAsValid = Boolean(this.fullyValid || !this.isRequired)
    return showAsValid
  }
  /** `fullyValid` makes the final determination of validity.
   * @Important: Always returns current validity or invalidity whether it is
   * required or not.
   * @Recommend:
   * - Could be overridden, but often doesn't need to be: -
   * Case:`kb-input-select-multi.js` - Better to overide the customTestFailed
   * property as with Case:`kb-input-email.js`. */
  get fullyValid() {
    if (!this.value || 0 === this.value.length) {
      this.validateMessage = this.emptyMessage
      return false
    } else if (this.customTestFailed) {
      this.validateMessage = this.customTestFailed
      return false
    } else if (this.maxExceeded) {
      this.validateMessage = this.tooMaxMessage
      return false
    } else if (this.minDestitute) {
      this.validateMessage = this.tooMinMessage
      return false
    } else {
      this.validateMessage = "Appears to be valid"
      return true
    }
  }
  /** `fullyValid` makes the final determination of validity.
   * @Important: Always returns current validity or invalidity whether it is
   * required or not.
   * @Recommend:
   * - Could/Should be overridden, but not often needed:
   *  - Case:`kb-input-select.js` and Case:`kb-input-email.js` */
  get customTestFailed() {
    // Not overidden, this is a validation which never fails.
    return false
  }
  /** `value` The goto, up-to-date reference point of the COMPONENT value.
   * @Important: Some kb-inputs, like kb-input-boolean and kb-input-select, need
   * an actionUp methodology, because their values cannot be mutated */
  // get value() {
  //   return this.args.value
  // }
  get changeHookValue() {
    return this.value
  }
  // Some default validity messages, which should/could be overridden for some
  // messages relevant to the cmmponents datatype. Case:`kb-input-taggit.js`
  tooMaxMessage = "Too big"
  tooMinMessage = "Too small"
  emptyMessage = "This field is required"
  /** GENERAL PURPOSE VALIDATION METHODS
   * @Recommend: Generally no need to override these
   * You'll see they often fit all cases, like string.length and array.length,
   * etc  */
  get maxExceeded() {
    if (this.maxParam) {
      if (this.minMaxValue > this.maxParam) {
        return true
      }
    }
    return false
  }
  get minDestitute() {
    if (this.minParam) {
      if (this.minMaxValue < this.minParam) {
        return true
      }
    }
    return false
  }
  get minMaxValue() {
    return this.value
  }
  get maxParam() {
    return this.args.max
  }
  get minParam() {
    return this.args.min ? this.args.min : 1
  }
  get showValidations() {
    if (isBlank(this.args.showValidations)) return false
    return Boolean(this.wasTouched || this.args.showValidations)
  }
  get isRequired() {
    let isRequired =
      this.args.required === "false" ? false : Boolean(this.args.required)
    return isRequired
  }
  get showTick() {
    return Boolean(this.showValidations && this.validOrNotRequired)
  }
  get showErr() {
    return Boolean(this.showValidations && !this.validOrNotRequired)
  }
  get type() {
    return this.args.type ? this.args.type : "text"
  }
  get visible() {
    return this.args.inputIsVisible
  }
}
