import KbComponentBase from "../kb-component-base"

export default class KbFormErrorComponent extends KbComponentBase {
  get visibility() {
    return this.args.showErr ? "visible" : "hidden"
  }
}
