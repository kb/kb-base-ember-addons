/* global CKEDITOR */
import Ember from "ember"
import KbFormBaseComponent from "./base"
import { action } from "@ember/object"
import { debounce } from "@ember/runloop"
// import { registerWaiter } from "@ember/test"
import { task } from "ember-concurrency"
import { tracked } from "@glimmer/tracking"

const DEBOUNCE_MS = 250

export default class KbFormCkEditorComponent extends KbFormBaseComponent {
  tooMaxMessage = `${this.args.max} is the maximum number of characters`
  tooMinMessage = `${this.args.min} is the minimum number of characters`

  @tracked ckeditor = null
  @tracked html = ""

  get value() {
    return this.html
  }

  get minMaxValue() {
    return this.html.length
  }

  get testing() {
    return Ember.testing
  }

  @action
  setStartingValue(val) {
    this.html = this.args.value
    this.actionValidate()
    this.actionChange(true)
    if (this.args.form) {
      this.args.form.actionToRegisterChildInput(this.args.key, this)
    }
  }

  @action
  onTextareaChange() {
    this.html = this.args.value
    this.touched()
  }

  @action
  onCkChange(ckeditor) {
    this.html = ckeditor.getData()
    this.touched()
  }

  @action
  registerCkEditor(element) {
    this.initializeEditor.perform(element)
  }

  @action
  unregisterCkEditor(element) {
    if (element) {
      this.ckeditor.destroy()
      this.ckeditor = null
      this.isReady = false
    }
  }

  @(task(function* (element) {
    let CK = yield new CKEDITOR.replace(element)
    CKEDITOR.config.forcePasteAsPlainText = true
    CKEDITOR.config.placeholder = this.args.placeholder
    if (!this.isDestroyed && !this.isDestroying) {
      CK.on("change", () => {
        // Event fires several times in initialization. Debouncing to fix.
        debounce(this, "onCkChange", ...[CK, ...arguments], DEBOUNCE_MS)
      })
    }
    yield CK.setData(this.args.value)
    this.ckeditor = CK
  }).drop())
  initializeEditor
}
