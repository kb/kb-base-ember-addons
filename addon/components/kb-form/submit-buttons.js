import KbComponentBase from "../kb-component-base"
import { action } from "@ember/object"

export default class KbFormSubmitButtonsComponent extends KbComponentBase {
  @action
  actionSubmit() {
    if (this.args.actionSubmitHook) {
      this.args.actionSubmitHook(true)
    }
  }

  @action
  actionCancel() {
    if (this.args.actionCancelHook) {
      this.args.actionCancelHook(false)
    }
  }
}
