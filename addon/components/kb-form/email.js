import KbFormBaseComponent from "./base"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"

export default class KbFormEmailComponent extends KbFormBaseComponent {
  tooMaxMessage = `${this.args.max} is the maximum number of characters`
  tooMinMessage = `${this.args.min} is the minimum number of characters`

  @tracked value = false

  @action
  setStartingValue(val) {
    this.value = String(this.args.value)
    this.actionValidate()
    this.actionChange(true)
    if (this.args.form) {
      this.args.form.actionToRegisterChildInput(this.args.key, this)
    }
  }

  get customTestFailed() {
    var mailformat = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/
    if (this.value.match(mailformat)) {
      return false
    }
    return "This email address is not valid"
  }

  get minMaxValue() {
    return this.value.length
  }
}
