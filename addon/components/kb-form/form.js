import Component from "@glimmer/component"
import { action } from "@ember/object"

export default class KbFormFormComponent extends Component {
  @action
  submit(event) {
    if (this.args.onSubmit) {
      this.args.onSubmit()
      event.preventDefault()
    } else {
      console.error("'KbFormFormComponent' needs 'this.args.onSubmit'")
    }
  }
}
