import Component from "@glimmer/component"

export default class KbFormFormFieldInputComponent extends Component {
  get inputClass() {
    if (this.args.disabled) {
      return "bg-gray-100"
    } else {
      return ""
    }
  }
}
