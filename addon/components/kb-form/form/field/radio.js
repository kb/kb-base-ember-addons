import Component from "@glimmer/component"
import { action } from "@ember/object"

export default class KbFormFormFieldRadioComponent extends Component {
  @action
  checkedRadio(event) {
    event.preventDefault()
    if (this.args.changeset && this.args.key) {
      this.args.changeset.set(this.args.key, event.target.id)
    } else {
      console.error(
        "'KbFormFormFieldRadioComponent' needs 'this.args.changeset' and 'this.args.key'"
      )
    }
  }
}
