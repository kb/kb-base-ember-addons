import Component from "@glimmer/component"

export default class ButtonComponent extends Component {
  /**
   * The submit button on a form...
   *
   * Do not call 'this.args.onClick' because the action is on the form element.
   *
   * .. tip:: For a normal button, use ``KbButton``
   *
   */

  get buttonClass() {
    if (this.args.buttonType === "submit") {
      return "text-white bg-blue-600 hover:bg-blue-700 focus:ring-blue-500"
    } else if (this.args.buttonType === "warning") {
      return "text-red-700 bg-red-100 hover:bg-red-200 focus:ring-red-500"
    } else {
      return "text-blue-700 bg-blue-100 hover:bg-blue-200 focus:ring-blue-500"
    }
  }
}
