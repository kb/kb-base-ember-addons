import KbComponentBase from "../kb-component-base"

export default class KbTabBarPanelsComponent extends KbComponentBase {
  get panelClass() {
    return `mx-0 sm:-ml-2 -mt-2 -mr-2 mb-3 p-0 pr-6 bg-white sm:border-b sm:border-gray-300`.trim()
  }
}
