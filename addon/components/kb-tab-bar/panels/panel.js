import KbComponentBase from "../../kb-component-base"

export default class KbTabBarPanelsPanelComponent extends KbComponentBase {
  get panelClass() {
    if (this.args.tabBar) {
      return this.args.tabBar.selectedTabKey === this.args.key
        ? `w-full block`
        : `hidden`
    }
  }
}
