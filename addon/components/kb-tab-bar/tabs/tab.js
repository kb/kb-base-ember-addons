import KbComponentBase from "../../kb-component-base"
import { action } from "@ember/object"

export default class KbTabBarTabsTabComponent extends KbComponentBase {
  @action
  actionTabSelect() {
    if (this.args.tabBar) {
      this.args.tabBar.actionTabChange(this.args.key)
    } else {
      console.error("'KbTabBarTabsTabComponent' needs 'this.args.tabBar'")
    }
  }

  get ariaCurrent() {
    if (this.isCurrentTab) {
      return "page"
    } else {
      return ""
    }
  }

  get isCurrentTab() {
    let result = false
    if (this.args.tabBar) {
      if (this.args.tabBar.selectedTabKey === this.args.key) {
        result = true
      }
    } else {
      console.error("'KbTabBarTabsTabComponent' needs 'this.args.tabBar'")
    }
    return result
  }

  get tabClass() {
    if (this.isCurrentTab) {
      return "border-blue-500 text-blue-600"
    } else {
      return "border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-200"
    }
  }
}
