import Component from "@glimmer/component"
import { action } from "@ember/object"
import { inject as service } from "@ember/service"

export default class KbMessageComponent extends Component {
  @service kbMessages

  get heading() {
    let count = this.kbMessages.countErrors
    if (count === 1) {
      return "There has been an error"
    } else {
      return `There have been ${count} errors`
    }
  }

  @action
  clearErrors() {
    this.kbMessages.clearErrors()
  }

  @action
  clearMessages() {
    this.kbMessages.clearMessages()
  }
}
