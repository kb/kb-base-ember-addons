import KbComponentBase from "../kb-component-base"
import { inject as service } from "@ember/service"

export default class KbToolBarToolComponent extends KbComponentBase {
  @service router

  get isActive() {
    /** Is this *link* active? */
    let currentRoute = this.router.currentRouteName
    return currentRoute.includes(this.args.route)
  }

  get isButton() {
    /**
     * May be a tab or a button
     */
    return this.args.icon !== undefined
  }

  get tabClass() {
    /**
     * If the component gets an ``@icon``, then style as a button.
     */
    let result = ""
    if (this.isButton) {
      result =
        "relative inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md text-blue-700 bg-blue-100 shadow-sm hover:bg-blue-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
    } else {
      if (this.isActive) {
        result = "border-blue-500 text-gray-900"
      } else {
        result =
          "border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700"
      }
      result =
        result +
        " inline-flex items-center px-1 pt-1 border-b-2 text-sm font-medium"
    }
    return result
  }
}

/**
Current
border-blue-500
text-gray-900

Default
border-transparent
text-gray-500
hover:border-gray-300
hover:text-gray-700

Common
inline-flex
items-center
px-1
pt-1
border-b-2
text-sm
font-medium
*/
