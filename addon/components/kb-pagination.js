import Component from "@glimmer/component"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"

export default class KbPaginationComponent extends Component {
  /**
   * Paginate an Ember concurrency model using a next and previous button.
   *
   * To use this component, pass the following:
   *
   * 1. The '@model' (ember concurrency)
   * 2. The '@page' number
   * 3. The number of '@recordsPerPage'
   * 4. An action method which can be called to '@setPage'
   *
   */
  // @tracked pageNumber = undefined

  get firstRecord() {
    let result = (this.currentPage - 1) * this.recordsPerPage
    if (result < 1) {
      result = 1
    }
    return result
  }

  get lastRecord() {
    let result = undefined
    let last = this.currentPage * this.recordsPerPage
    let total = this.totalRecords
    if (last > total) {
      result = total
    } else {
      result = last
    }
    return result
  }

  get currentPage() {
    return parseInt(this.args.page, 10) || 1
  }

  get recordsPerPage() {
    return this.args.recordsPerPage
  }

  get showNext() {
    let result = true
    if (this.lastRecord === this.totalRecords) {
      result = false
    }
    return result
  }

  get showPrevious() {
    let result = true
    if (this.currentPage === 1) {
      result = false
    }
    return result
  }

  get totalRecords() {
    return this.args.model.meta.total
  }

  @action
  nextPage() {
    if (this.args.setPage === undefined) {
      throw "'KbPaginationComponent', the 'setPage' parameter is 'undefined'"
    } else {
      let currentPage = this.currentPage
      if (currentPage !== undefined) {
        this.args.setPage(currentPage + 1)
      }
    }
  }

  @action
  previousPage() {
    let currentPage = this.currentPage
    if (currentPage !== undefined) {
      let page = currentPage
      if (page > 1) {
        page = page - 1
      } else {
        page = 1
      }
      if (this.args.setPage) {
        this.args.setPage(page)
      }
    }
  }
}
