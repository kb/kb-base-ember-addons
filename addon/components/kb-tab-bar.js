import KbComponentBase from "./kb-component-base"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"

export default class KbTabBarComponent extends KbComponentBase {
  @tracked selectedTabKey = this.args.startTab

  @action
  actionTabChange(key) {
    this.selectedTabKey = key
    if (this.args.onTabChange) {
      this.args.onTabChange(key)
    }
  }

  @action
  exposeTabBar(e) {
    console.log("exposeTabBar event", e)
  }
}
