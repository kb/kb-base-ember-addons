// import Component from "@glimmer/component"
import Component from "@ember/component"
import ENV from "front/config/environment"
import fetch from "fetch"
import FileSaverMixin from "ember-cli-file-saver/mixins/file-saver"
import { action } from "@ember/object"
import { inject as service } from "@ember/service"

export default class AttachmentComponent extends Component.extend(
  FileSaverMixin
) {
  @service cookies
  @service kbMessages
  @service session

  @action
  actionDownload() {
    let self = this
    let csrfToken = this.cookies.read("csrftoken")
    let authToken = this.session.data.authenticated.token

    let downloadUrl = `${ENV.APP.API_URL}${this.args.download_url}?type=${this.args.download_type}`
    fetch(downloadUrl, {
      responseType: "blob",
      headers: {
        "X-CSRFToken": csrfToken,
        credentials: "include",
        Authorization: "Token " + authToken
      }
    })
      .then(response => {
        if (response.ok) {
          return response.blob()
        }
        throw new Error("Cannot download file")
      })
      .then(myBlob => {
        self.saveFileAs(this.filename, myBlob)
      })
      .catch(error => {
        console.log(error)
        this.kbMessages.addError("Cannot download file", error)
      })
  }
}
