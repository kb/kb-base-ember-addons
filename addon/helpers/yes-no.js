import { helper } from "@ember/component/helper"

export default helper(function yesNo(params /*, hash*/) {
  return params[0] ? "Yes" : "No"
})
