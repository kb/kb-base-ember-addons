import { helper } from "@ember/component/helper"

export function eyesOnly(params /*, hash*/) {
  let authentication = params[0]
  let isAuthenticated = params[1]
  let permissionLevel = params[2]
  authentication = authentication ? authentication.toString() : "false"
  permissionLevel = permissionLevel ? permissionLevel.toString() : "none"

  if (!authentication || authentication == "false") {
    return true
  } else {
    if (authentication === "authenticated") {
      return Boolean(isAuthenticated)
    }
    if (authentication.length) {
      return (
        Boolean(isAuthenticated) &&
        (authentication.includes(permissionLevel) ||
          authentication.includes("authenticated"))
      )
    } else {
      return Boolean(isAuthenticated)
    }
  }
}

export default helper(eyesOnly)
