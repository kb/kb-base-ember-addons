import { helper } from "@ember/component/helper"

export function isTagged(params /*, hash*/) {
  if (!params[0]) {
    return false
  } else {
    return params[0].includes(params[1])
  }
}

export default helper(isTagged)
