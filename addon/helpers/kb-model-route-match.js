import { helper } from "@ember/component/helper"

export function kbModelRouteMatch(params /*, hash*/) {
  let childRoute = params[0]
  let parentRoute = params[1]
  let routeId = params[2]
  let isActive = params[3]
  if (!routeId) {
    routeId = ""
  }
  if (!childRoute || !parentRoute) {
    return false
  }
  let childModel = childRoute.split(".")[0]
  let childRouteSuffix = childRoute.split(".").slice(1).toString()
  if (
    routeId &&
    ["delete", "detail", "edit"].find(r => childRouteSuffix.includes(r))
  ) {
    childRouteSuffix = ""
  }
  childRoute = `${childModel}/${routeId}/${childRouteSuffix}`

  let parentModel = parentRoute.split(".")[0]
  let parentRouteSuffix = ""
  if (isActive) {
    parentRouteSuffix = parentRoute.split(".").slice(1).toString()
    if (
      routeId &&
      ["delete", "detail", "edit"].find(r => parentRouteSuffix.includes(r))
    ) {
      parentRouteSuffix = ""
    }
  }
  parentRoute = `${parentModel}/${routeId}/${parentRouteSuffix}`

  if (childRoute && parentRoute) {
    if (isActive) {
      return childRoute === parentRoute
    } else {
      return childRoute.startsWith(`${parentRoute}`)
    }
  }
  return false
}

export default helper(kbModelRouteMatch)
