import { helper } from "@ember/component/helper"
import safeSelect from "@kb/ember-addon/utils/safe-select"

export default helper(function getSelectOption(params /*, hash*/) {
  return safeSelect(...params)
})
