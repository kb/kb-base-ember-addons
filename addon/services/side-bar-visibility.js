import Service from "@ember/service"

export default class SideBarVisibilityService extends Service {
  isVisible = false

  setNotifyFunction(notifyFunction) {
    this.notifyFunction = notifyFunction
  }

  toggle() {
    this.isVisible = !this.isVisible
    this.notifyFunction(this.isVisible)
  }

  close() {
    this.isVisible = false
    this.notifyFunction(this.isVisible)
  }
}
