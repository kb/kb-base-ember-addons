import ENV from "front/config/environment"
import Service from "@ember/service"
import { tracked } from "@glimmer/tracking"

export default class KbPageService extends Service {
  @tracked contactId = undefined
  @tracked contactName = undefined
  @tracked contactRoute = undefined
  @tracked signOutAction = undefined
  @tracked title = undefined

  constructor() {
    super(...arguments)
    let contactRoute = ENV.APP.contactRoute
    this.setContactRoute(contactRoute)
  }

  setContactId(contactId) {
    this.contactId = contactId
  }

  setContactName(contactName) {
    this.contactName = contactName
  }

  setContactRoute(contactRoute) {
    this.contactRoute = contactRoute
  }

  setSignOutAction(signOutAction) {
    this.signOutAction = signOutAction
  }

  setTitle(title) {
    this.title = `${title}`
  }

  signOut() {
    this.contactId = undefined
    this.contactName = undefined
  }
}
