import Service from "@ember/service"
import { dropTask, timeout } from "ember-concurrency"
import { tracked } from "@glimmer/tracking"

export default class KbMessagesService extends Service {
  messages = []

  @tracked errors = []
  @tracked currentMessage = undefined

  get countErrors() {
    return this.errors.length
  }

  get countMessages() {
    return this.messages.length
  }

  addError(description, e) {
    let message = ""
    if (e !== undefined) {
      if (e.errors === undefined) {
        if (e.message === undefined) {
          message = `${e}`.trim()
        } else {
          message = `${e.message}`
        }
      } else if (e.errors.length > 0) {
        let detail = e.errors[0]
        if (detail.detail !== undefined) {
          message = `${detail.detail}`.trim()
        } else if (detail.status !== undefined) {
          message = `${message} ${detail.status}`.trim()
        } else {
          message = `${message}`.trim()
        }
      }
      if (message.length > 0) {
        message = ` (${message})`
      }
    }
    let errorTime = new Date().toLocaleString().replace(",", "")
    this.errors.pushObject(`${errorTime}: ${description}${message}`)
  }

  addMessage(message) {
    let currentTime = new Date().toLocaleString().replace(",", "")
    this.messages.pushObject({ messageTime: currentTime, message: message })
    this.currentMessageTask.perform()
  }

  clearErrors() {
    this.errors.clear()
  }

  clearMessages() {
    this.messages.clear()
  }

  @dropTask *currentMessageTask() {
    do {
      if (this.currentMessage) {
        yield timeout(4000)
        this.currentMessage = undefined
        yield timeout(500)
      }
      if (this.messages) {
        this.currentMessage = this.messages.pop()
      } else {
      }
    } while (this.currentMessage)
  }
}
