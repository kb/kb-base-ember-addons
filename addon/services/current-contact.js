import Service from "@ember/service"
import { inject as service } from "@ember/service"

export default class CurrentContactService extends Service {
  @service kbMessages
  @service kbPage
  @service session
  @service store

  contact = null
  contactId = 0

  invalidate() {
    this.set("contact", null)
    this.set("contactId", 0)
    this.kbPage.setContactId(undefined)
  }

  isAppAdministrator() {
    let result = false
    if (this.session.isAuthenticated && this.contact) {
      result = this.contact.isAppAdministrator
    }
    return result
  }

  isAppAdministratorOrManager() {
    let result = false
    if (this.session.isAuthenticated && this.contact) {
      result = this.contact.isAppAdministrator || this.contact.isManager
    }
    return result
  }

  async load() {
    /**
     * Managing a Current User
     * https://github.com/simplabs/ember-simple-auth/blob/master/guides/managing-current-user.md
     */
    if (this.session.isAuthenticated) {
      try {
        let contactId = this.session.data.authenticated.contactId
        if (contactId) {
          let contact = await this.store.findRecord("contact", contactId)
          this.set("contact", contact)
          this.set("contactId", contactId)
          this.kbPage.setContactId(contactId)
          this.kbPage.setContactName(contact.fullName)
        }
      } catch (e) {
        this.kbMessages.addError("Cannot load contact", e)
      }
    }
  }
}
