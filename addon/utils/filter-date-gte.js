import filterNumberGte from "./filter-number-gte"

export default (rowFieldDate, filterSwitch) => {
  return filterNumberGte(
    new Date(rowFieldDate).valueOf(),
    new Date(filterSwitch).valueOf()
  )
}
