import ArrayProxy from "@ember/array/proxy"
import ObjectProxy from "@ember/object/proxy"
import { typeOf } from "@ember/utils"
import { get } from "@ember/object"

function isProxy(o) {
  return !!(o && (o instanceof ObjectProxy || o instanceof ArrayProxy))
}

function unwrapProxy(o) {
  return isProxy(o) ? unwrapProxy(get(o, "content")) : o
}

export default (obj, key) => {
  // ==== typeOf ===
  // | 'string'      | String primitive or String object.                   |
  // | 'number'      | Number primitive or Number object.                   |
  // | 'boolean'     | Boolean primitive or Boolean object.                 |
  // | 'null'        | Null value                                           |
  // | 'undefined'   | Undefined value                                      |
  // | 'function'    | A function                                           |
  // | 'array'       | An instance of Array                                 |
  // | 'regexp'      | An instance of RegExp                                |
  // | 'date'        | An instance of Date                                  |
  // | 'filelist'    | An instance of FileList                              |
  // | 'class'       | An Ember class (created using EmberObject.extend())  |
  // | 'instance'    | An Ember object instance                             |
  // | 'error'       | An instance of the Error object                      |
  // | 'object'      | A JavaScript object not inheriting from EmberObject
  let objType = typeOf(obj)
  if (
    [
      "string",
      "number",
      "boolean",
      "null",
      "undefined",
      "array",
      "date"
    ].includes(objType)
  ) {
    return obj
  } else if (objType === "object" && key) {
    // console.log("objType object", obj[key])
    return obj.hasOwnProperty(key) ? obj[key] : obj
  } else if (objType === "instance" && key) {
    let unwrappedObj = unwrapProxy(obj)
    // console.log("unwrappedObj instance", unwrappedObj)
    if (unwrappedObj) {
      let result = get(unwrappedObj, key)
      if (result === undefined) {
        return unwrappedObj
      } else {
        return result
      }
    } else {
      return obj
    }
  } else {
    return obj
  }
}
