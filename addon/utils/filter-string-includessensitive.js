export default (rowFieldString, filterString) => {
  let safeFieldString = rowFieldString ? String(rowFieldString) : ""
  let safeFilterString = filterString ? String(filterString) : ""
  return safeFieldString.includes(safeFilterString)
}
