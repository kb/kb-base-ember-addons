import objValue from "@kb/ember-addon/utils/obj-value"
import { typeOf } from "@ember/utils"

export default (a, b, sortOpts) => {
  let A = a
  let B = b
  if (sortOpts) {
    A = objValue(a, sortOpts.key)
    B = objValue(b, sortOpts.key)
    if (sortOpts.desc) {
      ;[A, B] = [B, A]
    }
  }
  if (typeOf(A) === "string") {
    A = A.toUpperCase()
  }
  if (typeOf(B) === "string") {
    B = B.toUpperCase()
  }
  if (A < B) {
    return -1
  }
  return A > B ? 1 : 0
}
