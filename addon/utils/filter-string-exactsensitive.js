export default (rowFieldString, filterString) => {
  let safeFieldString = rowFieldString ? rowFieldString : ""
  let safeFilterString = filterString ? filterString : ""
  return safeFieldString === safeFilterString
}
