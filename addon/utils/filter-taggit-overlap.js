import { isBlank } from "@ember/utils"

export default (rowFieldList, filterTaggit) => {
  return (
    filterTaggit.some(x => rowFieldList.includes(x)) || isBlank(filterTaggit)
  )
}
