export default (rows, sortObject) => {
  let sorters = Object.entries(sortObject)
  for (let [key, sorter] of sorters) {
    rows.sort((a, b) => sorter(a, b, { key: key }))
  }
  return rows
}
