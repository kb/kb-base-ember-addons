export default (rowFieldString, filterString) => {
  let safeFieldString = rowFieldString ? rowFieldString.toLowerCase() : ""
  let safeFilterString = filterString ? filterString.toLowerCase() : ""
  return safeFieldString.includes(safeFilterString)
}
