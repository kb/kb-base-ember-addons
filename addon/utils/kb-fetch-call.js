import { isEmpty } from "@ember/utils"

/**
  Call signature: fetch `type` with `data` with optional `authToken` and/or `csrfToken`
  @param {string} type POST, GET, or PATCH, etc.
  @param {json} data Data package as dictionary.
  @param {string} csrfToken Optional. Cookie token. e.g. `this.get('cookies').read('csrftoken')`
  @param {string} authToken Optional. Auth token. e.g. `this.get('session.data.authenticated.token')`
  @returns {json} The options required for a JQuery.ajax call.
  @example

  import fetch from 'fetch'

  let carList = fetch(
    '/cars',
    kbFetchCall('get', {'make': 'Ford'})
  )
    .then(response => response.json())
    .then(json => json.models)
    .catch(ex => // handle ex)
*/
export default (type, data, csrfToken, authToken, isProduction) => {
  let headers = {
    Accept: "application/json",
    "Content-type": "application/json;charset=utf-8"
  }
  if (isProduction && !isEmpty(csrfToken)) {
    headers["X-CSRFToken"] = csrfToken
    headers["credentials"] = "include"
  }
  if (!isEmpty(authToken)) {
    headers["Authorization"] = "Token " + authToken
    // headers["credentials"] = "include"  // Seems better without. Been a long without. Leaving just because you never know...
  }
  if (["get", "head"].includes(type.toLowerCase())) {
    return {
      headers: headers,
      method: type.toUpperCase()
    }
  } else {
    return {
      body: JSON.stringify(data),
      headers: headers,
      method: type.toUpperCase()
    }
  }
}
