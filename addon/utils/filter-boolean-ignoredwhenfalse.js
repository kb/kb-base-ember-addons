export default (rowFieldBoolean, filterSwitch) => {
  let safeFieldBoolean = rowFieldBoolean === "false" ? false : rowFieldBoolean
  let safeFilterBoolean = filterSwitch === "false" ? false : filterSwitch
  return Boolean(safeFieldBoolean) || Boolean(safeFilterBoolean) === false
}
