import sortDate from "./sort-date"

export default (a, b, sortOpts) => {
  if (!sortOpts) {
    sortOpts = new Object()
  }
  sortOpts["desc"] = true
  return sortDate(a, b, sortOpts)
}
