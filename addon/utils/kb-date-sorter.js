export default (a, b) => {
  let A = new Date(a)
  let B = new Date(b)
  return isNaN(Date.parse(a)) - isNaN(Date.parse(b)) || -(A < B) || +(A > B)
}
