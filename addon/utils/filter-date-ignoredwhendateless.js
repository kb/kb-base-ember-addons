import filterNumberIgnoredwhenzero from "./filter-number-ignoredwhenzero"

export default (rowFieldDate, filterSwitch) => {
  return filterNumberIgnoredwhenzero(
    new Date(rowFieldDate).valueOf(),
    new Date(filterSwitch).valueOf()
  )
}
