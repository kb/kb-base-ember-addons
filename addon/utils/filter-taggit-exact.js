export default (rowFieldList, filterTaggit) => {
  if (rowFieldList.length === filterTaggit.length) {
    return rowFieldList
      .sort()
      .every((listItem, index) => listItem === filterTaggit.sort()[index])
  }
  return false
}
