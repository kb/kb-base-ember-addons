import objValue from "@kb/ember-addon/utils/obj-value"

export default (a, b, sortOpts) => {
  let A = a
  let B = b
  if (sortOpts) {
    A = objValue(a, sortOpts.key)
    B = objValue(b, sortOpts.key)
    if (sortOpts.desc) {
      ;[A, B] = [B, A]
    }
  }
  return A - B
}
