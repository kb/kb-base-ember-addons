/**
// @BasedOn: https://www.freecodecamp.org/forum/t/filtering-json-object/244160/3
// @Originator: https://www.freecodecamp.org/forum/u/kevinSmith
*/
export default (row, filterObject) => {
  for (let func of Object.entries(filterObject).map(([key, func]) => func)) {
    if (!func(row)) {
      return false
    }
  }
  return true
}
