import sortString from "./sort-string"

export default (a, b, sortOpts) => {
  if (!sortOpts) {
    sortOpts = new Object()
  }
  sortOpts["desc"] = true
  return sortString(a, b, sortOpts)
}
