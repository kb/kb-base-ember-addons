import filterNumberExact from "./filter-number-exact"

export default (rowFieldDate, filterSwitch) => {
  return filterNumberExact(
    new Date(rowFieldDate).valueOf(),
    new Date(filterSwitch).valueOf()
  )
}
