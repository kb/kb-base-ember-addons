import { isBlank } from "@ember/utils"

export default (rowFieldList, filterValue) => {
  return (
    isBlank(filterValue) ||
    rowFieldList.map(l => String(l)).includes(String(filterValue))
  )
}
