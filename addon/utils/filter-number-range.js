import filterNumberGte from "./filter-number-gte"
import filterNumberLte from "./filter-number-lte"

export default (rowFieldNumber, filterStart, filterEnd) => {
  return (
    (filterNumberGte(rowFieldNumber, filterStart) || !filterStart) &&
    (filterNumberLte(rowFieldNumber, filterEnd) || !filterEnd)
  )
}
