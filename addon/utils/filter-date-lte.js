import filterNumberLte from "./filter-number-lte"

export default (rowFieldDate, filterSwitch) => {
  return filterNumberLte(
    new Date(rowFieldDate).valueOf(),
    new Date(filterSwitch).valueOf()
  )
}
