/** @file Extract a display string from an unknown value selected for PowerSelect.
 *  @tutorial The point is that we want to use the kb-addons kb-form/select
 * component with options that are objects or a primitive. We also want to
 * return the selected value either as a primitive or an object. Spelling it out:
 * (A) OPTIONS are PRIMITIVE, e.g. ["a", "b" "c"].
 *      Return PRIMITIVE VALUE "c" if selected.
 * (B) OPTIONS are OBJECTS, e.g. [{id: 1, name: "a"}, {id: 2, name: "b"}, {id: 2, name: "c"}]
 *      Return PRIMITIVE VALUE "c" or VALUE "2" if selected.
 * (C) OPTIONS are OBJECTS, e.g. [{id: 1, name: "a"}, {id: 2, name: "b"}, {id: 2, name: "c"}]
 *      Return OBJECT VALUE {id: 2, name: "c"} if selected.
 *  @usage (using the get-select-option helper)
*******************************************************************************
<KbPowerSelect
  @value={{@unknownWhetherPrimitiveOrObject}}
  @options={{@unknownWhetherPrimitivesOrObjects}}
  @searchField={{@searchField}}
  ....
  as |selectedOpt|
>
  {{get-select-option selectedOpt @searchField @valueField @unknownWhetherPrimitivesOrObjects }}
</PowerSelect>
*******************************************************************************
*/
import { typeOf } from "@ember/utils"

export default (val, displayKey, valKey, choices) => {
  if (displayKey) {
    if (typeOf(val) === "instance") {
      return val.get(displayKey)
    } else if (val.hasOwnProperty(displayKey) || val[displayKey]) {
      return val[displayKey]
    } else {
      let choicesAreObjects = false
      if (choices && choices.length) {
        choicesAreObjects = ["object", "instance"].includes(typeOf(choices[0]))
        if (choicesAreObjects) {
          // Choices are object, val seems primitive...
          let choiceFound = choices.find(c => c[valKey] == val)
          if (choiceFound) {
            // ... found matching choice based on val, SO... safely return
            // `object.displayKey`
            return choiceFound[displayKey]
          } else {
            // ... did not find matching choice based on val, SO... we can't do
            // anything except return the val and hope it's safe.
            return val
          }
        } else {
          // Choices are NOT object, val seems primitive...SO... safely return
          // val (doesn't matter if it can be found in the primitive choices).
          return val
        }
      } else {
        // No choices, val seems primitive, SO... safely return the val.
        return val
      }
    }
  }
  // No displayKey provided, SO... we can't do anything except return the val
  // and hope it's safe.
  return val
}
