function groupPhase(groupThis, toArray, groupByField) {
  groupThis.forEach(item => {
    // Look for an existing group
    let group = toArray.find(g => {
      return String(g.value) === String(item[groupByField])
    })
    if (group) {
      group.items.push(item)
    } else {
      // Start a group using this groupBy val.
      toArray.push({
        property: groupByField,
        value: item[groupByField],
        item: item,
        items: [item]
      })
    }
  })
  return toArray
}

export default (groupByItems, groupByField) => {
  let groups = new Array()
  if (!groupByItems) {
    return []
  }
  // Deal with the parents first:
  groups = groupPhase(
    groupByItems.filter(item => item.isGroupParent),
    groups,
    groupByField
  )
  groups = groupPhase(
    groupByItems.filter(item => !item.isGroupParent),
    groups,
    groupByField
  )
  return groups
}
