import sortNumerical from "./sort-numerical"

export default (a, b, sortOpts) => {
  if (!sortOpts) {
    sortOpts = new Object()
  }
  sortOpts["desc"] = true
  return sortNumerical(a, b, sortOpts)
}
