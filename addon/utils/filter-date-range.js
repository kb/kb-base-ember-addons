import filterNumberRange from "./filter-number-range"

export default (rowFieldDate, filterStart, filterEnd) => {
  return filterNumberRange(
    new Date(rowFieldDate).valueOf(),
    new Date(filterStart).valueOf(),
    new Date(filterEnd).valueOf()
  )
}
