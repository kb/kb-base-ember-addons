import { pluralize, singularize } from "ember-inflector"

export default heading => {
  let words = heading.split(" ")
  let singled = new Array()
  for (let [x, word] of words.entries()) {
    // See what the heading would look like without this word.
    let otherHeadingsSplit = heading.split(" ")
    otherHeadingsSplit.splice(x, 1)
    let singularizedWord = singularize(word)
    // Does the current singularizedWord match any other word
    if (!otherHeadingsSplit.includes(singularizedWord)) {
      singled.push(word)
    }
  }
  return singled.join(" ")
}
