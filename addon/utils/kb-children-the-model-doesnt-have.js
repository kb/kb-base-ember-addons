export default (alreadyHas, everything, targetId) => {
  let has = new Array()
  if (alreadyHas) {
    if (alreadyHas.length) {
      has = alreadyHas.map(item => String(item.get("id")))
    }
  }
  if (targetId) {
    has.push(targetId)
  }
  if (everything) {
    if (everything.length) {
      return everything.filter(item => !has.includes(String(item.get("id"))))
    }
  }
  return []
}
