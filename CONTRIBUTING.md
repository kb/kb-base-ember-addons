# How To Contribute

## Installation

- `git clone <repository-url>`
- `cd kb-base-ember-addons`
- `npm install`

## Documentation

We should replace the current `dummy` with `simple-ember` and document examples using:

<https://ember-learn.github.io/ember-cli-addon-docs/docs/quickstart>

## Linting

Using prettier. See below.

```
npm i prettier -D
npm i ember-template-lint -D
npm i ember-template-lint-plugin-prettier -D
```

Added to `package.json` as `npm run` commands in any Ember or other Node project

```
"scripts": {
  ...
  "prettier:hbs": "prettier  --write **/*.hbs --parser=glimmer",
  "prettier": "prettier --write \"**/*.{js,json,scss,md,html}\" \"!package.json\""
}
```

Commanded by:

- `npm run prettier`
- `npm run prettier:hbs`

Also consider:

- `npm run lint:hbs`
- `npm run lint:js`
- `npm run lint:js -- --fix`

## Running tests

- `ember test` – Runs the test suite on the current Ember version
- `ember test --server` – Runs the test suite in "watch mode"
- `ember try:each` – Runs the test suite against multiple Ember versions

## Running the dummy application

- `ember serve`
- Visit the dummy application at <http://localhost:4200>.

<<<<<<< HEAD

## Helpful

- [Render HBS with variables](https://github.com/ember-cli/babel-plugin-htmlbars-inline-precompile/issues/9)

# For more information on using ember-cli, visit <https://ember-cli.com/>.

For more information on using ember-cli, visit <https://ember-cli.com/>.

## prettier

[prettier](https://github.com/prettier) works just like `black` on Ember, as far as I can tell. I propose adopting the following standard.

This **kb-base-ember-addons** version of `.prettier.config.js` be [kb](https://gitlab.com/kb)software's standard and checked into the repository of any Ember project... Like this one. Or in `front` of a Django RestFramework, as another example.

`.prettierignore` should also be included in any repo, but with no standard, only flavoured for the app. `But` changes to it should be considered potentially breaking because they are a sign the contributor merging the code has put a new folder in their file system which has not been recognised as a requirement others will need to share.

The standard calls will be:

## `npm run prettier`

Shorthand for:

`prettier --write \"**/*.{js,json,scss,md,html}\" \"!package.json\""`

## `npm run prettier:hbs`

Shorthand for:

`prettier --write **/*.{hbs,js,json,html} --parser=glimmer`

Errors seen linting non-hbs files with the glimmerparser should be addressed.

glimmerparser will check any html like strings it finds even within a "not html" file: When html tags are seen in json string values, for instance.

<https://github.com/prettier/prettier/blob/master/src/language-handlebars/parser-glimmer.js>

The logged error message `[error] No parser could be inferred for file: <file> error` can be ignored.

> > > > > > > kb-prettier
